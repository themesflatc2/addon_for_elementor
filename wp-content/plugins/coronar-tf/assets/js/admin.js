;(function($) {

    "use strict";

    var color_picker = function() {
        $('.custom-color-picker').wpColorPicker();
    }

    $(function() {    
    	color_picker(); 
    });

})(jQuery);
