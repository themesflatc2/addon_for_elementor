;(function($) {
    "use strict";

    var tf_Counter = function() {
        if ( $().countTo ) {
            
            $('.counter').each(function() {
                $(this).find('.numb-count').each(function() { 
                    var to = parseInt( ($(this).attr('data-to')), 10 ), speed = parseInt( ($(this).attr('data-speed')), 10 );
                    if ( $().countTo ) {
                        $(this).countTo({
                            from: 0,
                            to: to,
                            speed: speed,
                        });
                    }
                });
           });
        }
    };

    $(function() { 
        $( window ).on( "load", function() {      
            tf_Counter();  
        });     
    });
})(jQuery);
