<?php
/**
 * Plugin Name:  TF Coronar
 * Plugin URI:  http://themesflat.com/
 * Description: The theme's components
 * Author:      ThemesFlat
 * Version:     1.0.0
 * Author URI: http://themesflat.com/
 */

class TF_Coronar {
    private static $_instance = null;
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct() {
        define('URL_PLUGIN_CORONAR', plugins_url('/', __FILE__));

        add_action( 'plugins_loaded', [ $this, 'load_textdomain' ] );        
        if( is_admin() ) {
            add_action( 'admin_menu', [ $this, 'admin_menu' ] );
            add_action('admin_init', [$this, 'setup_sections']);
            add_action('admin_init', [$this, 'setup_fields']); 
            add_action( 'admin_enqueue_scripts', [ $this, 'admin_styles' ] );           
        }

        add_action( 'wp_enqueue_scripts', [ $this, 'tf_styles' ], 100 );
        add_action( 'wp_enqueue_scripts', [ $this, 'tf_script' ], 100 );

        $this->inc();        
    }

    public function admin_menu() {
        
        add_menu_page(
            esc_html__( 'TF Coronar', 'tf-pg-coronar' ), esc_html__( 'TF Coronar', 'tf-pg-coronar' ), 'manage_options', 'tf-pg-coronar', array($this, 'display_admin_setting'), 'dashicons-buddicons-replies', 1);

        
        add_submenu_page( 'tf-pg-coronar', esc_html__( 'Settings', 'tf-pg-coronar' ), esc_html__( 'Settings', 'tf-pg-coronar' ), 'manage_options', 'tf-pg-coronar', array($this, 'display_admin_setting') );
    }

    public function display_admin_setting(){
        require_once plugin_dir_path( __FILE__ ) . 'includes/template-settings.php';
    }

    public function setup_sections() {
        add_settings_section( 'section_shortcode', esc_html__('Shortcode' , 'tf-pg-coronar'), array($this, 'section_callback'), 'coronar-setting-options' );
        add_settings_section( 'section_setting', esc_html__('Settings' , 'tf-pg-coronar'), array($this, 'section_callback'), 'coronar-setting-options' );
        add_settings_section( 'section_style', esc_html__('Style' , 'tf-pg-coronar'), array($this, 'section_callback'), 'coronar-setting-options' );
    }

    public function section_callback( $arguments ) {
        switch( $arguments['id'] ){
            case 'section_shortcode':
                echo '<p><code>[tf-coronar]</code> <code>[tf-coronar-case value="world"]</code> <code>[tf-coronar-case value="top10"]</code></p>';
                break;
            case 'section_setting':
                echo '<p>' . esc_html__('This is settings label table' , 'tf-pg-coronar') . '</p>';
                break;
            case 'section_style':
                echo '<p>' . esc_html__('This is settings style' , 'tf-pg-coronar') . '</p>';
                break;
        }
    }

    public function setup_fields() {
        $fields = array(
            // Setting          
            array(
                'uid' => 'text_field_flag',
                'label' => esc_html__('Flag:' , 'tf-pg-coronar'),
                'section' => 'section_setting',
                'type' => 'text',
                'placeholder' => esc_html__('Flag', 'tf-pg-coronar'),
                'default' => "Flag",
            ),
            array(
                'uid' => 'text_field_country',
                'label' => esc_html__('Country:', 'tf-pg-coronar'),
                'section' => 'section_setting',
                'type' => 'text',
                'placeholder' => esc_html__('Country', 'tf-pg-coronar'),
                'default' => "Country",
            ),
            array(
                'uid' => 'text_field_new_confirmed',
                'label' => esc_html__('New Confirmed:', 'tf-pg-coronar'),
                'section' => 'section_setting',
                'type' => 'text',
                'placeholder' => esc_html__('New Confirmed', 'tf-pg-coronar'),
                'default' => "New Confirmed",
            ),
            array(
                'uid' => 'text_field_total_confirmed',
                'label' => esc_html__('Total Confirmed:', 'tf-pg-coronar'),
                'section' => 'section_setting',
                'type' => 'text',
                'placeholder' => esc_html__('Total Confirmed', 'tf-pg-coronar'),
                'default' => "Total Confirmed",
            ),  
            array(
                'uid' => 'text_field_new_deaths',
                'label' => esc_html__('New Deaths:', 'tf-pg-coronar'),
                'section' => 'section_setting',
                'type' => 'text',
                'placeholder' => esc_html__('New Deaths', 'tf-pg-coronar'),
                'default' => "New Deaths",
            ),  
            array(
                'uid' => 'text_field_total_deaths',
                'label' => esc_html__('Total Deaths:', 'tf-pg-coronar'),
                'section' => 'section_setting',
                'type' => 'text',
                'placeholder' => esc_html__('Total Deaths', 'tf-pg-coronar'),
                'default' => "Total Deaths",
            ),  
            array(
                'uid' => 'text_field_new_recovered',
                'label' => esc_html__('New Recovered:', 'tf-pg-coronar'),
                'section' => 'section_setting',
                'type' => 'text',
                'placeholder' => esc_html__('New Recovered', 'tf-pg-coronar'),
                'default' => "New Recovered",
            ),  
            array(
                'uid' => 'text_field_total_recovered',
                'label' => esc_html__('Total Recovered:', 'tf-pg-coronar'),
                'section' => 'section_setting',
                'type' => 'text',
                'placeholder' => esc_html__('Total Recovered', 'tf-pg-coronar'),
                'default' => "Total Recovered",
            ),
            // Style
            array(
                'uid' => 'color_lable',
                'label' => esc_html__('Color Label', 'tf-pg-coronar'),
                'section' => 'section_style',
                'type' => 'color_picker',
                'placeholder' => esc_html__('#ffffff', 'tf-pg-coronar'),
                'default' => "#000000",
                'description' => '',
            ),
            array(
                'uid' => 'color_text',
                'label' => esc_html__('Color Text', 'tf-pg-coronar'),
                'section' => 'section_style',
                'type' => 'color_picker',
                'placeholder' => esc_html__('#ffffff', 'tf-pg-coronar'),
                'default' => "#000000",
                'description' => '',
            ), 
            array(
                'uid' => 'color_lable_confirmed',
                'label' => esc_html__('Color Confirmed', 'tf-pg-coronar'),
                'section' => 'section_style',
                'type' => 'color_picker',
                'placeholder' => esc_html__('#ffffff', 'tf-pg-coronar'),
                'default' => "#f96c00",
            ), 

            array(
                'uid' => 'color_lable_deaths',
                'label' => esc_html__('Color Deaths', 'tf-pg-coronar'),
                'section' => 'section_style',
                'type' => 'color_picker',
                'placeholder' => esc_html__('#ffffff', 'tf-pg-coronar'),
                'default' => "#ce0000",
            ),
            array(
                'uid' => 'color_lable_recovered',
                'label' => esc_html__('Color Recovered', 'tf-pg-coronar'),
                'section' => 'section_style',
                'type' => 'color_picker',
                'placeholder' => esc_html__('#ffffff', 'tf-pg-coronar'),
                'default' => "#0c9300",
            ),             
            array(
                'uid' => 'text_field_flag_size',
                'label' => esc_html__('Flag Size', 'tf-pg-coronar'),
                'section' => 'section_style',
                'type' => 'text',
                'placeholder' => esc_html__('Flag Size', 'tf-pg-coronar'),
                'default' => "43px",
                'description' => esc_html__('ex: 43px' , 'tf-pg-coronar'),
            ), 
            array(
                'uid' => 'text_field_label_font_size',
                'label' => esc_html__('Label Font Size', 'tf-pg-coronar'),
                'section' => 'section_style',
                'type' => 'text',
                'placeholder' => esc_html__('Label Font Size', 'tf-pg-coronar'),
                'default' => "15px",
                'description' => esc_html__('ex: 15px' , 'tf-pg-coronar'),
            ),
            array(
                'uid' => 'text_field_font_size',
                'label' => esc_html__('Font Size', 'tf-pg-coronar'),
                'section' => 'section_style',
                'type' => 'text',
                'placeholder' => esc_html__('Font Size', 'tf-pg-coronar'),
                'default' => "15px",
                'description' => esc_html__('ex: 15px' , 'tf-pg-coronar'),
            ),              
        );
        
        foreach( $fields as $field ){
            add_settings_field( $field['uid'], $field['label'], array($this, 'field_callback'), 'coronar-setting-options', $field['section'], $field );
            register_setting( 'coronar-setting-options', $field['uid'] );
        }
    }

    public function field_callback($arguments) {
        $value = get_option( $arguments['uid'] );
        if(!$value) {
            $value = $arguments['default'];
        }
        switch( $arguments['type'] ){
            case 'text':
                printf( '<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />', $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value );
                break;
            case 'color_picker':
                printf( '<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" class="custom-color-picker" />', $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value );
                break;
        }

        if( array_key_exists('description',$arguments) && $description = $arguments['description'] ){
            printf( '<p class="desc">%s</p>', $description );
        }
    }

    public function load_textdomain() {
        load_plugin_textdomain( 'tf-pg-coronar', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    }

    public function admin_styles() {
        wp_enqueue_style( 'wp-color-picker' ); 
        wp_enqueue_script( 'custom-color-pick', plugins_url( 'assets/js/admin.js', __FILE__ ), array( 'wp-color-picker' ), false, true );        
    }

    public function tf_styles() {
        wp_register_style('tf-main-coronar', plugins_url('assets/css/main.css', __FILE__), array());
        wp_enqueue_style('tf-main-coronar');
    }

    public function tf_script() {
        wp_register_script('countTo', plugins_url('assets/js/jquery-countTo.js', __FILE__), array());
        wp_register_script('tf-main-coronar', plugins_url('assets/js/main.js', __FILE__), array());
        wp_enqueue_script('tf-main-coronar');
        wp_enqueue_script('countTo');
    }

    public function inc() {
        require_once plugin_dir_path( __FILE__ ).'includes/helper.php';
        require_once plugin_dir_path( __FILE__ ).'includes/shortcode.php';
        require_once plugin_dir_path( __FILE__ ).'includes/fetch.php';       
    }

}
TF_Coronar::instance();





