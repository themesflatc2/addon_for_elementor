<?php 
class TF_Shortcode {
    private static $_instance = null;
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct() {
    	$this->tf_shortcode_coronar_all_country();
    	add_action( 'wp_footer', [ $this, 'ajax_fetch' ] );
    }

    public function tf_shortcode_coronar_all_country(  ) {	    
		$summary = tf_sort_array('NewConfirmed');
		ob_start();	    			
		    if ( ! $summary ) { 
		        return "No matching records found"; 
		    }else {
		    	?>
		    	<div class="tf-wrap-coronar">		    	
					<select class="filter-countries custom-select" name="keyword" id="keyword" onchange="fetch()">
				    	<option value="all"><?php echo esc_html__('All Country', 'tf-pg-coronar'); ?></option>
				    	<?php  foreach ($summary as $value): ?>
					    	<?php if( $value["Country"] != "" && $value["Slug"] != 'viet-nam'  ): ?>
					    		<option value="<?php echo esc_attr($value['Slug']) ?>"><?php echo esc_attr($value['Country']); ?></option>
					    	<?php endif; ?>
						<?php endforeach; ?>
					</select>

			    	<table class="tf-coronar-table">
			    		<thead>
				    		<tr>
				    			<th class="tf-coronar-lable"><?php echo tf_get_option('text_field_flag'); ?></th>
							    <th class="tf-coronar-lable"><?php echo tf_get_option('text_field_country'); ?></th>
							    <th class="tf-coronar-lable"><?php echo tf_get_option('text_field_new_confirmed'); ?></th>
							    <th class="tf-coronar-lable"><?php echo tf_get_option('text_field_total_confirmed'); ?></th>
							    <th class="tf-coronar-lable"><?php echo tf_get_option('text_field_new_deaths'); ?></th>
							    <th class="tf-coronar-lable"><?php echo tf_get_option('text_field_total_deaths'); ?></th>
							    <th class="tf-coronar-lable"><?php echo tf_get_option('text_field_new_recovered'); ?></th>
							    <th class="tf-coronar-lable"><?php echo tf_get_option('text_field_total_recovered'); ?></th>
							  </tr>
						</thead>
						<tbody>						
				        <?php foreach ($summary as $value): ?>
							<?php if( $value["Country"] != "" ): ?>
							<tr>
							  	<td><?php echo tf_country_flag($value["Slug"]); ?></td>
							    <td><?php echo esc_attr($value['Country']); ?></td>
							    <td class="tf-coronar-confirmed"><?php echo esc_attr($value['NewConfirmed']); ?></td>
							    <td class="tf-coronar-confirmed"><?php echo esc_attr($value['TotalConfirmed']); ?></td>
							    <td class="tf-coronar-deaths"><?php echo esc_attr($value['NewDeaths']); ?></td>
							    <td class="tf-coronar-deaths"><?php echo esc_attr($value['TotalDeaths']); ?></td>
							    <td class="tf-coronar-recovered"><?php echo esc_attr($value['NewRecovered']); ?></td>
							    <td class="tf-coronar-recovered"><?php echo esc_attr($value['TotalRecovered']); ?></td>
							</tr>
							<?php endif; ?>
				        <?php endforeach; ?>
			        	</tbody>
					</table>
				</div>
		        <?php	            
		    }
	    $ob_get_contents = ob_get_contents();
	    ob_end_clean();
        return $ob_get_contents;	    
    }
    
	public function ajax_fetch() {
		?>
		<script type="text/javascript">
		function fetch(){
		    jQuery.ajax({
		        url: '<?php echo admin_url('admin-ajax.php'); ?>',
		        type: 'POST',
		        dataType: "html",
		        data: { action: 'data_fetch', keyword: jQuery('#keyword').val() },
		        beforeSend: function() {
				    jQuery('.tf-coronar-table tbody').html( '<tr class="loading"><td colspan="8"><img src="<?php echo URL_PLUGIN_CORONAR ?>/assets/image/loading_large.gif" alt="loading" /></td></tr>' );
				    jQuery('.tf-wrap-coronar #keyword').prop('disabled', true).css({'color':'#ccc', 'border-color':'#ccc'});
				},
				complete: function(){
				    jQuery('.tf-coronar-table tbody .loading').remove();
				    jQuery('.tf-wrap-coronar #keyword').prop('disabled', false).css({'color':'#666', 'border-color':'#666'});
				},
		        success: function(data) {
		        	console.log(data);
		        	if (data == 0) {
		        		jQuery('.tf-coronar-table tbody').html( '<tr class="data-empty"><td colspan="8"><?php echo esc_html__('No matching records found', 'tf-pg-coronar') ?></td></tr>' );
		        	}else{
		        		jQuery('.tf-coronar-table tbody').html( data );
		        	}		            
		        }
		    });
		}
		</script>
		<?php
	}	

	public function tf_shortcode_coronar_case( $args ) {
	    switch ($args['value']) {
	    	case 'world':

				$totalConfirmed = tf_count_array('TotalConfirmed');
				$totalRecovered = tf_count_array('TotalRecovered');
				$totalDeaths = tf_count_array('TotalDeaths');	
	    		ob_start();	    			
					?>
					<div class="tf-tracking-coronar-world counter">
						<div class="item">
							<div class="tf-tracking-coronar-inner">
								<div class="thumb"><img src="<?php echo URL_PLUGIN_CORONAR.'assets/image/01.png' ?>" alt="image"></div>
								<div class="content">
									<h4 class="count numb-count" data-to="<?php echo esc_attr($totalConfirmed); ?>" data-speed="3000"><?php echo esc_attr($totalConfirmed); ?></h4>
									<p>Coronavirus Cases</p>	
								</div>	
							</div>			
						</div>
						<div class="item">
							<div class="tf-tracking-coronar-inner">
								<div class="thumb"><img src="<?php echo URL_PLUGIN_CORONAR.'assets/image/02.png' ?>" alt="image"></div>
								<div class="content">
									<h4 class="count numb-count" data-to="<?php echo esc_attr($totalRecovered); ?>" data-speed="3000"> <?php echo esc_attr($totalRecovered); ?></h4>
									<p>Recovered Cases</p>
								</div>
							</div>					
						</div>
						<div class="item">
							<div class="tf-tracking-coronar-inner">
								<div class="thumb"><img src="<?php echo URL_PLUGIN_CORONAR.'assets/image/03.png' ?>" alt="image"></div>
								<div class="content">
									<h4 class="count numb-count" data-to="<?php echo esc_attr($totalDeaths); ?>" data-speed="3000"> <?php echo esc_attr($totalDeaths); ?></h4>
									<p>Total Deaths</p>
								</div>
							</div>					
						</div>
					</div>
					<?php
				$ob_get_contents = ob_get_contents();
			    ob_end_clean();
		        return $ob_get_contents;
	    		break;

	    	case 'top10':
	    		$summary = tf_sort_array('TotalConfirmed');
	    		$summary = array_slice( $summary, 0, 10 );
	    		ob_start();
				    ?>
				    <div class="tf-wrap-coronar-top">
					    <table class="tf-coronar-table-top">
				    		<thead>
					    		<tr>
					    			<th class="tf-coronar-lable"><?php echo tf_get_option('text_field_flag'); ?></th>
								    <th class="tf-coronar-lable"><?php echo tf_get_option('text_field_country'); ?></th>
								    <th class="tf-coronar-lable"><?php echo tf_get_option('text_field_new_confirmed'); ?></th>
								    <th class="tf-coronar-lable"><?php echo tf_get_option('text_field_total_confirmed'); ?></th>
								    <th class="tf-coronar-lable"><?php echo tf_get_option('text_field_new_deaths'); ?></th>
								    <th class="tf-coronar-lable"><?php echo tf_get_option('text_field_total_deaths'); ?></th>
								    <th class="tf-coronar-lable"><?php echo tf_get_option('text_field_new_recovered'); ?></th>
								    <th class="tf-coronar-lable"><?php echo tf_get_option('text_field_total_recovered'); ?></th>
								  </tr>
							</thead>
							<tbody>						
					        <?php foreach ($summary as $value): ?>
								<?php if( $value["Country"] != "" ): ?>
								<tr>
								  	<td><?php echo tf_country_flag($value["Slug"]); ?></td>
								    <td><?php echo esc_attr($value['Country']); ?></td>
								    <td class="tf-coronar-confirmed"><?php echo esc_attr($value['NewConfirmed']); ?></td>
								    <td class="tf-coronar-confirmed"><?php echo esc_attr($value['TotalConfirmed']); ?></td>
								    <td class="tf-coronar-deaths"><?php echo esc_attr($value['NewDeaths']); ?></td>
								    <td class="tf-coronar-deaths"><?php echo esc_attr($value['TotalDeaths']); ?></td>
								    <td class="tf-coronar-recovered"><?php echo esc_attr($value['NewRecovered']); ?></td>
								    <td class="tf-coronar-recovered"><?php echo esc_attr($value['TotalRecovered']); ?></td>
								</tr>
								<?php endif; ?>
					        <?php endforeach; ?>
				        	</tbody>
						</table>
					</div>
				    <?php
			    $ob_get_contents = ob_get_contents();
			    ob_end_clean();
		        return $ob_get_contents;
	    		break;
	    }	    
    }
    
}

add_shortcode( 'tf-coronar', array( TF_Shortcode::instance(), 'tf_shortcode_coronar_all_country' ) );

add_shortcode( 'tf-coronar-case', array( TF_Shortcode::instance(), 'tf_shortcode_coronar_case' ) );