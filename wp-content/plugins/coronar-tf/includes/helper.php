<?php 

function tf_get_default($default) {
	$default_array = array(
		'text_field_flag' => 'Flag',
		'text_field_country' => 'Country',
		'text_field_new_confirmed' => 'New Confirmed',
		'text_field_total_confirmed' => 'Total Confirmed',
		'text_field_new_deaths' => 'New Deaths',
		'text_field_total_deaths' => 'Total Deaths',
		'text_field_new_recovered' => 'New Recovered',
		'text_field_total_recovered' => 'Total Recovered',
		'color_lable' => '#000000',
		'color_text' => '#000000',
		'color_lable_confirmed' => '#f96c00',
		'color_lable_deaths' => '#ce0000',
		'color_lable_recovered' => '#0c9300',
		'text_field_flag_size' => '43px',
		'text_field_label_font_size' => '15px',
		'text_field_font_size' => '15px',
	);
	return $default_array[$default];
}

function tf_get_option($key) {
	if ( get_option($key) != '' ) {
		return get_option($key);
	}
	return tf_get_default($key);
}

function tf_render_styles( $style ) {
	wp_enqueue_style( 'coronar-inline-style', URL_PLUGIN_CORONAR . 'assets/css/coronar-inline-style.css' );
	$style = '';

	$style  .= ".tf-wrap-coronar table .tf-coronar-lable, 
				.tf-wrap-coronar-top table .tf-coronar-lable { color:" . tf_get_option('color_lable') . ";}"."\n";

	$style  .= ".tf-wrap-coronar table, 
				.tf-wrap-coronar-top table { color:" . tf_get_option('color_text') . ";}"."\n";

	$style  .= ".tf-wrap-coronar table .tf-coronar-confirmed,
				.tf-wrap-coronar-top table .tf-coronar-confirmed { color:" . tf_get_option('color_lable_confirmed') . ";}"."\n";

	$style  .= ".tf-wrap-coronar table .tf-coronar-deaths,
				.tf-wrap-coronar-top table .tf-coronar-deaths { color:" . tf_get_option('color_lable_deaths') . ";}"."\n";

	$style  .= ".tf-wrap-coronar table .tf-coronar-recovered,
				.tf-wrap-coronar-top table .tf-coronar-recovered { color:" . tf_get_option('color_lable_recovered') . ";}"."\n";
	
	$style  .= ".tf-wrap-coronar table tr th,
				.tf-wrap-coronar-top table tr th { font-size:" . tf_get_option('text_field_label_font_size') . ";}"."\n";
	
	$style  .= ".tf-wrap-coronar table,
				.tf-wrap-coronar-top table { font-size:" . tf_get_option('text_field_font_size') . ";}"."\n";

	wp_add_inline_style( 'coronar-inline-style', $style );
	
}
add_action( 'wp_enqueue_scripts', 'tf_render_styles' );

function tf_country_flag( $slug ) {
    $flag_name =  $slug . '.svg';
    $src = URL_PLUGIN_CORONAR.'assets/image/'.$flag_name;

    $src = URL_PLUGIN_CORONAR.'assets/image/_none.svg';
    if ( file_exists( plugin_dir_path( dirname( __FILE__ ) ) . '/assets/image/' . $flag_name ) ) {
	    $src = URL_PLUGIN_CORONAR.'assets/image/'.$flag_name;
    }
    ?>
    <img src="<?php esc_attr_e( $src ); ?>" alt="<?php esc_attr_e( $slug ); ?>" /><?php

}

function tf_sort_array( $orderby ) {
	$summary_url = 'https://api.covid19api.com/summary';
	$summary = wp_remote_retrieve_body( wp_remote_get( $summary_url ));
	$summary = json_decode($summary, true);

	$sortArray = [];

	foreach($summary['Countries'] as $country){
	    foreach($country as $key => $value){
	        if(!isset($sortArray[$key])){
	            $sortArray[$key] = array();
	        }
	        $sortArray[$key][] = $value;
	    }
	}

	array_multisort($sortArray[$orderby],SORT_DESC,$summary['Countries']);

	return $summary['Countries'];
}

function tf_count_array( $atts ) {
	$summary_url = 'https://api.covid19api.com/summary';
	$summary = wp_remote_retrieve_body( wp_remote_get( $summary_url ));
	$summary = json_decode($summary, true);
	
	$world = [];
    foreach ( $summary['Countries'] as $key => $country ) {
    	if ( $country['Slug'] != '' ) {
    		$world[$key] = $country[$atts];
    	}  
    }

    $world = array_unique($world);
	$world = array_sum($world);

    return $world;
}
