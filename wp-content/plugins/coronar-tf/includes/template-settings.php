<?php
/**
 * @package coronar-tf
 */
?>
<div class="wrap-setting">

    <form method="POST" action="options.php">
    <?php
	    settings_fields('coronar-setting-options');
	    do_settings_sections('coronar-setting-options');
	    submit_button();
    ?>
    </form>

</div>