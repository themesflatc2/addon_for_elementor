<?php 
add_action('wp_ajax_data_fetch' , 'data_fetch');
add_action('wp_ajax_nopriv_data_fetch','data_fetch');
function data_fetch(){
	$summary_url = 'https://api.covid19api.com/summary';
    $summary = wp_remote_retrieve_body( wp_remote_get( $summary_url ));
    $summary = json_decode($summary, true);
    if ( ! $summary ) { 
        return "No matching records found"; 
    }else {    	
    	?>	    	
        <?php foreach ($summary['Countries'] as $value): ?>
        	<?php if( $value["Country"] != "" ): ?>
	        	<?php if ($_POST['keyword'] == $value['Slug']): ?>
				  	<tr>
					  	<td><?php echo tf_country_flag($value["Slug"]); ?></td>
					    <td><?php echo esc_attr($value['Country']); ?></td>
					    <td class="tf-coronar-confirmed"><?php echo esc_attr($value['NewConfirmed']); ?></td>
					    <td class="tf-coronar-confirmed"><?php echo esc_attr($value['TotalConfirmed']); ?></td>
					    <td class="tf-coronar-deaths"><?php echo esc_attr($value['NewDeaths']); ?></td>
					    <td class="tf-coronar-deaths"><?php echo esc_attr($value['TotalDeaths']); ?></td>
					    <td class="tf-coronar-recovered"><?php echo esc_attr($value['NewRecovered']); ?></td>
					    <td class="tf-coronar-recovered"><?php echo esc_attr($value['TotalRecovered']); ?></td>
				  	</tr>
				<?php elseif ($_POST['keyword'] == 'all') : ?>
					<tr>
					  	<td><img src="<?php echo URL_PLUGIN_CORONAR . 'assets/image/' . esc_attr($value["Slug"]) . '.svg' ?>" alt="image"></td>
					    <td><?php echo esc_attr($value['Country']); ?></td>
					    <td class="tf-coronar-confirmed"><?php echo esc_attr($value['NewConfirmed']); ?></td>
					    <td class="tf-coronar-confirmed"><?php echo esc_attr($value['TotalConfirmed']); ?></td>
					    <td class="tf-coronar-deaths"><?php echo esc_attr($value['NewDeaths']); ?></td>
					    <td class="tf-coronar-deaths"><?php echo esc_attr($value['TotalDeaths']); ?></td>
					    <td class="tf-coronar-recovered"><?php echo esc_attr($value['NewRecovered']); ?></td>
					    <td class="tf-coronar-recovered"><?php echo esc_attr($value['TotalRecovered']); ?></td>
				  </tr>
				<?php endif; ?>
			<?php endif; ?>
        <?php endforeach;             
    }
}