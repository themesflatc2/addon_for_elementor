;(function($) {

    "use strict";

    var blogPostsOwl = function() {
    	if ( $().owlCarousel ) {
            $('.tf-posts-wrap.has-carousel').each(function(){
                var
                $this = $(this),
                item = $this.data("column"),
                item2 = $this.data("column2"),
                item3 = $this.data("column3"),
                spacer = Number($this.data("spacer")),
                prev_icon = $this.data("prev_icon"),
                next_icon = $this.data("next_icon");

                var loop = false;
                if ($this.data("loop") == 'yes') {
                	loop = true;
                }

                var arrow = false;
                if ($this.data("arrow") == 'yes') {
                	arrow = true;
                } 

                var auto = false;
                if ($this.data("auto") == 'yes') {
                	auto = true;
                }                

                $this.find('.owl-carousel').owlCarousel({
                    loop: loop,
                    margin: spacer,
                    nav: true,
                    pagination: false,
                    autoplay: auto,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: true,
                    animateIn: 'fadeIn',
              		animateOut: 'fadeOut',
                    navText : ["<i class=\""+prev_icon+"\"></i>","<i class=\""+next_icon+"\"></i>"],
                    responsive: {
                        0:{
                            items:item3
                        },
                        768:{
                            items:item2
                        },
                        1000:{
                            items:item
                        }
                    }
                });

            });
        }
    }

    var blogLoadMore = function() {

        var $container_wrap = $('.tf-posts-wrap'); 
        var $container = $('.tf-posts-wrap').find('.tf-posts');  

        $('.navigation.loadmore a').on('click', function(e) {
            e.preventDefault(); 

            $container.after('<div class="tfpost-loading"><span></span></div>');

            $.ajax({
                type: "GET",
                url: $(this).attr('href'),
                dataType: "html",
                success: function( out ) {
                    var result = $(out).find('.column');  
                    var nextlink = $(out).find('.navigation.loadmore a').attr('href');

                    result.css({ opacity: 0 , visibility: 'hidden' });
                    if ($container.hasClass('masonry')) {
                        $container.append(result).imagesLoaded(function () {
                            result.css({ opacity: 1 , visibility: 'visible' });
                            $container.isotope('appended', result);
                        });
                    }
                    else {
                    	$container.append(result).imagesLoaded(function () {
                            result.css({ opacity: 1 , visibility: 'visible' });
                            $container.isotope('appended', result);
                        });	                        
                    }

                    if ( nextlink != undefined ) {
                        $('.navigation.loadmore a').attr('href', nextlink);
                        $container_wrap.find('.tfpost-loading').remove();
                    } else {
                        $container_wrap.find('.tfpost-loading').addClass('no-ajax').text('All posts loaded').delay(2000).queue(function() {$(this).remove();});
                        $('.navigation.loadmore a').remove();
                    }
                }
            });
        });
             
    }

    var blogMasonry = function() {
        $('.tf-posts-wrap .tf-posts').each(function(){
        	var $this = $(this);
        	if ($this.hasClass('masonry')) {
	        	var $grid = $this.isotope({
					itemSelector: '.column',
					percentPosition: true,
					masonry: {
					columnWidth: '.grid-sizer'
					}
				});
				
				$grid.imagesLoaded().progress( function() {
					$grid.isotope('layout');
				});
			} 
        });            
    }  
	

    $(window).on('elementor/frontend/init', function() {
        elementorFrontend.hooks.addAction( 'frontend/element_ready/tfposts.default', blogPostsOwl );
        elementorFrontend.hooks.addAction( 'frontend/element_ready/tfposts.default', blogLoadMore );
        elementorFrontend.hooks.addAction( 'frontend/element_ready/tfposts.default', blogMasonry );
    });

})(jQuery);
