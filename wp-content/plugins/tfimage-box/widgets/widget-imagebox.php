<?php
class TFImageBox_Widget extends \Elementor\Widget_Base {

	public function get_name() {
        return 'tfimagebox';
    }
    
    public function get_title() {
        return esc_html__( 'TF Image Box', 'tf-addon-for-elementer' );
    }

    public function get_icon() {
        return 'eicon-image-box';
    }
    
    public function get_categories() {
        return [ 'themesflat_addons' ];
    }

	protected function _register_controls() {
		// Start Image        
			$this->start_controls_section( 
				'section_image',
	            [
	                'label' => esc_html__('Image', 'tf-addon-for-elementer'),
	            ]
	        );	

	        $this->add_control(
				'image',
				[
					'label' => esc_html__( 'Choose Image', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::MEDIA,
					'default' => [
						'url' => \Elementor\Utils::get_placeholder_image_src(),
					],
				]
			);

			$this->add_group_control(
				\Elementor\Group_Control_Image_Size::get_type(),
				[
					'name' => 'thumbnail',
					'exclude' => [ 'custom' ],
					'include' => [],
					'default' => 'large',
				]
			);

	    	$this->end_controls_section();
	    // /.End Image

        // Start Content        
			$this->start_controls_section( 
				'section_content',
	            [
	                'label' => esc_html__('Content', 'tf-addon-for-elementer'),
	            ]
	        );	

	        $this->add_control(
				'icon_name',
				[
					'label' => esc_html__( 'Icon', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::ICONS,
					'default' => [
						'value' => 'fas fa-star',
						'library' => 'fa-solid',
					],
				]
			);         	

			$this->add_control(
				'title',
				[
					'label' => esc_html__( 'Title', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::TEXTAREA,
					'default' => esc_html__( 'FINANCIAL PROJECTIONS AND ANALYSIS', 'tf-addon-for-elementer' ),
				]
			); 

			$this->add_control(
				'description',
				[
					'label' => esc_html__( 'Description', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::TEXTAREA,
					'default' => 'Sed ut perspiciatis unde omnis iste natus error voluptatem accusantium doloremque laudantium, totam aperiam.',
				]
			); 
					
	        $this->end_controls_section();
        // /.End Content

	    // Start Button        
			$this->start_controls_section( 
				'section_button',
	            [
	                'label' => esc_html__('Button', 'tf-addon-for-elementer'),
	            ]
	        );

	        $this->add_control(
				'show_button',
				[
					'label' => esc_html__( 'Show Button', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::SWITCHER,
					'label_on' => esc_html__( 'Show', 'tf-addon-for-elementer' ),
					'label_off' => esc_html__( 'Hide', 'tf-addon-for-elementer' ),
					'return_value' => 'yes',
					'default' => 'yes',
				]
			);

			$this->add_control( 
				'button_text',
				[
					'label' => esc_html__( 'Button Text', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::TEXT,
					'default' => esc_html__( 'Read More', 'tf-addon-for-elementer' ),
					'condition' => [
	                    'show_button'	=> 'yes',
	                ],
				]
			);

	        $this->add_control(
				'link',
				[
					'label' => esc_html__( 'Link', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::URL,
					'placeholder' => esc_html__( 'https://your-link.com', 'tf-addon-for-elementer' ),
					'condition' => [
						'show_button' => 'yes'
					]
				]
			);

	        $this->end_controls_section();
        // /.End Button	

	    // Start General Style       
			$this->start_controls_section( 
				'section_style_general',
	            [
	                'label' => esc_html__('General', 'tf-addon-for-elementer'),
	                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
	            ]
	        );

	        $this->add_control(
				'wrap_align',
				[
					'label' => esc_html__( 'Alignment', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::CHOOSE,
					'options' => [
						'left'    => [
							'title' => esc_html__( 'Left', 'tf-addon-for-elementer' ),
							'icon' => 'eicon-text-align-left',
						],
						'center' => [
							'title' => esc_html__( 'Center', 'tf-addon-for-elementer' ),
							'icon' => 'eicon-text-align-center',
						],
						'right' => [
							'title' => esc_html__( 'Right', 'tf-addon-for-elementer' ),
							'icon' => 'eicon-text-align-right',
						],
						'justify' => [
							'title' => esc_html__( 'Justified', 'tf-addon-for-elementer' ),
							'icon' => 'eicon-text-align-justify',
						],
					],
					'default' => 'center',
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox' => 'text-align: {{VALUE}}',
					],
				]
			);

	    	$this->end_controls_section();
        // /.End General Style  
        
        // Start Image Style       
			$this->start_controls_section( 
				'section_style_image',
	            [
	                'label' => esc_html__('Image', 'tf-addon-for-elementer'),
	                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
	            ]
	        ); 	  

			$this->add_group_control( 
				\Elementor\Group_Control_Border::get_type(),
				[
					'name' => 'image_border',
					'label' => esc_html__( 'Border', 'tf-addon-for-elementer' ),
					'selector' => '{{WRAPPER}} .tf-imagebox .image',
				]
			);

	        $this->add_responsive_control( 
				'image_border_radius',
				[
					'label' => esc_html__( 'Border Radius', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px' , '%' ],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .image, {{WRAPPER}} .tf-imagebox .image img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			); 

			$this->add_group_control(
				\Elementor\Group_Control_Background::get_type(),
				[
					'name' => 'image_background',
					'label' => esc_html__( 'Background', 'tf-addon-for-elementer' ),
					'types' => [ 'classic', 'gradient' ],
					'selector' => '{{WRAPPER}} .tf-imagebox .image',
				]
			);

			$this->add_group_control(
				\Elementor\Group_Control_Box_Shadow::get_type(),
				[
					'name' => 'image_box_shadow',
					'label' => esc_html__( 'Box Shadow', 'tf-addon-for-elementer' ),
					'selector' => '{{WRAPPER}} .tf-imagebox .image',
				]
			);

			$this->add_control( 
				'image_padding',
				[
					'label' => esc_html__( 'Padding', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em' ],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .image' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_control( 
				'image_margin',
				[
					'label' => esc_html__( 'Margin', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em' ],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .image' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->start_controls_tabs( 
				'image_style_tabs' 
				);

	        	$this->start_controls_tab( 
	        		'image_style_normal_tab',
					[
						'label' => esc_html__( 'Normal', 'tf-addon-for-elementer' ),
					] );	
	        		
	        		$this->add_control( 
						'image_opacity',
						[
							'label' => esc_html__( 'Image Opacity', 'tf-addon-for-elementer' ),
							'type' => \Elementor\Controls_Manager::SLIDER,
							'size_units' => [ 'px' ],
							'range' => [
								'px' => [
									'min' => 0,
									'max' => 1,
									'step' => 0.01,
								],
							],
							'default' => [
								'unit' => 'px',
								'size' => 1,
							],
							'selectors' => [
								'{{WRAPPER}} .tf-imagebox .image img' => 'opacity: {{SIZE}};',
							],
						]
					);			
					
				$this->end_controls_tab();

				$this->start_controls_tab( 
					'image_style_hover_tab',
					[
						'label' => esc_html__( 'Hover', 'tf-addon-for-elementer' ),
					] );

					$this->add_control( 
						'image_opacity_hover',
						[
							'label' => esc_html__( 'Image Opacity', 'tf-addon-for-elementer' ),
							'type' => \Elementor\Controls_Manager::SLIDER,
							'size_units' => [ 'px' ],
							'range' => [
								'px' => [
									'min' => 0,
									'max' => 1,
									'step' => 0.01,
								],
							],
							'default' => [
								'unit' => 'px',
								'size' => 1,
							],
							'selectors' => [
								'{{WRAPPER}} .tf-imagebox:hover .image img' => 'opacity: {{SIZE}};',
							],
						]
					);

					$this->add_control( 
						'image_scale_hover',
						[
							'label' => esc_html__( 'Image Scale', 'tf-addon-for-elementer' ),
							'type' => \Elementor\Controls_Manager::SLIDER,
							'size_units' => [ 'px' ],
							'range' => [
								'px' => [
									'min' => 1,
									'max' => 2,
									'step' => 0.1,
								],
							],
							'default' => [
								'unit' => 'px',
								'size' => 1,1,
							],
							'selectors' => [
								'{{WRAPPER}} .tf-imagebox:hover .image img' => 'transform: scale({{SIZE}});',
							],
						]
					);	
										
				$this->end_controls_tab();

			$this->end_controls_tabs();

			$this->add_control(
				'image_overlay',
				[
					'label' => esc_html__( 'Overlay', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'show_image_overlay',
				[
					'label' => esc_html__( 'Show Overlay', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::SWITCHER,
					'label_on' => esc_html__( 'Show', 'tf-addon-for-elementer' ),
					'label_off' => esc_html__( 'Hide', 'tf-addon-for-elementer' ),
					'return_value' => 'yes',
					'default' => 'yes',
				]
			);

			$this->add_control(
				'image_overlay_background_color',
				[
					'label' => esc_html__( 'Background Color', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => 'rgba(0, 0, 0, 0.5)',
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .image .image-overlay' => 'background-color: {{VALUE}};',
					],
					'condition' => [
	                    'show_image_overlay'	=> 'yes',
	                ]
				]
			);

			$this->add_control(
				'image_overlay_effect',
				[
					'label' => esc_html__( 'Effect Overlay', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::SELECT,
					'default' => 'fade-in',
					'options' => [
						'default' => esc_html__( 'Default', 'tf-addon-for-elementer' ),
						'fade-in' => esc_html__( 'Fade In', 'tf-addon-for-elementer' ),
						'fade-in-up' => esc_html__( 'Fade In Up', 'tf-addon-for-elementer' ),
						'fade-in-down' => esc_html__( 'Fade In Down', 'tf-addon-for-elementer' ),
						'fade-in-left' => esc_html__( 'Fade In Left', 'tf-addon-for-elementer' ),
						'fade-in-right' => esc_html__( 'Fade In Right', 'tf-addon-for-elementer' ),
					],
					'condition' => [
	                    'show_image_overlay'	=> 'yes',
	                ]
				]
			);	       

	        $this->end_controls_section();
        // /.End Image Style 

        // Start Content Style        
			$this->start_controls_section( 
				'section_style_content',
	            [
	                'label' => esc_html__('Content', 'tf-addon-for-elementer'),
	                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
	            ]
	        ); 

	        $this->add_control(
				'content_style',
				[
					'label' => esc_html__( 'Style', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::SELECT,
					'default' => 'style-1',
					'options' => [
						'style-1' => esc_html__( 'Default', 'tf-addon-for-elementer' ),
						'style-2' => esc_html__( 'Content Absolute (Full)', 'tf-addon-for-elementer' ),
						'style-3' => esc_html__( 'Content Absolute (Only Title)', 'tf-addon-for-elementer' )
					],
				]
			);   

			$this->add_control(
				'content_effect',
				[
					'label' => esc_html__( 'Effect', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::SELECT,
					'default' => 'fade-in',
					'options' => [
						'fade-in' => esc_html__( 'Fade In', 'tf-addon-for-elementer' ),
						'fade-in-up' => esc_html__( 'Fade In Up', 'tf-addon-for-elementer' ),
						'fade-in-down' => esc_html__( 'Fade In Down', 'tf-addon-for-elementer' ),
						'fade-in-left' => esc_html__( 'Fade In Left', 'tf-addon-for-elementer' ),
						'fade-in-right' => esc_html__( 'Fade In Right', 'tf-addon-for-elementer' ),
					],
					'condition' => [
	                    'content_style'	=> 'style-2',
	                ]
				]
			);	

	        $this->add_responsive_control( 
				'content_padding',
				[
					'label' => esc_html__( 'Padding', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em' ],
					'default' => [
						'top' => '20',
						'right' => '20',
						'bottom' => '20',
						'left' => '20',
						'unit' => 'px',
					],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);		

			$this->add_responsive_control( 
				'content_margin',
				[
					'label' => esc_html__( 'Margin', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em' ],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			); 

			$this->add_group_control(
				\Elementor\Group_Control_Box_Shadow::get_type(),
				[
					'name' => 'content_box_shadow',
					'label' => esc_html__( 'Box Shadow', 'tf-addon-for-elementer' ),
					'selector' => '{{WRAPPER}} .tf-imagebox .content',
				]
			);

			$this->add_control( 
				'content_border_radius',
				[
					'label' => esc_html__( 'Border Radius', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_control( 
				'content_background_color',
				[
					'label' => esc_html__( 'Background Color', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '',
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content' => 'background-color: {{VALUE}}',
					],
				]
			); 

			$this->add_control( 
				'content_background_color_hover',
				[
					'label' => esc_html__( 'Background Color Hover', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '',
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content:hover' => 'background-color: {{VALUE}}',
					],
				]
			); 

			$this->add_control( 
				'heading_title_show',
				[
					'label' => esc_html__( 'Title Show', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::HEADING,
					'separator' => 'before',
					'condition' => [
						'content_style' => 'style-3'
					]
				]
			);

			$this->add_control( 
				'title_show_color',
				[
					'label' => esc_html__( 'Color', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '#ffffff',
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content-only .title a' => 'color: {{VALUE}}',
					],
					'condition' => [
						'content_style' => 'style-3'
					]
				]
			); 

			$this->add_control( 
				'title_show_background_color',
				[
					'label' => esc_html__( 'Background Color', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '#23A455',
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content-only' => 'background-color: {{VALUE}}',
					],
					'condition' => [
						'content_style' => 'style-3'
					]
				]
			); 

			$this->add_responsive_control( 
				'title_padding_show',
				[
					'label' => esc_html__( 'Padding', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em' ],
					'default' => [
						'top' => '15',
						'right' => '20',
						'bottom' => '15',
						'left' => '20',
						'unit' => 'px',
						'isLinked' => 'false',
					],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content-only .title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'condition' => [
						'content_style' => 'style-3'
					]
				]
			);

			$this->add_responsive_control( 
				'title_spacer_show',
				[
					'label' => esc_html__( 'Margin', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em' ],
					'default' => [
						'top' => '0',
						'right' => '0',
						'bottom' => '0',
						'left' => '0',
						'unit' => 'px',
						'isLinked' => 'false',
					],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content-only .title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'condition' => [
						'content_style' => 'style-3'
					]
				]
			);	

			$this->add_control( 
				'heading_title_show_icon',
				[
					'label' => esc_html__( 'Icon Title Show', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::HEADING,
					'condition' => [
						'content_style' => 'style-3'
					]
				]
			);

			$this->add_control( 
				'title_show_icon_color',
				[
					'label' => esc_html__( 'Color', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '#ffffff',
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content-only .wrap-icon' => 'color: {{VALUE}}',
					],
					'condition' => [
						'content_style' => 'style-3'
					]
				]
			); 

			$this->add_control( 
				'title_show_icon_background_color',
				[
					'label' => esc_html__( 'Background Color', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '#d83030',
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content-only .wrap-icon' => 'background-color: {{VALUE}}',
					],
					'condition' => [
						'content_style' => 'style-3'
					]
				]
			); 

			$this->add_responsive_control(
				'title_show_icon_size',
				[
					'label' => esc_html__( 'Icon Size', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::SLIDER,
					'size_units' => [ 'px', '%' ],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 100,
							'step' => 1,
						],
						'%' => [
							'min' => 0,
							'max' => 100,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 70,
					],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content-only .wrap-icon i' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}}; line-height: {{SIZE}}{{UNIT}}',
						'{{WRAPPER}} .tf-imagebox .content-only .wrap-icon' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
						'{{WRAPPER}} .tf-imagebox .content-only .title' => 'max-width: calc(100% - {{SIZE}}{{UNIT}});',							
					],
					'condition' => [
						'content_style' => 'style-3',
						'icon_name[value]!' => '',
					]
				]
			);

			$this->add_responsive_control(
				'title_show_icon_font_size',
				[
					'label' => esc_html__( 'Icon Font Size', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::SLIDER,
					'size_units' => [ 'px' ],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 100,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 25,
					],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content-only .wrap-icon' => 'font-size: {{SIZE}}{{UNIT}};',
						'{{WRAPPER}} .tf-imagebox .content-only .wrap-icon img' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
					],
					'condition' => [
						'content_style' => 'style-3'
					]
				]
			);

			$this->add_control( 
				'heading_icon',
				[
					'label' => esc_html__( 'Icon', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);	

			$this->add_control( 
				'icon_color',
				[
					'label' => esc_html__( 'Color', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '#000000',
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content .wrap-icon' => 'color: {{VALUE}}',
					],
				]
			);	

			$this->add_responsive_control(
				'icon_font_size',
				[
					'label' => esc_html__( 'Icon Font Size', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::SLIDER,
					'size_units' => [ 'px' ],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 100,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 30,
					],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content .wrap-icon' => 'font-size: {{SIZE}}{{UNIT}};',
						'{{WRAPPER}} .tf-imagebox .content .wrap-icon img' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
					],
				]
			); 

			$this->add_responsive_control( 
				'icon_spacer',
				[
					'label' => esc_html__( 'Margin', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em' ],
					'default' => [
						'top' => '0',
						'right' => '0',
						'bottom' => '20',
						'left' => '0',
						'unit' => 'px',
						'isLinked' => 'false',
					],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content .wrap-icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_control( 
				'heading_title',
				[
					'label' => esc_html__( 'Title', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'wrap_heading',
				[
					'label' => esc_html__( 'Wrap Heading', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::SELECT,
					'default' => 'h4',
					'options' => [
						'h1'  => esc_html__( 'H1', 'tf-addon-for-elementer' ),
						'h2'  => esc_html__( 'H2', 'tf-addon-for-elementer' ),
						'h3'  => esc_html__( 'H3', 'tf-addon-for-elementer' ),
						'h4'  => esc_html__( 'H4', 'tf-addon-for-elementer' ),
						'h5'  => esc_html__( 'H5', 'tf-addon-for-elementer' ),
						'h6'  => esc_html__( 'H6', 'tf-addon-for-elementer' ),
					],
				]
			);

	        $this->add_group_control( 
	        	\Elementor\Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'label' => esc_html__( 'Typography', 'tf-addon-for-elementer' ),
					'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .tf-imagebox .title a',
				]
			);

			$this->add_group_control(
				\Elementor\Group_Control_Text_Shadow::get_type(),
				[
					'name' => 'title_text_shadow',
					'label' => esc_html__( 'Text Shadow', 'tf-addon-for-elementer' ),
					'selector' => '{{WRAPPER}} .tf-imagebox .title a',
				]
			);

			$this->add_control( 
				'title_color',
				[
					'label' => esc_html__( 'Color', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '#000000',
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .title a' => 'color: {{VALUE}}',
					],
				]
			);

			$this->add_control( 
				'title_color_hover',
				[
					'label' => esc_html__( 'Color Hover', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '#23A455',
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .title a:hover' => 'color: {{VALUE}}',
					],
				]
			);

			$this->add_responsive_control( 
				'title_spacer',
				[
					'label' => esc_html__( 'Margin', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em' ],
					'default' => [
						'top' => '0',
						'right' => '0',
						'bottom' => '10',
						'left' => '0',
						'unit' => 'px',
						'isLinked' => 'false',
					],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .content .title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_control( 
				'heading_description',
				[
					'label' => esc_html__( 'Description', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_group_control( 
	        	\Elementor\Group_Control_Typography::get_type(),
				[
					'name' => 'description_typography',
					'label' => esc_html__( 'Typography', 'tf-addon-for-elementer' ),
					'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_3,
					'selector' => '{{WRAPPER}} .tf-imagebox .description',
				]
			);

			$this->add_group_control(
				\Elementor\Group_Control_Text_Shadow::get_type(),
				[
					'name' => 'description_text_shadow',
					'label' => esc_html__( 'Text Shadow', 'tf-addon-for-elementer' ),
					'selector' => '{{WRAPPER}} .tf-imagebox .description',
				]
			);

			$this->add_control( 
				'description_color',
				[
					'label' => esc_html__( 'Color', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'default' => '#000000',
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .description' => 'color: {{VALUE}}',
					],
				]
			);

			$this->add_responsive_control( 
				'description_spacer',
				[
					'label' => esc_html__( 'Margin', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em' ],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .description' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

	    	$this->end_controls_section();
        // /.End Content Style 

	    // Start Button Style 
		    $this->start_controls_section( 
		    	'section_style_button',
	            [
	                'label' => esc_html__( 'Button', 'tf-addon-for-elementer' ),
	                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
	            ]
	        );

	        $this->add_control(
				'button_align',
				[
					'label' => esc_html__( 'Alignment', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::CHOOSE,
					'options' => [
						'left'    => [
							'title' => esc_html__( 'Left', 'tf-addon-for-elementer' ),
							'icon' => 'eicon-text-align-left',
						],
						'center' => [
							'title' => esc_html__( 'Center', 'tf-addon-for-elementer' ),
							'icon' => 'eicon-text-align-center',
						],
						'right' => [
							'title' => esc_html__( 'Right', 'tf-addon-for-elementer' ),
							'icon' => 'eicon-text-align-right',
						],
						'justify' => [
							'title' => esc_html__( 'Justified', 'tf-addon-for-elementer' ),
							'icon' => 'eicon-text-align-justify',
						],
					],
				]
			);

	        $this->add_group_control( 
	        	\Elementor\Group_Control_Typography::get_type(),
				[
					'name' => 'button_typography',
					'label' => esc_html__( 'Typography', 'tf-addon-for-elementer' ),
					'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .tf-imagebox .tf-button',
				]
			);

			$this->add_responsive_control( 
				'button_padding',
				[
					'label' => esc_html__( 'Padding', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em' ],
					'default' => [
						'top' => '15',
						'right' => '30',
						'bottom' => '15',
						'left' => '30',
						'unit' => 'px',
					],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .tf-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);		

			$this->add_responsive_control( 
				'button_margin',
				[
					'label' => esc_html__( 'Margin', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em' ],
					'default' => [
						'top' => '20',
						'right' => '0',
						'bottom' => '0',
						'left' => '0',
						'unit' => 'px',
					],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .tf-button' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->start_controls_tabs( 
				'button_style_tabs' 
				);

	        	$this->start_controls_tab( 
	        		'button_style_normal_tab',
					[
						'label' => esc_html__( 'Normal', 'tf-addon-for-elementer' ),
					] );	
	        		$this->add_control( 
						'button_color',
						[
							'label' => esc_html__( 'Color', 'tf-addon-for-elementer' ),
							'type' => \Elementor\Controls_Manager::COLOR,
							'default' => '#ffffff',
							'selectors' => [
								'{{WRAPPER}} .tf-imagebox .tf-button' => 'color: {{VALUE}}',
								'{{WRAPPER}} .tf-imagebox .tf-button i' => 'color: {{VALUE}}',
								'{{WRAPPER}} .tf-imagebox .tf-button svg' => 'fill: {{VALUE}}',
							],
						]
					);

					$this->add_control( 
						'button_bg_color',
						[
							'label' => esc_html__( 'Background Color', 'tf-addon-for-elementer' ),
							'type' => \Elementor\Controls_Manager::COLOR,
							'default' => '#23A455',
							'selectors' => [
								'{{WRAPPER}} .tf-imagebox .tf-button' => 'background-color: {{VALUE}}',
							],
						]
					);

					$this->add_group_control( 
						\Elementor\Group_Control_Border::get_type(),
						[
							'name' => 'button_border',
							'label' => esc_html__( 'Border', 'tf-addon-for-elementer' ),
							'selector' => '{{WRAPPER}} .tf-imagebox .tf-button',
						]
					);

					$this->add_control( 
						'button_border_radius',
						[
							'label' => esc_html__( 'Border Radius', 'tf-addon-for-elementer' ),
							'type' => \Elementor\Controls_Manager::DIMENSIONS,
							'size_units' => [ 'px', 'em', '%' ],
							'selectors' => [
								'{{WRAPPER}} .tf-imagebox .tf-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
							],
						]
					);				
					
				$this->end_controls_tab();

				$this->start_controls_tab( 
					'button_style_hover_tab',
					[
						'label' => esc_html__( 'Hover', 'tf-addon-for-elementer' ),
					] );

					$this->add_control( 
						'button_color_hover',
						[
							'label' => esc_html__( 'Color Hover', 'tf-addon-for-elementer' ),
							'type' => \Elementor\Controls_Manager::COLOR,
							'default' => '#ffffff',
							'selectors' => [
								'{{WRAPPER}} .tf-imagebox .tf-button:hover' => 'color: {{VALUE}}',
								'{{WRAPPER}} .tf-imagebox .tf-button:hover i' => 'color: {{VALUE}}',
								'{{WRAPPER}} .tf-imagebox .tf-button:hover svg' => 'fill: {{VALUE}}',
							],
						]
					);

					$this->add_control( 
						'button_bg_color_hover',
						[
							'label' => esc_html__( 'Background Color Hover', 'tf-addon-for-elementer' ),
							'type' => \Elementor\Controls_Manager::COLOR,
							'default' => '#000000',
							'selectors' => [
								'{{WRAPPER}} .tf-imagebox .hover-default.tf-button:hover, {{WRAPPER}} .tf-imagebox .btn-overlay:after' => 'background-color: {{VALUE}}',
							],
						]
					);

					$this->add_control(
						'button_animation_options',
						[
							'label' => esc_html__( 'Effect Type', 'tf-addon-for-elementer' ),
							'type' => \Elementor\Controls_Manager::SELECT,
							'default' => 'default',
							'options' => [
								'default' => esc_html__( 'Default', 'tf-addon-for-elementer' ),
								'button' => esc_html__( 'Elementor Button Effect', 'tf-addon-for-elementer' ),
								'button-overlay' => esc_html__( 'TF Effect', 'tf-addon-for-elementer' ),
							]
						]
					);

					$this->add_control(
						'button_animation_overlay',
						[
							'label' => esc_html__( 'Style', 'tf-addon-for-elementer' ),
							'type' => \Elementor\Controls_Manager::SELECT,
							'default' => 'from-top',
							'options' => [								
								'from-top' => esc_html__( 'From Top', 'tf-addon-for-elementer' ),
								'from-bottom' => esc_html__( 'From Bottom', 'tf-addon-for-elementer' ),
								'from-left' => esc_html__( 'From Left', 'tf-addon-for-elementer' ),
								'from-right' => esc_html__( 'From Right', 'tf-addon-for-elementer' ),
								'from-center' => esc_html__( 'From Center', 'tf-addon-for-elementer' ),
								'skew' => esc_html__( 'Skew', 'tf-addon-for-elementer' ),								
							],
							'condition'=> [
								'button_animation_options' => 'button-overlay',
							],
						]
					);	

					$this->add_control(
						'button_animation',
						[
							'label' => esc_html__( 'Hover Animation', 'tf-addon-for-elementer' ),
							'type' => \Elementor\Controls_Manager::SELECT,
							'default' => 'elementor-animation-push',
							'options' => [
								'elementor-animation-grow' => esc_html__( 'Grow', 'tf-addon-for-elementer' ),
								'elementor-animation-shrink' => esc_html__( 'Shrink', 'tf-addon-for-elementer' ),
								'elementor-animation-pulse' => esc_html__( 'Pulse', 'tf-addon-for-elementer' ),
								'elementor-animation-pulse-grow' => esc_html__( 'Pulse Grow', 'tf-addon-for-elementer' ),
								'elementor-animation-pulse-shrink' => esc_html__( 'Pulse Shrink', 'tf-addon-for-elementer' ),
								'elementor-animation-push' => esc_html__( 'Push', 'tf-addon-for-elementer' ),
								'elementor-animation-pop' => esc_html__( 'Pop', 'tf-addon-for-elementer' ),
								'elementor-animation-bob' => esc_html__( 'Bob', 'tf-addon-for-elementer' ),
								'elementor-animation-hang' => esc_html__( 'Hang', 'tf-addon-for-elementer' ),
								'elementor-animation-skew' => esc_html__( 'Skew', 'tf-addon-for-elementer' ),
								'elementor-animation-wobble-vertical' => esc_html__( 'Wobble Vertical', 'tf-addon-for-elementer' ),
								'elementor-animation-wobble-horizontal' => esc_html__( 'Wobble Horizontal', 'tf-addon-for-elementer' ),

							],
							'condition'=> [
								'button_animation_options' => 'button',
							],
						]
					);				

					$this->add_group_control( 
						\Elementor\Group_Control_Border::get_type(),
						[
							'name' => 'button_border_hover',
							'label' => esc_html__( 'Border', 'tf-addon-for-elementer' ),
							'selector' => '{{WRAPPER}} .tf-imagebox .tf-button:hover',
						]
					);

					$this->add_control( 
						'button_border_radius_hover',
						[
							'label' => esc_html__( 'Border Radius', 'tf-addon-for-elementer' ),
							'type' => \Elementor\Controls_Manager::DIMENSIONS,
							'size_units' => [ 'px', 'em', '%' ],
							'selectors' => [
								'{{WRAPPER}} .tf-imagebox .tf-button:hover' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
							],
						]
					);
					
				$this->end_controls_tab();

			$this->end_controls_tabs();

			$this->add_control( 
				'heading_button_icon',
				[
					'label' => esc_html__( 'Icon', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::HEADING,
					'separator' => 'before',
				]
			); 

			$this->add_control( 
				'icon_button',
				[
					'label' => esc_html__( 'Icon Button', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::ICONS,
					'fa4compatibility' => 'icon_bt',
					'default' => [
						'value' => 'fas fa-angle-double-right',
						'library' => 'fa-solid',
					],				
				]
			);

			$this->add_control( 
				'button_icon_size',
				[
					'label' => esc_html__( 'Icon Size', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::SLIDER,
					'size_units' => [ 'px' ],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 50,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 15,
					],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .tf-button i' => 'font-size: {{SIZE}}{{UNIT}};',
						'{{WRAPPER}} .tf-imagebox .tf-button svg' => 'width: {{SIZE}}{{UNIT}};',
					],
				]
			); 

			$this->add_control( 
				'button_icon_position',
				[
					'label' => esc_html__( 'Icon Position', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::SELECT,
					'default' => 'bt_icon_after',
					'options' => [
						'bt_icon_before'  => esc_html__( 'Before', 'tf-addon-for-elementer' ),
						'bt_icon_after' => esc_html__( 'After', 'tf-addon-for-elementer' ),
					],
				]
			);

			$this->add_control( 
				'button_icon_spacer',
				[
					'label' => esc_html__( 'Icon Spacer', 'tf-addon-for-elementer' ),
					'type' => \Elementor\Controls_Manager::SLIDER,
					'size_units' => [ 'px' ],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 50,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 10,
					],
					'selectors' => [
						'{{WRAPPER}} .tf-imagebox .tf-button.bt_icon_before i' => 'margin-right: {{SIZE}}{{UNIT}};',
						'{{WRAPPER}} .tf-imagebox .tf-button.bt_icon_before svg' => 'margin-right: {{SIZE}}{{UNIT}};',
						'{{WRAPPER}} .tf-imagebox .tf-button.bt_icon_after i' => 'margin-left: {{SIZE}}{{UNIT}};',
						'{{WRAPPER}} .tf-imagebox .tf-button.bt_icon_after svg' => 'margin-left: {{SIZE}}{{UNIT}};',
					],
				]
			);

		    $this->end_controls_section();
	    // /.End Button Style
	}

	protected function render($instance = []) {
		$settings = $this->get_settings_for_display();
		
		$image =  \Elementor\Group_Control_Image_Size::get_attachment_image_html( $settings, 'thumbnail', 'image' );

		$html_title = $html_description = $html_image_overlay = $button = $icon_button = $icon_name = $html_icon = $has_icon = '';

		if ( isset( $settings['icon_button']['value'] ) ) {
			if ( !empty( $settings['icon_button']['value']['url'] ) ) {
				$icon_button .= sprintf(
		           '<img class="logo_svg" src="%1$s" alt="%2$s"/>',
		             $settings['icon_button']['value']['url'],
		             $settings['icon_button']['value']['id']
		            
		         ); 
			} else {
				$icon_button .= sprintf(
		             '<i class="%1$s"></i>',
		            $settings['icon_button']['value']
		        );  
			}
		} 	

		$btn_animation = 'hover-default';
		if ($settings['button_animation_options'] == 'button') {
			$btn_animation = 'hover-default ' . $settings['button_animation'];
		}elseif ($settings['button_animation_options'] == 'button-overlay') {
			$btn_animation = 'btn-overlay ' . $settings['button_animation_overlay'];
		}

		if ( $settings['show_button'] == 'yes' ) {
			if ($settings['button_icon_position'] == 'bt_icon_after') {
				$button =  sprintf ('<div class="tf-button-container %4$s"><a class="tf-button %5$s %6$s" href="%3$s">%1$s %2$s</a></div>',$settings['button_text'] , $icon_button, $settings['link']['url'], $settings['button_align'], $settings['button_icon_position'], $btn_animation );
			}else{
				$button =  sprintf ('<div class="tf-button-container %4$s"><a class="tf-button %5$s %6$s" href="%3$s">%2$s %1$s</a></div>',$settings['button_text'] , $icon_button, $settings['link']['url'], $settings['button_align'], $settings['button_icon_position'], $btn_animation );
			}
			
		}		

		if ($settings['show_image_overlay'] == 'yes') {
			$html_image_overlay = sprintf('<div class="image-overlay %1$s"></div>', $settings['image_overlay_effect']);
		}

		if ($settings['title'] != '') {
			$html_title = sprintf('<%2$s class="title"><a href="%3$s">%1$s</a></%2$s>', $settings['title'], $settings['wrap_heading'], $settings['link']['url']);
		}

		if ($settings['description'] != '') {
			$html_description = sprintf('<div class="description">%1$s</div>', $settings['description']);
		}

		if ( $settings['icon_name']['value'] != '' ) {
			if ( !empty( $settings['icon_name']['value']['url'] ) ) {
				$icon_name = sprintf(
		           '<img class="logo_svg" src="%1$s" alt="%2$s"/>',
		             $settings['icon_name']['value']['url'],
		             $settings['icon_name']['value']['id']		            
		        ); 

		        $html_icon = sprintf('<div class="wrap-icon">%1$s</div>', $icon_name);
			} else {
				$icon_name = sprintf(
		             '<i class="%1$s"></i>',
		            $settings['icon_name']['value']
		        );  
		        $html_icon = sprintf('<div class="wrap-icon">%1$s</div>', $icon_name);
			}

			$has_icon = 'has-icon';
		}		

		echo sprintf ( 
			'<div class="tf-imagebox %6$s"> 
                <div class="image">%1$s %5$s</div>
                <div class="content-only %9$s"> 
                	%8$s               	              
					%2$s
				</div>
                <div class="content %7$s">	
                	%8$s                
					%2$s
	                %3$s
	                %4$s
				</div>
            </div>',
            $image,
            $html_title,
            $html_description,           
            $button,
            $html_image_overlay,
            $settings['content_style'],
            $settings['content_effect'],
            $html_icon,
            $has_icon
        );
			
	}

	protected function _content_template() {}	

}