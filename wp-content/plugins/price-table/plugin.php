<?php

namespace PriceTable_Widgets;


class Plugin
{

  private static $_instance = null;

  public static function instance()
  {
    if (is_null(self::$_instance)) {
      self::$_instance = new self();
    }
    return self::$_instance;
  }  

  public function widget_styles()
  {
    wp_register_style('font-awesome', ELEMENTOR_ASSETS_URL . 'lib/font-awesome/css/font-awesome.css', __FILE__);
    wp_register_style('price-table-widgets-style', plugins_url('/assets/css/price-table-widgets-style.css', __FILE__));
  }

  private function include_widgets_files()
  {
    require_once(__DIR__ . '/widgets/pricetable.php');
  }

  public function register_widgets()
  {
    $this->include_widgets_files();
    $this->reflection = new \ReflectionClass( $this );

    $class_name = $this->reflection->getNamespaceName() . '\Widgets\\' . 'PriceTable';

    \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new $class_name());
  }

  public static function tf_admin_enqueue_scripts( $hook ) {

    // Register styles.
    wp_register_style(
      'tf-style',
      plugins_url('/assets/css/admin/styles.css', __FILE__),
      array(),
      null
    );

    wp_enqueue_style( 'tf-style' );
  }

  public static function load_admin() {
    add_action( 'elementor/editor/after_enqueue_styles', __CLASS__ . '::tf_admin_enqueue_scripts' );
  }

  public function __construct()
  {
    add_action('elementor/frontend/after_register_styles', [$this, 'widget_styles']);

    add_action('elementor/preview/enqueue_styles', function () {
      wp_enqueue_style('price-table-widgets-style');
    });

    add_action('elementor/widgets/widgets_registered', [$this, 'register_widgets']);
    add_action( 'elementor/init', __CLASS__ . '::load_admin', 0 );
  }

}

Plugin::instance();