<?php
/**
 * The template for displaying header.
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
$site_name = get_bloginfo( 'name' );
$tagline   = get_bloginfo( 'description', 'display' );
$logo_site = themesflat_choose_opt('site_logo');
$logo_retina_site = themesflat_choose_opt('site_retina_logo');
?>
<header class="site-header" role="banner">
	<div class="site-header-inner">		
		<div class="site-branding">
			<?php if ( $logo_site ) : ?>
			    <div id="logo" class="logo" >                  
			        <a href="<?php echo esc_url( home_url('/') ); ?>"  title="<?php bloginfo('name'); ?>">
			            <?php if  (!empty($logo_site)) { ?>
			                <img class="site-logo"  src="<?php echo esc_url($logo_site); ?>" alt="<?php bloginfo('name'); ?>"  data-retina="<?php echo esc_url( $logo_retina_site ); ?>" />
			            <?php } ?>
			        </a>
			    </div>
			<?php else : ?>
			    <div id="logo" class="logo">
			    	<h1 class="site-title"><a href="<?php echo esc_url( home_url('/') ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>			
					<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>	
			    </div><!-- /.site-infomation -->          
			<?php endif; ?>		
		</div>

		<?php if ( has_nav_menu( 'menu-1' ) ) : ?>
		<nav class="site-navigation" role="navigation">
			<div id="mainnav">
			<?php wp_nav_menu( array( 'theme_location' => 'menu-1' ) ); ?>
			</div>
		</nav>
		<div class="btn-menu">
	        <span></span>
	    </div><!-- //mobile menu button -->
		<?php endif; ?>

	</div>
</header>
