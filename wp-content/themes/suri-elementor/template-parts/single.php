<?php
/**
 * The template for displaying singular post-types: posts, pages and user-defined custom post types.
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<?php
while ( have_posts() ) : the_post();
	?>

<main <?php post_class( 'site-main' ); ?> role="main">
	<!-- <?php //if ( apply_filters( 'hello_elementor_page_title', true ) ) : ?>
		<header class="page-header">
			<?php //the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header>
	<?php //endif; ?> -->
	<div class="page-content">		
		<?php if (is_single()): ?>
			<div class="featured-post"><?php echo get_the_post_thumbnail( get_the_ID(), 'themesflat-blog-single' ); ?></div>
		
	        <ul class="post-meta">                    
		        <li class="post-author"> 
		            <span class="far fa-user" aria-hidden="true"></span>                       
		            <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_the_author(); ?></a>
		        </li>                    
		        <li class="post-category">
		            <span class="far fa-folder-open" aria-hidden="true"></span>
		            <?php echo get_the_category_list(', '); ?>
		        </li>
		        <li class="post-date">
		            <span class="far fa-calendar" aria-hidden="true"></span>    
		            <?php
		            $archive_year  = get_the_time('Y'); 
		            $archive_month = get_the_time('m'); 
		            $archive_day   = get_the_time('d');
		            ?>                        
		            <a href="<?php echo get_day_link($archive_year, $archive_month, $archive_day); ?>"><?php echo get_the_date(); ?></a>
		        </li>                   
		        <li class="post-comments"> 
		            <span class="far fa-comments" aria-hidden="true"></span>                           
		            <?php echo comments_popup_link( esc_html__( 'Comment (0)', 'suri-elementor' ), esc_html__(  'Comment (1)', 'suri-elementor' ), esc_html__( 'Comments (%)', 'suri-elementor' ) ); ?>                                         
		        </li>                    
		    </ul>

			<?php printf( '<h2 class="%s">%s</h2>', 'entry-title', esc_html( get_the_title() ) ); ?>

			<div class="content-post"><?php the_content(); ?></div>
		<?php else : ?>
			<?php the_content(); ?>
		<?php endif ?>
		

		<div class="post-tags">
			<?php the_tags( '<span class="tag-links">' . __( 'Tagged ', 'suri-elementor' ), null, '</span>' ); ?>
		</div>
		<?php wp_link_pages(); ?>
	</div>

	<?php comments_template(); ?>
</main>

	<?php
endwhile;
