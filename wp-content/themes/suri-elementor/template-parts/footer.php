<?php
/**
 * The template for displaying footer.
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<footer id="footer" class="footer">
	<div class="footer-widgets-container">
        <div class="footer-widgets">
            <?php
            $footer_column = get_theme_mod( 'footer_widget_areas', '4' );                
            switch ( $footer_column ):                    
                case 1:
                    $class = 'col-lg-12';
                    break;
                case 2:
                    $class = 'col-lg-6';
                    break;
                case 3:
                    $class = 'col-lg-4';
                    break;
                case 4:
                    $class = 'col-lg-3';
                    break;
            endswitch;
            ?>
            
           <?php for ( $i = 1; $i <= $footer_column; $i++ ) : ?>
            <div class="<?php echo esc_attr($class); ?>">
                <?php
                    themesflat_dynamic_sidebar( "footer-$i" );
                ?>
            </div><!-- /.col-lg- -->

            <?php endfor; ?>                
        </div><!-- /.footer-widgets -->
    </div><!-- /.container --> 
</footer>

<!-- Bottom -->
<div class="bottom">
    <div class="container">           
        <div class="bottom-row">
            <div class="col-lg-12">
                <div class="copyright">                        
                    <?php echo wp_kses_post(themesflat_choose_opt( 'footer_copyright')); ?>
                </div>                                  
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div>
