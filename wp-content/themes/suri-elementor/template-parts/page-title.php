<?php 
    
    if ( is_page() && is_page_template( 'template-parts/front-page.php' ) ) {
        return;
    }

    $title = esc_html__( 'Archives', 'suri-elementor' );

    if ( is_home() ) {
        if (is_front_page() ) {
            $title = esc_html__( 'Blog', 'suri-elementor' );
        }elseif ( get_page_template_slug() == 'template-parts/blog.php' ) {
            $title = get_the_title();
        }
        else {
            $title = esc_html( wp_title('',FALSE) );
        }

    } elseif ( is_singular('post') ) {
        $title = esc_html__( 'Blog', 'suri-elementor' );
    } elseif ( is_singular() ) {
        if (is_single()) {
            $title = get_the_title();

        }elseif(is_page_template('template-parts/page_single.php')) {
            $title = get_the_title();
        }
         elseif(is_page_template('template-parts/page_no_title.php')) {
            $title = '';
        }else {
            $title = get_the_title();
        }
    } elseif ( is_search() ) {
        $title = sprintf( esc_html__( 'Search results for &quot;%s&quot;', 'suri-elementor' ), get_search_query() );
    } elseif ( is_404() ) {
        $title = esc_html__( 'Not Found', 'suri-elementor' );
    } elseif ( is_author() ) {
        the_post();
        $title = sprintf( esc_html__( 'Author Archives: %s', 'suri-elementor' ), get_the_author() );
        rewind_posts();
    } elseif ( is_day() ) {
        $title = sprintf( esc_html__( 'Daily Archives: %s', 'suri-elementor' ), get_the_date() );
    } elseif ( is_month() ) {
        $title = sprintf( esc_html__( 'Monthly Archives: %s', 'suri-elementor' ), get_the_date( 'F Y' ) );
    } elseif ( is_year() ) {
        $title = sprintf( esc_html__( 'Yearly Archives: %s', 'suri-elementor' ), get_the_date( 'Y' ) );
    } elseif ( is_tax() || is_category() || is_tag() ) {
        $title = single_term_title( '', false );
    }

?>

<!-- Page title -->
<div class="page-title <?php echo esc_attr( themesflat_choose_opt('page_title_styles') ); ?> ">
    <div class="overlay-image"></div>
    <div class="overlay"></div>   
    <div class="container"> 
        <div class="row">
            <div class="col-md-12 page-title-container">
                <?php if ( themesflat_get_opt( 'show_page_title' ) == 1 ): ?>
                    <div class="page-title-heading">
                        <h1 class="title"><?php echo esc_html( $title ); ?></h1>
                    </div><!-- /.page-title-captions --> 
                <?php endif; ?>

                <?php 
                if ( get_theme_mod( 'breadcrumb_enabled', 1 ) == 1 ):

                    themesflat_breadcrumb_trail( array(
                        'separator'   => get_theme_mod( 'breadcrumb_separator', '>' ),
                        'show_browse' => true,
                        'labels'      => array(
                        'browse'  => themesflat_get_opt( 'bread_crumb_prefix', esc_html__( '', 'suri-elementor' ) ),
                            'home'    => esc_html__( 'Home', 'suri-elementor' )
                        )
                    ) );
                
                endif;                       
               ?>                 
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /.page-title --> 