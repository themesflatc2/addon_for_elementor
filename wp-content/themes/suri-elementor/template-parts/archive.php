<?php
/**
 * The template for displaying archive pages.
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<main class="site-main" role="main">

	<!-- <?php //if ( apply_filters( 'hello_elementor_page_title', true ) ) : ?>
		<header class="page-header">
			<?php
			//the_archive_title( '<h1 class="entry-title">', '</h1>' );
			//the_archive_description( '<p class="archive-description">', '</p>' );
			?>
		</header>
	<?php //endif; ?> -->
	<div class="page-content">
        <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); $post_link = get_permalink();?>
            <article class="post">
                <div class="featured-post"><?php printf( '<a href="%s">%s</a>', esc_url( $post_link ), get_the_post_thumbnail( $post, 'themesflat-blog' ) ); ?></div>
                <ul class="post-meta">                    
                    <li class="post-author"> 
                        <span class="far fa-user" aria-hidden="true"></span>                       
                        <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_the_author(); ?></a>
                    </li>                    
                    <!-- <li class="post-category">
                        <span class="far fa-user" aria-hidden="true"></span>    
                        <?php //echo get_the_category_list(', '); ?>
                    </li> -->
                    <li class="post-date">
                        <span class="far fa-calendar" aria-hidden="true"></span>    
                        <?php
                        $archive_year  = get_the_time('Y'); 
                        $archive_month = get_the_time('m'); 
                        $archive_day   = get_the_time('d');
                        ?>                        
                        <a href="<?php echo get_day_link($archive_year, $archive_month, $archive_day); ?>"><?php echo get_the_date(); ?></a>
                    </li>                   
                    <li class="post-comments"> 
                        <span class="far fa-comments" aria-hidden="true"></span>                           
                        <?php echo comments_popup_link( esc_html__( 'Comment (0)', 'suri-elementor' ), esc_html__(  'Comment (1)', 'suri-elementor' ), esc_html__( 'Comments (%)', 'suri-elementor' ) ); ?>                                         
                    </li>                    
                </ul>
                <?php
                printf( '<h2 class="%s"><a href="%s">%s</a></h2>', 'entry-title', esc_url( $post_link ), esc_html( get_the_title() ) );   
                ?>
                <div class="content-post"><?php echo wp_trim_words( get_the_content(), 20, '&hellip;' );//the_excerpt(); ?></div>
                <div class="tf-button-container">
                    <a href="<?php echo esc_url( get_permalink() ) ?>" class="tf-button">
                        <?php echo esc_html__( 'Read More', 'suri-elementor' ); ?>
                        <span class="fas fa-arrow-right" aria-hidden="true"></span> 
                    </a>
            </article>
        <?php endwhile;?>
    </div>

	<?php wp_link_pages(); ?>

	<?php
	global $wp_query;
	if ( $wp_query->max_num_pages > 1 ) :
		?>
		<nav class="pagination" role="navigation">
			<?php /* Translators: HTML arrow */ ?>
			<div class="nav-previous"><?php next_posts_link( sprintf( __( '%s older', 'suri-elementor' ), '<span class="meta-nav">&larr;</span>' ) ); ?></div>
			<?php /* Translators: HTML arrow */ ?>
			<div class="nav-next"><?php previous_posts_link( sprintf( __( 'newer %s', 'suri-elementor' ), '<span class="meta-nav">&rarr;</span>' ) ); ?></div>
		</nav>
	<?php endif; ?>
</main>
