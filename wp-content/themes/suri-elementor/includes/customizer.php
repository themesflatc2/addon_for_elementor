<?php
/**
 * suri-elementor Theme Customizer
 *
 * @package suri-elementor
 */

function themesflat_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
    $wp_customize->get_section( 'header_image' )->title = esc_html('Backgound PageTitle', 'suri-elementor');
    $wp_customize->get_section( 'header_image' )->priority = '22';   
    $wp_customize->get_section( 'title_tagline' )->priority = '1';
    $wp_customize->get_section( 'title_tagline' )->title = esc_html('General', 'suri-elementor');
    $wp_customize->get_section( 'colors' )->title = esc_html('Layout Style', 'suri-elementor'); 
    $wp_customize->remove_control('custom_logo');
    remove_theme_support( 'custom-header' );
  
    //Heading
    class themesflat_Info extends WP_Customize_Control {
        public $type = 'heading';
        public $label = '';
        public function render_content() {
        ?>
            <h3 class="themesflat-title-control"><?php echo esc_html( $this->label ); ?></h3>
        <?php
        }
    }    

    //Title
    class themesflat_Title_Info extends WP_Customize_Control {
        public $type = 'title';
        public $label = '';
        public function render_content() {
        ?>
            <h4><?php echo esc_html( $this->label ); ?></h4>
        <?php
        }
    }    

    //Desc
    class themesflat_Theme_Info extends WP_Customize_Control {
        public $type = 'info';
        public $label = '';
        public function render_content() {
        ?>
            <h3><?php echo esc_html( $this->label ); ?></h3>
        <?php
        }
    }    

    //Desc
    class themesflat_Desc_Info extends WP_Customize_Control {
        public $type = 'desc';
        public $label = '';
        public function render_content() {
        ?>
            <p class="themesflat-desc-control"><?php echo esc_html( $this->label ); ?></p>
        <?php
        }
    }  

    //___General___//
    $wp_customize->add_setting('themesflat_options[info]', array(
            'type'              => 'info_control',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'esc_attr',            
        )
    );     

    //___Header___//
    $wp_customize->add_section(
        'themesflat_header',
        array(
            'title'         => esc_html('Header', 'suri-elementor'),
            'priority'      => 2,
        )
    );

    // Heading custom logo
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-logo', array(
        'label' => esc_html('Custom Logo', 'suri-elementor'),
        'section' => 'themesflat_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 2
        ) )
    );   
    // Desc custon logo
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_logo', array(
        'label' => esc_html('In this section You can upload your own custom logo, change the way your logo can be displayed', 'suri-elementor'),
        'section' => 'themesflat_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 3
        ) )
    );

    //Logo
    $wp_customize->add_setting(
        'site_logo',
        array(
            'default' => themesflat_customize_default('site_logo'),
            'sanitize_callback' => 'esc_url_raw',
        )
    );    
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'site_logo',
            array(
               'label'          => esc_html( 'Upload Your Logo ', 'suri-elementor' ),
               'description'    => esc_html( 'The best size is 190x51px ( If you don\'t display logo please remove it your website display 
                Site Title default in General )', 'suri-elementor' ),
               'type'           => 'image',
               'section'        => 'themesflat_header',
               'priority'       => 4,
            )
        )
    );

    // Logo Retina
    $wp_customize->add_setting(
        'site_retina_logo',
        array(
            'default'           => themesflat_customize_default('site_retina_logo'),
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'site_retina_logo',
            array(
               'label'          => esc_html( 'Upload Your Logo Retina', 'suri-elementor' ),
               'description'    => esc_html( 'The best size is 372x90px', 'suri-elementor' ),
               'type'           => 'image',
               'section'        => 'themesflat_header',
               'priority'       => 5,
            )
        )
    );

    // Height
    $wp_customize->add_setting(
        'logo_width',
        array(
            'default' => themesflat_customize_default('logo_width'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control(
        'logo_width',
        array(
            'label' => esc_html( 'Width (px)', 'suri-elementor' ),
            'section' => 'themesflat_header',
            'type' => 'text',
            'priority' => 6
        )
    );

    // Box control
    $wp_customize->add_setting(
        'logo_controls',
        array(
            'default' => themesflat_customize_default('logo_controls'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control( new themesflat_BoxControls($wp_customize,
        'logo_controls',
        array(
            'label' => esc_html( 'Logo Box Controls (px)', 'suri-elementor' ),
            'section' => 'themesflat_header',
            'type' => 'box-controls',
            'priority' => 7
        ))
    ); 

    // Section Typography
    $wp_customize->add_section(
        'flat_typography',
        array(
            'title' => esc_html('Typography', 'suri-elementor'),
            'priority' => 3,            
        )
    );
    // Heading Typography
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-typography', array(
        'label' => esc_html('BODY FONT', 'suri-elementor'),
        'section' => 'flat_typography',
        'settings' => 'themesflat_options[info]',
        'priority' => 2
        ) )
    );    

    // Desc Typography
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_logo', array(
        'label' => esc_html('You can modify the font family, size, color, ... for global content.', 'suri-elementor'),
        'section' => 'flat_typography',
        'settings' => 'themesflat_options[info]',
        'priority' => 3
        ) )
    );

    // Key Google Api
    $wp_customize->add_setting(
        'key_google_api',
        array(
            'default' => themesflat_customize_default('key_google_api'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control(
        'key_google_api',
        array(
            'label' => esc_html( 'Key Google Api', 'suri-elementor' ),
            'section' => 'flat_typography',
            'type' => 'text',
            'priority' => 3
        )
    );

    // Body fonts
    $wp_customize->add_setting(
        'body_font_name',
        array(
            'default' => themesflat_customize_default('body_font_name'),
            'sanitize_callback' => 'esc_html',
        )
    );
    $wp_customize->add_control( new themesflat_Typography($wp_customize,
        'body_font_name',
        array(
            'label' => esc_html( 'Font name/style/sets', 'suri-elementor' ),
            'section' => 'flat_typography',
            'type' => 'typography',
            'fields' => array('family','style','line_height','size'),
            'priority' => 4
        ))
    );

    // Headings fonts
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-heading-font', array(
        'label' => esc_html('Headings fonts', 'suri-elementor'),
        'section' => 'flat_typography',
        'settings' => 'themesflat_options[info]',
        'priority' => 8
        ) )
    );    

    // Desc font
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_heading-font', array(
        'label' => esc_html('You can modify the font options for your headings. h1, h2, h3, h4, ...', 'suri-elementor'),
        'section' => 'flat_typography',
        'settings' => 'themesflat_options[info]',
        'priority' => 9
        ) )
    );   

    $wp_customize->add_setting(
        'headings_font_name',
        array(
            'default' => themesflat_customize_default('headings_font_name'),
            'sanitize_callback' => 'esc_html',
        )
    );
    $wp_customize->add_control( new themesflat_Typography($wp_customize,
        'headings_font_name',
        array(
            'label' => esc_html( 'Font name/style/sets', 'suri-elementor' ),
            'section' => 'flat_typography',
            'type' => 'typography',
            'fields' => array('family','style'),
            'priority' => 11
        ))
    );

    // H1 size
    $wp_customize->add_setting(
        'h1_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           => themesflat_customize_default('h1_size'),
        )       
    );
    $wp_customize->add_control( 'h1_size', array(
        'type'        => 'number',
        'priority'    => 13,
        'section'     => 'flat_typography',
        'label'       => esc_html('H1 font size (px)', 'suri-elementor'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // H2 size
    $wp_customize->add_setting(
        'h2_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           =>  themesflat_customize_default('h2_size'),
        )       
    );
    $wp_customize->add_control( 'h2_size', array(
        'type'        => 'number',
        'priority'    => 14,
        'section'     => 'flat_typography',
        'label'       => esc_html('H2 font size (px)', 'suri-elementor'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // H3 size
    $wp_customize->add_setting(
        'h3_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           => themesflat_customize_default('h3_size'),
        )       
    );
    $wp_customize->add_control( 'h3_size', array(
        'type'        => 'number',
        'priority'    => 15,
        'section'     => 'flat_typography',
        'label'       => esc_html('H3 font size (px)', 'suri-elementor'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // H4 size
    $wp_customize->add_setting(
        'h4_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           =>  themesflat_customize_default('h4_size'),
        )       
    );
    $wp_customize->add_control( 'h4_size', array(
        'type'        => 'number',
        'priority'    => 16,
        'section'     => 'flat_typography',
        'label'       => esc_html('H4 font size (px)', 'suri-elementor'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // H5 size
    $wp_customize->add_setting(
        'h5_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           =>  themesflat_customize_default('h5_size'),
        )       
    );
    $wp_customize->add_control( 'h5_size', array(
        'type'        => 'number',
        'priority'    => 17,
        'section'     => 'flat_typography',
        'label'       => esc_html('H5 font size (px)', 'suri-elementor'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // H6 size
    $wp_customize->add_setting(
        'h6_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           =>  themesflat_customize_default('h6_size'),
        )       
    );
    $wp_customize->add_control( 'h6_size', array(
        'type'        => 'number',
        'priority'    => 18,
        'section'     => 'flat_typography',
        'label'       => esc_html('H6 font size (px)', 'suri-elementor'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // Heading Menu fonts
    $wp_customize->add_setting('themesflat_options[info]', array(
            'type'              => 'info_control',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'esc_attr',            
        )
    );
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'menu_fonts', array(
        'label' => esc_html('Menu fonts', 'suri-elementor'),
        'section' => 'flat_typography',
        'settings' => 'themesflat_options[info]',
        'priority' => 19
        ) )
    );

    $wp_customize->add_setting(
        'menu_font_name',
        array(
            'default' => themesflat_customize_default('menu_font_name'),
                'sanitize_callback' => 'esc_html',
        )
    );
    $wp_customize->add_control( new themesflat_Typography($wp_customize,
        'menu_font_name',
        array(
            'label' => esc_html( 'Font name/style/sets', 'suri-elementor' ),
            'section' => 'flat_typography',
            'type' => 'typography',
            'fields' => array('family','style','size','line_height'),
            'priority' => 20
        ))
    );


    //__Color__//
    $wp_customize->add_panel('color_panel',array(
        'title'         => 'Color',
        'description'   => 'This is panel Description',
        'priority'      => 10,
    ));

    // ADD SECTION GENERAL
    $wp_customize->add_section('color_general',array(
        'title'         => 'General',
        'priority'      => 10,
        'panel'         => 'color_panel',
    ));

    // Heading Color Scheme
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'color_scheme', array(
        'label' => esc_html('SCHEME COLOR', 'suri-elementor'),
        'section' => 'color_general',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );    

    // Desc color scheme
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_color_schemer', array(
        'label' => esc_html('Select the color that will be used for theme color.','suri-elementor'),
        'section' => 'color_general',
        'settings' => 'themesflat_options[info]',
        'priority' => 2
        ) )
    ); 

    $wp_customize->add_setting(
        'primary_color',
        array(
            'default'           => themesflat_customize_default('primary_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'primary_color',
            array(
                'label'         => esc_html('Scheme color', 'suri-elementor'),
                'section'       => 'color_general',
                'settings'      => 'primary_color',
                'priority'      => 3
            )
        )
    );

    // Body Color
    $wp_customize->add_setting(
        'body_text_color',
        array(
            'default'           => themesflat_customize_default('body_text_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'body_text_color',
            array(
                'label'         => esc_html('Body Color', 'suri-elementor'),
                'section'       => 'color_general',
                'settings'      => 'body_text_color',
                'priority'      => 4
            )
        )
    ); 

    // ADD SECTION HEADER COLOR
    $wp_customize->add_section('color_header',array(
        'title'=>'Header',
        'priority'=>11,
        'panel'=>'color_panel',
    ));

    // MENU COLOR
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'menu_color', array(
        'label' => esc_html('MENU COLOR', 'suri-elementor'),
        'section' => 'color_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 3
        ) )
    );    

    // Desc
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_menu_color', array(
        'label' => esc_html('Select color for background menu, background submenu color menu a, color menu a:hover, background menu a:hover...','suri-elementor'),
        'section' => 'color_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 4
        ) )
    );   

    // Menu Background
    $wp_customize->add_setting(
        'header_backgroundcolor',
        array(
            'default'           => themesflat_customize_default('header_backgroundcolor'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( new WP_Customize_Color_Control(
            $wp_customize,
            'header_backgroundcolor',
            array(
                'label'         => esc_html__('Header Background', 'suri-elementor'),
                'section'       => 'color_header',
                'priority'      => 5
            )
        )
    );   

    // Menu a color
    $wp_customize->add_setting(
        'mainnav_color',
        array(
            'default'           => themesflat_customize_default('mainnav_color'),
            'sanitize_callback' => 'esc_attr'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'mainnav_color',
            array(
                'label' => esc_html('Mainnav a color', 'suri-elementor'),
                'section' => 'color_header',
                'priority' => 6
            )
        )
    );

    // Menu a:hover color
    $wp_customize->add_setting(
        'mainnav_hover_color',
        array(
            'default'           => themesflat_customize_default('mainnav_hover_color'),
            'sanitize_callback' => 'esc_attr'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'mainnav_hover_color',
            array(
                'label' => esc_html('Mainnav a:hover color & active', 'suri-elementor'),
                'section' => 'color_header',
                'priority' => 7
            )
        )
    );

    // Sub menu a color
    $wp_customize->add_setting(
        'sub_nav_color',
        array(
            'default'           => themesflat_customize_default('sub_nav_color'),
            'sanitize_callback' => 'esc_attr'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'sub_nav_color',
            array(
                'label' => esc_html('Sub nav a color', 'suri-elementor'),
                'section' => 'color_header',
                'priority' => 8
            )
        )
    );

    // Sub nav background
    $wp_customize->add_setting(
        'sub_nav_background',
        array(
            'default'           => themesflat_customize_default('sub_nav_background'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'sub_nav_background',
            array(
                'label' => esc_html('Sub nav background color', 'suri-elementor'),
                'section' => 'color_header',
                'priority' => 9
            )
        )
    );

    // Border color li sub nav
    $wp_customize->add_setting(
        'border_color_sub_nav',
        array(
            'default'           => themesflat_customize_default('border_color_sub_nav'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'border_color_sub_nav',
            array(
                'label' => esc_html('Border color sub nav', 'suri-elementor'),
                'section' => 'color_header',
                'priority' => 10
            )
        )
    );

    // Sub nav background hover
    $wp_customize->add_setting(
        'sub_nav_color_hover',
        array(
            'default'   => themesflat_customize_default('sub_nav_color_hover'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'sub_nav_color_hover',
            array(
                'label' => esc_html('Sub-menu color hover', 'suri-elementor'),
                'section' => 'color_header',
                'priority' => 11
            )
        )
    );

    // Sub nav background hover
    $wp_customize->add_setting(
        'sub_nav_background_hover',
        array(
            'default'   => themesflat_customize_default('sub_nav_background_hover'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'sub_nav_background_hover',
            array(
                'label' => esc_html('Sub-menu background color hover', 'suri-elementor'),
                'section' => 'color_header',
                'priority' => 11
            )
        )
    );

    // ADD SECTION COLOR FOOTER
    $wp_customize->add_section('color_footer',array(
        'title'=>'Footer',
        'priority'=>12,
        'panel'=>'color_panel',
    ));    

    // Footer background color    
    $wp_customize->add_setting(
        'footer_background_color',
        array(
            'default'           => themesflat_customize_default('footer_background_color'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( 
        new WP_Customize_Color_Control(
            $wp_customize,
            'footer_background_color',
            array(
                'label'         => esc_html__('Footer Backgound', 'suri-elementor'),
                'section'       => 'color_footer',
                'settings'      => 'footer_background_color',
                'priority'      => 12
            )
        )
    );

    // Footer text color
    $wp_customize->add_setting(
        'footer_text_color',
        array(
            'default'           => themesflat_customize_default('footer_text_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'footer_text_color',
            array(
                'label'         => esc_html('Footer Text Color', 'suri-elementor'),
                'section'       => 'color_footer',
                'settings'      => 'footer_text_color',
                'priority'      => 13
            )
        )
    );

    // bottom background color
    $wp_customize->add_setting(
        'bottom_background_color',
        array(
            'default'           => themesflat_customize_default('bottom_background_color'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'bottom_background_color',
            array(
                'label'         => esc_html('Bottom Backgound', 'suri-elementor'),
                'section'       => 'color_footer',
                'settings'      => 'bottom_background_color',
                'priority'      => 14
            )
        )
    );

    // Bottom text color
    $wp_customize->add_setting(
        'bottom_text_color',
        array(
            'default'           => themesflat_customize_default('bottom_text_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'bottom_text_color',
            array(
                'label'         => esc_html('Bottom Text Color', 'suri-elementor'),
                'section'       => 'color_footer',
                'settings'      => 'bottom_text_color',
                'priority'      => 15
            )
        )
    ); 

    //___Footer___//
    $wp_customize->add_section(
        'flat_footer',
        array(
            'title'         => esc_html('Footer', 'suri-elementor'),
            'priority'      => 4,
        )
    );

    // Footer column
    $wp_customize->add_setting(
        'footer_widget_areas',
        array(
            'default'           => '3',
            'sanitize_callback' => 'themesflat_sanitize_fw',
        )
    );
    $wp_customize->add_control(
        'footer_widget_areas',
        array(
            'type'      => 'select',
            'label'     => esc_html__('Widgets Layout', 'suri-elementor'),
            'section'   => 'flat_footer',
            'priority'  => 12,
            'choices'   => array(
                '4'           => esc_html__( '4 Columns', 'suri-elementor' ),
                '3'           => esc_html__( '3 Columns', 'suri-elementor' ),
                '1'           => esc_html__( '1 Column', 'suri-elementor' ),
                '2'           => esc_html__( '2 Columns', 'suri-elementor' ),
            ),
        )
    );

    // Footer Box control
    $wp_customize->add_setting(
        'footer_controls',
        array(
            'default' => themesflat_customize_default('footer_controls'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control( new themesflat_BoxControls($wp_customize,
        'footer_controls',
        array(
            'label' => esc_html( 'Footer Box Controls (px)', 'suri-elementor' ),
            'section' => 'flat_footer',
            'type' => 'box-controls',
            'priority' => 18
        ))
    ); 

    // Footer Copyright
    $wp_customize->add_setting(
        'footer_copyright',
        array(
            'default' => themesflat_customize_default('footer_copyright'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control(
        'footer_copyright',
        array(
            'label' => esc_html( 'Copyright', 'suri-elementor' ),
            'section' => 'flat_footer',
            'type' => 'textarea',
            'priority' => 19
        )
    );

    //__Page title and breadcrumb__//
    $wp_customize->add_panel('page_title_panel',array(
        'title'         => esc_html__('Page Title & Breadcrumb','suri-elementor'),
        'description'   => 'This is panel Description',
        'priority'      => 10,
    ));

    // ADD SECTION PAGE TITLE
    $wp_customize->add_section('page_title_style',array(
        'title'         => esc_html__('Page Title Style','suri-elementor'),
        'priority'      => 10,
        'panel'         => 'page_title_panel',
    ));

   // Heading Page Title
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'page_title_style', array(
        'label' => esc_html__('Page Title Style', 'suri-elementor'),
        'section' => 'page_title_style',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );  

    // Show page title
    $wp_customize->add_setting(
      'show_page_title',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('show_page_title'),     
        )   
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'show_page_title',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Show page title', 'suri-elementor'),
            'section' => 'page_title_style',
            'priority' => 2,
        ))
    ); 

    $wp_customize->add_setting(
        'page_title_styles',
        array(
            'default'           => themesflat_customize_default('page_title_styles'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( new themesflat_RadioImages($wp_customize,
        'page_title_styles',
        array(
            'type'      => 'radio-images',           
            'section'   => 'page_title_style',
            'priority'  => 3,
            'label'         => esc_html('Page Title Style', 'suri-elementor'),
            'choices'   => array (
                'pagetitle_style_1'=> array (
                   'tooltip'   => esc_html('Page Title Style1','suri-elementor'),
                   'src'       => THEMESFLAT_LINK . 'images/controls/breadcrumbs_01.jpg'
                ) ,
                'pagetitle_style_2'=>  array (
                   'tooltip'   => esc_html('Page Title Style2','suri-elementor'),
                   'src'       => THEMESFLAT_LINK . 'images/controls/breadcrumbs_02.jpg'
                ) ,
                'pagetitle_style_3'=>  array (
                   'tooltip'   => esc_html('Page Title Style3','suri-elementor'),
                    'src'      => THEMESFLAT_LINK . 'images/controls/breadcrumbs_03.jpg'
                ),        
            ),
        ))
    ); 

    // Box control
    $wp_customize->add_setting(
        'page_title_controls',
        array(
            'default' => themesflat_customize_default('page_title_controls'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control( new themesflat_BoxControls($wp_customize,
        'page_title_controls',
        array(
            'label' => esc_html( 'Page Title Controls (px)', 'suri-elementor' ),
            'section' => 'page_title_style',
            'type' => 'box-controls',
            'priority' => 4
        ))
    );  

    // Page Title Color
    $wp_customize->add_setting(
        'page_title_text_color',
        array(
            'default'           => themesflat_customize_default('page_title_text_color'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'page_title_text_color',
            array(
                'label'         => esc_html('Page Heading Text Color', 'suri-elementor'),
                'section'       => 'page_title_style',
                'priority'      => 5
            )
        )
    );

    // Page Title Link Color
    $wp_customize->add_setting(
        'page_title_link_color',
        array(
            'default'           => themesflat_customize_default('page_title_link_color'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'page_title_link_color',
            array(
                'label'         => esc_html('Breadcrumb Text Color', 'suri-elementor'),
                'section'       => 'page_title_style',
                'priority'      => 8
            )
        )
    );

    // Page Title Overlay
    $wp_customize->add_setting(
        'page_title_background_color',
        array(
            'default'           => themesflat_customize_default('page_title_background_color'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( new themesflat_ColorOverlay(
            $wp_customize,
            'page_title_background_color',
            array(
                'label'         => esc_html__('Page Title Background Color', 'suri-elementor'),
                'section'       => 'page_title_style',
                'priority'      => 9
            )
        )
    );

     //Page Title Background Image
    $wp_customize->add_setting(
        'page_title_background_image',
        array(
            'default' => themesflat_customize_default('page_title_background_image'),
            'sanitize_callback' => 'esc_url_raw',
        )
    );    
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'page_title_background_image',
            array(
               'label'          => esc_html__( 'Upload Your Page Title Background Image', 'suri-elementor' ),
               'type'           => 'image',
               'section'        => 'page_title_style',
               'priority'       => 9,
            )
        )
    );

    // ADD SECTION BREADCRUMB
    $wp_customize->add_section('page_break_crumb_section',array(
        'title'         => esc_html__('Page Breadcrumb','suri-elementor'),
        'priority'      => 10,
        'panel'         => 'page_title_panel',
    ));

   // Breadcrumb section
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'page_break_crumb_section', array(
        'label' => esc_html__('Page Breadcrumb', 'suri-elementor'),
        'section' => 'page_break_crumb_section',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );  

    // Breadcrumb
    $wp_customize->add_setting(
      'breadcrumb_enabled',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('breadcrumb_enabled'),     
        )   
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'breadcrumb_enabled',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Enable Breadcrumb', 'suri-elementor'),
            'section' => 'page_break_crumb_section',
            'priority' => 14,
        ))
    );    

    $wp_customize->add_setting (
        'bread_crumb_prefix',
        array(
            'default' => themesflat_customize_default('bread_crumb_prefix') ,
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'bread_crumb_prefix',
        array(
            'type'      => 'text',
            'label'     => esc_html('Breadcrumb Prefix', 'suri-elementor'),
            'section'   => 'page_break_crumb_section',
            'priority'  => 15
        )
    );

    $wp_customize->add_setting (
        'breadcrumb_separator',
        array(
            'default' => themesflat_customize_default('breadcrumb_separator'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'breadcrumb_separator',
        array(
            'type'      => 'text',
            'label'     => esc_html('Breadcrumb Separator', 'suri-elementor'),
            'section'   => 'page_break_crumb_section',
            'priority'  => 16
        )
    );  

}
add_action( 'customize_register', 'themesflat_customize_register' );

// Text
function themesflat_sanitize_text( $input ) {
    return wp_kses_post( $input );
}

// Background size
function themesflat_sanitize_bg_size( $input ) {
    $valid = array(
        'cover'     => esc_html('Cover', 'suri-elementor'),
        'contain'   => esc_html('Contain', 'suri-elementor'),
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Blog Layout
function themesflat_sanitize_blog( $input ) {
    $valid = array(
        'sidebar-right'    => esc_html( 'Sidebar right', 'suri-elementor' ),
        'sidebar-left'    => esc_html( 'Sidebar left', 'suri-elementor' ),
        'fullwidth'  => esc_html( 'Full width (no sidebar)', 'suri-elementor' )

    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// themesflat_sanitize_pagination
function themesflat_sanitize_pagination ( $input ) {
    $valid = array(
        'pager' => esc_html__('Pager', 'suri-elementor'),
        'numeric' => esc_html__('Numeric', 'suri-elementor'),
        'page_numeric' => esc_html__('Pager & Numeric', 'suri-elementor')                
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// themesflat_sanitize_pagination
function themesflat_sanitize_layout_version ( $input ) {
    $valid = array(
        'boxed' => esc_html__('Boxed', 'suri-elementor'),
        'wide' => esc_html__('Wide', 'suri-elementor')          
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// themesflat_sanitize_related_post
function themesflat_sanitize_related_post ( $input ) {
    $valid = array(
        'simple_list' => esc_html__('Simple List', 'suri-elementor'),
        'grid' => esc_html__('Grid', 'suri-elementor')        
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Footer widget areas
function themesflat_sanitize_fw( $input ) {
    $valid = array(
        '0' => esc_html__('footer_default', 'suri-elementor'),
        '1' => esc_html__('One', 'suri-elementor'),
        '2' => esc_html__('Two', 'suri-elementor'),
        '3' => esc_html__('Three', 'suri-elementor'),
        '4' => esc_html__('Four', 'suri-elementor')
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Header style sanitize
function themesflat_sanitize_headerstyle( $input ) {
    $valid = themesflat_predefined_header_styles();
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Checkboxes
function themesflat_sanitize_checkbox( $input ) {
    if ( $input == 1 ) {
        return 1;
    } else {
        return '';
    }
}

// Themesflat_sanitize_related_portfolio
function themesflat_sanitize_related_portfolio( $input ) {
    $valid = array(
        'grid'                 => esc_html( 'Grid', 'suri-elementor' ),
        'grid_masonry'         => esc_html( 'Grid Masonry', 'suri-elementor' ),
        'grid_nomargin'        => esc_html( 'Grid Masonry No Margin', 'suri-elementor' ),
        'carosuel'             => esc_html( 'Carosuel', 'suri-elementor' ),
        'carosuel_nomargin'    => esc_html( 'Carosuel No Margin', 'suri-elementor' )       
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Themesflat_sanitize_portfolio_pagination
function themesflat_sanitize_portfolio_pagination( $input ) {
    $valid = array(
        'page_numeric'         => esc_html( 'Pager & Numeric', 'suri-elementor' ),
        'load_more'         => esc_html( 'Load More', 'suri-elementor' )     
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Themesflat_sanitize_portfolio_order
function themesflat_sanitize_portfolio_order( $input ) {
    $valid = array(
        'date'          => esc_html( 'Date', 'suri-elementor' ),
        'id'            => esc_html( 'Id', 'suri-elementor' ),
        'author'        => esc_html( 'Author', 'suri-elementor' ),
        'title'         => esc_html( 'Title', 'suri-elementor' ),
        'modified'      => esc_html( 'Modified', 'suri-elementor' ),
        'comment_count' => esc_html( 'Comment Count', 'suri-elementor' ),
        'menu_order'    => esc_html( 'Menu Order', 'suri-elementor' )     
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Themesflat_sanitize_portfolio_order_direction
function themesflat_sanitize_portfolio_order_direction( $input ) {
    $valid = array(
        'DESC' => esc_html( 'Descending', 'suri-elementor' ),
        'ASC'  => esc_html( 'Assending', 'suri-elementor' )       
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Themesflat_sanitize_grid_portfolio
function themesflat_sanitize_grid_portfolio( $input ) {
    $valid = array(
        'portfolio-two-columns'     => esc_html( '2 Columns', 'suri-elementor' ),
        'portfolio-three-columns'     => esc_html( '3 Columns', 'suri-elementor' ),
        'portfolio-four-columns'     => esc_html( '4 Columns', 'suri-elementor' ),
        'portfolio-five-columns'     => esc_html( '5 Columns', 'suri-elementor' )
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// themesflat_sanitize_grid_portfolio_related
function themesflat_sanitize_grid_portfolio_related( $input ) {
    $valid = array(
        'portfolio-one-columns'     => esc_html( '1 Columns', 'suri-elementor' ),
        'portfolio-two-columns'     => esc_html( '2 Columns', 'suri-elementor' ),
        'portfolio-three-columns'     => esc_html( '3 Columns', 'suri-elementor' ),
        'portfolio-four-columns'     => esc_html( '4 Columns', 'suri-elementor' ),
        'portfolio-five-columns'     => esc_html( '5 Columns', 'suri-elementor' )
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Themesflat_sanitize_grid_post_related
function themesflat_sanitize_grid_post_related( $input ) {
    $valid = array(        
        2     => esc_html( '2 Columns', 'suri-elementor' ),
        3   => esc_html( '3 Columns', 'suri-elementor' ),
        4    => esc_html( '4 Columns', 'suri-elementor' ), 
        5    => esc_html( '5 Columns', 'suri-elementor' ),       
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// themesflat_sanitize_layout_product
function themesflat_sanitize_layout_product( $input ) {
    $valid = array(        
        'fullwidth'         => esc_html( 'No Sidebar', 'suri-elementor' ),
        'sidebar-right'           => esc_html( 'Sidebar Right', 'suri-elementor' ),
        'sidebar-left'         => esc_html( 'Sidebar Left', 'suri-elementor' )
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

