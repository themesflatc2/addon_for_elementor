<?php
/**
 * @package evockans
 */
//Output all custom styles for this theme

function themesflat_custom_styles( $custom ) {
	$custom = '';
	$logo_position = themesflat_decode(themesflat_get_opt('logo_controls'));
	themesflat_render_box_position(".logo",$logo_position);

	$logo_width = themesflat_get_opt('logo_width');

	// Logo Height
	if ( $logo_width !='' ) {
		$custom .= ".logo img, .logo svg { width:" . $logo_width . "px; }"."\n";
	}
    $footer_controls = themesflat_decode(themesflat_choose_opt('footer_controls'));
    themesflat_render_box_position(".footer",$footer_controls);

	$page_title_controls = themesflat_decode(themesflat_choose_opt('page_title_controls'));
    themesflat_render_box_position(".page-title",$page_title_controls);

    //  Page Title Opacity
	$page_title_background_color = themesflat_choose_opt( 'page_title_background_color');
	if ( $page_title_background_color !='' ) {
		$custom .= ".page-title .overlay { background:" . esc_attr($page_title_background_color) . ";}"."\n";
	}

	$page_title_img = themesflat_get_opt('page_title_background_image');
    if ( $page_title_img !='' ) {
	    $custom .= '.page-title .overlay-image {background: url('.$page_title_img.') center no-repeat;}'."\n";  
	}
	
	$custom .= ".page-title h1 {color:" . themesflat_choose_opt('page_title_text_color') . ";}"."\n";

	$custom .= ".breadcrumbs span, .breadcrumbs span a, .breadcrumbs a, .breadcrumbs span i, .breadcrumbs span.trail-browse i {color:" . themesflat_choose_opt('page_title_link_color') . ";}"."\n"; 

	$font = themesflat_get_json('body_font_name');

	$font_style = themesflat_font_style($font['style']);

	$body_fonts = $font['family'];

	$body_line_height = $font['line_height'];

	$body_font_weight = $font_style[0];

	$body_font_style = $font_style[1];

	$body_size = $font['size'];		

	$headings_fonts_ = themesflat_get_json('headings_font_name');

	$headings_fonts_family = $headings_fonts_['family'];	

	$headings_style = themesflat_font_style( $headings_fonts_['style'] );

	$headings_font_weight = $headings_style[0];

	$headings_font_style = $headings_style[1];

	$menu_fonts_ = themesflat_get_json('menu_font_name');

	$menu_fonts_family = $menu_fonts_['family'];

	$menu_fonts_size = $menu_fonts_['size'];

	$menu_line_height = $menu_fonts_['line_height'];

	$menu_style = themesflat_font_style( $menu_fonts_['style'] );

	$menu_font_weight = $menu_style[0];

	$menu_font_style = $menu_style[1];	

	// Body font family
	if ( $body_fonts !='' ) {
		$custom .= "body,button,input,select,textarea { font-family:'" . $body_fonts . "';}"."\n";
	}

	// Body font weight
	if ( $body_font_weight !='' ) {
		$custom .= "body,button,input,select,textarea { font-weight:" . $body_font_weight . ";}"."\n";
	}

	// Body font style
	if ( isset( $body_font_style ) ) {
        $custom .= "body,button,input,select,textarea { font-style:" . $body_font_style . "; }"."\n";        
	}

    // Body font size
    if ( $body_size !=''  ) {
        $custom .= "body,button,input,select,textarea { font-size:" . intval( $body_size ) . "px; }"."\n";    
    }

    // Body line height
    if ( $body_line_height != '' ) {
        $custom .= "body,button,input,select,textarea { line-height:" . intval( $body_line_height ) . "px ; }"."\n";    
    }

	// Headings font family
	if ( $headings_fonts_family !='' ) {
		$custom .= "h1,h2,h3,h4,h5,h6 { font-family:" . $headings_fonts_family . ";}"."\n";

	}

	//Headings font weight
	if ( $headings_font_weight !='' ) {
		$custom .= "h1,h2,h3,h4,h5,h6 { font-weight:" . $headings_font_weight . ";}"."\n";
	}

	// Headings font style
	if ( isset( $headings_font_style )) {
        $custom .= "h1,h2,h3,h4,h5,h6  { font-style:" . $headings_font_style . "; }"."\n";
	}

	// Menu font family
	if ( $menu_fonts_family != '') {
		$custom .= "#mainnav ul.menu > li > a, #mainnav ul.sub-menu > li > a { font-family:" . $menu_fonts_family . ";}"."\n";
	}

	// Menu font weight
	if ( $menu_font_weight != '' ) {
		$custom .= "#mainnav ul.menu > li > a, #mainnav ul.sub-menu > li > a { font-weight:" . $menu_font_weight . ";}"."\n";
	}

	// Menu font style
	if ( isset( $menu_font_style )) {
        $custom .= "#mainnav ul.menu > li > a, #mainnav ul.sub-menu > li > a  { font-style:" . $menu_font_style . "; }"."\n";   
	}

    // Menu font size
    if ( $menu_fonts_size != '' ) {
        $custom .= "#mainnav ul li a, #mainnav ul.sub-menu > li > a { font-size:" . intval($menu_fonts_size) . "px;}"."\n";
    }

    // Menu line height
    if ( $menu_line_height != '' ) {
        $custom .= "#mainnav ul.menu > li > a { line-height:" . intval($menu_line_height) . "px;}"."\n";
    }  

	// H1 font size
	if ( $h1_size = themesflat_get_opt( 'h1_size' ) ) {
		$custom .= "h1 { font-size:" . intval($h1_size) . "px; }"."\n";
	}

    // H2 font size
    if ( $h2_size = themesflat_get_opt( 'h2_size' ) ) {
        $custom .= "h2 { font-size:" . intval($h2_size) . "px; }"."\n";
    }

    // H3 font size
    if ( $h3_size = themesflat_get_opt( 'h3_size' ) ) {
        $custom .= "h3 { font-size:" . intval($h3_size) . "px; }"."\n";
    }

    // H4 font size
    if ( $h4_size = themesflat_get_opt( 'h4_size' ) ) {
        $custom .= "h4 { font-size:" . intval($h4_size) . "px; }"."\n";
    }

    // H5 font size
    if ( $h5_size = themesflat_get_opt( 'h5_size' ) ) {
        $custom .= "h5 { font-size:" . intval($h5_size) . "px; }"."\n";
    }

    // H6 font size
    if ( $h6_size = themesflat_get_opt( 'h6_size' ) ) {
        $custom .= "h6 { font-size:" . intval($h6_size) . "px; }"."\n";
    }   

    // Body color
	$body_text = themesflat_get_opt( 'body_text_color' );

	if ($body_text !='') {
		
		$custom .= "body { color:" . esc_attr($body_text) . "}"."\n";
	}

	// background bodycolor
	$custom .= ".page-links a:hover, .page-links a:focus, .page-links > span { border-color:" . esc_attr($body_text) . "}"."\n";

    if ( themesflat_get_opt ('top_background_color') !='' ) {
		$custom .= ".themesflat-top { background-color:" . esc_attr(themesflat_get_opt ('top_background_color')) ." ; } "."\n";
    }

    // background bodycolor
    if ( themesflat_choose_opt ('body_background_color') !='' ) {
		$custom .= "body, .page-wrap, .boxed .themesflat-boxed { background-color:" . esc_attr(themesflat_choose_opt ('body_background_color')) ." ; } "."\n";
    }	

    //Top text color
    $top_text_color = themesflat_get_opt( 'topbar_textcolor' );
    if ( themesflat_get_opt( 'topbar_textcolor' ) !='' ) {
	    $border_topbar_color = themesflat_hex2rgba($top_text_color,0.2);
    	$custom .= ".themesflat-top, .themesflat-top .content-left ul > li, .themesflat-top ul.themesflat-socials > li, .themesflat-top ul.themesflat-socials > li:last-child { border-color: ".esc_attr($border_topbar_color).";}";

		$custom .= ".themesflat-top, .info-top-right, .themesflat-top a  { color:" . esc_attr( themesflat_get_opt( 'topbar_textcolor' ) ) ." ;} "."\n";
    }	  

    // Header Background
	$header_backgroundcolor = themesflat_choose_opt( 'header_backgroundcolor');
	if ( $header_backgroundcolor !='' ) {		
		$custom .= ".site-header { background-color:" . esc_attr( $header_backgroundcolor ) . ";}"."\n";
	} 	

	// Menu mainnav a color
	$mainnav_color = themesflat_choose_opt( 'mainnav_color');
	if ( $mainnav_color !='' ) {
		$custom .= "#mainnav ul.menu > li > a, .btn-menu:before, .btn-menu:after, .btn-menu span { color:" . esc_attr( $mainnav_color ) . ";}"."\n";
		$custom .= ".btn-menu:before, .btn-menu:after, .btn-menu span { background:" . esc_attr( $mainnav_color ) . ";}"."\n";
	}

	// mainnav_hover_color
	$mainnav_hover_color = themesflat_choose_opt( 'mainnav_hover_color');

	if ( $mainnav_hover_color !='' ) {
		$custom .= "#mainnav ul.menu > li > a:hover,#mainnav ul.menu > li.current-menu-item > a, #mainnav ul.menu > li.current-menu-ancestor > a { color:" . esc_attr( $mainnav_hover_color ) . ";}"."\n";
	}

	//Subnav a color
	$sub_nav_color = themesflat_get_opt( 'sub_nav_color');
	if ( $sub_nav_color !='' ) {
		$custom .= "#mainnav ul.sub-menu > li > a, #mainnav li.megamenu > ul.sub-menu > .menu-item-has-children > a { color:" . esc_attr( $sub_nav_color ) . ";}"."\n";
	}

	//Subnav background color
	$sub_nav_background = themesflat_get_opt( 'sub_nav_background');
	if ( $sub_nav_background !='' ) {
		$custom .= "#mainnav ul.sub-menu { background-color:" . esc_attr( $sub_nav_background ) . ";}"."\n";			
	}

	//sub_nav_color_hover
	$sub_nav_color_hover = themesflat_choose_opt( 'sub_nav_color_hover');
	if ( $sub_nav_color_hover !='' ) {
		$custom .= "#mainnav ul.sub-menu > li > a:hover, #mainnav ul.sub-menu > li.current-menu-item > a, #mainnav-mobi ul li.current-menu-item > a, #mainnav-mobi ul li.current-menu-ancestor > a, #mainnav ul.sub-menu > li.current-menu-ancestor > a, #mainnav-mobi ul li .current-menu-item > a, #mainnav-mobi ul li.current-menu-item .btn-submenu:before, #mainnav-mobi ul li .current-menu-item .btn-submenu:before { color:" . esc_attr( $sub_nav_color_hover ) . ";}"."\n";
	}

	//sub_nav_background_hover
	$sub_nav_background_hover = themesflat_get_opt( 'sub_nav_background_hover');
	if ( $sub_nav_background_hover !='' ) {
		$custom .= "#mainnav ul.sub-menu > li > a:hover, #mainnav ul.sub-menu > li.current-menu-item > a { background-color:" . esc_attr($sub_nav_background_hover) . ";}"."\n";
	}

	//border color sub nav
	$border_color_sub_nav = themesflat_get_opt( 'border_color_sub_nav');
	if ( $border_color_sub_nav !='' ) {
		$custom .= "#mainnav ul.sub-menu > li { border-color:" . esc_attr($border_color_sub_nav) . "!important;}"."\n";
	}

	$footer_background_color = themesflat_choose_opt( 'footer_background_color');
	if ( $footer_background_color !='' ) {
		$custom .= ".footer { background-color:" . esc_attr($footer_background_color) . ";}"."\n";
	}

	$footer_background_image = themesflat_choose_opt('footer_background_image');
    if ( $footer_background_image !='' ) {
	    $custom .= '.footer_background {background: url('.$footer_background_image.') center /cover no-repeat;}'."\n";  
	}

	// Footer simple text color
	$footer_text_color = themesflat_choose_opt( 'footer_text_color');
	if ( $footer_text_color !='' ) {
		$custom .= ".footer a, .footer, .footer-widgets .widget .widget-title { color:" . esc_attr($footer_text_color) . ";}"."\n";
	}

	$footer_text_color_opacity = themesflat_hex2rgba($footer_text_color,1);
	if ( $footer_text_color !='' ) {
		$custom .= ".footer .widget.widget_recent_entries ul li .post-date, .footer .widget.widget_latest_news ul li .post-date, .footer .widget.widget_text .textwidget p, .footer-widgets .widget.widget_text ul.contact-info li span { color:" . esc_attr($footer_text_color_opacity) . ";}"."\n";
	}

	// bottom_background_color
	$bottom_background_color = themesflat_choose_opt( 'bottom_background_color');
	if ( $bottom_background_color !='' ) {
		$custom .= ".bottom { background-color:" . esc_attr( $bottom_background_color ) . ";}"."\n";
	}

	// Bottom text color
	$bottom_text_color = themesflat_choose_opt( 'bottom_text_color');

	if ( $bottom_text_color !='' ) {
		$custom .= ".bottom .copyright, .bottom .copyright a, .bottom #menu-bottom li a, .bottom  #menu-bottom-menu li a, .bottom ul li a { color:" . esc_attr( $bottom_text_color ) . ";}"."\n";
		$custom .= ".bottom  #menu-bottom-menu li a:before { background-color:" . esc_attr( $bottom_text_color ) . ";}"."\n";
	}

	// Primary color
    $primary_color = themesflat_choose_opt( 'primary_color' );
    $links_color = get_theme_mod('links_color');

    if ( $primary_color !='' ) {
    	//color
    	$custom .= "a:hover, .bottom .copyright a:hover, .breadcrumbs a:hover { color:" . esc_attr($primary_color) . ";}"."\n";

    	// Background color
		$custom .= 'button, input[type="button"], input[type="reset"], input[type="submit"] { background:' . esc_attr($primary_color) . "; }"."\n";		

		// border color 
		$custom .= 'textarea:focus, select:focus, input[type=\"text\"]:focus, input[type=\"password\"]:focus, input[type=\"datetime\"]:focus, input[type=\"datetime-local\"]:focus, input[type=\"date\"]:focus, input[type=\"month\"]:focus, input[type=\"time\"]:focus, input[type=\"week\"]:focus, input[type=\"number\"]:focus, input[type=\"email\"]:focus, input[type=\"url\"]:focus, input[type=\"search\"]:focus, input[type=\"tel\"]:focus, input[type=\"color\"]:focus { border-color:' . esc_attr($primary_color) . " }"."\n";
    }

	$custom = apply_filters('themesflat/render/style',$custom);
	wp_add_inline_style( 'inline-css', $custom );

}

add_action( 'wp_enqueue_scripts', 'themesflat_custom_styles' );