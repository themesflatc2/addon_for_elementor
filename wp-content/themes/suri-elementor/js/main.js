/**
* isMobile
* headerFixed
* headerAbsolute
* responsiveMenu
* headerLeft
* headerStickyLogo
* themesflatSearch
* detectViewport
* blogLoadMore
* commingsoon
* goTop
* retinaLogos
* customizable_carousel
* parallax
* iziModal
* bg_particles
* pagetitleVideo
* removePreloader
*/

;(function($) {

    "use strict";

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };  

    var responsiveMenu = function() {
        var menuType = 'desktop';

        $(window).on('load resize', function() {
            var currMenuType = 'desktop';

            if ( matchMedia( 'only screen and (max-width: 991px)' ).matches ) {                
                currMenuType = 'mobile';                              
            }

            if ( currMenuType !== menuType ) {
                menuType = currMenuType;

                if ( currMenuType === 'mobile' ) {
                    var $mobileMenu = $('#mainnav').attr('id', 'mainnav-mobi').hide();
                    var hasChildMenu = $('#mainnav-mobi').find('li:has(ul)');

                    $('.site-header .site-navigation').after($mobileMenu);
                    hasChildMenu.children('ul').hide();
                    if (hasChildMenu.find(">span").length == 0) {
                        hasChildMenu.children('a').after('<span class="btn-submenu"></span>');
                    }
                    $('.btn-menu').removeClass('active');
                } else {
                    var $desktopMenu = $('#mainnav-mobi').attr('id', 'mainnav').removeAttr('style');
                    $desktopMenu.find('.sub-menu').removeAttr('style');
                    $('.site-header').find('.site-navigation').append($desktopMenu);
              
                }
            }
        });

        $('.btn-menu').on('click', function() {  
            var header = $(".site-header");
            var offset = 0;
            if (typeof(header.data('offset')) != 'undefined') {
                offset = header.data('offset');
            }
        
            var $top = $(window).scrollTop() + header.height() + header.position().top + offset;
            $('#mainnav-mobi').slideToggle(300);
            $(this).toggleClass('active');
        });

        $(document).on('click', '#mainnav-mobi li .btn-submenu', function(e) {
            $(this).toggleClass('active').next('ul').slideToggle(300);
            e.stopImmediatePropagation();
        });
    };   

    var retinaLogos = function() {
        var retina = window.devicePixelRatio > 1 ? true : false;
        var top_height = $('.site-header').height();
        var img_width =  $('.logo .site-logo').data('width');  
        var img_retina = $('.logo .site-logo').data('retina');
        if( retina ) {
            //console.log('http://localhost/eslider/wp-content/uploads/2020/02/slide.jpg');
            $('#logo').find('img').attr({src:img_retina,width:img_width,height:'auto'});           
        }
    };


// Dom Ready
$(function() {
    responsiveMenu();
    retinaLogos(); 
});
})(jQuery);