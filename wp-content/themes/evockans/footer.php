<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package evockans
 */
?>

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- #content -->
    
    <?php if (!is_404()): ?>
        <?php 
            $show_action_box = themesflat_choose_opt('show_action_box');
            $show_action_box_array = array('1', '0');
            if( !in_array($show_action_box, $show_action_box_array) ) {
                $show_action_box = themesflat_get_opt('show_action_box');
            }
            if( $show_action_box == 1 ): 
        ?>
        <div class="action-box themesflat-action-box">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner">
                            <div class="heading-wrap">
                                <?php echo wp_kses_post( themesflat_get_opt('text_action_box') ); ?>                            
                            </div>                
                            <div class="button-wrap">
                                <a href="#" class="themesflat-button"><?php echo wp_kses_post( themesflat_get_opt('text_button_action_box') ); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    <?php endif; ?>


    <!-- Start Footer -->   
    <?php 
    $footer_style = themesflat_choose_opt('footer_style');
    $footer_style_array = array(1, 2, 3, 4);
    if( !in_array($footer_style, $footer_style_array) ) {
        $footer_style = themesflat_get_opt('footer_style');
    }

    switch ( $footer_style ):
        case 1:
            get_template_part( 'tpl/footer/footer-style1');
            break;
        case 2:
            get_template_part( 'tpl/footer/footer-style2');
            break;
        case 3:
            get_template_part( 'tpl/footer/footer-style3');
            break;
        case 4:
            get_template_part( 'tpl/footer/footer-style4');
            break;
    endswitch;
    ?> 
    <!-- End Footer --> 
    <?php if ( themesflat_get_opt( 'go_top') == 1 ) : ?>
        <!-- Go Top -->
        <a class="go-top">
            <i class="fas fa-chevron-up"></i>
        </a>
    <?php endif; ?> 
</div><!-- /#boxed -->
<?php wp_footer(); ?>
</body>
</html>