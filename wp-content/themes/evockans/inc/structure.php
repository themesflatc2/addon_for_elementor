<?php
if ( ! function_exists( 'themesflat_body_classes' ) ) {
	add_filter( 'body_class', 'themesflat_body_classes' );

	function themesflat_body_classes( $classes ) {	
		$custom_page_class = themesflat_meta('custom_page_class');

		$classes[] = $custom_page_class;

		if ( themesflat_meta('enable_custom_topbar') == 1 ) {
			if (themesflat_meta( 'topbar_show' ) == 1 )	{	
				$classes[] = 'has-topbar';
			}			
		}
		else {
			if ( themesflat_get_opt( 'topbar_show') == 1 )
				$classes[] = 'has-topbar';		
		}

		if ( themesflat_get_opt('header_sticky') == 1 ) {	
			$classes[] = 'header_sticky';
		}

		/**
		 * Portfolio template
		 */
		if ( is_page_template( 'tpl/portfolio.php' ) ) $classes[] = 'page-portfolio';

		/**
		 * Full-Width layout template
		 */
		if ( is_page_template( 'tpl/page_nosidebar.php' ) ) $classes[] = 'page-nosidebar';
		$classes[] =  themesflat_get_opt('layout_version');

		if ( is_404() ) $classes[] = themesflat_get_opt('page_404_style');

		/**
		 * Full Width Sidebar Position
		 */
		$page_layout = themesflat_choose_opt('page_layout');
	    $page_layout_array = array('sidebar-right', 'sidebar-left', 'fullwidth', 'fullwidth-small', 'fullwidth-center');
	    if( !in_array($page_layout, $page_layout_array) ) {
	        $page_layout = themesflat_get_opt('page_layout');
	    }
		$classes[] = $page_layout;

		/**
		 * Bottom style
		 */
		$classes [] = themesflat_get_opt('bottom_style');

		/**
		 * Header Absolute
		 */
		$header_absolute = themesflat_choose_opt('header_absolute');
		$header_absolute_array = array('1', '0');
		if( !in_array($header_absolute, $header_absolute_array) ) {
		    $header_absolute = themesflat_get_opt('header_absolute');
		}
		if ( $header_absolute == 1 ) {
			$classes[] = 'header-absolute';
		}

		$header_style = themesflat_choose_opt('style_header');
		$header_style_array = array('header-style1', 'header-style2', 'header-style3', 'header-style4');
		if( !in_array($header_style, $header_style_array) ) {
		    $header_style = themesflat_get_opt('style_header');
		}
		if ( $header_style == 'header-style4' ) {	
			$classes[] = 'header-left';
		}

		/**
		 * Topbar Absolute
		 */
		if ( themesflat_get_opt('topbar_absolute')==1 ) {
			$classes[] = 'topbar-absolute';
		}

		return $classes;
	}
}