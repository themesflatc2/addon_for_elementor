<?php 
if ( ! class_exists('themesflat_class_extend') ) {
	class themesflat_class_extend {
		// Blog post render
		public static function themesflat_posts( $args ) {
			if ( is_numeric( $args['limit'] ) && $args['limit'] >= 0 ) {
				$args['posts_per_page'] = $args['limit'];
			}

			if ( ! empty( $args['exclude'] ) ) {
				$exclude = $args['exclude'];

				if ( ! is_array( $exclude ) )
					$exclude = explode( ',', $exclude );

				$args['post__not_in'] = $exclude;
			}

			$query = new WP_Query( $args );	
			$GLOBALS['wp_query']->max_num_pages = $query->max_num_pages; 

			if ( ! $query->have_posts() )
				return;

			$class[] = $args['layout'];

			if ( $args['grid_columns'] != '' ) {
			    $class[] = 'columns-' . $args['grid_columns'] ;
			}	

			global $themesflat_thumbnail;
			$imgs = array(
			    'blog-grid' 		=> 'themesflat-blog-grid',
			    'blog-grid-simple' 	=> 'themesflat-blog-grid',
			    'blog-list' 		=> 'themesflat-blog',
			    );
			$themesflat_thumbnail = $imgs[$args['layout']];	
			?>
			<?php if ( ! empty( $args['title'] ) ): ?>
				<h2 class="title"><?php echo esc_html( $args['title'] ) ?></h2>
			<?php endif ?>
				<div class="wrap-blog-article <?php echo esc_attr( implode( ' ', $class ) ) ?>">
					
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>	
						<div class="item">
							<article <?php echo esc_attr(post_class('entry'));?>>
								<div class="entry-border">
									<?php if( $args['layout'] == 'blog-list' || $args['layout'] == 'blog-grid' ): ?>									
	                                <?php
										$feature_post = '';
										switch ( get_post_format() ) {	
											case 'gallery':
											$size = 'themesflat-blog';
											$images = themesflat_decode(themesflat_meta( 'gallery_images'));

											if ( empty( $images ) )
												break;
											?>		
											<div class="featured-post">		
												<div class="customizable-carousel" data-loop="true" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-space="15" data-autoplay="true" data-autospeed="4000" data-nav-dots="false" data-nav-arrows="true">
														<?php 
														if ( !empty( $images ) && is_array( $images ) ) {
															foreach ( $images as $image ) { ?>
																<div class="item-gallery">  
																    <?php echo wp_get_attachment_image($image,$themesflat_thumbnail); ?>
																</div>
															<?php }
														} 
														?> 
												</div>		
											</div><!-- /.feature-post -->
											<?php 
											break;
											case 'video':	
											$video = themesflat_meta('video_url');
											if ( !$video ) 
												break;
											global $_wp_additional_image_sizes;
											$video_size = array( 
												'height' => $_wp_additional_image_sizes[$themesflat_thumbnail]['height'],
												'width' => $_wp_additional_image_sizes[$themesflat_thumbnail]['width']
												);
												$end = "";
												if ( has_post_thumbnail() ){
													$feature_post .= '<div class="themesflat_video_embed">';
													$feature_post .= get_the_post_thumbnail(null,$themesflat_thumbnail).'
													<div class="video-video-box-overlay">
														<div class="video-video-box-button-sm">					
															<button class="video-video-play-icon" data-izimodal-open="#format-video">
																<i class="fas fa-play"></i>
															</button>
														</div>					
													</div>';
													$end = '</div>
													<div class="izimodal" id="format-video" data-izimodal-width="850px" data-iziModal-fullscreen="true">
													    <iframe height="430" src="'.esc_url($video).'" class="fullwidth shadow-primary" style="width: 100%; max-width: 100%;"></iframe>
													</div>';
												}
												$feature_post .= $end;
												break;
												default:

												$size = is_single() ? 'themesflat-blog' : $themesflat_thumbnail;
												
												$thumb = get_the_post_thumbnail( get_the_ID(), $size );
												if ( empty( $thumb ) )
													return;

												$feature_post .= get_the_post_thumbnail( get_the_ID(), $size );
											}
										if ( $feature_post )
										echo '<div class="featured-post">' . $feature_post . '</div>'; 
									?>

									<?php endif; ?>

									<div class="content-post">																				
										<h3 class="entry-title"><a href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>"><?php the_title();?></a></h3>
										<?php if( $args['layout'] == 'blog-list' || $args['layout'] == 'blog-grid' ): ?>
										<div class="entry-meta meta-on clearfix">
											<?php themesflat_render_meta($args['layout']); ?>
										</div><!-- /.entry-meta -->	
										<?php endif; ?>	
										<div class="entry-content">
										<?php $readmore = $args['hide_readmore'] == 1  ? wp_kses_post( $args['readmore_text'], 'evockans' ) : '[...]';
											if ($args['show_content'] == 1) {
												themesflat_render_post($args['layout'],$readmore,$args['content_length']);
											} ?>
										</div>
									</div>
								</div>
							</article><!-- /.entry -->
						</div>
					<?php endwhile;?>
					
				</div>
			<?php 
			wp_reset_postdata();				
		}
	}
}