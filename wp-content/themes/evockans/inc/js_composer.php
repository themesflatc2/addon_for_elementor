<?php 
if ( function_exists( 'visual_composer' ) ) {
	add_action( 'vc_before_init', 'themesflat_add_param' );
	function themesflat_add_param() {
		vc_add_param( 'vc_row', array(
				'type'             => 'colorpicker',
				'heading'          => esc_html__( 'Overlay Color', 'evockans' ),
				'param_name'       => 'overlay_color',
				'std'				=> '',
				'description'      => esc_html__( 'Select Overlay color', 'evockans' ),
			) );
		vc_add_param('vc_row',themesflat_shortcode_default_id());
	}

	add_action( 'vc_before_init', 'themesflat_add_param_img_fixed' );
	function themesflat_add_param_img_fixed() {
		vc_add_param( 'vc_row', array(
				'type'             => 'checkbox',
				'heading'          => esc_html__( 'Background Image & Parallax Background Image Fixed', 'evockans' ),
				'param_name'       => 'themesflat_bg_image_fixed',
				'value'      => array( esc_html__( 'Yes', 'evockans' ) => 'yes' ),
				'description'      => esc_html__( 'If checked Background Image Fixed.', 'evockans' ),
			) );
		vc_add_param('vc_row',themesflat_shortcode_default_id());
	}
}
?>