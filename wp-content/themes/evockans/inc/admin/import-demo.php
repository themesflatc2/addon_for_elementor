<?php
/**
 * Demo Import Data
 * @package evockans
 */

function themesflat_import_files() {      
    return array(
        array(
            'import_file_name'             => 'Multi-purpose Agency',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/demo1/content_demo1.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p1.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  => 'http://corpthemes.com/wordpress/evockans/',
        ),

        array(
            'import_file_name'             => 'Consulting Agency',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/demo2/content_demo2.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p2.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  =>   'http://corpthemes.com/wordpress/evockans/consulting-agency',
        ),

        array(
            'import_file_name'             => 'Corporate Agency',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/demo3/content_demo3.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p3.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  => 'http://corpthemes.com/wordpress/evockans/corporate-agency',
        ),

        array(
            'import_file_name'             => 'Finance Agency',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/demo4/content_demo4.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p4.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  => 'http://corpthemes.com/wordpress/evockans/finance-agency',
        ),

        array(
            'import_file_name'             => 'Classic Business Agency',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/demo5/content_demo5.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p5.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  => 'http://corpthemes.com/wordpress/evockans/classic-business-agency',
        ),

        array(
            'import_file_name'             => 'Marketing Agency',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/demo6/content_demo6.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p6.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  => 'http://corpthemes.com/wordpress/evockans/marketing-agency',
        ),

        array(
            'import_file_name'             => 'Startup',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/demo7/content_demo7.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p7.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  => 'http://corpthemes.com/wordpress/evockans/startup',
        ),

        array(
            'import_file_name'             => 'Web Agency',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/demo8/content_demo8.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p8.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  => 'http://corpthemes.com/wordpress/evockans/web-agency',
        ),

        array(
            'import_file_name'             => 'Digital Agency',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/demo9/content_demo9.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p9.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  => 'http://corpthemes.com/wordpress/evockans/digital-agency',
        ),

        array(
            'import_file_name'             => 'Creative Agency',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/demo10/content_demo10.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p10.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  => 'http://corpthemes.com/wordpress/evockans/creative-agency',
        ),

        array(
            'import_file_name'             => 'Creative Studio',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/demo11/content_demo11.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p11.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  => 'http://corpthemes.com/wordpress/evockans/creative-studio',
        ),

        array(
            'import_file_name'             => 'Modern Agency',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/demo12/content_demo12.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p12.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  => 'http://corpthemes.com/wordpress/evockans/modern-agency',
        ),

        array(
            'import_file_name'             => 'Small Business',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/demo13/content_demo13.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p13.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  => 'http://corpthemes.com/wordpress/evockans/small-business',
        ),

        array(
            'import_file_name'             => 'SEO Agency',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/demo14/content_demo14.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p14.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  => 'http://corpthemes.com/wordpress/evockans/seo-agency',
        ),

        array(
            'import_file_name'             => 'Digital Product',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/demo15/content_demo15.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p15.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  => 'http://corpthemes.com/wordpress/evockans/digital-product',
        ),

        array(
            'import_file_name'             => 'All Demo',
            'import_file_url'              => 'http://corpthemes.com/wordpress/evockans/demo/fulldemo/content_full.xml',
            'import_widget_file_url'       => 'http://corpthemes.com/wordpress/evockans/demo/widgets.wie',
            'import_customizer_file_url'   => 'http://corpthemes.com/wordpress/evockans/demo/options.dat',
            'import_preview_image_url'     => 'http://corpthemes.com/wordpress/evockans/demo/images/p1.png',
            'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the MailChimp form.', 'evockans' ),
            'preview_url'                  => 'http://corpthemes.com/wordpress/evockans/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'themesflat_import_files' );

function themesflat_before_content_import() {    
    if(get_post(1)){
        wp_delete_post(1);
    }      
}
add_action( 'pt-ocdi/before_content_import', 'themesflat_before_content_import' );

function themesflat_after_import_setup( $selected_import ) {
    $main_menu = get_term_by( 'name', 'Main', 'nav_menu' );
    $footer_menu = get_term_by( 'name', 'Menu Footer', 'nav_menu' );
    $bottom_menu = get_term_by( 'name', 'Menu Bottom', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'footer' => $footer_menu->term_id,
            'bottom' => $footer_menu->term_id,
        )
    );

    if ( 'Multi-purpose Agency' === $selected_import['import_file_name'] ) { 
        $front_page = get_page_by_title( 'Multi-purpose Agency' ); 
    } elseif ('Consulting Agency' === $selected_import['import_file_name']) {
        $front_page = get_page_by_title( 'Consulting Agency' );
    } elseif ('Corporate Agency' === $selected_import['import_file_name']) {
        $front_page = get_page_by_title( 'Corporate Agency' );
    } elseif ('Finance Agency' === $selected_import['import_file_name']) {
        $front_page = get_page_by_title( 'Finance Agency' );
    } elseif ('Classic Business Agency' === $selected_import['import_file_name']) {
        $front_page = get_page_by_title( 'Classic Business Agency' );
    } elseif ('Marketing Agency' === $selected_import['import_file_name']) {
        $front_page = get_page_by_title( 'Marketing Agency' );
    } elseif ('Startup' === $selected_import['import_file_name']) {
        $front_page = get_page_by_title( 'Startup' );
    } elseif ('Web Agency' === $selected_import['import_file_name']) {
        $front_page = get_page_by_title( 'Web Agency' );
    } elseif ('Digital Agency' === $selected_import['import_file_name']) {
        $front_page = get_page_by_title( 'Digital Agency' );
    } elseif ('Creative Agency' === $selected_import['import_file_name']) {
        $front_page = get_page_by_title( 'Creative Agency' );
    } elseif ('Creative Studio' === $selected_import['import_file_name']) {
        $front_page = get_page_by_title( 'Creative Studio' );
    } elseif ('Modern Agency' === $selected_import['import_file_name']) {
        $front_page = get_page_by_title( 'Modern Agency' );
    } elseif ('Small Business' === $selected_import['import_file_name']) {
        $front_page = get_page_by_title( 'Small Business' );
    } elseif ('SEO Agency' === $selected_import['import_file_name']) {
        $front_page = get_page_by_title( 'SEO Agency' );       
    } elseif ('Digital Product' === $selected_import['import_file_name']) {
        $front_page = get_page_by_title( 'Digital Product' );
    } elseif ('All Demo' === $selected_import['import_file_name']) {
        $front_page = get_page_by_title( 'Multi-purpose Agency' );
    } 
    
    $blog_page  = get_page_by_title( 'Blog List' );
    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page->ID );
    update_option( 'page_for_posts', $blog_page->ID );
    update_option( 'posts_per_page', 3 ); 

    global $wp_rewrite;
    $wp_rewrite->set_permalink_structure('/%postname%/');
    $wp_rewrite->flush_rules(); 

}
add_action( 'pt-ocdi/after_import', 'themesflat_after_import_setup' );

function themesflat_import_revsliders( $selected_import ) {
    if ( 'Multi-purpose Agency' === $selected_import['import_file_name'] ) { 

        if ( class_exists( 'RevSlider' ) ) {
            $slider_array = array(
                THEMESFLAT_DIR . '/demo/slide-1.zip'
            );
            $slider = new RevSlider();       
            foreach($slider_array as $filepath){
                $slider->importSliderFromPost(true,true,$filepath);  
            }       
            return 'Revolution Slider imported';
        }

    } elseif ('Consulting Agency' === $selected_import['import_file_name']) {
        
        if ( class_exists( 'RevSlider' ) ) {
            $slider_array = array(
                THEMESFLAT_DIR . '/demo/slide-2.zip'
            );
            $slider = new RevSlider();       
            foreach($slider_array as $filepath){
                $slider->importSliderFromPost(true,true,$filepath);  
            }       
            return 'Revolution Slider imported';
        }

    } elseif ('Corporate Agency' === $selected_import['import_file_name']) {
        
        if ( class_exists( 'RevSlider' ) ) {
            $slider_array = array(
                THEMESFLAT_DIR . '/demo/slide-2.zip'
            );
            $slider = new RevSlider();       
            foreach($slider_array as $filepath){
                $slider->importSliderFromPost(true,true,$filepath);  
            }       
            return 'Revolution Slider imported';
        }

    } elseif ('Finance Agency' === $selected_import['import_file_name']) {
        
        if ( class_exists( 'RevSlider' ) ) {
            $slider_array = array(
                THEMESFLAT_DIR . '/demo/slide-3.zip'
            );
            $slider = new RevSlider();       
            foreach($slider_array as $filepath){
                $slider->importSliderFromPost(true,true,$filepath);  
            }       
            return 'Revolution Slider imported';
        }

    } elseif ('Classic Business Agency' === $selected_import['import_file_name']) {
        
        if ( class_exists( 'RevSlider' ) ) {
            $slider_array = array(
                THEMESFLAT_DIR . '/demo/slide-1.zip'
            );
            $slider = new RevSlider();       
            foreach($slider_array as $filepath){
                $slider->importSliderFromPost(true,true,$filepath);  
            }       
            return 'Revolution Slider imported';
        }

    } elseif ('Web Agency' === $selected_import['import_file_name']) {
       
        if ( class_exists( 'RevSlider' ) ) {
            $slider_array = array(
                THEMESFLAT_DIR . '/demo/slide-2.zip'
            );
            $slider = new RevSlider();       
            foreach($slider_array as $filepath){
                $slider->importSliderFromPost(true,true,$filepath);  
            }       
            return 'Revolution Slider imported';
        }

    } elseif ('Digital Agency' === $selected_import['import_file_name']) {
        
        if ( class_exists( 'RevSlider' ) ) {
            $slider_array = array(
                THEMESFLAT_DIR . '/demo/slide-1.zip'
            );
            $slider = new RevSlider();       
            foreach($slider_array as $filepath){
                $slider->importSliderFromPost(true,true,$filepath);  
            }       
            return 'Revolution Slider imported';
        }

    } elseif ('Creative Agency' === $selected_import['import_file_name']) {
       
        if ( class_exists( 'RevSlider' ) ) {
            $slider_array = array(
                THEMESFLAT_DIR . '/demo/slide-1.zip'
            );
            $slider = new RevSlider();       
            foreach($slider_array as $filepath){
                $slider->importSliderFromPost(true,true,$filepath);  
            }       
            return 'Revolution Slider imported';
        }

    } elseif ('Creative Studio' === $selected_import['import_file_name']) {
        
        if ( class_exists( 'RevSlider' ) ) {
            $slider_array = array(
                THEMESFLAT_DIR . '/demo/slide-2.zip'
            );
            $slider = new RevSlider();       
            foreach($slider_array as $filepath){
                $slider->importSliderFromPost(true,true,$filepath);  
            }       
            return 'Revolution Slider imported';
        }

    } elseif ('SEO Agency' === $selected_import['import_file_name']) {
        
        if ( class_exists( 'RevSlider' ) ) {
            $slider_array = array(
                THEMESFLAT_DIR . '/demo/slide-2.zip'
            );
            $slider = new RevSlider();       
            foreach($slider_array as $filepath){
                $slider->importSliderFromPost(true,true,$filepath);  
            }       
            return 'Revolution Slider imported';
        } 

    } elseif ('All Demo' === $selected_import['import_file_name']) {
        
        if ( class_exists( 'RevSlider' ) ) {
            $slider_array = array(
                THEMESFLAT_DIR . '/demo/slide-1.zip',
                THEMESFLAT_DIR . '/demo/slide-2.zip',
                THEMESFLAT_DIR . '/demo/slide-3.zip'
            );
            $slider = new RevSlider();       
            foreach($slider_array as $filepath){
                $slider->importSliderFromPost(true,true,$filepath);  
            }       
            return 'Revolution Slider imported';
        }

    } 
}
add_action( 'pt-ocdi/after_import', 'themesflat_import_revsliders' );