<?php
require THEMESFLAT_DIR . 'inc/plugins/plugin-activation.php';
// Register action to declare required plugins
add_action('tgmpa_register', 'themesflat_recommend_plugin');
function themesflat_recommend_plugin() {
    
    $plugins = array( 
        array(
            'name'      => 'WPBakery Visual Composer',
            'slug'      => 'js_composer',
            'source'    => esc_url('http://corpthemes.com/wordpress/evockans/evokans_plugins/js_composer.zip'),
            'required'  => false
        ),
        array(
            'name' => 'Revslider',
            'slug' => 'revslider',
            'source' => esc_url('http://corpthemes.com/wordpress/evockans/evokans_plugins/revslider.zip'),
            'required' => false
        ),
        array(
            'name' => 'ThemesFlat',
            'slug' => 'themesflat',
            'source' => THEMESFLAT_DIR . 'inc/plugins/themesflat.zip',
            'required' => false
        ),
        array(
            'name' => 'Advanced Custom Fields PRO',
            'slug' => 'advanced-custom-fields-pro',
            'source' => esc_url('http://corpthemes.com/wordpress/evockans/evokans_plugins/advanced-custom-fields-pro.zip'),
            'required' => false
        ),
        array(
            'name'      => 'Envato Market',
            'slug'      => 'envato-market', 
            'source'    => esc_url('http://corpthemes.com/wordpress/evockans/evokans_plugins/envato-market.zip'),   
            'required'  => false,            
        ),
        array(
            'name' => 'Contact Form 7',
            'slug' => 'contact-form-7',
            'required' => false
        ),        
        array(
            'name' => 'Mailchimp',
            'slug' => 'mailchimp-for-wp',
            'required' => false
        ),
        array(
          'name'            => 'One Click Demo Import',
          'slug'            => 'one-click-demo-import',
          'required'        => false,
        )
        
    );

    $config = array(
        'default_path' => '',                      
        'menu'         => 'tgmpa-install-plugins', 
        'has_notices'  => true,                   
        'dismissable'  => true,                    
        'dismiss_msg'  => '',                      
        'is_automatic' => true,                   
        'message'      => '',                      
        'strings'      => array(
            'page_title'                      => esc_html__( 'Install Required Plugins', 'evockans' ),
            'menu_title'                      => esc_html__( 'Install Plugins', 'evockans' ),
            'installing'                      => esc_html__( 'Installing Plugin: %s', 'evockans' ),
            'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'evockans' ),
            'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'evockans' ),
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'evockans' ),
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'evockans' ),
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'evockans' ),
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'evockans' ),
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'evockans' ),
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'evockans' ),
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'evockans' ),
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'evockans' ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'evockans' ),
            'return'                          => esc_html__( 'Return to Required Plugins Installer', 'evockans' ),
            'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'evockans' ),
            'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'evockans' ),
            'nag_type'                        => 'updated'
        )
    );
    
    tgmpa($plugins, $config);
}

