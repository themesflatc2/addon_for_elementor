<?php
get_header(); 
$featured_img = "themesflat-blog-grid";
?>
<div class="col-lg-12">
	<div class="featured-services-single">
    	<?php $images = themesflat_decode(themesflat_meta( 'services_gallery_images'));
    	if ( !empty( $images ) && is_array( $images ) ):  ?> 
        	<div class="themesflat-services-single-carousel">
                <div class="featured-post-gallery">		
					<div class="customizable-carousel" data-loop="true" data-items="2" data-md-items="2" data-sm-items="2" data-xs-items="1" data-space="15" data-autoplay="true" data-autospeed="4000" data-nav-dots="false" data-nav-arrows="true">
							<?php 
							if ( !empty( $images ) && is_array( $images ) ) {
								foreach ( $images as $image ) { ?>
									<div class="item-gallery">  
									    <?php echo wp_get_attachment_image($image,$featured_img); ?>
									</div>
								<?php }
							} 
							?> 
					</div>		
				</div><!-- /.feature-post -->	                    
            </div><!-- /.themesflat-services-single-carousel --> 
        <?php else: ?>
        	<div class="featured-post">
                <?php  ?>
                    <div>
                    <?php 			            		
	            		echo '<div>';
	            			the_post_thumbnail($featured_img);
	            		echo '</div>'; 
	        		?>                         
                    </div>	                    
            </div><!-- /.themesflat-services-single-carousel --> 
        <?php endif; ?>
    </div>			            		            
</div>
<div id="primary" class="single-cause-area">
	<main id="main" class="post-wrap" role="main">
		<div class="services-single services-post-<?php the_ID(); ?>">
			<?php while ( have_posts() ) : the_post(); ?>	
			<div class="container">		
				<div class="row">					 	
		            <div class="col-lg-8">
		            	<div class="entry-content">
		            		<h2 class="entry-title"><?php the_title(); ?></h2>
		            		<?php the_content(); ?>
		            	</div>
		            </div>
					<div class="col-lg-4">
						<div class="services-information">
							<div class="event-finder">
								<h5>Services Information</h5>					
								<ul class="entry-services-details">
									<li><span><?php esc_html_e( 'Category:','evockans' ) ?></span>
										<?php echo esc_attr ( the_terms( get_the_ID(), 'services_category', 
				                            '', ', ', '' ) ); ?>
									</li>										
									<li>
				            			<span><?php esc_html_e( 'Date:','evockans' );  ?></span>
				            			<?php echo esc_attr( the_date() ); ?>
				            		</li>	
								</ul>
							</div>
						</div>		
		            </div>		
				</div>
			</div>
			<?php endwhile; // end of the loop. ?>
		</div><!-- services-single -->
	</main>
</div>
<?php get_footer(); ?>