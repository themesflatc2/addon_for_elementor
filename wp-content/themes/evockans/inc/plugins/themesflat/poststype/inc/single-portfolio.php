<?php
get_header();
$featured_img = "themesflat-blog-grid";
?>
<div class="col-lg-12">
	<div class="featured-portfolio-single clearfix">
    	<?php $images = themesflat_decode(themesflat_meta( 'portfolio_gallery_images'));
    	if ( !empty( $images ) && is_array( $images ) ):  ?> 
        	<div class="themesflat-portfolio-single-carousel">
                <div class="featured-post-gallery">		
					<div class="owl-carousel owl-theme customizable-carousel" data-loop="true" data-items="2" data-md-items="2" data-sm-items="2" data-xs-items="1" data-space="15" data-autoplay="true" data-autospeed="4000" data-nav-dots="false" data-nav-arrows="true">
							<?php 
							if ( !empty( $images ) && is_array( $images ) ) {
								foreach ( $images as $image ) { ?>
									<div class="item-gallery">  
									    <?php echo wp_get_attachment_image($image,$featured_img); ?>
									</div>
								<?php }
							} 
							?>
					</div>		
				</div><!-- /.feature-post -->	                    
            </div><!-- /.themesflat-portfolio-single-carousel --> 
        <?php else: ?>
        	<div class="featured-post">
                <?php  ?>
                    <div>
                    <?php 			            		
	            		echo '<div>';
	            			the_post_thumbnail($featured_img);
	            		echo '</div>'; 
	        		?>                         
                    </div>	                    
            </div><!-- /.themesflat-portfolio-single-carousel --> 
        <?php endif; ?>
    </div><!-- featured-portfolio-single  -->           
</div><!-- col-lg-12 -->
<div id="primary" class="single-cause-area">
	<main id="main" class="post-wrap" role="main">
		<div class="portfolio-single portfolio-post-<?php the_ID(); ?>">
			<?php while ( have_posts() ) : the_post(); ?>

				<div class="container">		
					<div class="row">						
			            <div class="col-lg-8">
			            	<div class="entry-content">
			            		<?php the_content(); ?>
			            	</div>
			            </div><!-- col-lg-8 -->
						<div class="col-lg-4">
							<div class="portfolio-information">
								<div class="event-finder">
									<h5>Project Information</h5>					
									<ul class="entry-portfolio-details">
										<li><span><?php esc_html_e( 'Category:','evockans' ) ?></span>
											<?php echo esc_attr ( the_terms( get_the_ID(), 'portfolios_category', 
					                            '', ', ', '' ) ); ?>
										</li>

										<?php if ( themesflat_meta( 'portfolio_status' ) != "" ) : ?>
											<li><span><?php esc_html_e( 'Status:','evockans' ) ?></span>
												<?php echo esc_attr( themesflat_meta( 'portfolio_status' ) )?>
											</li>
										<?php endif;  ?>
					            		
					            		<?php if ( themesflat_meta( 'portfolio_client' ) != "" ) : ?>
											<li><span><?php esc_html_e( 'Client:','evockans' ) ?></span>
												<?php echo esc_attr( themesflat_meta( 'portfolio_client' ) )?>
											</li>
										<?php endif;  ?>
										
										<li>
					            			<span><?php esc_html_e( 'Date:','evockans' );  ?></span>
					            			<?php echo esc_attr( the_date() ); ?>
					            		</li>								

										<?php if ( themesflat_meta( 'portfolio_website' ) != "" ) : ?>
											<li><span><?php esc_html_e( 'Website:','evockans' ) ?></span>
												<a href="<?php echo esc_url( themesflat_meta( 'portfolio_website' ) )?>"><?php echo esc_attr( themesflat_meta( 'portfolio_website' ) )?></a>
											</li>
										<?php endif;  ?>	
									</ul>
								</div>
							</div>		
			            </div><!-- col-lg-4 -->		           
					</div><!-- row -->
				</div><!-- container -->		
							
				<?php if ( themesflat_get_opt( 'portfolio_show_post_navigator') == 1 ): ?>
				<div class="col-md-12"><?php themesflat_post_navigation(); ?></div>
				<?php endif; ?>

			<?php endwhile; ?>
		</div><!-- portfolio-single -->
	</main>
</div>
<?php get_template_part( 'tpl/portfolio-related' ); ?>
<?php get_footer(); ?>