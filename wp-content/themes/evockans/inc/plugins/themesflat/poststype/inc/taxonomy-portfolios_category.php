<?php
get_header(); 
$term_slug = $wp_query->tax_query->queries[0]['terms'][0];
?>
<div class="col-md-12">
    <div class="themesflat-portfolio-taxonomy">
        <?php        
        $args = array(
            'showcase' => 'style-1',
            'items'    => -1, 
            'gapv'  => 15,
            'gaph'  => 15,
            'image_crop' => 'rectangle3',
            'column' => 3,
            'cat_slug' => $term_slug,
            );       
        ?>
        <?php 
        if (class_exists('themesflat_custom_post_type')) {
            echo themesflat_custom_post_type::portfolio_post_grid( $args );
        }
        ?>
        </div><!-- /.portfolio-container -->
</div><!-- /.col-md-12 -->
<?php get_footer(); ?>