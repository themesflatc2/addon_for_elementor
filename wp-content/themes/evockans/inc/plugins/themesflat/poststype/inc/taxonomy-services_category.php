<?php
get_header();
$term_slug = $wp_query->tax_query->queries[0]['terms'][0];
?>
<div class="col-md-12">
    <div class="themesflat-services-taxonomy">
        <?php        
        $args = array(
            'style' => 'grid-style1',
            'items'    => -1, 
            'pagination' => 'true',
            'column' => 3,
            'image_crop' => 'custom',
            'image_size' => '500x350',
            'cat_slug' => $term_slug,
            );       
        ?>
        <?php 
        if (class_exists('themesflat_custom_post_type')) {
            echo themesflat_custom_post_type::themesflat_shortcode_services( $args );
        }
        ?>
        </div><!-- /.portfolio-container -->
</div><!-- /.col-md-12 -->
<?php get_footer(); ?>