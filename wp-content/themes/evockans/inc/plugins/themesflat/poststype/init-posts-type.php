<?php 
/* Custom Post Type
===================================*/
if ( ! class_exists( 'themesflat_custom_post_type' ) ) {
    class themesflat_custom_post_type {
        function __construct() {
            
            require_once THEMESFLAT_PATH . '/poststype/register-portfolio.php';
            require_once THEMESFLAT_PATH . '/poststype/register-services.php';
            require_once THEMESFLAT_PATH . '/poststype/register-gallery.php';           

            add_filter( 'single_template', array( $this,'themesflat_single_portfolio' ) );  
            add_filter( 'taxonomy_template', array( $this,'themesflat_taxonomy_portfolio' ) );

            add_filter( 'single_template', array( $this,'themesflat_single_services' ) );
            add_filter( 'taxonomy_template', array( $this,'themesflat_taxonomy_services' ) );

            add_action( 'init', array( $this, 'integrateWithVC' ) );
            $shortcodes = 'portfolio_post_grid,themesflat_shortcode_services,themesflat_shortcode_gallery';
            $shortcodes = explode(",", $shortcodes);
            $shortcodes = array_map("trim", $shortcodes);
            foreach ( $shortcodes as $shortcode ) {
                add_shortcode($shortcode, array( $this, $shortcode ) );
            }        
        }        

        /* Temlate Portfolio */
        function themesflat_single_portfolio( $single_template ) {
            global $post;
            if ( $post->post_type == 'portfolios' ) $single_template = THEMESFLAT_PATH . '/poststype/inc/single-portfolio.php';
            return $single_template;
        }
        function themesflat_taxonomy_portfolio( $taxonomy_template ) {
            global $post;
            if ( $post->post_type == 'portfolios' ) $taxonomy_template = THEMESFLAT_PATH . '/poststype/inc/taxonomy-portfolios_category.php';
            return $taxonomy_template;
        }

        /* Temlate Services */
        function themesflat_single_services( $single_template ) {
            global $post;
            if ( $post->post_type == 'services' ) $single_template = THEMESFLAT_PATH . '/poststype/inc/single-services.php';
            return $single_template;
        }
        function themesflat_taxonomy_services( $taxonomy_template ) {
            global $post;
            if ( $post->post_type == 'services' ) $taxonomy_template = THEMESFLAT_PATH . '/poststype/inc/taxonomy-services_category.php';
            return $taxonomy_template;
        }

        public function integrateWithVC() {
            /* Start Portfolio Post 
            ====================================*/
            $category_portfolio = get_terms( 'portfolios_category','orderby=name&hide_empty=0' );
            $category_portfolio_name[] = 'All';
            $category_order = array();
            foreach ( $category_portfolio as $category ) {                
                $category_portfolio_name[$category->name] = $category->slug;
                $category_order[] = $category->slug;        
            }
            $category_order = implode(',', $category_order);
            vc_map( array(
                'name' => esc_html__('Portfolio Post', 'evockans'),
                'description' => esc_html__('Displaying portfolio posts with filter bar.', 'evockans'),
                'base' => 'portfolio_post_grid',
                'weight'    =>  180,
                'icon' => THEMESFLAT_ICON,
                'category' => esc_html__('THEMESFLAT', 'evockans'),
                'params'      => array(
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Showcase', 'evockans' ),
                        'param_name' => 'showcase',
                        'value'      => array(
                            'Style 1' => 'style-1',
                            'Style 2' => 'style-2',
                            'Style 3' => 'style-3',
                            'Style 4' => 'style-4',
                            'Style 5' => 'style-5',
                            'Style 6' => 'style-6',
                        ),
                        'std'       => 'style-1',
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Image Cropping', 'evockans' ),
                        'param_name' => 'image_crop',
                        'value'      => array(   
                            'Full' => 'full',          
                            '600 x 600' => 'square',
                            '400 x 400' => 'square2',
                            '600 x 500' => 'rectangle',
                            '600 x 390' => 'rectangle2',
                            '600 x 450' => 'rectangle3',
                            '600 x auto' => 'auto2', 
                            'Custom' => 'custom',                           
                        ),
                        'std'       => 'rectangle3',
                        'description'   => esc_html__('Choose auto option to keep the same aspect ratio when cropping.', 'evockans'),
                    ), 
                    array(
                        'type'       => 'textfield',
                        'heading'    => esc_html__( 'Image Size', 'evockans' ),
                        'description'    => esc_html__( '( Alternatively enter size in pixels (Example: 750x750 (Width x Height)).', 'evockans' ),
                        'param_name' => 'image_size',
                        'value' => '400x268',
                        'dependency' => array( 'element' => 'image_crop', 'value' => 'custom' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Wrap Rounded.', 'evockans'),
                        'param_name' => 'wrap_rounded',
                        'value' => '',
                        'description'   => esc_html__('Top Right Bottom Left.', 'evockans'),
                    ),                   
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Spacing between items', 'evockans'),
                        'param_name' => 'gapv',
                        'value' => '0',
                        'description'   => esc_html__('Ex: 30', 'evockans'),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Spacing below items', 'evockans'),
                        'param_name' => 'gaph',
                        'value' => '0',
                        'description'   => esc_html__('Ex: 30', 'evockans'),
                    ),  
                    /*Query*/                  
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Category', 'evockans'),
                        'param_name' => 'cat_slug',
                        'value' => $category_portfolio_name,
                        'group' => esc_html__( 'Query', 'evockans' ),
                        'description'   => esc_html__('Displaying posts that have this category.', 'evockans'),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Posts Per Page', 'evockans'),
                        'param_name' => 'items',
                        'group'      => esc_html__( 'Query', 'evockans' ),
                        'value' => '9',
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'My Custom Ids', 'evockans' ),
                        'param_name'  => 'post_in',
                        'value'       => '',                
                        'description' => esc_html__( 'Just Show these Portfolio Post by IDs EX:1,2,3', 'evockans' ),
                        'group' => esc_html__( 'Query', 'evockans' ),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'Post Ids Will Be Inorged. Ex: 1,2,3', 'evockans' ),
                        'param_name'  => 'exclude',
                        'value'       => '',
                        'group' => esc_html__( 'Query', 'evockans' ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__( 'Show Pagination?', 'evockans' ),
                        'param_name' => 'pagination',
                        'group' => esc_html__( 'Query', 'evockans' ),
                        'value' => array(
                            'Yes' => 'true',
                            'No' => 'false',
                        ),
                        'std'       => 'false',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__( 'Pagination Alignment', 'evockans' ),
                        'param_name' => 'pagination_alignment',
                        'group' => esc_html__( 'Query', 'evockans' ),
                        'value' => array(
                            'Left' => '',
                            'Center' => 'text-center',
                            'Right' => 'text-right',
                        ),
                        'dependency' => array( 'element' => 'pagination', 'value' => 'true' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Pagination: Top Margin', 'evockans'),
                        'param_name' => 'pagination_top_margin',
                        'value' => '40px',
                        'group' => esc_html__( 'Query', 'evockans' ),
                        'dependency' => array( 'element' => 'pagination', 'value' => 'true' ),
                    ),
                    /* Filter */
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__( 'Show Filter Bar?', 'evockans' ),
                        'param_name' => 'show_filter',
                        'group' => esc_html__( 'Filter', 'evockans' ),
                        'value' => array(
                            'Yes' => 'true',
                            'No' => 'false',
                        ),
                        'std'       => 'false',
                    ),
                    array(
                        'type'       => 'checkbox',
                        'heading'    => esc_html__( 'Filter posts by default?', 'evockans' ),
                        'param_name' => 'filter_by_default',
                        'group' => esc_html__( 'Filter', 'evockans' ),
                        'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
                        'dependency' => array( 'element' => 'show_filter', 'value' => 'true' ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Filter Category', 'evockans'),
                        'param_name' => 'filter_cat_slug',
                        'value' => $category_portfolio_name,
                        'group' => esc_html__( 'Filter', 'evockans' ),
                        'dependency' => array( 'element' => 'filter_by_default', 'value' => 'yes' ),
                        'description'   => esc_html__('Filter posts from this category by default.', 'evockans'),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Button: All', 'evockans'),
                        'param_name' => 'filter_button_all',
                        'value' => 'All',
                        'group' => esc_html__( 'Filter', 'evockans' ),
                        'description'   => esc_html__('Leave it empty to disable.', 'evockans'),
                        'dependency' => array( 'element' => 'show_filter', 'value' => 'true' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Bottom Margin', 'evockans'),
                        'param_name' => 'bottom_filter',
                        'group' => esc_html__( 'Filter', 'evockans' ),
                        'value' => '',
                        'description'   => esc_html__('Ex: 45px.', 'evockans'),
                        'dependency' => array( 'element' => 'show_filter', 'value' => 'true' ),
                    ),
                    // Typography
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Filter: Font Family', 'evockans' ),
                        'param_name' => 'filter_font_family',
                        'value'      =>  themesflat_plugin_google_font(),
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Filter: Font Weight', 'evockans' ),
                        'param_name' => 'filter_font_weight',
                        'value'      => array(
                            'Default' => 'Default',
                            '300' => '300',
                            '400' => '400',
                            '500' => '500',
                            '600' => '600',
                            '700' => '700',
                            '800' => '800',
                            '900' => '900',
                        ),
                        'std'       => 'Default',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Filter: Font Size', 'evockans'),
                        'param_name' => 'filter_font_size',
                        'value' => '',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Filter: Line-Height', 'evockans'),
                        'param_name' => 'filter_line_height',
                        'value' => '',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Filter: Letter Spacing', 'evockans'),
                        'param_name' => 'filter_letter_spacing',
                        'value' => '',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Filter: Text Transform', 'evockans' ),
                        'param_name' => 'filter_text_tranform',
                        'value'      => array(
                            'Capitalize' => 'capitalize',
                            'Uppercase' => 'uppercase',
                        ),
                        'std'       => 'uppercase',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    // Column
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'If Wrap > 1100px', 'evockans' ),
                        'param_name' => 'column',
                        'group'      => esc_html__( 'Columns', 'evockans' ),
                        'value'      => array(
                            '2 Columns' => '2c',
                            '3 Columns' => '3c',
                            '4 Columns' => '4c',
                            '5 Columns' => '5c',
                            '6 Columns' => '6c'
                        ),
                        'std'       => '4c',
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'If Wrap from 800px to 1099px', 'evockans' ),
                        'param_name' => 'column2',
                        'group'      => esc_html__( 'Columns', 'evockans' ),
                        'value'      => array(
                            '2 Columns' => '2c',
                            '3 Columns' => '3c',
                            '4 Columns' => '4c',
                        ),
                        'std'       => '3c',
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'If Wrap from 550px to 799px', 'evockans' ),
                        'param_name' => 'column3',
                        'group'      => esc_html__( 'Columns', 'evockans' ),
                        'value'      => array(
                            '1 Column' => '1c',
                            '2 Columns' => '2c',
                            '3 Columns' => '3c',
                        ),
                        'std'       => '2c',
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'If Wrap < 549px', 'evockans' ),
                        'param_name' => 'column4',
                        'group'      => esc_html__( 'Columns', 'evockans' ),
                        'value'      => array(
                            '1 Column' => '1c',
                            '2 Columns' => '2c',
                        ),
                        'std'       => '1c',
                    ),        
                )
            ) );
            /* End Portfolio Post 
            ====================================*/

            /* Start Services Post 
            ====================================*/
            $category_services = get_terms( 'services_category', 'orderby=name&hide_empty=0' );
            $category_services_name[] = 'All';
            foreach($category_services as $category){
                $category_services_name[$category->name] = $category->slug;
            }
            vc_map( array(
                'name' => esc_html__('Services Posts', 'evockans'),
                'description' => esc_html__('Display Posts From Some Categories.', 'evockans'),
                'base' => 'themesflat_shortcode_services',
                'weight'    =>  180,
                'icon' => THEMESFLAT_ICON,
                'category' => esc_html__('THEMESFLAT', 'evockans'),
                'params' => array(
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Style', 'evockans' ),
                        'param_name' => 'style',
                        'value'      => array(
                            'Grid Style 1' => 'grid-style1',
                            'Grid Style 2' => 'grid-style2',
                            'Grid Style 3' => 'grid-style3',                            
                        ),
                        'std'       => 'grid-style1',               
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Text Alignment', 'evockans' ),
                        'param_name' => 'alignment',
                        'value'      => array(
                            'Left' => '',
                            'Center' => 'text-center',
                            'Right' => 'text-right',
                        ),
                    ),          
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Column', 'evockans' ),
                        'param_name' => 'column',
                        'value'      => array(
                            '2 Columns' => '2c',
                            '3 Columns' => '3c',
                            '4 Columns' => '4c',
                        ),
                        'std'       => '3c',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Wrap Padding.', 'evockans'),
                        'param_name' => 'wrap_padding',
                        'value' => '',
                        'description'   => esc_html__('Top Right Bottom Left.', 'evockans'),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Wrap Margin.', 'evockans'),
                        'param_name' => 'wrap_margin',
                        'value' => '',
                        'description'   => esc_html__('Top Right Bottom Left.', 'evockans'),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Wrap Rounded.', 'evockans'),
                        'param_name' => 'wrap_rounded',
                        'value' => '',
                        'description'   => esc_html__('Top Right Bottom Left.', 'evockans'),
                    ),
                    array(
                        'type' => 'colorpicker',
                        'heading' => esc_html__('Wrap Background', 'evockans'),
                        'param_name' => 'wrap_background',
                        'value' => '',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Border Width', 'evockans'),
                        'param_name' => 'border_width',
                        'value' => '',
                        'description'   => esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
                    ),
                    array(
                        'type' => 'colorpicker',
                        'heading' => esc_html__('Border Color', 'evockans'),
                        'param_name' => 'border_color',
                        'value' => '',
                    ),
                    array(
                        'type'       => 'checkbox',
                        'heading'    => esc_html__( 'Add Shadow', 'evockans' ),
                        'param_name' => 'add_shadow',
                        'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
                    ),
                    // Image
                    array(
                        'type' => 'headings',
                        'text' => esc_html__('Image', 'evockans'),
                        'param_name' => 'image_setting',
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Thumbnail Cropping', 'evockans' ),
                        'param_name' => 'image_crop',
                        'value'      => array(
                            'Full' => 'full',
                            '600 x 600' => 'square',
                            '600 x 500' => 'rectangle',
                            '600 x 390' => 'rectangle2',
                            'Custom' => 'custom',
                        ),
                        'std'       => 'custom',
                    ),
                    array(
                        'type'       => 'textfield',
                        'heading'    => esc_html__( 'Image Size', 'evockans' ),
                        'description'    => esc_html__( '( Alternatively enter size in pixels (Example: 750x750 (Width x Height)).', 'evockans' ),
                        'param_name' => 'image_size',
                        'value' => '750x525',
                        'dependency' => array( 'element' => 'image_crop', 'value' => 'custom' ),
                    ),
                    // Content
                    array(
                        'type' => 'headings',
                        'text' => esc_html__('Content', 'evockans'),
                        'param_name' => 'content_setting',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Content Padding.', 'evockans'),
                        'param_name' => 'content_padding',
                        'value' => '',
                        'description'   => esc_html__('Top Right Bottom Left.', 'evockans'),
                    ),
                    // Query
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Category', 'evockans') ,
                        'param_name' => 'cat_slug',
                        'value' => $category_services_name,
                        'description' => esc_html__('Display posts from some categories.', 'evockans') ,
                        'group' => esc_html__( 'Query', 'evockans' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Number of items', 'evockans'),
                        'param_name' => 'items',
                        'value' => '3',
                        'group' => esc_html__( 'Query', 'evockans' ),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'My Custom Ids', 'evockans' ),
                        'param_name'  => 'post_in',
                        'value'       => '',                
                        'description' => esc_html__( 'Just Show these Services Post by IDs EX:1,2,3', 'evockans' ),
                        'group' => esc_html__( 'Query', 'evockans' ),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'Post Ids Will Be Inorged. Ex: 1,2,3', 'evockans' ),
                        'param_name'  => 'exclude',
                        'value'       => '',
                        'group' => esc_html__( 'Query', 'evockans' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Excerpt Length', 'evockans'),
                        'param_name' => 'excerpt_lenght',
                        'value' => '15',
                        'description' => esc_html('The number of words you wish to display in the excerpt. Enter "0" to hide the excerpt.', 'evockans'),
                        'group' => esc_html__( 'Query', 'evockans' ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__( 'Show Pagination?', 'evockans' ),
                        'param_name' => 'pagination',
                        'group' => esc_html__( 'Query', 'evockans' ),
                        'value' => array(
                            'Yes' => 'true',
                            'No' => 'false',
                        ),
                        'std'       => 'false',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__( 'Pagination Alignment', 'evockans' ),
                        'param_name' => 'pagination_alignment',
                        'group' => esc_html__( 'Query', 'evockans' ),
                        'value' => array(
                            'Left' => '',
                            'Center' => 'text-center',
                            'Right' => 'text-right',
                        ),
                        'dependency' => array( 'element' => 'pagination', 'value' => 'true' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Pagination: Top Margin', 'evockans'),
                        'param_name' => 'pagination_top_margin',
                        'value' => '40px',
                        'group' => esc_html__( 'Query', 'evockans' ),
                        'dependency' => array( 'element' => 'pagination', 'value' => 'true' ),
                    ),
                    // Button
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Button Text', 'evockans'),
                        'param_name' => 'button_text',
                        'value' => 'Read More',
                        'group' => esc_html__( 'Button', 'evockans' ),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Button Style', 'evockans' ),
                        'param_name' => 'button_style',
                        'value'      => array(
                            'Simple Link' => 'simple_link',
                            'Accent' => 'accent',
                            'Dark' => 'dark',
                            'Light' => 'light',
                            'Very Light' => 'very-light',
                            'White' => 'white',
                            'Outline' => 'outline',
                            'Outline Dark' => 'outline_dark',
                            'Outline Light' => 'outline_light',
                            'Outline Very light' => 'outline_very-light',
                            'Outline White' => 'outline_white',
                        ),
                        'std'       => 'simple_link',
                        'group' => esc_html__( 'Button', 'evockans' ),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Button Size', 'evockans' ),
                        'param_name' => 'button_size',
                        'value'      => array(
                            'Medium' => '',
                            'Small' => 'small',
                            'Very mall' => 'xsmall',
                            'Big' => 'big',
                        ),
                        'std'       => 'small',
                        'group' => esc_html__( 'Button', 'evockans' ),
                        'dependency' => array(
                            'element' => 'button_style',
                            'value' => array (
                                'accent',
                                'dark',
                                'light',
                                'very-light',
                                'white',
                                'outline',
                                'outline_dark',
                                'outline_light',
                                'outline_very-light',
                                'outline_white'
                            )
                        ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Button Rounded', 'evockans'),
                        'param_name' => 'button_rounded',
                        'value' => '',
                        'description'   => esc_html__('ex: 10px', 'evockans'),
                        'group' => esc_html__( 'Button', 'evockans' ),
                        'dependency' => array(
                            'element' => 'button_style',
                            'value' => array (
                                'accent',
                                'dark',
                                'light',
                                'very-light',
                                'white',
                                'outline',
                                'outline_dark',
                                'outline_light',
                                'outline_very-light',
                                'outline_white'
                            )
                        ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Button Padding', 'evockans'),
                        'param_name' => 'button_padding',
                        'value' => '',
                        'group' => esc_html__( 'Button', 'evockans' ),
                    ),
                    array(
                        'type' => 'colorpicker',
                        'heading' => esc_html__('Link Color', 'evockans'),
                        'param_name' => 'link_color',
                        'value' => '',
                        'group' => esc_html__( 'Button', 'evockans' ),
                        'dependency' => array( 'element' => 'button_style', 'value' => 'simple_link' ),
                    ),
                    array(
                        'type'       => 'checkbox',
                        'heading'    => esc_html__( 'Add Icon Button', 'evockans' ),
                        'param_name' => 'add_icon_button',
                        'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
                        'group' => esc_html__( 'Button', 'evockans' ),
                    ),  
                    // Typography
                    array(
                        'type' => 'headings',
                        'text' => esc_html__('Heading', 'evockans'),
                        'param_name' => 'heading_typograpy',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Heading Font Family', 'evockans' ),
                        'param_name' => 'heading_font_family',
                        'value'      =>  themesflat_plugin_google_font(),
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Heading Font Weight', 'evockans' ),
                        'param_name' => 'heading_font_weight',
                        'value'      => array(
                            'Default' => 'Default',
                            '300' => '300',
                            '400' => '400',
                            '500' => '500',
                            '600' => '600',
                            '700' => '700',
                            '800' => '800',
                            '900' => '900',
                        ),
                        'std'       => 'Default',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type' => 'colorpicker',
                        'heading' => esc_html__('Heading Color', 'evockans'),
                        'param_name' => 'heading_color',
                        'value' => '',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Heading Font Size', 'evockans'),
                        'param_name' => 'heading_font_size',
                        'value' => '',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Heading Line-Height', 'evockans'),
                        'param_name' => 'heading_line_height',
                        'value' => '',
                        'description'   => esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type' => 'headings',
                        'text' => esc_html__('Excerpt', 'evockans'),
                        'param_name' => 'desc_typograpy',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Excerpt Font Family', 'evockans' ),
                        'param_name' => 'desc_font_family',
                        'value'      =>  themesflat_plugin_google_font(),
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Excerpt Font Weight', 'evockans' ),
                        'param_name' => 'desc_font_weight',
                        'value'      => array(
                            'Default' => 'Default',
                            '300' => '300',
                            '400' => '400',
                            '500' => '500',
                            '600' => '600',
                            '700' => '700',
                            '800' => '800',
                            '900' => '900',
                        ),
                        'std'       => 'Default',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type' => 'colorpicker',
                        'heading' => esc_html__('Excerpt Color', 'evockans'),
                        'param_name' => 'desc_color',
                        'value' => '',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Excerpt Font Size', 'evockans'),
                        'param_name' => 'desc_font_size',
                        'value' => '',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Excerpt Line-height', 'evockans'),
                        'param_name' => 'desc_line_height',
                        'value' => '',
                        'description'   => esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type' => 'headings',
                        'text' => esc_html__('Button', 'evockans'),
                        'param_name' => 'button_typograpy',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Button Font Family', 'evockans' ),
                        'param_name' => 'button_font_family',
                        'value'      =>  themesflat_plugin_google_font(),
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Button Font Weight', 'evockans' ),
                        'param_name' => 'button_font_weight',
                        'value'      => array(
                            'Default' => 'Default',
                            '300' => '300',
                            '400' => '400',
                            '500' => '500',
                            '600' => '600',
                            '700' => '700',
                            '800' => '800',
                            '900' => '900',
                        ),
                        'std'       => 'Default',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Button Font Size', 'evockans'),
                        'param_name' => 'button_font_size',
                        'value' => '',
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Button Line-Height', 'evockans'),
                        'param_name' => 'button_line_height',
                        'value' => '',
                        'description'   => esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
                        'group' => esc_html__( 'Typography', 'evockans' ),
                    ), 
                    // Spacing
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Heading: Top Margin', 'evockans'),
                        'param_name' => 'heading_top_margin',
                        'value' => '',
                        'group' => esc_html__( 'Spacing', 'evockans' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Heading: Bottom Margin', 'evockans'),
                        'param_name' => 'heading_bottom_margin',
                        'value' => '',
                        'group' => esc_html__( 'Spacing', 'evockans' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Description: Top Margin', 'evockans'),
                        'param_name' => 'desc_top_margin',
                        'value' => '',
                        'group' => esc_html__( 'Spacing', 'evockans' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Description: Bottom Margin', 'evockans'),
                        'param_name' => 'desc_bottom_margin',
                        'value' => '',
                        'group' => esc_html__( 'Spacing', 'evockans' ),
                    ),  
                )
            ) );
            /* End Services Post
            ====================================*/

            /* Start Gallery Post 
            ====================================*/
            $category_gallery = get_terms( 'gallery_category', 'orderby=name&hide_empty=0' );
            $category_gallery_name[] = 'All';
            foreach($category_gallery as $category){
                $category_gallery_name[$category->name] = $category->slug;
            }
            vc_map( array(
                'name' => esc_html__('Gallery', 'evockans'),
                'description' => esc_html__('Displaying gallery', 'evockans'),
                'base' => 'themesflat_shortcode_gallery',
                'weight'    =>  180,
                'icon' => THEMESFLAT_ICON,
                'category' => esc_html__('THEMESFLAT', 'evockans'),
                'params' => array(
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Showcase', 'evockans' ),
                        'param_name' => 'showcase',
                        'value'      => array(
                            'Justified' => 'justified',
                            'Grid' => 'grid',
                        ),
                        'std'       => 'grid',
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Image Cropping', 'evockans' ),
                        'param_name' => 'image_crop',
                        'value'      => array(   
                            'Full' => 'full',          
                            '600 x 600' => 'square',
                            '400 x 400' => 'square2',
                            '600 x 500' => 'rectangle',
                            '600 x 390' => 'rectangle2',
                            '600 x 450' => 'rectangle3',
                            '600 x auto' => 'auto2', 
                            'Custom' => 'custom',                           
                        ),
                        'std'       => 'full',
                        'description'   => esc_html__('Choose auto option to keep the same aspect ratio when cropping.', 'evockans'),
                    ), 
                    array(
                        'type'       => 'textfield',
                        'heading'    => esc_html__( 'Image Size', 'evockans' ),
                        'description'    => esc_html__( '( Alternatively enter size in pixels (Example: 750x750 (Width x Height)).', 'evockans' ),
                        'param_name' => 'image_size',
                        'value' => '555x549',
                        'dependency' => array( 'element' => 'image_crop', 'value' => 'custom' ),
                    ),                   
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Spacing between items', 'evockans'),
                        'param_name' => 'gapv',
                        'value' => '0',
                        'description'   => esc_html__('Ex: 30', 'evockans'),
                        'dependency' => array( 'element' => 'showcase', 'value' => 'grid' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Spacing below items', 'evockans'),
                        'param_name' => 'gaph',
                        'value' => '0',
                        'description'   => esc_html__('Ex: 30', 'evockans'),
                        'dependency' => array( 'element' => 'showcase', 'value' => 'grid' ),
                    ), 
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Row Height', 'evockans'),
                        'param_name' => 'height_item',
                        'value' => '300',
                        'description'   => esc_html__('Ex: 300', 'evockans'),
                        'dependency' => array( 'element' => 'showcase', 'value' => 'justified' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Spacing between and spacing below items', 'evockans'),
                        'param_name' => 'gapvh',
                        'value' => '10',
                        'description'   => esc_html__('Ex: 10', 'evockans'),
                        'dependency' => array( 'element' => 'showcase', 'value' => 'justified' ),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'Hover Style', 'evockans' ),
                        'param_name' => 'hover_style',
                        'value'      => array(
                            'Hover Style 1' => 'hover-style1',
                            'Hover Style 2' => 'hover-style2',
                            'Hover Style 3' => 'hover-style3',
                            'Hover Style 4' => 'hover-style4',
                        ),
                        'std'       => 'hover-style1',
                    ),
                    /*Query*/                  
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Category', 'evockans'),
                        'param_name' => 'cat_slug',
                        'value' => $category_portfolio_name,
                        'group' => esc_html__( 'Query', 'evockans' ),
                        'description'   => esc_html__('Displaying posts that have this category.', 'evockans'),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Posts Per Page', 'evockans'),
                        'param_name' => 'items',
                        'group'      => esc_html__( 'Query', 'evockans' ),
                        'value' => '9',
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'My Custom Ids', 'evockans' ),
                        'param_name'  => 'post_in',
                        'value'       => '',                
                        'description' => esc_html__( 'Just Show these Portfolio Post by IDs EX:1,2,3', 'evockans' ),
                        'group' => esc_html__( 'Query', 'evockans' ),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'Post Ids Will Be Inorged. Ex: 1,2,3', 'evockans' ),
                        'param_name'  => 'exclude',
                        'value'       => '',
                        'group' => esc_html__( 'Query', 'evockans' ),
                    ),
                    // Column
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'If Wrap > 1100px', 'evockans' ),
                        'param_name' => 'column',
                        'group'      => esc_html__( 'Columns', 'evockans' ),
                        'value'      => array(
                            '2 Columns' => '2c',
                            '3 Columns' => '3c',
                            '4 Columns' => '4c',
                            '5 Columns' => '5c',
                            '6 Columns' => '6c'
                        ),
                        'std'       => '4c',
                        'dependency' => array( 'element' => 'showcase', 'value' => 'grid' ),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'If Wrap from 800px to 1099px', 'evockans' ),
                        'param_name' => 'column2',
                        'group'      => esc_html__( 'Columns', 'evockans' ),
                        'value'      => array(
                            '2 Columns' => '2c',
                            '3 Columns' => '3c',
                            '4 Columns' => '4c',
                        ),
                        'std'       => '3c',
                        'dependency' => array( 'element' => 'showcase', 'value' => 'grid' ),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'If Wrap from 550px to 799px', 'evockans' ),
                        'param_name' => 'column3',
                        'group'      => esc_html__( 'Columns', 'evockans' ),
                        'value'      => array(
                            '1 Column' => '1c',
                            '2 Columns' => '2c',
                            '3 Columns' => '3c',
                        ),
                        'std'       => '2c',
                        'dependency' => array( 'element' => 'showcase', 'value' => 'grid' ),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__( 'If Wrap < 549px', 'evockans' ),
                        'param_name' => 'column4',
                        'group'      => esc_html__( 'Columns', 'evockans' ),
                        'value'      => array(
                            '1 Column' => '1c',
                            '2 Columns' => '2c',
                        ),
                        'std'       => '1c',
                        'dependency' => array( 'element' => 'showcase', 'value' => 'grid' ),
                    ),
                )
            ) );
            /* End Gallery Post
            ====================================*/

        }

        public static function portfolio_post_grid( $atts, $content = null ) {
            extract( shortcode_atts( array(
                'showcase' => 'style-1',
                'image_crop' => 'rectangle3',
                'image_size' => '400x268',
                'wrap_rounded' => '',
                'items' => '9',
                'post_in' => '',
                'exclude' => '',
                'cat_slug'  => 'All',
                'pagination' => 'false',
                'pagination_alignment' => 'text-center',
                'pagination_top_margin' => '40px',
                'gapv'          => '0',
                'gaph'          => '0',
                'show_filter'   => 'false',
                'filter_by_default' => '',
                'filter_cat_slug' => '',
                'filter_button_all' => 'All',
                'bottom_filter' => '',
                'column'        => '4c',
                'column2'       => '3c',
                'column3'       => '2c',
                'column4'       => '1c',
                'filter_font_family' => 'Default',
                'filter_font_weight' => 'Default',
                'filter_font_size' => '',
                'filter_line_height' => '',
                'filter_letter_spacing' => '',
                'filter_text_tranform' => 'uppercase'
            ), $atts ) );

            $column = intval( $column );
            $column2 = intval( $column2 );
            $column3 = intval( $column3 );
            $column4 = intval( $column4 );
            $wrap_rounded = intval( $wrap_rounded );
            $items = intval( $items );
            $gapv = intval( $gapv );
            $gaph = intval( $gaph );
            $bottom_filter = intval( $bottom_filter );
            $filter_font_size = intval( $filter_font_size );
            $filter_line_height = intval( $filter_line_height );
            $filter_letter_spacing = intval( $filter_letter_spacing );
            $pagination_top_margin = intval( $pagination_top_margin );

            if ( empty( $items ) )
                return;

            if ( empty( $gapv ) ) $gapv = 0;
            if ( empty( $gaph ) ) $gaph = 0;

            $filter_css = $filter_wrap_css  = $filter_data = $pagination_css = $item_css = '';
            if ( $bottom_filter ) $filter_wrap_css = 'margin-bottom:'. $bottom_filter . 'px;';
            if( $pagination == 'true' ){
                if ($pagination_top_margin) $pagination_css = 'margin-top:' . $pagination_top_margin . 'px;';
            }

            $filter_css .= 'text-transform:'. $filter_text_tranform .';';
            if ( $filter_font_weight != 'Default' ) $filter_css .= 'font-weight:'. $filter_font_weight .';';
            if ( $filter_font_size ) $filter_css .= 'font-size:'. $filter_font_size .'px;';
            if ( $filter_line_height ) $filter_css .= 'line-height:'. $filter_line_height .'px;';
            if ( $filter_letter_spacing ) $filter_css .= 'letter-spacing:'. $filter_letter_spacing .'px;';
            if ( $filter_font_family != 'Default' ) {
                $filter_css .= 'font-family:'. $filter_font_family .';';
            }

            if ( $wrap_rounded ) $item_css .= 'border-radius:'. $wrap_rounded .'px;';

            if ( ! empty( $filter_cat_slug ) && $filter_by_default  )
                $filter_data = strtolower( $filter_cat_slug );

            if ( get_query_var('paged') ) {
               $paged = get_query_var('paged');
            } elseif ( get_query_var('page') ) {
               $paged = get_query_var('page');
            } else {
               $paged = 1;
            }

            $query_args = array(
                'post_type' => 'portfolios',
                'posts_per_page' => $items,
                'paged'     => $paged
            );

            if ( $post_in ) {   
                if ( trim($post_in) != '') {
                    $query_args['post__in'] = explode(',', trim($post_in));
                    $query_args['orderby'] = 'post__in';
                }
            }

            if ( ! empty( $exclude ) ) {                
                if ( ! is_array( $exclude ) ){
                    $exclude = explode( ',', $exclude );
                    $query_args['post__not_in'] = $exclude;
                }
            }

            if ( $cat_slug != 'All' ) {
                $query_args['tax_query'] = array(
                    array(
                        'taxonomy' => 'portfolios_category',
                        'field'    => 'slug',
                        'terms'    => $cat_slug
                    ),
                );
            }

            $query = new WP_Query( $query_args );
            if ( ! $query->have_posts() ) { echo "Portfolios item not found!"; return; }
            ob_start(); ?> 

            <div class="themesflat_sc_vc-portfolios-grid clearfix" data-column="<?php echo esc_attr( $column ); ?>" data-column2="<?php echo esc_attr( $column2 ); ?>" data-column3="<?php echo esc_attr( $column3 ); ?>" data-column4="<?php echo esc_attr( $column4 ); ?>" data-gaph="<?php echo esc_attr( $gaph ); ?>" data-gapv="<?php echo esc_attr( $gapv ); ?>" data-filter="<?php echo esc_attr( $filter_data ); ?>">

                <?php if ( $query->have_posts() ) :
                    $terms = get_terms("portfolios_category");
                    if ( count( $terms ) > 0 && $show_filter == 'true' ) {
                        echo '<div id="portfolios-filter" class="cbp-l-filters-alignCenter" style="'. esc_attr( $filter_wrap_css ) .'"><div class="inner">';
                            if ( ! empty( $filter_button_all ) )
                                echo '<div data-filter="*" class="cbp-filter-item" style="'. $filter_css .'"><span>'. esc_html( $filter_button_all ) .'</span><div class="cbp-filter-counter"></div></div>';

                            foreach ( $terms as $term ) {
                                $termname = strtolower( $term->name );
                                $termname = str_replace( ' ', '-', $termname );
                                echo '<div data-filter=".'. esc_attr( $termname ) .'" class="cbp-filter-item" title="'. esc_attr( $term->name ) .'" style="'. $filter_css .'"><span>'. $term->name . '</span><div class="cbp-filter-counter"></div></div>';
                            }
                        echo '</div></div>';
                    } ?>

                    <div id="portfolio" class="cbp">
                        <?php while ( $query->have_posts() ) : $query->the_post();
                            wp_enqueue_script( 'themesflat_sc_vc-cubeportfolio' ); 
                            wp_enqueue_script( 'themesflat_sc_vc-magnificpopup' );
                            
                            global $post;
                            $term_list = '';
                            $terms = get_the_terms( $post->ID, 'portfolios_category' );

                            if ( $terms ) {
                                foreach ( $terms as $term ) {
                                    $term_list .= $term->slug .' ';
                                }
                            } ?>

                            <div class="cbp-item <?php echo esc_attr( $term_list ); ?>" style="<?php echo esc_attr( $item_css ); ?>">
                                <div class="portfolios-box <?php echo esc_attr( $showcase ); ?> portfolio-post-<?php the_ID(); ?>">
                                    <div class="inner">
                                        <div class="portfolios-wrap">
                                            <div class="portfolios-image">
                                                <?php 
                                                    if ( has_post_thumbnail() ) {                               
                                                        if ( $image_crop == 'custom' && $image_size != '' ) {
                                                                echo wpb_getImageBySize(
                                                                        array(
                                                                            'attach_id' => get_post_thumbnail_id(get_the_ID()),
                                                                            'thumb_size' => $image_size
                                                                        )
                                                                    )['thumbnail'];
                                                        } else {

                                                            if ( $image_crop == 'full' ) $img_size = 'full';
                                                            if ( $image_crop == 'square' ) $img_size = 'themesflat_sc_vc-square';
                                                            if ( $image_crop == 'square2' ) $img_size = 'themesflat_sc_vc-square2';
                                                            if ( $image_crop == 'rectangle' ) $img_size = 'themesflat_sc_vc-rectangle';
                                                            if ( $image_crop == 'rectangle2' ) $img_size = 'themesflat_sc_vc-rectangle2';
                                                            if ( $image_crop == 'rectangle3' ) $img_size = 'themesflat_sc_vc-rectangle3';
                                                            if ( $image_crop == 'auto2' ) $img_size = 'themesflat_sc_vc-small-auto';

                                                            echo get_the_post_thumbnail( get_the_ID(), $img_size );
                                                        }
                                                    }                                           
                                                ?>                                                    
                                            </div>
                                            <div class="portfolios-overlay">
                                                <div class="portfolios-content">
                                                    <div class="portfolios-text">
                                                        <h2 class="title"><a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>"><?php echo get_the_title(); ?></a></h2>
                                                        <?php if( $showcase == 'style-1' || $showcase == 'style-2' || $showcase == 'style-3' ): ?>
                                                        <h4 class="category"><?php the_terms( get_the_ID(), 'portfolios_category', '', ' , ', '' ) ?></h4>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> <!-- portfolios-wrap -->
                                    </div>
                                </div><!-- /.portfolios-box -->
                            </div><!-- /.cbp-item -->
                        <?php endwhile; ?>
                    </div><!-- /#portfolio -->

                    <?php if ( $pagination == 'true' ): ?>
                        <nav class="navigation paging-navigation numeric pagination-shortcodes <?php echo esc_attr($pagination_alignment); ?>" role="navigation" style="<?php echo esc_attr($pagination_css); ?>">
                            <div class="pagination loop-pagination">
                                <?php themesflat_pagination_shortcodes($query); ?>
                            </div>
                        </nav>
                    <?php endif; ?>
                <?php endif; ?>

                <?php wp_reset_postdata(); ?>

            </div><!-- /.themesflat_sc_vc-portfolios -->
            <?php
            $return = ob_get_clean();
            return $return;
        }

        public static function themesflat_shortcode_services( $atts, $content = null ) {
            extract( shortcode_atts( array(
                'style' => 'grid-style1',
                'alignment' => '',
                'column' => '3c',
                'wrap_padding' => '',
                'wrap_margin' => '',
                'wrap_rounded' => '',
                'wrap_background' => '',
                'border_width' => '',
                'border_color' => '',
                'add_shadow' => '',
                'image_crop' => 'custom',
                'image_size' => '750x525',
                'content_padding' => '',
                'cat_slug' => 'All',
                'items' => '3',
                'post_in' => '',
                'exclude' => '',
                'excerpt_lenght' => '15',
                'pagination' => 'false',
                'pagination_alignment' => 'text-center',
                'pagination_top_margin' => '40px',
                'button_text' => 'Button Text',
                'button_text' => 'Read More',
                'button_style' => 'simple_link',
                'button_size' => 'small',
                'button_rounded' => '',
                'add_icon_button' => '',
                'button_padding' => '',
                'link_color' => '', 
                'heading_font_family' => 'Default',
                'heading_font_weight' => 'Default',
                'heading_color' => '',
                'heading_font_size' => '',
                'heading_line_height' => '',  
                'desc_font_family' => 'Default',
                'desc_font_weight' => 'Default',
                'desc_color' => '',
                'desc_font_size' => '',
                'desc_line_height' => '',
                'button_font_family' => 'Default',
                'button_font_weight' => 'Default',
                'button_font_size' => '',
                'button_line_height' => '',  
                'heading_top_margin' => '',
                'heading_bottom_margin' => '',
                'desc_top_margin' => '',
                'desc_bottom_margin' => '',
            ), $atts ) );   
            //extract($atts = vc_map_get_attributes( 'services', $atts ));
            $content = wpb_js_remove_wpautop($content, true);

            $column = intval( $column );
            $wrap_rounded = intval( $wrap_rounded );
            $heading_top_margin = intval( $heading_top_margin );
            $heading_bottom_margin = intval( $heading_bottom_margin );
            $desc_top_margin = intval( $desc_top_margin );
            $desc_bottom_margin = intval( $desc_bottom_margin );
            $pagination_top_margin = intval( $pagination_top_margin );

            $cls = $item_css = $content_css = $heading_css = $heading_a_color = $desc_css = $button_css = $button_icon_html = $pagination_css = '';

            $cls = $style . ' '.$alignment;

            if ($column) {
                $cls .= ' grid ' . 'columns-'.$column;
            }

            if( $pagination == 'true' ){
                if ($pagination_top_margin) $pagination_css = 'margin-top:' . $pagination_top_margin . 'px;';
            }

            /*wrap css*/
            if ( $wrap_padding ) $item_css .= 'padding:'. $wrap_padding .';';
            if ( $add_shadow ) $item_css .= '-webkit-box-shadow: 0px 10px 30px 0px rgba(50, 50, 50, 0.08);
                                            -moz-box-shadow: 0px 10px 30px 0px rgba(50, 50, 50, 0.08);
                                            box-shadow: 0px 10px 30px 0px rgba(50, 50, 50, 0.08);'; 
            if ( $wrap_rounded ) $item_css .= 'border-radius:'. $wrap_rounded .'px; overflow: hidden;';
            if ( $wrap_background ) $item_css .= 'background-color:'. $wrap_background .';';
            if ( $border_color && $border_width ) $item_css .= 'border-style:solid;border-width:'. $border_width .';border-color:'. $border_color .';';

            if ( $content_padding ) $content_css .= 'padding:'. $content_padding .';';

            if ( $heading_font_weight != 'Default' ) $heading_css .= 'font-weight:'. $heading_font_weight .';';
            if ( $heading_color ) $heading_css .= 'color:'. $heading_color .';';
            if ( $heading_font_size ) $heading_css .= 'font-size:'. $heading_font_size .'px;';
            if ( $heading_line_height ) $heading_css .= 'line-height:'. $heading_line_height .';';
            if ( $heading_top_margin ) $heading_css .= 'margin-top:'. $heading_top_margin .'px;';
            if ( $heading_bottom_margin ) $heading_css .= 'margin-bottom:'. $heading_bottom_margin .'px;';
            if ( $heading_font_family != 'Default' ) {
                $heading_css .= 'font-family:'. $heading_font_family .';';
            }
            if($heading_color) $heading_a_color .= 'color:'. $heading_color .';';

            if ( $desc_font_weight != 'Default' ) $desc_css .= 'font-weight:'. $desc_font_weight .';';
            if ( $desc_color ) $desc_css .= 'color:'. $desc_color .';';
            if ( $desc_font_size ) $desc_css .= 'font-size:'. $desc_font_size .'px;';
            if ( $desc_line_height ) $desc_css .= 'line-height:'. $desc_line_height .';';
            if ( $desc_top_margin ) $desc_css .= 'margin-top:'. $desc_top_margin .'px;';
            if ( $desc_bottom_margin ) $desc_css .= 'margin-bottom:'. $desc_bottom_margin .'px;';
            if ( $desc_font_family != 'Default' ) {
                $desc_css .= 'font-family:'. $desc_font_family .';';
            }

            if ( $button_font_weight != 'Default' ) $button_css .= 'font-weight:'. $button_font_weight .';';
            if ( $link_color ) $button_css .= 'color:'. $link_color .';';
            if ( $button_rounded ) $button_css .= 'border-radius:'. $button_rounded .'px;';
            if ( $button_padding ) $button_css .= 'padding:'. $button_padding .';';
            if ( $button_font_size ) $button_css .= 'font-size:'. $button_font_size .'px;';
            if ( $button_line_height ) $button_css .= 'line-height:'. $button_line_height .';';
            if ( $button_font_family != 'Default' ) {
                $button_css .= 'font-family:'. $button_font_family .';';
            }
            $button_cls = $button_size;
            if ( $button_style == 'simple_link' ) $button_cls .= ' simple-link';
            if ( $button_style == 'accent' ) $button_cls .= ' themesflat_sc_vc-button accent';
            if ( $button_style == 'dark' ) $button_cls .= ' themesflat_sc_vc-button dark';
            if ( $button_style == 'light' ) $button_cls .= ' themesflat_sc_vc-button light';
            if ( $button_style == 'very-light' ) $button_cls .= ' themesflat_sc_vc-button very-light';
            if ( $button_style == 'white' ) $button_cls .= ' themesflat_sc_vc-button white';
            if ( $button_style == 'outline' ) $button_cls .= ' themesflat_sc_vc-button outline ol-accent';
            if ( $button_style == 'outline_dark' ) $button_cls .= ' themesflat_sc_vc-button outline dark';
            if ( $button_style == 'outline_light' ) $button_cls .= ' themesflat_sc_vc-button outline light';
            if ( $button_style == 'outline_very-light' ) $button_cls .= ' themesflat_sc_vc-button outline very-light';
            if ( $button_style == 'outline_white' ) $button_cls .= '  themesflat_sc_vc-button outline white';

            if( $add_icon_button ) {        
                $button_icon_html = '<i class="fa fa-angle-right"></i>';        
            }

            if ( get_query_var('paged') ) {
               $paged = get_query_var('paged');
            } elseif ( get_query_var('page') ) {
               $paged = get_query_var('page');
            } else {
               $paged = 1;
            }
            
            $query_args = array(
                'post_type' => 'services',
                'posts_per_page' => $items,
                'paged'     => $paged
            );      

            if ( $post_in ) {   
                if ( trim($post_in) != '') {
                    $query_args['post__in'] = explode(',', trim($post_in));
                    $query_args['orderby'] = 'post__in';
                }
            }

            if ( ! empty( $exclude ) ) {                
                if ( ! is_array( $exclude ) ){
                    $exclude = explode( ',', $exclude );
                    $query_args['post__not_in'] = $exclude;
                }
            }

            if ( $cat_slug != 'All' ){ 
                $query_args['tax_query'] = array(
                    array(
                        'taxonomy' => 'services_category',
                        'field'    => 'slug',
                        'terms'    => $cat_slug
                    ),
                );
            }

            $query = new WP_Query( $query_args );
            if ( ! $query->have_posts() ) { echo "Services item not found!"; return; }
            ob_start(); ?>
            <?php if ( $query->have_posts() ) : ?>
            <div class="themesflat_sc_vc-services-post <?php echo esc_attr( $cls ); ?> clearfix">
                <?php while ( $query->have_posts() ) : $query->the_post(); ?>           
                    <div class="item services-post-<?php the_ID(); ?>">
                        <div class="inner" style="<?php echo esc_attr( $item_css ); ?>">
                            <div class="thumb">
                                <?php 
                                    if ( has_post_thumbnail() ) {                               
                                        if ( $image_crop == 'custom' && $image_size != '' ) {
                                                echo wpb_getImageBySize(
                                                        array(
                                                            'attach_id' => get_post_thumbnail_id(get_the_ID()),
                                                            'thumb_size' => $image_size
                                                        )
                                                    )['thumbnail'];
                                        } else {

                                            if ( $image_crop == 'full' ) $img_size = 'full';
                                            if ( $image_crop == 'square' ) $img_size = 'themesflat_sc_vc-square';
                                            if ( $image_crop == 'rectangle' ) $img_size = 'themesflat_sc_vc-rectangle';
                                            if ( $image_crop == 'rectangle2' ) $img_size = 'themesflat_sc_vc-rectangle2';

                                            echo get_the_post_thumbnail( get_the_ID(), $img_size );
                                        }
                                    }                                           
                                ?>
                            </div>
                            <div class="text-wrap" style="<?php echo esc_attr( $content_css ); ?>">
                                <?php if( $style == 'grid-style2' ): ?>
                                <div class="wrap-category">
                                    <div class="category"><?php echo the_terms( get_the_ID(), 'services_category', '', ' , ', '' ); ?></div>
                                </div>
                                <?php endif; ?>

                                <div class="wrap-title">
                                    <h4 class="title" style="<?php echo esc_attr( $heading_css ); ?>">
                                        <a href="<?php echo get_the_permalink(); ?>" style="<?php echo esc_attr( $heading_a_color ); ?>"><?php echo get_the_title(); ?></a>
                                    </h4>
                                </div>

                                <?php if( $style == 'grid-style3' ): ?>
                                <div class="wrap-category">
                                    <div class="category"><?php echo the_terms( get_the_ID(), 'services_category', '', ' , ', '' ); ?></div>
                                </div>
                                <?php endif; ?>

                                <?php if( $excerpt_lenght > 0 ): ?>
                                <div class="desc" style="<?php echo esc_attr( $desc_css ); ?>"><?php echo wp_trim_words( get_the_content(), $excerpt_lenght, '&hellip;' ); ?></div>
                                <?php endif; ?>

                                <?php if ( $button_text ): ?>
                                <div class="themesflat-button-container">
                                    <a class="<?php echo esc_attr( $button_cls ); ?>" href="<?php echo esc_url( get_permalink() ); ?>" style="<?php echo esc_attr( $button_css ); ?>"><?php echo esc_html( $button_text ); echo $button_icon_html; ?>
                                    </a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>                    
                <?php endwhile; ?>            
            </div>
            <?php if ( $pagination == 'true' ): ?>
                <nav class="navigation paging-navigation numeric pagination-shortcodes <?php echo esc_attr($pagination_alignment); ?>" role="navigation" style="<?php echo esc_attr($pagination_css); ?>">
                    <div class="pagination loop-pagination">
                        <?php themesflat_pagination_shortcodes($query); ?>
                    </div>
                </nav>
            <?php endif; ?>
            <?php endif; ?>
            <?php
            wp_reset_postdata();
            $return = ob_get_clean();
            return $return;
        }

        public static function themesflat_shortcode_gallery( $atts, $content = null ) {
            extract( shortcode_atts( array(
                'showcase' => 'grid',
                'image_crop' => 'full',
                'image_size' => '555x549',
                'gapv'          => '0',
                'gaph'          => '0',
                'height_item' => '300',
                'gapvh' => '10',
                'hover_style' => 'hover-style1',
                'items' => '9',
                'post_in' => '',
                'exclude' => '',
                'cat_slug'  => 'All',
                'column'        => '4c',
                'column2'       => '3c',
                'column3'       => '2c',
                'column4'       => '1c',
            ), $atts ) );   
            //extract($atts = vc_map_get_attributes( 'gallery', $atts ));
            $content = wpb_js_remove_wpautop($content, true);

            $column = intval( $column );
            $column2 = intval( $column2 );
            $column3 = intval( $column3 );
            $column4 = intval( $column4 );
            $items = intval( $items );
            $gapv = intval( $gapv );
            $gaph = intval( $gaph );
            $height_item = intval( $height_item );

            if( empty( $height_item ) ) $height_item = 300;
            if ( empty( $gapvh ) ) $gapvh = 10;

            $cls = '';

            $cls .= $hover_style;

            if ( empty( $items ) )
                return;

            if ( empty( $gapv ) ) $gapv = 0;
            if ( empty( $gaph ) ) $gaph = 0;

            $query_args = array(
                'post_type' => 'gallery',
                'posts_per_page' => $items
            );

            if ( $post_in ) {   
                if ( trim($post_in) != '') {
                    $query_args['post__in'] = explode(',', trim($post_in));
                    $query_args['orderby'] = 'post__in';
                }
            }

            if ( ! empty( $exclude ) ) {                
                if ( ! is_array( $exclude ) ){
                    $exclude = explode( ',', $exclude );
                    $query_args['post__not_in'] = $exclude;
                }
            }

            if ( $cat_slug != 'All' ) {
                $query_args['tax_query'] = array(
                    array(
                        'taxonomy' => 'gallery_category',
                        'field'    => 'slug',
                        'terms'    => $cat_slug
                    ),
                );
            }


            $query = new WP_Query( $query_args );
            if ( ! $query->have_posts() ) { echo "Gallery item not found!"; return; }
            ob_start(); ?> 

            <div class="themesflat_sc_vc-gallery <?php echo esc_attr($cls); ?> clearfix" data-column="<?php echo esc_attr( $column ); ?>" data-column2="<?php echo esc_attr( $column2 ); ?>" data-column3="<?php echo esc_attr( $column3 ); ?>" data-column4="<?php echo esc_attr( $column4 ); ?>" data-gaph="<?php echo esc_attr( $gaph ); ?>" data-gapv="<?php echo esc_attr( $gapv ); ?>">

                <?php if ( $query->have_posts() ) : ?>
                    <!-- Grid -->
                    <?php if( $showcase == 'grid' ): ?>
                        <div id="gallery" class="cbp">
                            <?php while ( $query->have_posts() ) : $query->the_post();
                                wp_enqueue_script( 'themesflat_sc_vc-cubeportfolio' ); 
                                wp_enqueue_script( 'themesflat_sc_vc-magnificpopup' );
                                
                                global $post;
                                $term_list = '';
                                $terms = get_the_terms( $post->ID, 'gallery_category' );

                                if ( $terms ) {
                                    foreach ( $terms as $term ) {
                                        $term_list .= $term->slug .' ';
                                    }
                                } ?>

                                <div class="cbp-item <?php echo esc_attr( $term_list ); ?>">
                                    <div class="gallery-box <?php echo esc_attr( $showcase ); ?> gallery-post-<?php the_ID(); ?>">
                                        <div class="inner">
                                            <div class="gallery-wrap">
                                                <div class="gallery-image">
                                                    <?php 
                                                        if ( has_post_thumbnail() ) {                               
                                                            if ( $image_crop == 'custom' && $image_size != '' ) {
                                                                    echo wpb_getImageBySize(
                                                                            array(
                                                                                'attach_id' => get_post_thumbnail_id(get_the_ID()),
                                                                                'thumb_size' => $image_size
                                                                            )
                                                                        )['thumbnail'];
                                                            } else {

                                                                if ( $image_crop == 'full' ) $img_size = 'full';
                                                                if ( $image_crop == 'square' ) $img_size = 'themesflat_sc_vc-square';
                                                                if ( $image_crop == 'square2' ) $img_size = 'themesflat_sc_vc-square2';
                                                                if ( $image_crop == 'rectangle' ) $img_size = 'themesflat_sc_vc-rectangle';
                                                                if ( $image_crop == 'rectangle2' ) $img_size = 'themesflat_sc_vc-rectangle2';
                                                                if ( $image_crop == 'rectangle3' ) $img_size = 'themesflat_sc_vc-rectangle3';
                                                                if ( $image_crop == 'auto2' ) $img_size = 'themesflat_sc_vc-small-auto';

                                                                echo get_the_post_thumbnail( get_the_ID(), $img_size );
                                                            }

                                                            $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' )[0];
                                                        }                                           
                                                    ?>  
                                                        
                                                </div>
                                                <div class="gallery-overlay">
                                                    <a class="icon-gallery image-popup-gallery" href="<?php echo esc_url($image_url); ?>"><i class="ti-image"></i></a>
                                                </div>
                                            </div> <!-- gallery-wrap -->
                                        </div>
                                    </div><!-- /.gallery-box -->
                                </div><!-- /.cbp-item -->
                            <?php endwhile; ?>
                        </div><!-- /#gallery -->
                    <?php endif; ?>
                    
                    <!-- Justified -->
                    <?php if( $showcase == 'justified' ): ?>
                        <div class="justified_gallery justified-gallery <?php echo esc_attr($cls); ?>" data-rowheight="<?php echo esc_attr($height_item); ?>" data-margins="<?php echo esc_attr( $gapvh ); ?>">
                            <?php while ( $query->have_posts() ) : $query->the_post();
                                wp_enqueue_style( 'themesflat_sc_vc-justified' );
                                wp_enqueue_script( 'themesflat_sc_vc-justified' );
                                wp_enqueue_script( 'themesflat_sc_vc-appear' );

                                global $post;
                                $term_list = '';
                                $terms = get_the_terms( $post->ID, 'gallery_category' );

                                if ( $terms ) {
                                    foreach ( $terms as $term ) {
                                        $term_list .= $term->slug .' ';
                                    }
                                } ?>
                                
                                <div class="gallery-box <?php echo esc_attr( $showcase ); ?> gallery-post-<?php the_ID(); ?> <?php echo esc_attr( $term_list ); ?>">
                                    <div class="inner">
                                        <div class="gallery-wrap">
                                            <div class="gallery-image">
                                                <?php 
                                                    if ( has_post_thumbnail() ) {                               
                                                        if ( $image_crop == 'custom' && $image_size != '' ) {
                                                                echo wpb_getImageBySize(
                                                                        array(
                                                                            'attach_id' => get_post_thumbnail_id(get_the_ID()),
                                                                            'thumb_size' => $image_size
                                                                        )
                                                                    )['thumbnail'];
                                                        } else {

                                                            if ( $image_crop == 'full' ) $img_size = 'full';
                                                            if ( $image_crop == 'square' ) $img_size = 'themesflat_sc_vc-square';
                                                            if ( $image_crop == 'square2' ) $img_size = 'themesflat_sc_vc-square2';
                                                            if ( $image_crop == 'rectangle' ) $img_size = 'themesflat_sc_vc-rectangle';
                                                            if ( $image_crop == 'rectangle2' ) $img_size = 'themesflat_sc_vc-rectangle2';
                                                            if ( $image_crop == 'rectangle3' ) $img_size = 'themesflat_sc_vc-rectangle3';
                                                            if ( $image_crop == 'auto2' ) $img_size = 'themesflat_sc_vc-small-auto';

                                                            echo get_the_post_thumbnail( get_the_ID(), $img_size );
                                                        }

                                                        $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' )[0];
                                                    }                                           
                                                ?>  
                                                    
                                            </div>
                                            <div class="gallery-overlay">
                                                <a class="icon-gallery image-popup-gallery" href="<?php echo esc_url($image_url); ?>"><i class="ti-image"></i></a>
                                            </div>
                                        </div> <!-- gallery-wrap -->
                                    </div>
                                </div><!-- /.gallery-box -->
                                
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>

            </div> <!-- themesflat_sc_vc-gallery -->
            <?php
            wp_reset_postdata();
            $return = ob_get_clean();
            return $return;
        }

    }
    
}
new themesflat_custom_post_type;


/* Custom Pagination Shortcodes
===================================*/
function themesflat_pagination_shortcodes( $query = '', $echo = true ) {    
    $prev_arrow = 'fa fa-angle-left';
    $next_arrow = 'fa fa-angle-right';
    
    // Get global $query
    if ( ! $query ) {
        global $wp_query;
        $query = $wp_query;
    }

    // Set vars
    $total  = $query->max_num_pages;
    $big    = 999999999;

    // Display pagination
    if ( $total > 1 ) {

        // Get current page
        if ( $current_page = get_query_var( 'paged' ) ) {
            $current_page = $current_page;
        } elseif ( $current_page = get_query_var( 'page' ) ) {
            $current_page = $current_page;
        } else {
            $current_page = 1;
        }

        // Get permalink structure
        if ( get_option( 'permalink_structure' ) ) {
            if ( is_page() ) {
                $format = 'page/%#%/';
            } else {
                $format = '/%#%/';
            }
        } else {
            $format = '&paged=%#%';
        }

        $args = array(
            'base'      => str_replace( $big, '%#%', html_entity_decode( get_pagenum_link( $big ) ) ),
            'format'    => $format,
            'current'   => max( 1, $current_page ),
            'total'     => $total,
            'mid_size'  => 3,
            'prev_text' => '<i class="'. $prev_arrow .'"></i>',
            'next_text' => '<i class="'. $next_arrow .'"></i>',
        );

        // Output pagination
        if ( $echo ) {
            echo paginate_links( $args );
        } else {
            return paginate_links( $args );
        }

    }
}

