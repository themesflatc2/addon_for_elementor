<?php
add_action('init', 'themesflat_register_services_post_type');
/**
  * Register project post type
*/
function themesflat_register_services_post_type() {
    $services_slug = 'services';
    $labels = array(
        'name'                  => esc_html__( 'Services', 'evockans' ),
        'singular_name'         => esc_html__( 'Services', 'evockans' ),
        'menu_name'             => esc_html__( 'Services', 'evockans' ),
        'add_new'               => esc_html__( 'New Services', 'evockans' ),
        'add_new_item'          => esc_html__( 'Add New Services', 'evockans' ),
        'new_item'              => esc_html__( 'New Services Item', 'evockans' ),
        'edit_item'             => esc_html__( 'Edit Services Item', 'evockans' ),
        'view_item'             => esc_html__( 'View Services', 'evockans' ),
        'all_items'             => esc_html__( 'All Services', 'evockans' ),
        'search_items'          => esc_html__( 'Search Services', 'evockans' ),
        'not_found'             => esc_html__( 'No Services Items Found', 'evockans' ),
        'not_found_in_trash'    => esc_html__( 'No Services Items Found In Trash', 'evockans' ),
        'parent_item_colon'     => esc_html__( 'Parent Services:', 'evockans' ),
        'not_found'             => esc_html__( 'No Services found', 'evockans' ),
        'not_found_in_trash'    => esc_html__( 'No Services found in Trash', 'evockans' )

    );
    $args = array(
        'labels'      => $labels,
        'supports'    => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
        'rewrite'       => array( 'slug' => $services_slug ),
        'public'      => true,        
    );
    register_post_type( 'services', $args );
    flush_rewrite_rules();
}

add_filter( 'post_updated_messages', 'themesflat_services_updated_messages' );
/**
  * Services update messages.
*/
function themesflat_services_updated_messages ( $messages ) {
    Global $post, $post_ID;
    $messages[esc_html__( 'services' )] = array(
        0  => '',
        1  => sprintf( esc_html__( 'Services Updated. <a href="%s">View services</a>', 'evockans' ), esc_url( get_permalink( $post_ID ) ) ),
        2  => esc_html__( 'Custom Field Updated.', 'evockans' ),
        3  => esc_html__( 'Custom Field Deleted.', 'evockans' ),
        4  => esc_html__( 'Services Updated.', 'evockans' ),
        5  => isset( $_GET['revision']) ? sprintf( esc_html__( 'Services Restored To Revision From %s', 'evockans' ), wp_post_revision_title((int)$_GET['revision'], false)) : false,
        6  => sprintf( esc_html__( 'Services Published. <a href="%s">View Services</a>', 'evockans' ), esc_url( get_permalink( $post_ID ) ) ),
        7  => esc_html__( 'Services Saved.', 'evockans' ),
        8  => sprintf( esc_html__('Services Submitted. <a target="_blank" href="%s">Preview Services</a>', 'evockans' ), esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
        9  => sprintf( esc_html__( 'Services Scheduled For: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Services</a>', 'evockans' ),date_i18n( esc_html__( 'M j, Y @ G:i', 'evockans' ), strtotime( $post->post_date ) ), esc_url( get_permalink( $post_ID ) ) ),
        10 => sprintf( esc_html__( 'Services Draft Updated. <a target="_blank" href="%s">Preview Services</a>', 'evockans' ), esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
    );
    return $messages;
}

add_action( 'init', 'themesflat_register_services_taxonomy' );
/**
  * Register project taxonomy
*/
function themesflat_register_services_taxonomy() {
    /*Services Categories*/    
    $services_cat_slug = 'services_category'; 
    $labels = array(
        'name'                       => esc_html__( 'Services Categories', 'evockans' ),
        'singular_name'              => esc_html__( 'Categories', 'evockans' ),
        'search_items'               => esc_html__( 'Search Categories', 'evockans' ),
        'menu_name'                  => esc_html__( 'Categories', 'evockans' ),
        'all_items'                  => esc_html__( 'All Categories', 'evockans' ),
        'parent_item'                => esc_html__( 'Parent Categories', 'evockans' ),
        'parent_item_colon'          => esc_html__( 'Parent Categories:', 'evockans' ),
        'new_item_name'              => esc_html__( 'New Categories Name', 'evockans' ),
        'add_new_item'               => esc_html__( 'Add New Categories', 'evockans' ),
        'edit_item'                  => esc_html__( 'Edit Categories', 'evockans' ),
        'update_item'                => esc_html__( 'Update Categories', 'evockans' ),
        'add_or_remove_items'        => esc_html__( 'Add or remove Categories', 'evockans' ),
        'choose_from_most_used'      => esc_html__( 'Choose from the most used Categories', 'evockans' ),
        'not_found'                  => esc_html__( 'No Categories found.' ),
        'menu_name'                  => esc_html__( 'Categories' ),
    );
    $args = array(
        'labels'        => $labels,
        'rewrite'       => array('slug'=>$services_cat_slug),
        'hierarchical'  => true,
    );
    register_taxonomy( 'services_category', 'services', $args );
    flush_rewrite_rules();
}

