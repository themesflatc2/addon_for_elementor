<?php
add_action('init', 'themesflat_register_gallery_post_type');
/**
  * Register gallery post type
*/
function themesflat_register_gallery_post_type() {
    $gallery_slug = 'gallery';

    $labels = array(
        'name'               => esc_html__( 'Gallery', 'evockans' ),
        'singular_name'      => esc_html__( 'Gallery Item', 'evockans' ),
        'add_new'            => esc_html__( 'Add New', 'evockans' ),
        'add_new_item'       => esc_html__( 'Add New Item', 'evockans' ),
        'new_item'           => esc_html__( 'New Item', 'evockans' ),
        'edit_item'          => esc_html__( 'Edit Item', 'evockans' ),
        'view_item'          => esc_html__( 'View Item', 'evockans' ),
        'all_items'          => esc_html__( 'All Items', 'evockans' ),
        'search_items'       => esc_html__( 'Search Items', 'evockans' ),
        'parent_item_colon'  => esc_html__( 'Parent Items:', 'evockans' ),
        'not_found'          => esc_html__( 'No items found.', 'evockans' ),
        'not_found_in_trash' => esc_html__( 'No items found in Trash.', 'evockans' )
    );

    $args = array(
        'labels'        => $labels,
        'rewrite'       => array( 'slug' => $gallery_slug ),
        'supports'      => array( 'title', 'thumbnail' ),
        'public'        => true
    );

    register_post_type( 'gallery', $args );
}

add_filter( 'post_updated_messages', 'themesflat_gallery_updated_messages' );
/**
  * gallery update messages.
*/
function themesflat_gallery_updated_messages( $messages ) {
    $post             = get_post();
    $post_type        = get_post_type( $post );
    $post_type_object = get_post_type_object( $post_type );

    $messages['gallery'] = array(
        0  => '', // Unused. Messages start at index 1.
        1  => esc_html__( 'Gallery updated.', 'evockans' ),
        2  => esc_html__( 'Custom field updated.', 'evockans' ),
        3  => esc_html__( 'Custom field deleted.', 'evockans' ),
        4  => esc_html__( 'Gallery updated.', 'evockans' ),
        5  => isset( $_GET['revision'] ) ? sprintf( esc_html__( 'Gallery restored to revision from %s', 'evockans' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
        6  => esc_html__( 'Gallery published.', 'evockans' ),
        7  => esc_html__( 'Gallery saved.', 'evockans' ),
        8  => esc_html__( 'Gallery submitted.', 'evockans' ),
        9  => sprintf(
            esc_html__( 'Gallery scheduled for: <strong>%1$s</strong>.', 'evockans' ),
            date_i18n( esc_html__( 'M j, Y @ G:i', 'evockans' ), strtotime( $post->post_date ) )
        ),
        10 => esc_html__( 'Gallery draft updated.', 'evockans' )
    );
    return $messages;
}

add_action( 'init', 'themesflat_register_gallery_taxonomy' );
/**
  * Register gallery taxonomy
*/
function themesflat_register_gallery_taxonomy() {
    $cat_slug = 'gallery_category';

    $labels = array(
        'name'                       => esc_html__( 'Gallery Categories', 'evockans' ),
        'singular_name'              => esc_html__( 'Category', 'evockans' ),
        'search_items'               => esc_html__( 'Search Categories', 'evockans' ),
        'menu_name'                  => esc_html__( 'Categories', 'evockans' ),
        'all_items'                  => esc_html__( 'All Categories', 'evockans' ),
        'parent_item'                => esc_html__( 'Parent Category', 'evockans' ),
        'parent_item_colon'          => esc_html__( 'Parent Category:', 'evockans' ),
        'new_item_name'              => esc_html__( 'New Category Name', 'evockans' ),
        'add_new_item'               => esc_html__( 'Add New Category', 'evockans' ),
        'edit_item'                  => esc_html__( 'Edit Category', 'evockans' ),
        'update_item'                => esc_html__( 'Update Category', 'evockans' ),
        'add_or_remove_items'        => esc_html__( 'Add or remove categories', 'evockans' ),
        'choose_from_most_used'      => esc_html__( 'Choose from the most used categories', 'evockans' ),
        'not_found'                  => esc_html__( 'No Category found.', 'evockans' ),
        'menu_name'                  => esc_html__( 'Categories', 'evockans' ),
    );
    $args = array(
        'labels'        => $labels,
        'rewrite'       => array('slug'=>$cat_slug),
        'hierarchical'  => true,
    );
    register_taxonomy( 'gallery_category', 'gallery', $args );
}