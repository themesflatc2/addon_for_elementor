<?php
/**
 * Plugin Name: ThemesFlat By Themesflat.com
 * Plugin URI:  http://themesflat.com/
 * Description: The theme's components
 * Author:      ThemesFlat
 * Version:     1.0.1
 * Author URI: http://themesflat.com/
 */

define( 'THEMESFLAT_VERSION', '1.0.1' );
define( 'THEMESFLAT_PATH', plugin_dir_path( __FILE__ ) );
define( 'THEMESFLAT_URL', plugin_dir_url( __FILE__ ) );

$theme = wp_get_theme();
if ( 'Evockans' == $theme->name || 'Evockans' == $theme->parent_theme ) {
    // visual_composer
    if ( function_exists( 'visual_composer' ) ) {
        require_once THEMESFLAT_PATH . '/poststype/init-posts-type.php';	
        require_once THEMESFLAT_PATH . '/includes/shortcodes/action-box.php';        
        require_once THEMESFLAT_PATH . '/includes/shortcodes/accordions.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/accordion.php';        
        require_once THEMESFLAT_PATH . '/includes/shortcodes/alerts.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/animationblock.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/blog-posts.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/buttons.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/carouselbox.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/carouselgallerybox.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/contentbox.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/countdown.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/counter.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/divider.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/dropcaps.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/dtab.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/dtabs.php';   
        require_once THEMESFLAT_PATH . '/includes/shortcodes/headings.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/iconbox.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/icons.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/image-advanced.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/imagebox.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/lines.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/list-advanced.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/login-form.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/modal-popups.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/piechart.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/progressbar.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/rotate-box.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/rotate-box-primary.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/rotate-box-hover.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/section-advanced.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/pricetable.php';    
        require_once THEMESFLAT_PATH . '/includes/shortcodes/spacing.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/subscribe.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/table-data-list.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/table-data-list-item-content.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/team-members.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/testimonials.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/timeline.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/timelineinner.php';
        require_once THEMESFLAT_PATH . '/includes/shortcodes/video.php';

        require_once THEMESFLAT_PATH . '/includes/shortcodes/googlemap.php';
        require_once THEMESFLAT_PATH . '/google-api.php';
        // Add moreparams 
        add_action('init', 'vc_add_new_para', 100);         
    }
    
    require THEMESFLAT_PATH . "widgets/themesflat_recent_post.php";
    require THEMESFLAT_PATH . "widgets/themesflat_latest_news.php";   
    require THEMESFLAT_PATH . "widgets/themesflat_categories.php";  
    require THEMESFLAT_PATH . "widgets/themesflat_socials.php";
}

	
add_action( 'wp_enqueue_scripts', 'themesflat_shortcode_register_assets', 999999 );
function themesflat_shortcode_register_assets() {
    wp_enqueue_style( 'themesflat_sc_vc-flexslider', plugins_url('assets/css/flexslider.css', __FILE__), array(), '2.3.6' );
    wp_register_script( 'themesflat_sc_vc-flexslider', plugins_url('assets/js/flexslider.min.js', __FILE__), array('jquery'), '2.3.6', true );    
    wp_enqueue_style( 'themesflat_sc_vc-owlcarousel', plugins_url('assets/css/owl.carousel.css', __FILE__), array(), '2.2.1' );
    wp_register_script( 'themesflat_sc_vc-owlcarousel', plugins_url('assets/js/owl.carousel.min.js', __FILE__), array('jquery'), '2.2.1', true );

    wp_enqueue_style( 'themesflat_sc_vc-cubeportfolio', plugins_url('assets/css/cubeportfolio.min.css', __FILE__), array(), '3.4.0' );
    wp_register_script( 'themesflat_sc_vc-cubeportfolio', plugins_url('assets/js/cubeportfolio.min.js', __FILE__), array('jquery'), '3.4.0', true );
    wp_register_script( 'themesflat_sc_vc-countto', plugins_url('assets/js/countto.js', __FILE__), array('jquery'), '1.0.0', true );
    wp_register_script( 'themesflat_sc_vc-counterup', plugins_url('assets/js/counterup.js', __FILE__), array('jquery'), '1.0', true );
    wp_enqueue_script( 'themesflat_sc_vc-equalize', plugins_url('assets/js/equalize.min.js', __FILE__), array('jquery'), '1.0.0', true );
    wp_enqueue_style( 'themesflat_sc_vc-magnificpopup', plugins_url('assets/css/magnific.popup.css', __FILE__), array(), '1.0.0' );
    wp_register_script( 'themesflat_sc_vc-magnificpopup', plugins_url('assets/js/magnific.popup.min.js', __FILE__), array('jquery'), '1.0.0', true );
    wp_enqueue_style( 'themesflat_sc_vc-vegas', plugins_url('assets/css/vegas.css', __FILE__), array(), '2.3.1' );
    wp_register_script( 'themesflat_sc_vc-vegas', plugins_url('assets/js/vegas.js', __FILE__), array('jquery'), '2.3.1', true );
    wp_enqueue_style( 'themesflat_sc_vc-ytplayer', plugins_url('assets/css/ytplayer.css', __FILE__), array(), '3.2.8' );
    wp_register_script( 'themesflat_sc_vc-ytplayer', plugins_url('assets/js/ytplayer.js', __FILE__), array('jquery'), '3.2.8', true );
    wp_enqueue_script( 'themesflat_sc_vc-waypoints', plugins_url('assets/js/waypoints.js', __FILE__), array('jquery'), '2.0.4', true );
    wp_enqueue_script( 'themesflat_sc_vc-imagesloaded', plugins_url('assets/js/imagesloaded.min.js', __FILE__), array('jquery'), '3.1.8', true );
    wp_register_script( 'themesflat_sc_vc-typed', plugins_url('assets/typed.js', __FILE__), array('jquery'), '1.1.0', true );
    wp_register_script( 'themesflat_sc_vc-countdown', plugins_url('assets/js/countdown.js', __FILE__), array('jquery'), '1.0.0', true );
    wp_register_script( 'themesflat_sc_vc-appear', plugins_url('assets/js/appear.js', __FILE__), array('jquery'), '0.3.6', true );   
    wp_register_script( 'themesflat_sc_vc-piechart', plugins_url('assets/js/piechart.js', __FILE__), array('jquery'), '2.1.7', true );

    wp_enqueue_style( 'themesflat_sc_vc-justified', plugins_url('assets/css/jquery.justified.css', __FILE__), array(), '2.2.1' );
    wp_register_script( 'themesflat_sc_vc-justified', plugins_url('assets/js/jquery.justified.min.js', __FILE__), array('jquery'), '3.6.3', true );
    wp_register_script( 'themesflat_sc_vc-appear', plugins_url('assets/js/jquery.appear.min.js', __FILE__), array('jquery'), '1.0', true ); 

    wp_enqueue_script('google-maps-api', 'https://maps.googleapis.com/maps/api/js', array(), 'v3');	

	wp_enqueue_style( 'vc_extend_shortcode', plugins_url('assets/css/shortcodes.css', __FILE__), array() );
	wp_enqueue_script( 'themesflat-shortcode', plugins_url('assets/js/shortcodes.js', __FILE__), array(), '1.0', true );
}


// Show notice if your plugin is activated but Visual Composer is not
function showVcVersionNotice() {
    $plugin_data = get_plugin_data(__FILE__);
    echo '<div class="error">';
    echo '<p>' . wp_kses( esc_html__( $plugin_data['Name'] . ' - requires <a href="' . esc_url( admin_url( 'plugins.php') ) . '">Visual Composer</a>  plugin to be installed and activated on your site.', 'themesflat' ), array( 'a' => array( 'href' => array() ) ) ) . '</p>';
    echo '</div>';
}

// disable frontend editor
if ( function_exists('vc_disable_frontend') ) vc_disable_frontend();

// Add moreparams 
function vc_add_new_para() {
    // Add Number param
    vc_add_shortcode_param( 'number', 'themesflat_param_number' );
    function themesflat_param_number( $settings, $value ) {
        $dependency = '';
        $param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
        $type = isset($settings['type']) ? $settings['type'] : '';
        $suffix = isset($settings['suffix']) ? $settings['suffix'] : '';
        $class = isset($settings['class']) ? $settings['class'] : '';
        $output = '<input type="number" class="wpb_vc_param_value ' . $param_name . ' ' . $type . ' ' . $class . '" name="' . $param_name . '" value="'. $value.'" style="max-width:100px; margin-right: 10px;" />'. $suffix;
        return $output;
    }

    // Add Heading param
    vc_add_shortcode_param( 'headings', 'themesflat_param_heading' );
    function themesflat_param_heading( $settings, $value ) {
        $dependency = '';
        $param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
        $class = isset($settings['class']) ? $settings['class'] : '';
        $text = isset($settings['text']) ? $settings['text'] : '';
        $output = '<h4 '. $dependency .' class="wpb_vc_param_value '. $class .'" style="margin: 10px 0 0;padding:10px;font-size:14px; background:#ebebeb;color:#666;">'. $text .'</h4>';
        $output .= '<input type="hidden" name="'. $settings['param_name'].'" class="wpb_vc_param_value ultimate-param-heading '. $settings['param_name'] .' '. $settings['type'] .'_field" value="'. $value.'" '. $dependency.'/>';
        return $output;
    }
    
}

// Retrun animate classes
if ( ! function_exists('themesflat_animation') ) {
    function themesflat_animation() {
        return array(
            'fadeIn','fadeInDown','fadeInDownBig','fadeInLeftSmall','fadeInLeft','fadeInLeftBig','fadeInRightSmall','fadeInRight','fadeInRightBig','fadeInUp','fadeInUpBig','fadeOut','fadeOutDown','fadeOutDownBig','fadeOutLeft','fadeOutLeftBig','fadeOutRight','fadeOutRightBig','fadeOutUp','fadeOutUpBig','flash','pulse','rubberBand','shake','swing','tada','wobble','jello','hinge','rollIn','rollOut','bounce','bounceIn','bounceInDown','bounceInLeft','bounceInRight','bounceInUp','bounceOut','bounceOutDown','bounceOutLeft','bounceOutRight','bounceOutUp','flip','flipInX','flipInY','flipOutX','flipOutY','lightSpeedIn','lightSpeedOut','rotateIn','rotateInDownLeft','rotateInDownRight','rotateInUpLeft','rotateInUpRight','rotateOut','rotateOutDownLeft','rotateOutDownRight','rotateOutUpLeft','rotateOutUpRight','slideInUp','slideInDown','slideInLeft','slideInRight','slideOutUp','slideOutDown','slideOutLeft','slideOutRight','zoomIn','zoomInDown','zoomInLeft','zoomInRight','zoomInUp','zoomOut','zoomOutDown','zoomOutLeft','zoomOutRight','zoomOutUp'
        );
    }
}

// Add image sizes
add_action( 'after_setup_theme', 'add_image_sizes' );
function add_image_sizes() {
    add_image_size( 'themesflat_sc_vc-square', 600, 600, true );
    add_image_size( 'themesflat_sc_vc-square2', 400, 400, true );
    add_image_size( 'themesflat_sc_vc-rectangle', 600, 500, true );
    add_image_size( 'themesflat_sc_vc-rectangle2', 600, 390, true );
    add_image_size( 'themesflat_sc_vc-rectangle3', 600, 450, true );
    add_image_size( 'themesflat_sc_vc-medium-auto', 870, 9999 );
    add_image_size( 'themesflat_sc_vc-small-auto', 600, 9999 );
    add_image_size( 'themesflat_sc_vc-xsmall-auto', 480, 9999 );
}

// Get icon font base on font icon type
if ( ! function_exists('themesflat_sc_vc_get_icon_class') ) {
    function themesflat_sc_vc_get_icon_class( $atts, $icon_location ) {
        // Define vars
        $icon = '';
        $icon_type = ! empty( $atts['icon_type'] ) ? $atts['icon_type'] : 'fontawesome';

        // Generate fontawesome icon class
        if ( 'fontawesome' == $icon_type && ! empty( $atts[$icon_location] ) ) {
            $icon = $atts[$icon_location];
            $icon = str_replace( 'fa-', '', $icon );
            $icon = str_replace( 'fa ', '', $icon );
            $icon = 'fa fa-'. $icon;
        } elseif ( ! empty( $atts[ $icon_location .'_'. $icon_type ] ) ) {
            $icon = $atts[ $icon_location .'_'. $icon_type ];
        }

        // Sanitize
        $icon = in_array( $icon, array( 'icon', 'none' ) ) ? '' : $icon;

        // Return icon class
        return $icon;
    }
}

// Return simple fonts array
if ( ! function_exists('themesflat_plugin_google_font') ) {
    function themesflat_plugin_google_font() {
        if ( function_exists('themesflat_get_google_fonts_array') ) {
            $default = array( 0 => 'Default' );
            return array_merge( $default, themesflat_get_google_fonts_array() );
        } else {
            return array(
                'Default', 'Arial, Helvetica, sans-serif', 'Arial Black, Gadget, sans-serif',
                'Bookman Old Style, serif', 'Comic Sans MS, cursive', 'Courier, monospace',
                'Georgia, serif', 'Garamond, serif', 'Impact, Charcoal, sans-serif', 'Lucida Console, Monaco, monospace',
                'Lucida Sans Unicode, Lucida Grande, sans-serif', 'MS Sans Serif, Geneva, sans-serif', 'MS Serif, New York, sans-serif',
                'Palatino Linotype, Book Antiqua, Palatino, serif', 'Tahoma, Geneva, sans-serif', 'Times New Roman, Times, serif',
                'Trebuchet MS, Helvetica, sans-serif', 'Verdana, Geneva, sans-serif', 'Paratina Linotype', 'Trebuchet MS', 
            );
        }
    }

    function themesflat_get_google_fonts_array(){
        return array(
            'Roboto','Open Sans','Lato','Montserrat','Roboto Condensed','Oswald','Source Sans Pro','Raleway','Slabo 27px','Merriweather','Roboto Slab','PT Sans','Open Sans Condensed','Ubuntu','Noto Sans','Poppins','Playfair Display','Lora','Roboto Mono','PT Serif','Titillium Web','Muli','Arimo','Fira Sans','PT Sans Narrow','Nunito','Noto Serif','Inconsolata','Nanum Gothic','Work Sans','Dosis','Indie Flower','Crimson Text','Bitter','Josefin Sans','Quicksand','Oxygen','Libre Baskerville','Cabin','Anton','Hind','Rubik','Arvo','Fjalla One','Lobster','Libre Franklin','Exo 2','Karla','Pacifico','Varela Round','Yanone Kaffeesatz','Abel','Shadows Into Light','Dancing Script','Merriweather Sans','Source Serif Pro','Nunito Sans','Abril Fatface','Asap','Bree Serif','Acme','Ubuntu Condensed','Questrial','Hind Siliguri','Source Code Pro','Archivo Narrow','Amatic SC','Kanit','Gloria Hallelujah','Play','Signika','Exo','Comfortaa','Maven Pro','EB Garamond','Cairo','Francois One','Crete Round','Righteous','Teko','Encode Sans Condensed','PT Sans Caption','Patua One','Cinzel','Russo One','Heebo','Rokkitt','Catamaran','Cuprum','Pathway Gothic One','Vollkorn','Permanent Marker','Ropa Sans','Alegreya','Hind Madurai','Domine','Poiret One','Old Standard TT','Noticia Text','Great Vibes','Cookie','Courgette','Concert One','Kaushan Script','ABeeZee','Satisfy','Cardo','Rajdhani','Fira Sans Condensed','Orbitron','Prompt','Quattrocento Sans','Lobster Two','Assistant','Alegreya Sans','News Cycle','Archivo Black','Quattrocento','Istok Web','Philosopher','Arapey','Volkhov','Monoton','Amiri','Josefin Slab','Khand','Monda','Caveat','Tinos','Cormorant Garamond','Playfair Display SC','Sacramento','Basic','Passion One','Yantramanav','Jura','Nanum Myeongjo','Fredoka One','Didact Gothic','Alfa Slab One','Tangerine','Kalam','BenchNine','Pontano Sans','Merienda','Luckiest Guy','Handlee','Gudea','Economica','Cabin Condensed','Glegoo','Bangers','Neuton','Khula','Patrick Hand','Boogaloo','Hammersmith One','Prosto One','Sanchez','Fira Sans Extra Condensed','Cantarell','Amaranth','Armata','Forum','Ultra','Barlow','Pragati Narrow','Vidaloka','Zilla Slab','Audiowide','Gentium Basic','Marck Script','Unica One','Yrsa','Frank Ruhl Libre','Ruda','Chivo','Enriqueta','Architects Daughter','Sigmar One','Julius Sans One','Sorts Mill Goudy','Yellowtail','Barlow Semi Condensed','Kreon','Antic Slab','Prata','Hind Vadodara','Tenor Sans','Allerta','PT Mono','Oleo Script','Arsenal','Changa','Signika Negative','Bad Script','Shrikhand','Montserrat Alternates','Alice','Neucha','Advent Pro','Shadows Into Light Two','Martel','Eczar','Overpass','Lalezar','Nobile','Special Elite','Homemade Apple','Parisienne','Gentium Book Basic','Rancho','Cabin Sketch','Barlow Condensed','Fugaz One','Sintony','Arbutus Slab','Damion','Paytone One','Molengo','Covered By Your Grace','Adamina','Varela','Actor','Nothing You Could Do','Bevan','Playball','Aldrich','Bungee Inline','Scada','Do Hyeon','Chewy','Lusitana','Allura','Caveat Brush','Space Mono','Rambla','Ovo','Black Ops One','Hind Guntur','Share','Coda','Sarala','Rasa','Alegreya Sans SC','Michroma','PT Serif Caption','Alex Brush','Rock Salt','Spinnaker','Jaldi','Karma','Electrolize','Coustard','Rufina','Press Start 2P','Cantata One','Carter One','Lustria','Ubuntu Mono','Hanuman','Quantico','Syncopate','Slabo 13px','Oranienbaum','Fredericka the Great','Tajawal','Herr Von Muellerhoff','Gochi Hand','Titan One','Cousine','Halant','Just Another Hand','Mr Dafoe','Coming Soon','Squada One','Overlock','Viga','Magra','Taviraj','Lekton','Marcellus','Niconne','Spectral','Marcellus SC','Itim','Knewave','Nanum Pen Script','Kameron','Pinyon Script','Kelly Slab','Palanquin Dark','El Messiri','Days One','Archivo','Cormorant','Coda Caption','Cinzel Decorative','Jockey One','IBM Plex Sans','Alef','Fauna One','Leckerli One','Reenie Beanie','Corben','Italianno','Pridi','Unna','Palanquin','Grand Hotel','Radley','Candal','Baloo','VT323','Lateef','Rochester','Contrail One','Telex','Ceviche One','Marvel','Berkshire Swash','Skranji','Saira','Voltaire','Freckle Face','Allan','Headland One','Saira Semi Condensed','Anonymous Pro','Allerta Stencil','Scheherazade','Andika','Changa One','Carrois Gothic','Carme','Hanalei Fill','Copse','Rosario','Abhaya Libre','Saira Extra Condensed','Mitr','Antic','Goudy Bookletter 1911','Nanum Gothic Coding','Average','Reem Kufi','Nixie One','Marmelad','Alegreya SC','Nanum Brush Script','Martel Sans','Lilita One','Aclonica','Londrina Solid','Wendy One','Arima Madurai','Annie Use Your Telescope','Love Ya Like A Sister','Average Sans','Mada','GFS Didot','Kurale','Mukta','Metrophobic','Homenaje','Fira Mono','Calligraffitti','Yesteryear','Faster One','Asap Condensed','Buenard','Racing Sans One','Yatra One','Laila','Limelight','Tauri','Saira Condensed','Preahvihear','Petit Formal Script','Pangolin','Judson','Cutive','Averia Serif Libre','Cambo','Andada','Raleway Dots','Vesper Libre','Mukta Mahee','Fanwood Text','Gruppo','Caudex','Bowlby One SC','Inder','Puritan','Share Tech Mono','Balthazar','Yeseva One','Gilda Display','Bubblegum Sans','Chelsea Market','Biryani','Doppio One','Norican','Six Caps','Schoolbell','Convergence','Mukta Vaani','Graduate','Bungee','Give You Glory','Arizonia','Mr De Haviland','Trocchi','Delius','Quando','Sue Ellen Francisco','Pompiere','Ruslan Display','Merienda One','Capriola','Happy Monkey','Oxygen Mono','Rammetto One','Trirong','Montez','Zeyada','Belleza','Aladin','MedievalSharp','IM Fell Double Pica','Oregano','Kristi','Megrim','Cutive Mono','Shojumaru','Alike','The Girl Next Door','Mate','Poly','Miriam Libre','Sura','Cambay','Proza Libre','Cormorant SC','Bentham','Modern Antiqua','Mirza','Prociono','Wire One','Qwigley','Gurajada','Lemonada','Baloo Paaji','Maitree','Baumans','Fontdiner Swanky','Frijole','Encode Sans','IM Fell English','Vast Shadow','Gravitas One','Gabriela','Suez One','Seaweed Script','Duru Sans','Federo','Mako','Strait','Krona One','Secular One','Baloo Bhaina','Rouge Script','Pattaya','Elsie','Bilbo Swash Caps','Brawler','Meddon','Rozha One','Mouse Memoirs','Amiko','La Belle Aurore','IM Fell DW Pica','Expletus Sans','Short Stack','Walter Turncoat','Rye','Anaheim','Lily Script One','Tienne','Fondamento','Fjord One','Waiting for the Sunrise','Gafata','Patrick Hand SC','Delius Swash Caps','Carrois Gothic SC','Cedarville Cursive','Londrina Outline','Bowlby One','Orienta','Dorsa','Cherry Swash','Podkova','Stardos Stencil','Crafty Girls','Sofia','Clicker Script','Emilys Candy','UnifrakturMaguntia','Unkempt','Imprima','Iceland','Lemon','Port Lligat Slab','Crushed','Harmattan','Oleo Script Swash Caps','Just Me Again Down Here','IBM Plex Serif','Sriracha','NTR','Loved by the King','Belgrano','Holtwood One SC','Gothic A1','Nova Mono','David Libre','Salsa','Spirax','Eater','Vampiro One','Ledger','Scope One','Dawning of a New Day','Bungee Shade','Life Savers','Euphoria Script','Sniglet','Athiti','Finger Paint','Cantora One','Artifika','Sumana','Sunflower','Voces','Suranna','Amethysta','IM Fell English SC','Medula One','Condiment','Creepster','Vibur','Over the Rainbow','Nova Square','Bellefair','Denk One','Sansita','Cherry Cream Soda','Poller One','Wallpoet','Aguafina Script','Cormorant Infant','Kotta One','Shanti','Battambang','Rubik Mono One','Chonburi','Khmer','Slackey','Engagement','Ramabhadra','Sarpanch','McLaren','Baloo Chettan','Fenix','Alike Angular','Galada','Codystar','Geo','Share Tech','Chau Philomene One','Montserrat Subrayada','Numans','Habibi','Dynalight','Rakkas','Nova Slim','Timmana','Mukta Malar','Almendra','Averia Sans Libre','Coiny','Katibeh','Angkor','Nosifer','Markazi Text','Rationale','Buda','Flamenco','Germania One','Spectral SC','Dekko','Overpass Mono','Sail','Tulpen One','Mallanna','Mrs Saint Delafield','Stint Ultra Expanded','Simonetta','Amita','League Script','Delius Unicase','Aref Ruqaa','Port Lligat Sans','Pavanam','Cagliostro','Rosarivo','Junge','Sarina','Antic Didone','Nova Round','IBM Plex Sans Condensed','Chela One','Mate SC','Bubbler One','Milonga','Kadwa','Baloo Tamma','Kranky','Quintessential','Pirata One','Mogra','Stoke','Text Me One','Koulen','Averia Libre','Stalemate','Metamorphous','Stint Ultra Condensed','Faustina','Padauk','Linden Hill','Encode Sans Semi Condensed','Mandali','Paprika','Ranga','Overlock SC','Asul','Kite One','Princess Sofia','Mystery Quest','Baloo Bhai','Cormorant Upright','Maiden Orange','Encode Sans Semi Expanded','Jim Nightshade','IM Fell French Canon','Amarante','IBM Plex Mono','Donegal One','Mountains of Christmas','Arya','Sedgwick Ave','Italiana','Peralta','Sancreek','Ribeye','Swanky and Moo Moo','Irish Grover','Miniver','Bilbo','Sonsie One','Farsan','Joti One','Encode Sans Expanded','Inika','Ruluko','Redressed','Atma','Galindo','Esteban','Plaster','Content','Fresca','Flavors','Averia Gruesa Libre','Rhodium Libre','Englebert','Cormorant Unicase','Ranchers','Glass Antiqua','Lovers Quarrel','Baloo Bhaijaan','Revalia','Baloo Thambi','Trade Winds','Kavoon','BioRhyme','Croissant One','Uncial Antiqua','Wellfleet','Julee','Offside','Ruthie','Black Han Sans','IM Fell Great Primer','Monsieur La Doulaise','Song Myung','Della Respira','Inknut Antiqua','Eagle Lake','Griffy','Mina','Henny Penny','Ewert','Smythe','IM Fell Double Pica SC','Autour One','Chicle','Miltonian Tattoo','Gugi','Zilla Slab Highlight','Metal Mania','Jua','UnifrakturCook','Petrona','Nova Flat','IM Fell DW Pica SC','Galdeano','Monofett','Marko One','Sree Krushnadevaraya','Iceberg','Fascinate Inline','Purple Purse','Lancelot','Diplomata','Snowburst One','Trykker','Rum Raisin','Elsie Swash Caps','Oldenburg','Snippet','Manuale','Modak','Spicy Rice','Asset','Montaga','New Rocker','Jomhuria','Jacques Francois Shadow','Kavivanar','Sahitya','Piedra','Hi Melody','Akronim','Original Surfer','IM Fell French Canon SC','Bigshot One','Keania One','Siemreap','Kumar One','Tillana','Bokor','Chango','Mrs Sheppards','Meera Inimai','Odor Mean Chey','Devonshire','Diplomata SC','Goblin One','Asar','Underdog','Lakki Reddy','Kirang Haerang','Suwannaphum','Ribeye Marrow','Stalinist One','Molle','Meie Script','Barrio','Ravi Prakash','Felipa','Kantumruy','Ruge Boogie','GFS Neohellenic','Nokora','Bayon','Sunshiney','Gaegu','Ramaraja','Moul','Caesar Dressing','Nova Script','Arbutus','Astloch','Bungee Outline','Dr Sugiyama','Chathura','Jacques Francois','Romanesco','Seymour One','IM Fell Great Primer SC','Geostar Fill','Londrina Shadow','Atomic Age','Nova Oval','Bahiana','Freehand','Kenia','Almendra Display','Macondo','Almendra SC','Margarine','Smokum','Vollkorn SC','Dangrek','Fruktur','Risque','Trochut','Sedgwick Ave Display','Combo','Nova Cut','Fascinate','Passero One','Jolly Lodger','Supermercado One','Sirin Stencil','Warnes','Bigelow Rules','Baloo Da','Butterfly Kids','Bungee Hairline','Gorditas','Tenali Ramakrishna','Taprom','Sevillana','Miltonian','Geostar','Metal','Erica One','Miss Fajardose','Federant','Butcherman','Bonbon','Macondo Swash Caps','Sofadi One','Peddana','Aubrey','Mr Bedfort','Londrina Sketch','Chenla','Kdam Thmor','Libre Barcode 39 Text','Libre Barcode 39 Extended Text','Cute Font','Gamja Flower','Baloo Tammudu','Gidugu','Emblema One','Suravaram','Dhurjati','Fasthand','Unlock','Hanalei','Libre Barcode 128','Moulpali','Libre Barcode 39 Extended','Poor Story','BioRhyme Expanded','East Sea Dokdo','Libre Barcode 39','Stylish','Yeon Sung','Libre Barcode 128 Text','Kumar One Outline','Dokdo','Black And White Picture'
        );
    }    
}
