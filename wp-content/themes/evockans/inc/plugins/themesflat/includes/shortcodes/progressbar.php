<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_progressbar_shortcode_params' );

function themesflat_progressbar_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Progress Bar', 'evockans'),
        'description' => esc_html__('Displaying progress bars.', 'evockans'),
        'base' => 'progressbar',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'params' => array(
            array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Title', 'evockans'),
				'param_name' => 'title',
				'value' => esc_html__('Title', 'evockans'),
				'description' => esc_html__('Title of the ProgressBar.', 'evockans')
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Text Color', 'evockans'),
				'param_name' => 'title_color',
				'value' => '',
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Percentage', 'evockans'),
				'param_name' => 'percent',
				'value' => '90',
				'description' => esc_html__('Percentage value of the ProgressBar', 'evockans')
            ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Percentage Style', 'evockans' ),
				'param_name' => 'percent_style',
				'value'      => array(
					'Simple' => 'pstyle-1',
					'Background Accent' => 'pstyle-2',
					'Background Grey' => 'pstyle-3',
					'Background Black' => 'pstyle-4',
					'Simple Right' => 'pstyle-5',
					'None' => 'pstyle-6',
				),
				'std'		=> 'pstyle-5',
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Percentage Color', 'evockans'),
				'param_name' => 'per_color',
				'value' => '',
				'dependency' => array( 'element' => 'percent_style', 'value' => array('pstyle-1','pstyle-5') ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Height of Bars', 'evockans'),
				'param_name' => 'height',
				'value' => '10px',
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Space between Text & Line', 'evockans'),
				'param_name' => 'space_between',
				'value' => '10px',
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Rounded', 'evockans'),
				'param_name' => 'rounded',
				'value' => '',
				'description'	=> esc_html__('Ex: 5px', 'evockans'),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Margin', 'evockans'),
				'param_name' => 'margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Line 1', 'evockans'),
				'param_name' => 'line_one',
				'value' => '#2387ea',
				'group' => esc_html__( 'Line Color', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Line 2', 'evockans'),
				'param_name' => 'line_two',
				'value' => '#e9ecef',
				'group' => esc_html__( 'Line Color', 'evockans' ),
            ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Family', 'evockans' ),
				'param_name' => 'font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Weight', 'evockans' ),
				'param_name' => 'font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Font Size', 'evockans'),
				'param_name' => 'font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
        )
    ) );
}

add_shortcode( 'progressbar', 'themesflat_shortcode_progressbar' );

/**
 * progressbar shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_progressbar( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'title' => 'Title',
		'percent' => '90',
		'percent_style' => 'pstyle-5',
		'per_color' => '',
		'space_between' => '10px',
		'height' => '10px',
		'rounded' => '',
		'margin' => '',
		'line_one' => '#2387ea',
		'line_two' => '#e9ecef',
		'font_family' => 'Default',
		'font_weight' => 'Default',
		'title_color' => '',
		'font_size' => '',
		'per_font_family' => 'Default',
		'per_font_weight' => 'Default',
		'per_font_size' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'progressbar', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$font_size = intval( $font_size );
	$percent = intval( $percent );
	$space_between = intval( $space_between );
	$height = intval( $height );
	$rounded = intval( $rounded );

	$cls = $cls1 = $wrap_css = $css = $pcss = $css1 = $css2 = $tit = $per = '';
	if ( $margin ) $wrap_css .= 'margin:'. $margin .';';
	$cls = $percent_style;

	if ( empty( $percent ) ) $percent = 90;
	if ( empty( $height ) ) $height = 10;

	if ( $line_one == '#1a7dd7' ) {
		$cls1 .= ' accent';
	} else {
		if ( $line_one ) $css1 = 'background-color:'. $line_one .';';
	}

	if ( $height ) $css1 .= 'height:'. $height .'px;';

	if ( $line_two ) $css2 = 'background-color:'. $line_two .';';
	if ( $rounded ) $css2 .= 'overflow:hidden;border-radius:'. $rounded .'px;';

	if ( $font_weight != 'Default' ) $css .= 'font-weight:'. $font_weight .';';
	if ( $title_color ) $css .= 'color:'. $title_color .';';
	if ( $font_size ) $css .= 'font-size:'. $font_size .'px;';
	if ( $font_family != 'Default' ) {
		 $font_family;
		$css .= 'font-family:'. $font_family .';';
	}

	if ( $per_color ) $pcss .= 'color:'. $per_color .';';

	if ( ! empty( $title ) )
		$tit = '<h3 class="title" style="'. $css .'">'. esc_html( $title ) .'</h3>';

	if ( ! empty( $percent ) ) {		
		if ($percent >= 100) {
			$percent = 100;
		}
		if ($percent <= 0) {
			$percent = 0;
		}
		if ( $percent_style == 'pstyle-5' ) {
			$per = '<div class="perc-wrap" style="'. $css .'"><div class="perc show" style="'. $pcss .'"><span>'. $percent .'%</span></div></div>';
		}else {	
			$per = '<div class="perc-wrap" style="'. $css .'"><div class="perc" style="'. $pcss .'"><span>'. $percent .'%</span></div></div>';
		}	
				
	}

	if ( $space_between ) $css2 .= 'margin-top:'. $space_between .'px;';
	wp_enqueue_script( 'themesflat_sc_vc-appear' );

	return sprintf( '
		<div class="themesflat_sc_vc-progress clearfix %7$s" style="%6$s">%1$s %2$s
			<div class="progress-wrap" style="%4$s">
				<div class="progress-animate %8$s" data-valuemax="100" data-valuemin="0" data-valuenow="%3$s" style="%5$s">
				</div>
			</div>
		</div>', $tit, $per, $percent, $css2, $css1, $wrap_css, $cls, $cls1
	);
}