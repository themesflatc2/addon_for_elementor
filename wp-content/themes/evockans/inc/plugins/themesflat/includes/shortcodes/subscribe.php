<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_subscribe_shortcode_params' );

function themesflat_subscribe_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Subscribe Form', 'evockans'),
        'description' => esc_html__('Displaying mailchimp newsletter form.', 'evockans'),
        'base' => 'subscribe',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'params' => array(
        	array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'evockans' ),
				'param_name' => 'style',
				'value'      => array(
					'Style 1' => 'style1',
					'Style 2' => 'style2',
					'Style 3' => 'style3',
				),
				'std'		=> 'style1',
				'dependency' => array( 'element' => 'icon_display', 'value' => 'no-icon' ),
			),
        	array(
				'type' => 'textfield',
				'heading' => esc_html__('Mailchimp Form ID', 'evockans'),
				'param_name' => 'form_id',
				'value' => '',
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap: Padding', 'evockans'),
				'param_name' => 'padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 50px 57px 46px 57px', 'evockans'),
	        ),
			array(
				'type' => 'textarea',
				'holder' => 'div',
				'heading' => esc_html__( 'Sub-Heading (Optional)', 'evockans' ),
				'param_name' => 'subheading',
			),
        )
    ) );
}

add_shortcode( 'subscribe', 'themesflat_shortcode_subscribe' );

/**
 * subscribe shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_subscribe( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'style' => 'style1',
		'form_id' => '',
		'padding' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'subscribe', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	if( function_exists( 'mc4wp_show_form' ) ) {
		$form_id = intval($form_id); 
		$config = array();
		$echo = false; 
		$content = mc4wp_show_form($form_id, $config, $echo);
	} 

	$cls = $css = $html = $text_html = '';
	$cls .= $style;
	$css .= 'padding:'. $padding .';';
	

	return sprintf( '<div class="themesflat_sc_vc-subscribe clearfix %2$s" style="%3$s"><div class="form-wrap">%1$s</div></div>', $content, $cls, $css );
}