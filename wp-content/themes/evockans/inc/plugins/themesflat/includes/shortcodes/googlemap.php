<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_googlemap_shortcode_params' );

function themesflat_googlemap_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Google Maps', 'evockans'),
        'description' => esc_html__('Displaying Google Maps.', 'evockans'),
        'base' => 'googlemap',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'params' => array(
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Latitude', 'evockans' ),
				'param_name'  => 'lat',
				'value'		  => '51.5165957',
				'description' => '<a href="http://universimmedia.pagesperso-orange.fr/geo/loc.htm" target="_blank">'. esc_html__('Here is a tool', 'evockans').'</a> '. esc_html__('where you can find Latitude & Longitude of your location', 'evockans'),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Longitude', 'evockans' ),
				'param_name'  => 'lng',
				'value'		  => '-0.1277179',
				'description' => '<a href="http://universimmedia.pagesperso-orange.fr/geo/loc.htm" target="_blank">'. esc_html__('Here is a tool', 'evockans').'</a> '. esc_html__('where you can find Latitude & Longitude of your location', 'evockans'),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Map Width', 'evockans' ),
				'param_name' => 'width',
				'value'      => ''
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Map Height', 'evockans' ),
				'param_name' => 'height',
				'value'      => 350
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Zoom Level', 'evockans' ),
				'param_name'  => 'zoom',
				'description' => esc_html__( 'Select the default zoom level for the Maps', 'evockans' ),
				'value'       => array_combine( range( 1, 24 ), range( 1, 24 ) ),
				'std'		  => '12'
			),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Dragging on Mobile' ),
                'param_name' => 'drag_mobile',
                'value' => array( esc_html__( 'Enable' ) => 'true', esc_html__( 'Disable' ) => 'false'),
                'std' => 'true'
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Dragging on Desktop' ),
                'param_name' => 'drag_desktop',
                'value' => array( esc_html__( 'Enable' ) => 'true', esc_html__( 'Disable' ) => 'false'),
                'std' => 'true'
            ),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Show The Marker', 'evockans' ),
				'param_name'  => 'marker_type',
				'value'       => array(
					'Simple'          => 'simple',
					'Custom Image' => 'image',
				),
				'std'		=> 'simple',
				'group' => esc_html__( 'Maker', 'evockans' ),
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image', 'evockans'),
				'param_name' => 'image',
				'value' => '',
				'group' => esc_html__( 'Maker', 'evockans' ),
				'dependency' => array( 'element' => 'marker_type', 'value' => 'image' ),
			),
        )
    ) );
}

add_shortcode( 'googlemap', 'themesflat_shortcode_googlemap' );

/**
 * googlemap shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_googlemap( $atts, $content = null ) {
	extract( shortcode_atts( array(
        'lat' => '51.5165957',
        'lng' => '-0.1277179',
        'width' => '',
        'height' => '350',
        'zoom' => '12',
        'drag_mobile' => 'true',
        'drag_desktop' => 'true',
        'marker_type' => 'simple',
        'image' => '',
    ), $atts ) );
    $content = wpb_js_remove_wpautop($content, true);

    $css = $dragging = '';
    $width = intval( $width );
    $height = intval( $height );

    if ( $width ) $css .= 'width:'. $width .'px;';
    if ( $height ) $css .= 'height:'. $height .'px;';

    $id = "map_".uniqid();
    if ( wp_is_mobile() ) { $dragging = $drag_mobile; }
    else { $dragging = $drag_desktop; }

    if ( $image && $marker_type == 'image' )
        $image = wp_get_attachment_image_src( $atts['image'], 'full' )[0];

    $html = '
    <script type="text/javascript">
        var places = [['.$lat.', '.$lng.']];

        function themesflat_vc_gmap() {
            var mapOptions = {
                scrollwheel: false,
                draggable: '.$dragging.',
                zoom: '.$zoom.',
                center: new google.maps.LatLng('.$lat.', '.$lng.'),
                mapTypeControlOptions: {
                    mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.HYBRID , google.maps.MapTypeId.SATELLITE]
                },
                styles:[
					{
						"featureType": "water",
						"elementType": "geometry",
						"stylers": [
							{
							  "color": "#e9e9e9"
							},
							{
							  "lightness": 17
							}
						]
					},
					{
						"featureType": "landscape",
						"elementType": "geometry",
						"stylers": [
							{
							  "color": "#f5f5f5"
							},
							{
							  "lightness": 20
							}
						]
					},
					{
						"featureType": "road.highway",
						"elementType": "geometry.fill",
						"stylers": [
							{
							  "color": "#ffffff"
							},
							{
							  "lightness": 17
							}
						]
					},
					{
						"featureType": "road.highway",
						"elementType": "geometry.stroke",
						"stylers": [
							{
							  "color": "#ffffff"
							},
							{
							  "lightness": 29
							},
							{
							  "weight": 0.2
							}
						]
					},
					{
						"featureType": "road.arterial",
						"elementType": "geometry",
						"stylers": [
							{
							  "color": "#ffffff"
							},
							{
							  "lightness": 18
							}
						]
					},
					{
						"featureType": "road.local",
						"elementType": "geometry",
						"stylers": [
							{
							  "color": "#ffffff"
							},
							{
							  "lightness": 16
							}
						]
					},
					{
						"featureType": "poi",
						"elementType": "geometry",
						"stylers": [
							{
							  "color": "#f5f5f5"
							},
							{
							  "lightness": 21
							}
						]
					},
					{
						"featureType": "poi.park",
						"elementType": "geometry",
						"stylers": [
							{
							  "color": "#dedede"
							},
							{
							  "lightness": 21
							}
						]
					},
					{
						"elementType": "labels.text.stroke",
						"stylers": [
							{
							  "visibility": "on"
							},
							{
							  "color": "#ffffff"
							},
							{
							  "lightness": 16
							}
						]
					},
					{
						"elementType": "labels.text.fill",
						"stylers": [
							{
							  "saturation": 36
							},
							{
							  "color": "#333333"
							},
							{
							  "lightness": 40
							}
						]
					},
					{
						"elementType": "labels.icon",
						"stylers": [
							{
							  "visibility": "off"
							}
						]
					},
					{
						"featureType": "transit",
						"elementType": "geometry",
						"stylers": [
							{
							  "color": "#f2f2f2"
							},
							{
							  "lightness": 19
							}
						]
					},
					{
						"featureType": "administrative",
						"elementType": "geometry.fill",
						"stylers": [
							{
							  "color": "#fefefe"
							},
							{
							  "lightness": 20
							}
						]
					},
					{
						"featureType": "administrative",
						"elementType": "geometry.stroke",
						"stylers": [
							{
							  "color": "#fefefe"
							},
							{
							  "lightness": 17
							},
							{
							  "weight": 1.2
							}
						]
					}
                ],

            }

            var map = new google.maps.Map(document.getElementById("'.$id.'"), mapOptions);
           
            setMarkers(map, places);
        }

        function setMarkers(map, locations) {
            for (var i = 0; i < locations.length; i++) {
                var place = locations[i];
                var myLatLng = new google.maps.LatLng(place[0], place[1]);
                var marker = new google.maps.Marker( {
                    position: myLatLng,
                    map: map,
                    icon: "'.$image.'",
                    zIndex: place[2],
                    animation: google.maps.Animation.DROP
                });

                google.maps.event.addListener(marker, "click", function () {
                    infowindow.setContent(decodeURIComponent(this.html));
                    infowindow.open(map, this);
                });
            }
        }

        google.maps.event.addDomListener(window, "load", themesflat_vc_gmap);
    </script>';

    return sprintf('%1$s<div id="%2$s" class="themesflat_sc_vc-google-map" style="%3$s"></div>', $html, $id, $css );
}