<?php
/**
 * Register filter for append custom class name
 * that generated from visual-composer
 */
class WPBakeryShortCode_rotatebox extends WPBakeryShortCodesContainer {}
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_rotatebox_shortcode_params' );

function themesflat_rotatebox_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Rotate Box', 'evockans'),
        'description' => esc_html__('Empty space with custom height.', 'evockans'),
        'base' => 'rotatebox',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'as_parent' => array('only' => 'rotateboxprimary, rotateboxhover'),
        'controls' => 'full',
		'show_settings_on_create' => true,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'js_view' => 'VcColumnView',
        'params' => array(  
        	array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Primary Title Alignment', 'evockans' ),
				'param_name' => 'alignment',
				'value'      => array(
					'Left' => 'text-left',
					'Center' => 'text-center',
					'Right' => 'text-right',
				),
				'std'		=> 'text-center',
			),  
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Height', 'evockans'),
				'param_name' => 'height',
				'value' => '350px',
				'description'	=> esc_html__('Ex: 5px', 'evockans'),
	        ),
        )
    ) );
}

add_shortcode( 'rotatebox', 'themesflat_shortcode_rotatebox' );

/**
 * rotatebox shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_rotatebox( $atts, $content = null ) {
	extract( shortcode_atts( array(		
	    'alignment' => 'text-center',
	    'height' => '350px'
	), $atts ) );	
	//extract($atts = vc_map_get_attributes( 'rotatebox', $atts ));
	$content = wpb_js_remove_wpautop($content, true);	

	$css = '';

	$height = intval( $height );

	if ( $height ) $css = 'min-height:' . $height . 'px;';	

	return sprintf( '
		<div class="themesflat_sc_vc-rotatebox clearfix">
			<div class="flipcard %1$s" style="%3$s">
				%2$s
			</div>
		</div>',
		$alignment,
		do_shortcode($content),
		$css
	);
}