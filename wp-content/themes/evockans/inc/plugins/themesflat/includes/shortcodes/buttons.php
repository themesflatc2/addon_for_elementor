<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_buttons_shortcode_params' );

function themesflat_buttons_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Buttons', 'evockans'),
        'description' => esc_html__('Advanced Buttons.', 'evockans'),
        'base' => 'buttons',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'params' => array(
            array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Text', 'evockans'),
				'param_name' => 'text',
				'value' => 'Button Text',
            ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'evockans' ),
				'param_name' => 'style',
				'value'      => array(
					'Accent' => 'accent',
					'Dark' => 'dark',
					'Light' => 'light',
					'Very Light' => 'very-light',
					'White' => 'white',
					'Outline' => 'outline',
					'Outline Dark' => 'outline_dark',
					'Outline Light' => 'outline_light',
					'Outline Very light' => 'outline_very-light',
					'Outline White' => 'outline_white',
					'Design your own' => 'custom',
				),
				'std'		=> 'accent',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Size', 'evockans' ),
				'param_name' => 'size',
				'value'      => array(
					'Default' => '',					
					'Big' => 'big',
					'Small' => 'small',
					'Extra Small' => 'xsmall',
				),
				'std'		=> '',
				'dependency' => array(
					'element' => 'style',
					'value' => array (
						'accent',
						'dark',
						'light',
						'very-light',
						'white',
						'outline',
						'outline_dark',
						'outline_light',
						'outline_very-light',
						'outline_white'
					)
				),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Alignment', 'evockans' ),
				'param_name' => 'alignment',
				'value'      => array(
					'Left' => '',
					'Center' => 'text-center',
					'Right' => 'text-right',
				),
				'std'		=> '',
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Padding', 'evockans'),
				'param_name' => 'padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Ex: 13px 40px 13px 40px', 'evockans'),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Background Color', 'evockans'),
				'param_name' => 'background_color',
				'value' => '',
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Hover: Background Color', 'evockans'),
				'param_name' => 'hover_background_color',
				'value' => '',
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Border Color', 'evockans'),
				'param_name' => 'border_color',
				'value' => '',
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Hover: Border Color', 'evockans'),
				'param_name' => 'hover_border_color',
				'value' => '',
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Text Color', 'evockans'),
				'param_name' => 'text_color',
				'value' => '',
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Hover: Text Color', 'evockans'),
				'param_name' => 'hover_text_color',
				'value' => '',
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Rounded', 'evockans'),
				'param_name' => 'rounded',
				'value' => '',
				'description'	=> esc_html__('ex: 10px', 'evockans'),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Border Width', 'evockans'),
				'param_name' => 'border_width',
				'value' => '',
				'description'	=> esc_html__('Default: 2px', 'evockans'),
				'dependency' => array(
					'element' => 'style',
					'value' => array (
						'custom',
						'outline',
						'outline_dark',
						'outline_light',
						'outline_very-light',
						'outline_white'
					)
				),
	        ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Border Style', 'evockans' ),
				'param_name' => 'border_style',
				'value'      => array(
					'None' => 'none',
					'Solid' => 'solid',
					'Dotted' => 'dotted',
					'Dashed' => 'dashed',
					'Double' => 'double',
				),
				'std'		=> 'none',
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Margin', 'evockans'),
				'param_name' => 'margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
	        ),
	        array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Button Inline?', 'evockans' ),
				'param_name' => 'button_inline',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
			),
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Full-width Button?', 'evockans' ),
				'param_name' => 'full_width',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
			),
			// Icon
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'evockans' ),
				'param_name' => 'icon_type',
				'description' => esc_html__( 'Select icon library.', 'evockans' ),
				'value' => array(
					esc_html__( 'None', 'evockans' ) => '',
					esc_html__( 'Mono Social', 'evockans' ) => 'monosocial',
					esc_html__( 'Simple Line', 'evockans' ) => 'simpleline',
					esc_html__( 'Font Ionicons', 'evockans' ) => 'ionicons',
					esc_html__( 'Font Elegant', 'evockans' ) => 'eleganticons',
					esc_html__( 'Font Themify Icons', 'evockans' ) => 'themifyicons',
					esc_html__( 'Font Icomoon Icons', 'evockans' ) => 'icomoon',
					esc_html__( 'FontAwesome', 'evockans' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'evockans' ) => 'openiconic',
					esc_html__( 'Typicons', 'evockans' ) => 'typicons',
					esc_html__( 'Entypo', 'evockans' ) => 'entypo',
					esc_html__( 'Linecons', 'evockans' ) => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_monosocial',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'monosocial',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'monosocial',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_simpleline',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'simpleline',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'simpleline',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_ionicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'ionicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'ionicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_eleganticons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'eleganticons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'eleganticons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_themifyicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'themifyicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'themifyicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_icomoon',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'icomoon',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'icomoon',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon',
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'fontawesome',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_openiconic',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'openiconic',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'openiconic',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_typicons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'typicons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'typicons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_entypo',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'entypo',
					'iconsPerPage' => 300,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'entypo',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_linecons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'linecons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon Color', 'evockans'),
				'param_name' => 'icon_color',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Font Size', 'evockans'),
				'param_name' => 'icon_font_size',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
	        ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Icon Position', 'evockans' ),
				'param_name' => 'icon_position',
				'value'      => array(
					'Icon Left' => 'icon-left',
					'Icon Right' => 'icon-right',
				),
				'std'		=> 'icon-right',
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon: Right Padding', 'evockans'),
				'param_name' => 'icon_right_padding',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'description'	=> esc_html__('Spacing between the icon and the text. Ex: 40px.', 'evockans'),
				'dependency' => array( 'element' => 'icon_position', 'value' => 'icon-left' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Left Padding', 'evockans'),
				'param_name' => 'icon_left_padding',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'description'	=> esc_html__('Spacing between the icon and the text. Ex: 40px.', 'evockans'),
				'dependency' => array( 'element' => 'icon_position', 'value' => 'icon-right' ),
	        ),
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Enable separate line?', 'evockans' ),
				'param_name' => 'separate',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array(
					'element' => 'style',
					'value' => array (
						'accent',
						'dark',
					)
				),
			),
	        // Hyperlink
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Link (URL):', 'evockans'),
				'param_name' => 'link_url',
				'value' => '',
				'group' => esc_html__( 'Hyperlink', 'evockans' ),
            ),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Open Link in a new Tab', 'evockans' ),
				'param_name' => 'new_tab',
				'group' => esc_html__( 'Hyperlink', 'evockans' ),
				'value' => array(
					'Yes' => 'yes',
					'No' => 'no',
				),
				'std'		=> 'yes',
			),
	        // Typography
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Family', 'evockans' ),
				'param_name' => 'font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Weight', 'evockans' ),
				'param_name' => 'font_weight',
				'value'      => array(
					'Default' => 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Font Size', 'evockans'),
				'param_name' => 'font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Line-Height', 'evockans'),
				'param_name' => 'line_height',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Extra class name', 'evockans'),
				'param_name' => 'class',
				'value' => '',
				'group' => esc_html__( 'Class', 'evockans' ),
	        ),
        )
    ) );
}

add_shortcode( 'buttons', 'themesflat_shortcode_buttons' );

/**
 * buttons shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_buttons( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'text' => 'Button Text',
		'style' => 'accent',
		'size' => '',
		'alignment' => '',
		'padding' => '',
		'background_color' => '',
		'hover_background_color' => '',
		'border_color' => '',
		'hover_border_color' => '',
		'text_color' => '',
		'hover_text_color' => '',
		'rounded' => '',
		'border_width' => '',
		'border_style' => 'none',
		'margin' => '',
		'button_inline' => '',
		'full_width' => '',
		'icon_type' => '',
		'icon' => '',
		'icon_color' => '',
		'icon_font_size' => '',
		'icon_position' => 'icon-right',
		'icon_left_padding' => '',
		'icon_right_padding' => '',
		'separate' => '',
		'font_family' => 'Default',
		'font_weight' => 'Default',
		'font_size' => '',
		'line_height' => '',
		'link_url' => '',
		'new_tab' => 'yes',
		'class' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'buttons', $atts ));

	$rounded = intval( $rounded );
	$border_width = intval( $border_width );
	$icon_font_size = intval( $icon_font_size );
	$icon_right_padding = intval( $icon_right_padding );
	$icon_left_padding = intval( $icon_left_padding );
	$font_size = intval( $font_size );
	$line_height = intval( $line_height );

	$css = $icon_html = $icon_css = $wrap_css = $btn_html = $inner_css = $btn_css_js = '';
	$cls = $size .' '. $style;

	$wrap_cls = $icon_position;
	if ( $separate ) $wrap_cls .= ' separate';
	if ( $class ) $wrap_cls .= ' ' . $class;

	if ( $alignment == 'text-center' ) { 
		$wrap_css .= 'text-align:center;'; 
	} else if ( $alignment == 'text-right' ) {
		$wrap_css .= 'text-align:right;';
	}
	if ( $button_inline ) { $wrap_css .= 'display:inline-block;'; }

	if ( $margin ) $wrap_css .= 'margin:'. $margin .';';	
	if ( $full_width ) $css .= 'display:block; text-align:center;';

	if ( $style == 'dark' ) $cls .= ' dark';
	if ( $style == 'light' ) $cls .= ' light';
	if ( $style == 'very-light' ) $cls .= ' very-light';
	if ( $style == 'white' ) $cls .= ' white';
	if ( $style == 'outline' ) $cls .= ' outline ol-accent';
	if ( $style == 'outline_dark' ) $cls .= ' outline dark';
	if ( $style == 'outline_light' ) $cls .= ' outline light';
	if ( $style == 'outline_very-light' ) $cls .= ' outline very-light';
	if ( $style == 'outline_white' ) $cls .= ' outline white';

	if ( $style == 'custom' ) {
		if ( $padding ) $css .= 'padding:'. $padding .';';
		if ( $background_color ) $css .= 'background-color:'. $background_color .';';
		if ( $border_color ) $css .= 'border-color:'. $border_color .';';
		if ( $text_color ) $css .= 'color:'. $text_color .';';
		if ( $icon_color ) $icon_css .= 'color:'. $icon_color .';';
	}

	$icon = themesflat_sc_vc_get_icon_class( $atts, 'icon' );
	if ( $icon && $icon_type != '' ) {
		$wrap_cls .= ' has-icon';
		vc_icon_element_fonts_enqueue( $icon_type );

		if ( $icon_font_size ) $icon_css .= 'font-size:'. $icon_font_size .'px;';
		$icon_html = sprintf('<span class="icon" style="%2$s"><i class="%1$s"></i></span>', $icon, $icon_css );
	}

	if ( $font_weight != 'Default' ) $css .= 'font-weight:'. $font_weight .';';
	if ( $font_size ) $css .= 'font-size:'. $font_size .'px;';
	if ( $line_height ) $css .= 'line-height:'. $line_height .'px;';
	if ( $rounded ) $css .= 'border-radius:'. $rounded .'px;';
	if ( $border_width ) $css .= 'border-width:'. $border_width .'px;';
	if ( $style == 'custom' ) {
		if ( $border_style ) $css .= 'border-style:'. $border_style .';';
		$cls .= ' '.$border_style;
	}	
	if ( $font_family != 'Default' ) {
		$css .= 'font-family:'. $font_family .';';
	}

	if ( $icon_left_padding ) $inner_css = 'padding-right:'. $icon_left_padding .'px;';
	if ( $icon_right_padding ) $inner_css = 'padding-left:'. $icon_right_padding .'px;';

	$new_tab = $new_tab == 'yes' ? '_blank' : '_self';

	if ( $hover_background_color ) $btn_css_js .= 'data-btn_bgcolor_hover="'. $hover_background_color .'" ';
	if ( $background_color ) $btn_css_js .= 'data-btn_bgcolor="'. $background_color .'" ';
	if ( $text_color ) $btn_css_js .= 'data-btn_color="'. $text_color .'" ';
	if ( $hover_text_color ) $btn_css_js .= 'data-btn_color_hover="'. $hover_text_color .'" ';
	if ( $hover_border_color ) $btn_css_js .= 'data-btn_bordercolor_hover="'. $hover_border_color .'" ';
	if ( $border_color ) $btn_css_js .= 'data-btn_bordercolor="'. $border_color .'" ';
	

	$btn_html = sprintf(
		'<a href="%5$s" target="%6$s" class="themesflat_sc_vc-button %1$s" style="%2$s" %8$s>
			<span style="%4$s">%7$s %3$s</span>
		</a>',
		esc_attr( $cls ),
		esc_attr( $css ),
		$icon_html,
		esc_attr( $inner_css ),
		esc_attr( $link_url ),
		esc_attr( $new_tab ),
		esc_html( $text ),
		$btn_css_js
	);

	return sprintf( '<div class="button-wrap %1$s" style="%2$s">%3$s</div>',
		esc_attr( $wrap_cls ),
		esc_attr( $wrap_css ),
		$btn_html
	);
}