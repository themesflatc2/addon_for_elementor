<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_spacing_shortcode_params' );

function themesflat_spacing_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Spacing', 'evockans'),
        'description' => esc_html__('Empty space with custom height.', 'evockans'),
        'base' => 'spacing',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'params' => array(
	        array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Desktop: Height', 'evockans'),
				'param_name' => 'desktop_height',
				'value' => '90',
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Mobile: Height', 'evockans'),
				'param_name' => 'mobile_height',
				'value' => '60',
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Small Mobile: Height', 'evockans'),
				'param_name' => 'smobile_height',
				'value' => '60',
	        ),
        )
    ) );
}

add_shortcode( 'spacing', 'themesflat_shortcode_spacing' );

/**
 * spacing shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_spacing( $atts, $content = null ) {
	extract( shortcode_atts( array(
	    'desktop_height' => '90',
	    'mobile_height' => '60',
	    'smobile_height' => '60'
	), $atts ) );	
	//extract($atts = vc_map_get_attributes( 'spacing', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$desktop_height = intval( $desktop_height );
	$mobile_height = intval( $mobile_height );
	$smobile_height = intval( $smobile_height );

	return sprintf( '
		<div class="themesflat_sc_vc-spacer clearfix" data-desktop="%1$s" data-mobi="%2$s" data-smobi="%3$s"></div>',
		esc_html( $desktop_height ),
		esc_attr( $mobile_height ),
		esc_attr( $smobile_height )
	);
}