<?php
/**
 * Register filter for append custom class name
 * that generated from visual-composer
 */
class WPBakeryShortCode_dtabs extends WPBakeryShortCodesContainer {}
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_dtabs_shortcode_params' );

function themesflat_dtabs_shortcode_params() {
	vc_map( array(
		'name'        => esc_html__( 'Tabs', 'evockans' ),
        'description' => esc_html__('Displaying Tabs Content.', 'evockans'),
		'base'        => 'dtabs',
		'weight'	=>	180,
		'icon' => THEMESFLAT_ICON,
		'category' => esc_html__('THEMESFLAT', 'evockans'),
		'as_parent' => array( 'only' => 'dtab' ),
		'controls' => 'full',
		'show_settings_on_create' => true,		
		'js_view' => 'VcColumnView',
		'params' => array(
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'evockans' ),
				'param_name' => 'style',
				'value'      => array(
					'Horizontal Style 1' => 'style-1',
					'Horizontal Style 2' => 'style-2',
					'Vertical Style 3' => 'style-3',
					'Vertical Style 4' => 'style-4',
				),
				'std'		=> 'style-1',
			),
	        array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Title: Width', 'evockans' ),
				'param_name' => 'title_width',
				'value'      => array(
					'150px' => 'w150',
					'160px' => 'w160',
					'170px' => 'w170',
					'180px' => 'w180',
					'190px' => 'w190',
					'200px' => 'w200',
					'220px' => 'w220',
					'240px' => 'w240',
					'260px' => 'w260',
					'280px' => 'w280',
					'300px' => 'w300',
				),
				'std'		=> 'w190',
				'dependency' => array( 'element' => 'style', 'value' => array('style-3','style-4')),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap Title: Rounded', 'evockans'),
				'param_name' => 'wrap_title_rounded',
				'value' => '',
				'description'	=> esc_html__('Ex: 5px', 'evockans'),
				'group' => esc_html__( 'Wrap Title', 'evockans' ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap Title: Margin Bottom', 'evockans'),
				'param_name' => 'wrap_title_margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Wrap Title', 'evockans' ),
	        ),	
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Title: Color', 'evockans'),
				'param_name' => 'title_color',
				'value' => '#111111',
				'group' => esc_html__( 'Wrap Title', 'evockans' ),
            ),            
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Title: Color Hover', 'evockans'),
				'param_name' => 'title_color_hover',
				'value' => '#2387ea',
				'group' => esc_html__( 'Wrap Title', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Title: Color Active', 'evockans'),
				'param_name' => 'title_color_active',
				'value' => '#2387ea',
				'group' => esc_html__( 'Wrap Title', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Title: Background Color', 'evockans'),
				'param_name' => 'title_bg_color',
				'value' => '#ffffff',
				'group' => esc_html__( 'Wrap Title', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Title: Background Color Hover', 'evockans'),
				'param_name' => 'title_bg_color_hover',
				'value' => '',
				'group' => esc_html__( 'Wrap Title', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Title: Background Color Active', 'evockans'),
				'param_name' => 'title_bg_color_active',
				'value' => '',
				'group' => esc_html__( 'Wrap Title', 'evockans' ),
            ),        
	        // Box Shadow
	        array(
				'type' => 'headings',
				'text' => esc_html__('Wrap Title', 'evockans'),
				'param_name' => 'wrap_title_box_shadow',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Horizontal (Required)', 'evockans'),
				'param_name' => 'horizontal',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Vertical (Required)', 'evockans'),
				'param_name' => 'vertical',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 20px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Blur (Required)', 'evockans'),
				'param_name' => 'blur',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 40px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Spread (Required)', 'evockans'),
				'param_name' => 'spread',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Box Shadow Color (Required)', 'evockans'),
				'param_name' => 'shadow_color',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: #eeeeee', 'evockans'),
            ),
		)
	) );
}

add_shortcode( 'dtabs', 'themesflat_shortcode_dtabs' );

/**
 * dtabs shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_dtabs( $atts, $content = null ) {
	extract( shortcode_atts( array(
	    'style' => 'style-1',
	    'title_width' => 'w190',
	    'wrap_title_rounded' => '',
	    'wrap_title_margin' => '',
	    'horizontal' => '',
        'vertical' => '',
        'blur' => '',
        'spread' => '',
        'shadow_color' => '',
        'title_color' => '#111111',
		'title_bg_color' => '#ffffff',
		'title_color_hover' => '#2387ea',
		'title_color_active' => '#2387ea',
		'title_bg_color_hover' => '',
		'title_bg_color_active'	=> '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'dtabs', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$wrap_title_rounded = intval($wrap_title_rounded);

	$cls = $style;
	$tab_title_css = $title_css_js = '';

	if ( $style == 'style-3' || $style == 'style-4' ){
		$cls .= ' title-'. $title_width;
	}

	if ( $wrap_title_rounded ) { $tab_title_css .= 'border-radius:' . $wrap_title_rounded . 'px;'; }
	if ( $wrap_title_margin ) { $tab_title_css .= 'margin:' . $wrap_title_margin . ';'; }
	if ( $horizontal && $vertical && $blur && $spread && $shadow_color ){
    	$tab_title_css .= 'box-shadow:'. $horizontal .' '. $vertical .' '. $blur .' '. $spread .' '. $shadow_color .';';
	}

	//color js	
	if ( $title_color ) $title_css_js .= 'data-color="' . $title_color . '" ';
	if ( $title_color_hover ) $title_css_js .= 'data-colorhover="' . $title_color_hover . '" ';
	if ( $title_color_active ) $title_css_js .= 'data-coloractive="' . $title_color_active . '" ';
	if ( $title_bg_color ) $title_css_js .= 'data-bgcolor="' . $title_bg_color . '" ';
	if ( $title_bg_color_hover ) $title_css_js .= 'data-bgcolorhover="' . $title_bg_color_hover . '" ';
	if ( $title_bg_color_active ) $title_css_js .= 'data-bgcoloractive="' . $title_bg_color_active . '" ';

	return sprintf( '
		<div class="themesflat_sc_vc-tabs clearfix %2$s">
		<ul class="tab-title clearfix" style="%3$s" %4$s></ul>
		<div class="tab-content-wrap">%1$s</div></div>',
		do_shortcode($content),
		$cls,
		$tab_title_css,
		$title_css_js
	);
}