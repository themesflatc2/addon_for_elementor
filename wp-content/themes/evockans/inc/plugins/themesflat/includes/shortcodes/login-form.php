<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_loginform_shortcode_params' );

function themesflat_loginform_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Login Form', 'evockans'),
        'description' => esc_html__('Displaying Login Form.', 'evockans'),
        'base' => 'loginform',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'params' => array(
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Login Form: Padding', 'evockans'),
				'param_name' => 'padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Login Form: Margin', 'evockans'),
				'param_name' => 'margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
	        ),
        )
    ) );
}

add_shortcode( 'loginform', 'themesflat_shortcode_loginform' );

/**
 * loginform shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_loginform( $atts, $content = null ) {
	extract( shortcode_atts( array(
	    'padding' => '',
	    'margin' => '',
	), $atts ) );	
	//extract($atts = vc_map_get_attributes( 'loginform', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$css = '';

	if ( $padding ) $css .= 'padding:' . $padding . ';';
	if ( $margin ) $css .= 'margin:' . $margin . ';';

	$defaults = array(  
		'redirect' =>  site_url( $_SERVER['REQUEST_URI'] )
        );

    extract(shortcode_atts($defaults, $atts)); 
    if (!is_user_logged_in()) {   
	    $content .= sprintf('
	    <div class="themesflat_sc_vc-loginform clearfix" style="%1$s">'
			. wp_login_form( array( 'echo' => false, 'redirect' => $redirect ) ) . 
		    '<p class="reset-password">Forgot your password? <a href="'. wp_lostpassword_url() .'" class="underline">Reset Password</a></p>
			<p class="create-account">Don\'t have an account? <a href="'. wp_registration_url() .'" class="underline">Create account</a></p>
		</div>', $css);
	}
    
    return $content;

}