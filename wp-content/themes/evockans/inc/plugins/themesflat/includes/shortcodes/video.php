<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_video_shortcode_params' );

function themesflat_video_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Video', 'evockans'),
        'description' => esc_html__('Empty space with custom height.', 'evockans'),
        'base' => 'video',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'params' => array(
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Link Youtube/Vimeo (URL)', 'evockans'),
				'param_name' => 'video_url',
				'value' => 'https://player.vimeo.com/video/176916362',
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Video Frame: Width', 'evockans' ),
				'param_name' => 'frame_width',
				'value' => '100%',
				'description'	=> esc_html__('Ex: 100% or 500px', 'evockans'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Video Frame: Height', 'evockans' ),
				'param_name' => 'frame_height',
				'value' => '320px',
				'description'	=> esc_html__('Ex: 350px', 'evockans'),
			),
        )
    ) );
}

add_shortcode( 'video', 'themesflat_shortcode_video' );

/**
 * video shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_video( $atts, $content = null ) {
	extract( shortcode_atts( array(
	    'video_url' => 'https://player.vimeo.com/video/176916362',
	    'frame_width' => '100%',
	    'frame_height' => '320px'
	), $atts ) );	
	//extract($atts = vc_map_get_attributes( 'video', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	
	if ( strpos($frame_width,"%") ) {
		$frame_width = intval( $frame_width ) . '%';
	} else {
		$frame_width = intval( $frame_width );
	}

	$frame_height = intval( $frame_height );	
	
	return sprintf( '
		<div class="themesflat_sc_vc-video clearfix">
		<iframe width="%2$s" height="%3$s" src="%1$s"></iframe>
		</div>
		',
		$video_url,
		$frame_width,
		$frame_height
	);	
	
}