<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_countdown_shortcode_params' );

function themesflat_countdown_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('CountDown', 'evockans'),
        'description' => esc_html__('Displaying Countdown Timer.', 'evockans'),
        'base' => 'countdown',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'params' => array(
        	array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Number And Text Position', 'evockans' ),
				'param_name' => 'style',
				'value'      => array(
					'Default' => 'default',
					'Inline' => 'inline',
				),
				'std'		=> 'accent',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Width Item Time', 'evockans'),
				'param_name' => 'width_item',
				'value' => '',
				'description'	=> esc_html__('% Or px ex: 18%.', 'evockans'),
	        ),			
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Padding', 'evockans'),
				'param_name' => 'padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Margin', 'evockans'),
				'param_name' => 'margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Color', 'evockans'),
				'param_name' => 'color',
				'value' => '',
            ),	
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Background Color', 'evockans'),
				'param_name' => 'background_color',
				'value' => '',
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Rounded', 'evockans'),
				'param_name' => 'rounded',
				'value' => '',
				'description'	=> esc_html__('ex: 10px', 'evockans'),
            ),
            array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Full-width Box Countdown?', 'evockans' ),
				'param_name' => 'full_width',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'dependency' => array(
					'element' => 'style',
					'value' => array (
						'default'
					)
				),
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Time', 'evockans' ),
				'param_name' => 'time',
				'value' => '2021/01/01',
				'description'	=> esc_html__('Year/Month/Day', 'evockans'),
				'group' => esc_html__( 'Time', 'evockans' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Time Margin', 'evockans'),
				'param_name' => 'time_margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
				'group' => esc_html__( 'Time', 'evockans' ),
	        ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Prefix Text', 'evockans'),
				'param_name' => 'prefix_text',
				'value' => '',
				'description'	=> esc_html__('ex: 10px', 'evockans'),
				'group' => esc_html__( 'Text', 'evockans' ),
				'dependency' => array(
					'element' => 'style',
					'value' => array (
						'inline'
					)
				),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Text Margin', 'evockans'),
				'param_name' => 'margin_text',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 10px 0px 0px 0px', 'evockans'),
				'group' => esc_html__( 'Text', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('DAY', 'evockans'),
				'param_name' => 'day',
				'value' => 'Day',
				'group' => esc_html__( 'Text', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('HOURS', 'evockans'),
				'param_name' => 'hours',
				'value' => 'Hours',
				'group' => esc_html__( 'Text', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('MINUTES', 'evockans'),
				'param_name' => 'minutes',
				'value' => 'Minutes',
				'group' => esc_html__( 'Text', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('SECONDS', 'evockans'),
				'param_name' => 'seconds',
				'value' => 'Seconds',
				'group' => esc_html__( 'Text', 'evockans' ),
	        ),
            // Typography
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Time Font Family', 'evockans' ),
				'param_name' => 'time_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Time Font Weight', 'evockans' ),
				'param_name' => 'time_font_weight',
				'value'      => array(
					'Default' => 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Time Font Size', 'evockans'),
				'param_name' => 'time_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Time Line Height', 'evockans'),
				'param_name' => 'time_line_height',
				'value' => '',
				'description'	=> esc_html__('24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Text Font Family', 'evockans' ),
				'param_name' => 'text_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Text Font Weight', 'evockans' ),
				'param_name' => 'text_font_weight',
				'value'      => array(
					'Default' => 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Text Font Size', 'evockans'),
				'param_name' => 'text_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Text Line Height', 'evockans'),
				'param_name' => 'text_line_height',
				'value' => '',
				'description'	=> esc_html__('24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),	
        )
    ) );
}

add_shortcode( 'countdown', 'themesflat_shortcode_countdown' );

/**
 * countdown shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_countdown( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'style' => '',
	    'time' => '2021/01/01',	
	    'width_item' => '',
	    'padding' => '',
	    'margin' => '',
	    'color' => '',
	    'background_color' => '',
	    'rounded' => '',    
	    'full_width' => '',	    
	    'time_font_family' => '',
	    'time_font_weight' => '',
	    'time_font_size' => '',
	    'time_line_height' => '',
	    'time_margin' => '',
	    'text_font_family' => '',
	    'text_font_weight' => '',
	    'text_font_size' => '',
	    'text_line_height' => '',
	    'day' => 'Day',
	    'hours' => 'Hours',
	    'minutes' => 'Minutes',
	    'seconds' => 'Seconds',
	    'prefix_text' => '',
	    'margin_text' => '',	    
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'countdown', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$cls = $style;

	$column_css = $numb_text_css = $numb_css = $text_css = $prefix = '';

	$time_font_size = intval($time_font_size);
	$text_font_size = intval($text_font_size);
	$rounded = intval($rounded);

	if ( $full_width ) 			$column_css .= 'display:block; text-align:center;';
	//if ( $width_item ) 			$column_css .= 'width:'. $width_item .';';
	if ( $padding ) 			$column_css .= 'padding:'. $padding .';';
	if ( $margin ) 				$column_css .= 'margin:'. $margin .';';
	if ( $background_color ) 	$column_css .= 'background-color:'. $background_color .';';
	if ( $rounded ) 			$column_css .= 'border-radius:'. $rounded .'px;';

	if ( $color ) $numb_text_css .= 'color:'. $color .';';

	if ( $time_margin )      $numb_css .= 'margin:'. $time_margin .';';
	if ( $time_font_family ) $numb_css .= 'font-family:'. $time_font_family .';';
	if ( $time_font_weight ) $numb_css .= 'font-weight:'. $time_font_weight .';';
	if ( $time_font_size )   $numb_css .= 'font-size:'. $time_font_size .'px;';
	if ( $time_line_height ) $numb_css .= 'line-height:'. $time_line_height .';';	
	
	if ( $margin_text )      $text_css .= 'margin:'. $margin_text .';';
	if ( $text_font_family ) $text_css .= 'font-family:'. $text_font_family .';';
	if ( $text_font_weight ) $text_css .= 'font-weight:'. $text_font_weight .';';
	if ( $text_font_size )   $text_css .= 'font-size:'. $text_font_size .'px;';
	if ( $text_line_height ) $text_css .= 'line-height:'. $text_line_height .';';	


	if ($prefix_text) $prefix .='<div class="prefix">'. $prefix_text .'<div>';

	if ( $time ) {
		wp_enqueue_script( 'themesflat_sc_vc-countdown' );

		return sprintf( '
		<div class="themesflat_sc_vc-countdown clearfix %1$s">
			<div id="countdown" class="countdown clearfix" data-date="%2$s">
				<div class="row">
					<div class="col-md-3 col-sm-6 col-12 wrap-column">
		                <div class="column days" style="%3$s">
		                   <div class="numb time-day" style="%4$s%5$s"></div>%7$s
		                   <div class="text" style="%4$s%6$s">%8$s</div>
		                </div>
		            </div>
		            <div class="col-md-3 col-sm-6 col-12 wrap-column">
		                <div class="column hours" style="%3$s">%7$s
		                   <div class="numb time-hours" style="%4$s%5$s"></div>
		                   <div class="text" style="%4$s%6$s">%9$s</div>
		                </div>
					</div>
		            <div class="col-md-3 col-sm-6 col-12 wrap-column">
		                <div class="column mins" style="%3$s">%7$s
		                   <div class="numb time-mins" style="%4$s%5$s"></div>
		                   <div class="text" style="%4$s%6$s">%10$s</div>
		                </div>
		            </div>
		            <div class="col-md-3 col-sm-6 col-12 wrap-column">
		                <div class="column secs" style="%3$s">%7$s
		                   <div class="numb time-secs" style="%4$s%5$s"></div>
		                   <div class="text" style="%4$s%6$s">%11$s</div>
		                </div>
					</div>
				</div>
            </div>
		</div>', $cls, esc_html( $time ), $column_css, $numb_text_css, $numb_css, $text_css, $prefix, $day, $hours, $minutes, $seconds );
	}
}

