<?php
/**
 * Register filter for append custom class name
 * that generated from visual-composer
 */
class WPBakeryShortCode_contentbox extends WPBakeryShortCodesContainer {}
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_contentbox_shortcode_params' );

function themesflat_contentbox_shortcode_params() {
	vc_map( array(
		'name' => esc_html__('Content Box', 'evockans'),
		'description' => esc_html__('Content Box.', 'evockans'),
		'base' => 'contentbox',
		'weight'	=>	180,
		'icon' => THEMESFLAT_ICON,
		'as_parent' => array('except' => 'contentbox'),
		'controls' => 'full',
		'show_settings_on_create' => true,
		'category' => esc_html__('THEMESFLAT', 'evockans'),
		'js_view' => 'VcColumnView',
		'params' => array(
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Effects Hover Box', 'evockans' ),
				'param_name' => 'effect',
				'value'      => array(
					'None' => 'hover-none',
					'Fade' => 'hover-fade',
					'Grow' => 'hover-grow',
					'Shrink' => 'hover-shrink',
					'Rotate Z -30' => 'hover-rotate-z-30',
					'Rotate Z 30' => 'hover-rotate-z30',
					'3D shadow' => 'hover-3d-shadow',
					'Bottom To Top' => 'hover-bottom-to-top',
					'Background Dark' => 'hover-bg-dark',
					'Background Light' => 'hover-bg-light',
					'Background Accent' => 'hover-bg-accent',				
				),
				'std'		=> 'none',
			),            
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Border Width', 'evockans'),
				'param_name' => 'border_width',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Ex: 1px 1px 1px 1px', 'evockans'),
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Border Color', 'evockans'),
				'param_name' => 'border_color',
				'value' => '',
            ),
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Hide Border on Moblie?', 'evockans' ),
				'param_name' => 'hide_boder',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Rounded', 'evockans'),
				'param_name' => 'rounded',
				'value' => '',
				'description'	=> esc_html__('Ex: 6px', 'evockans'),
            ),
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Hide This Box on Moblie?', 'evockans' ),
				'param_name' => 'hide_on_mobile',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
			),
            // Background
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Background Color Style', 'evockans' ),
				'param_name' => 'background',
				'value'      => array(
					'Transparent' => 'style-1',
					'Accent' => 'style-2',
					'Dark Accent' => 'style-3',
					'Light Accent' => 'style-4',
					'Custom' => 'style-5',
				),
				'std'		=> 'style-1',
				'group' => esc_html__( 'Background', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Background Color', 'evockans'),
				'param_name' => 'background_color',
				'value' => '',
				'group' => esc_html__( 'Background', 'evockans' ),
				'dependency' => array( 'element' => 'background', 'value' => 'style-5' ),
            ),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Background Image', 'evockans'),
				'param_name' => 'bg_image',
				'value' => '',
				'group' => esc_html__( 'Background', 'evockans' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Background Position', 'evockans' ),
				'param_name'  => 'bg_position',
				'value'       => array(
					'Left Top' => 'lt',
					'Right Top' => 'rt',
					'Center Top' => 'ct',
					'Center Center' => 'cc',
					'Center Bottom'   => 'cb',
					'Left Bottom' => 'lb',
					'Right Bottom'   => 'rb',
				),
				'std'		=> 'lt',
				'group' => esc_html__( 'Background', 'evockans' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Background Repeat', 'evockans' ),
				'param_name'  => 'bg_repeat',
				'value'       => array(
					'No Repeat' => 'no-repeat',
					'Repeat'   => 'repeat',
					'Repeat X'   => 'repeat-x',
					'Repeat Y'   => 'repeat-y',
				),
				'std'		=> 'no-repeat',
				'group' => esc_html__( 'Background', 'evockans' ),
			),
            // Padding & Margin
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Padding', 'evockans'),
				'param_name' => 'padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. You can use % or px value.', 'evockans'),
				'group' => esc_html__( 'Padding & Margins', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Padding on mobile', 'evockans'),
				'param_name' => 'mobile_padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. You can use % or px value.', 'evockans'),
				'group' => esc_html__( 'Padding & Margins', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Margin', 'evockans'),
				'param_name' => 'margin',
				'value' => '',
				'group' => esc_html__( 'Padding & Margins', 'evockans' ),
				'description'	=> esc_html__('Top Right Bottom Left. You can use % or px value.', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Margin on mobile', 'evockans'),
				'param_name' => 'mobile_margin',
				'value' => '',
				'group' => esc_html__( 'Padding & Margins', 'evockans' ), 
				'description'	=> esc_html__('Top Right Bottom Left. You can use % or px value.', 'evockans'),
	        ),
			// Box Shadow
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Horizontal (Required)', 'evockans'),
				'param_name' => 'horizontal',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Vertical (Required)', 'evockans'),
				'param_name' => 'vertical',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 20px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Blur (Required)', 'evockans'),
				'param_name' => 'blur',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 40px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Spread (Required)', 'evockans'),
				'param_name' => 'spread',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Box Shadow Color (Required)', 'evockans'),
				'param_name' => 'shadow_color',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: #eeeeee', 'evockans'),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Extra class name', 'evockans'),
				'param_name' => 'class',
				'value' => '',
				'group' => esc_html__( 'Class', 'evockans' ),
	        ),
        )
    ) );
}

add_shortcode( 'contentbox', 'themesflat_shortcode_contentbox' );

/**
 * contentbox shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_contentbox( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'effect' => 'hover-none',
        'padding' => '',
        'mobile_padding' => '',
        'background' => 'style-1',
        'background_color' => '',
        'border_color' => '',
        'border_width' => '',
        'rounded' => '',
        'hide_boder' => '',
        'margin' => '',
        'hide_on_mobile' => '',
        'mobile_margin' => '',
        'bg_image' => '',
        'bg_position' => 'lt',
        'bg_repeat' => 'no-repeat',
        'horizontal' => '',
        'vertical' => '',
        'blur' => '',
        'spread' => '',
        'shadow_color' => '',
        'class' => '',
    ), $atts ) );
    //extract($atts = vc_map_get_attributes( 'contentbox', $atts ));
    $content = wpb_js_remove_wpautop($content, true);

    $rounded = intval( $rounded );

    if ( $bg_position == 'lt' ) $bg_position = 'left top';
    if ( $bg_position == 'rt' ) $bg_position = 'right top';
    if ( $bg_position == 'ct' ) $bg_position = 'center top';
    if ( $bg_position == 'cc' ) $bg_position = 'center center';
    if ( $bg_position == 'cb' ) $bg_position = 'center bottom';
    if ( $bg_position == 'lb' ) $bg_position = 'left bottom';
    if ( $bg_position == 'rb' ) $bg_position = 'right bottom';

    $css = $css_rounded = $cls = $inner_cls = '';

    if ( $background_color == '#1a7dd7' || $background == 'style-2' ) {
    	$inner_cls .= ' accent';
    } else {
    	if ( $background_color ) $css .= 'background-color:'. $background_color .';';
    }

    if ( $background == 'style-3' ) $inner_cls .= ' dark-accent';
    if ( $background == 'style-4' ) $inner_cls .= ' light-accent';

    if ( $rounded) $css .= 'border-radius:'. $rounded .'px; overflow: hidden;';
    if ( $border_color && $border_width ) $css .= 'border-style:solid;border-width:'. $border_width .';border-color:'. $border_color .';';
    if ( $bg_image ) $css .= 'background:url('. wp_get_attachment_image_src( $bg_image, 'full' )[0] .') '. $bg_position .' '. $bg_repeat .';';
    if ( $horizontal && $vertical && $blur && $spread && $shadow_color )
    	$css .= 'box-shadow:'. $horizontal .' '. $vertical .' '. $blur .' '. $spread .' '. $shadow_color .';';

    if ( $hide_boder ) $cls .= ' hide-border';
    if ( $hide_on_mobile ) $cls .= ' hide-on-mobile';
    if ( $effect != 'hover-none' ) $cls .= ' ' . $effect;
    if ( $rounded) $css_rounded .= 'border-radius:'. $rounded .'px;';

    return sprintf(
    	'<div class="themesflat_sc_vc-content-box clearfix %3$s %10$s" data-padding="%6$s" data-mobipadding="%7$s" data-margin="%4$s" data-mobimargin="%5$s" style="%9$s">
    		<div class="inner %8$s" style="%2$s">
    			%1$s
    		</div>
    	</div>',
    	do_shortcode($content),
    	$css,
        $cls,
    	$margin,
    	$mobile_margin,
    	$padding,
    	$mobile_padding,
    	$inner_cls,
    	$css_rounded,
    	$class
    );
}