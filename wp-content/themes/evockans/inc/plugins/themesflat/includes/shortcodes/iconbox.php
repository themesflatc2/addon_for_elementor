<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_iconbox_shortcode_params' );

function themesflat_iconbox_shortcode_params() {
	vc_map( array(
		'name'        => esc_html__( 'Icon Box', 'evockans' ),
        'description' => esc_html__('Displaying Icon Box with custom icon.', 'evockans'),
		'base'        => 'iconbox',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
		'params'      => array(
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'evockans' ),
				'param_name' => 'style',
				'value'      => array(
					'Icon Top' => 'icon-top',
					'Icon Top 2' => 'icon-top2',
					'Icon Left Inline' => 'icon-left-inline',
					'Icon Right Inline' => 'icon-right-inline',
					
				),
				'std'		=> 'icon-top',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Text Alignment', 'evockans' ),
				'param_name' => 'text_align',
				'value'      => array(
					'Left' => 'align-left',
					'Center' => 'align-center',
					'Right' => 'align-right',
				),
				'std'		=> 'align-left',
				'dependency' => array( 'element' => 'style', 'value' => 'icon-top' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Wrap Background', 'evockans'),
				'param_name' => 'wrap_background',
				'value' => '#f7f7f7',
				'dependency' => array( 'element' => 'style', 'value' => 'icon-top2' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap Padding', 'evockans'),
				'param_name' => 'wrap_padding',
				'value'      => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
            ),   	  	
			//Icon to Display
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Icon to Display', 'evockans' ),
				'param_name' => 'icon_display',
				'value'      => array(
					'Icon Font' => 'icon-font',
					'Icon Image' => 'icon-image',
				),
				'std'		=> 'icon-font',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Icon Showcase', 'evockans' ),
				'param_name' => 'icon_showcase',
				'value'      => array(
					'Simple' => 'simple',
					'Accent Background' => 'accent-bg',
					'Dark Background' => 'dark-bg',
					'Grey Background' => 'grey-bg',
					'Accent Outline' => 'accent-outline',
					'Dark Outline' => 'dark-outline',
					'Grey Outline' => 'grey-outline',
					'Circle Accent Background' => 'cricle-accent-bg',
					'Circle Dark Background' => 'cricle-dark-bg',
					'Circle Grey Background' => 'cricle-grey-bg',
					'Circle Accent Outline' => 'cricle-accent-outline',
					'Circle Dark Outline' => 'cricle-dark-outline',
					'Circle Grey Outline' => 'cricle-grey-outline',
					'Custom' => 'custom',
				),
				'std'		=> 'simple',
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-font' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Icon Wrap Width', 'evockans' ),
				'param_name' => 'icon_width',
				'value'      => '',
				'std'		=> '50',
				'dependency' => array( 'element' => 'icon_display', 'value' => array( 'icon-font' ) ),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon Wrap Background Color', 'evockans'),
				'param_name' => 'icon_wrap_background',
				'value' => '',
				'dependency' => array( 'element' => 'icon_showcase', 'value' => array( 'custom' ) ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon Wrap Background Color Hover', 'evockans'),
				'param_name' => 'icon_wrap_background_hover',
				'value' => '',
				'dependency' => array( 'element' => 'icon_showcase', 'value' => array( 'custom' ) ),
            ),
	        array(
				'type'       => 'textfield',
				'heading' => esc_html__('Icon Wrap Rounded', 'evockans'),
				'param_name' => 'icon_rounded',
				'value'      => '',
				'dependency' => array( 'element' => 'icon_showcase', 'value' => array( 'custom' ) ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Wrap Border Width', 'evockans'),
				'param_name' => 'icon_border_width',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
				'dependency' => array( 'element' => 'icon_showcase', 'value' => array( 'custom' ) ),
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon Wrap Border Color', 'evockans'),
				'param_name' => 'icon_border_color',
				'value' => '',
				'dependency' => array( 'element' => 'icon_showcase', 'value' => array( 'custom' ) ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon Wrap Border Color Hover', 'evockans'),
				'param_name' => 'icon_border_color_hover',
				'value' => '',
				'dependency' => array( 'element' => 'icon_showcase', 'value' => array( 'custom' ) ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Position Top', 'evockans'),
				'param_name' => 'icon_position',
				'value' => '',
				'dependency' => array( 'element' => 'style', 'value' => array('icon-left-inline','icon-right-inline') ),
				'description'	=> esc_html__('ex: 10px or -10px', 'evockans'),
	        ),
			// Icon
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'evockans' ),
				'param_name' => 'icon_type',
				'description' => esc_html__( 'Select icon library.', 'evockans' ),
				'value' => array(
					esc_html__( 'None', 'evockans' ) => '',
					esc_html__( 'Mono Social', 'evockans' ) => 'monosocial',
					esc_html__( 'Simple Line', 'evockans' ) => 'simpleline',
					esc_html__( 'Font Ionicons', 'evockans' ) => 'ionicons',
					esc_html__( 'Font Elegant', 'evockans' ) => 'eleganticons',
					esc_html__( 'Font Themify Icons', 'evockans' ) => 'themifyicons',
					esc_html__( 'Font Icomoon Icons', 'evockans' ) => 'icomoon',
					esc_html__( 'FontAwesome', 'evockans' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'evockans' ) => 'openiconic',
					esc_html__( 'Typicons', 'evockans' ) => 'typicons',
					esc_html__( 'Entypo', 'evockans' ) => 'entypo',
					esc_html__( 'Linecons', 'evockans' ) => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-font' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_monosocial',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'monosocial',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'monosocial',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_simpleline',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'simpleline',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'simpleline',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_ionicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'ionicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'ionicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_eleganticons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'eleganticons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'eleganticons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_themifyicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'themifyicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'themifyicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_icomoon',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'icomoon',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'icomoon',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon',
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'fontawesome',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_openiconic',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'openiconic',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'openiconic',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_typicons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'typicons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'typicons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_entypo',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'entypo',
					'iconsPerPage' => 300,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'entypo',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_linecons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'linecons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon Color', 'evockans'),
				'param_name' => 'icon_color',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => array( 'icon-font' ) ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon Color Hover', 'evockans'),
				'param_name' => 'icon_color_hover',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => array( 'icon-font' ) ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Font Size', 'evockans'),
				'param_name' => 'icon_font_size',
				'value' => '50',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-font' ),
	        ),	        
	        // Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image', 'evockans'),
				'param_name' => 'image',
				'value' => '',
				'group' => esc_html__( 'Image', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-image' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Image Width', 'evockans'),
				'param_name' => 'image_width',
				'value' => '',
				'description'	=> esc_html__('Ex: 100px', 'evockans'),
				'group' => esc_html__( 'Image', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-image' ),
	        ),
	        array(
				'type'       => 'textfield',
				'heading' => esc_html__('Image Rounded', 'evockans'),
				'param_name' => 'image_rounded',
				'value'      => '',
				'description'	=> esc_html__('Ex: 100px', 'evockans'),
				'group' => esc_html__( 'Image', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-image' ),
	        ),
			// Content
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Tag', 'evockans' ),
				'param_name' => 'tag',
				'value'      => array(
					'H1' => 'h1',
					'H2' => 'h2',
					'H3' => 'h3',
					'H4' => 'h4',
					'H5' => 'h5',
					'H6' => 'h6',
				),
				'std'		=> 'h3',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
            array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Heading Color', 'evockans'),
				'param_name' => 'heading_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Heading Color Hover', 'evockans'),
				'param_name' => 'heading_color_hover',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
			array(
				'type' => 'textarea',
				'heading' => esc_html__( 'Description', 'evockans' ),
				'param_name' => 'description',
				'group' => esc_html__( 'Content', 'evockans' ), 
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Description Color', 'evockans'),
				'param_name' => 'desc_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
			// Button
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Show button?', 'evockans' ),
				'param_name' => 'show_button',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Button', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Style', 'evockans' ),
				'param_name' => 'button_style',
				'value'      => array(
					'Simple Link' => 'simple_link',					
					'Accent' => 'accent',
					'Dark' => 'dark',
					'Light' => 'light',
					'Very Light' => 'very-light',
					'White' => 'white',
					'Outline' => 'outline',
					'Outline Dark' => 'outline_dark',
					'Outline Light' => 'outline_light',
					'Outline Very light' => 'outline_very-light',
					'Outline White' => 'outline_white',
					'Custom' => 'custom',
				),
				'std'		=> 'simple_link',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'show_button', 'value' => 'yes' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Size', 'evockans' ),
				'param_name' => 'button_size',
				'value'      => array(
					'Default' => '',
					'Small' => 'small',
					'Very Small' => 'xsmall',
					'Big' => 'big',
				),
				'std'		=> '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array(
					'element' => 'button_style',
					'value' => array (
						'custom',
						'accent',
						'dark',
						'light',
						'very-light',
						'white',
						'outline',
						'outline_dark',
						'outline_light',
						'outline_very-light',
						'outline_white'
					)
				),
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Rounded', 'evockans'),
				'param_name' => 'button_rounded',
				'value' => '',
				'description'	=> esc_html__('ex: 10px', 'evockans'),
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array(
					'element' => 'button_style',
					'value' => array (
						'custom',
						'accent',
						'dark',
						'light',
						'very-light',
						'white',
						'outline',
						'outline_dark',
						'outline_light',
						'outline_very-light',
						'outline_white'
					)
				),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Button Color', 'evockans'),
				'param_name' => 'button_color',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array('element' => 'button_style', 'value' => array('custom','simple_link' ) ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Button Color Hover', 'evockans'),
				'param_name' => 'button_color_hover',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array('element' => 'button_style', 'value' => array('custom','simple_link' ) ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Button Background Color', 'evockans'),
				'param_name' => 'button_bg_color',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'button_style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Button Background Color Hover', 'evockans'),
				'param_name' => 'button_bg_color_hover',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'button_style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Border Width', 'evockans'),
				'param_name' => 'button_border_width',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'button_style', 'value' => 'custom' ),
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Button Border Color', 'evockans'),
				'param_name' => 'button_border_color',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'button_style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Button Border Color Hover', 'evockans'),
				'param_name' => 'button_border_color_hover',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'button_style', 'value' => 'custom' ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Text (Required)', 'evockans'),
				'param_name' => 'button_text',
				'value' => 'Learn More',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'show_button', 'value' => 'yes' ),
	        ),	        
            array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Button Padding', 'evockans'),
				'param_name' => 'button_padding',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'dependency' => array('element' => 'button_style', 'value' => array('custom','accent','dark','light','very-light','white','outline','outline_dark','outline_light','outline_very-light','outline_white' ) ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Link URL (Required):', 'evockans'),
				'param_name' => 'link_url',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'show_button', 'value' => 'yes' ),
            ),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Open Link in a new Tab', 'evockans' ),
				'param_name' => 'new_tab',
				'value' => array(
					'Yes' => 'yes',
					'No' => 'no',
				),
				'std'		=> 'yes',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'show_button', 'value' => 'yes' ),
			),
			/*Button Icon*/
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Add Icon Button', 'evockans' ),
				'param_name' => 'add_icon_button',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'show_button', 'value' => 'yes' ),
			),
	        // Typography
			array(
				'type' => 'headings',
				'text' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Font Family', 'evockans' ),
				'param_name' => 'heading_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Font Weight', 'evockans' ),
				'param_name' => 'heading_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Font Size', 'evockans'),
				'param_name' => 'heading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Line-Height', 'evockans'),
				'param_name' => 'heading_line_height',
				'value' => '',
				'description'	=> esc_html__('24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Description', 'evockans'),
				'param_name' => 'desc_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Description Font Family', 'evockans' ),
				'param_name' => 'desc_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Description Font Weight', 'evockans' ),
				'param_name' => 'desc_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Description Font Size', 'evockans'),
				'param_name' => 'desc_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Description Line-Height', 'evockans'),
				'param_name' => 'desc_line_height',
				'value' => '',
				'description'	=> esc_html__('24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Button', 'evockans'),
				'param_name' => 'button_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Font Family', 'evockans' ),
				'param_name' => 'button_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Font Weight', 'evockans' ),
				'param_name' => 'button_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Font Size', 'evockans'),
				'param_name' => 'button_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Line-Height', 'evockans'),
				'param_name' => 'button_line_height',
				'value' => '',
				'description'	=> esc_html__('24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        // Spacing
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content: Left Padding', 'evockans'),
				'param_name' => 'content_left_padding',
				'value' => '85px',
				'group' => esc_html__( 'Spacing', 'evockans' ), 
				'dependency' => array( 'element' => 'style', 'value' => 'icon-left-inline' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content: Right Padding', 'evockans'),
				'param_name' => 'content_right_padding',
				'value' => '85px',
				'group' => esc_html__( 'Spacing', 'evockans' ), 
				'dependency' => array( 'element' => 'style', 'value' => 'icon-right-inline' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Top Margin', 'evockans'),
				'param_name' => 'heading_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Bottom Margin', 'evockans'),
				'param_name' => 'heading_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Description: Top Margin', 'evockans'),
				'param_name' => 'desc_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Description: Bottom Margin', 'evockans'),
				'param_name' => 'desc_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Description: Padding', 'evockans'),
				'param_name' => 'desc_padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Extra class name', 'evockans'),
				'param_name' => 'class',
				'value' => '',
				'group' => esc_html__( 'Class', 'evockans' ),
	        ),
		)
	) );
}

add_shortcode( 'iconbox', 'themesflat_shortcode_iconbox' );

/**
 * iconbox shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_iconbox( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'style' => 'icon-top',
		'text_align' => 'align-left',
		'wrap_background' => '#f7f7f7',
		'wrap_padding' => '',
		'icon_display' => 'icon-font',
		'image' => '',
		'image_width' => '',
		'image_rounded' => '',
		'icon_showcase' => 'simple',
		'icon_type' => '',
		'icon' => '',
		'icon_color' => '',
		'icon_color_hover' => '',
		'icon_width' => '50',
		'icon_wrap_background' => '',
		'icon_wrap_background_hover' => '',
		'icon_rounded' => '',
		'icon_border_width' => '',
		'icon_border_color' => '',
		'icon_border_color_hover' => '',
		'icon_font_size' => '50',
		'icon_position' => '',
		'tag' => 'h3',
		'heading' => '',
		'heading_color' => '',
		'heading_color_hover' => '',
		'description' => '',
		'desc_color' => '',
		'show_button' => '',
		'button_style' => 'simple_link',
		'button_size' => '',
		'add_icon_button' => '',
		'button_rounded' => '',
		'button_color' => '',
		'button_color_hover' => '',
		'button_bg_color' => '',
		'button_bg_color_hover' => '',
		'button_border_width' => '',
		'button_border_color' => '',
		'button_border_color_hover' => '',
		'button_text' => 'Learn More',
		'link_url' => '',
		'new_tab' => 'yes',
		'button_padding' => '',
		'heading_font_family' => 'Default',
		'heading_font_weight' => 'Default',
		'heading_font_size' => '',
		'heading_line_height' => '',
		'desc_font_family' => 'Default',
		'desc_font_weight' => 'Default',
		'desc_font_size' => '',
		'desc_line_height' => '',
		'button_font_family' => 'Default',
		'button_font_weight' => 'Default',
		'button_font_size' => '',
		'button_line_height' => '',
		'content_left_padding' => '85px',
		'content_right_padding' => '85px',
		'heading_top_margin' => '',
		'heading_bottom_margin' => '',
		'desc_top_margin' => '',
		'desc_bottom_margin' => '',
		'desc_padding' => '',
		'class' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'iconbox', $atts ));

	$image_width = intval( $image_width );
	$image_rounded = intval( $image_rounded );
	$icon_font_size = intval( $icon_font_size );
	$icon_position = intval( $icon_position ); 

	$content_left_padding = intval( $content_left_padding );
	$content_right_padding = intval( $content_right_padding );
	$heading_top_margin = intval( $heading_top_margin );
	$heading_bottom_margin = intval( $heading_bottom_margin );
	$desc_top_margin = intval( $desc_top_margin );
	$desc_bottom_margin = intval( $desc_bottom_margin );

	$heading_font_size = intval( $heading_font_size );
	$desc_font_size = intval( $desc_font_size );
	$button_font_size = intval( $button_font_size );
	$button_rounded = intval( $button_rounded );
	$icon_width = intval( $icon_width );
	$icon_rounded = intval( $icon_rounded );

	$cls = $style .' '. $icon_showcase .' '. $text_align;
	$cls .= ( $icon_showcase != 'simple' ) ? ' has-width' : ' simple';

	$heading_css = $heading_a_css = $heading_css_js = $heading_cls = $desc_css = $button_wrap_css = $button_css = $button_css_js = $button_cls = '';
	$text_css = $inner_css = $icon_offset = $icon_css = $icon_css_js = $icon_cls = $image_css = $image_css_rounded = $icon_html = $button_icon_html = $button_icon_css = $content_html = '';
	$line_step_html = $line_step_css = '';

	if ( $content_left_padding && $style == 'icon-left-inline' ) {
		$heading_css = $desc_css = $button_wrap_css = 'padding-left:'. $content_left_padding .'px;';
	} 
	if ( $content_right_padding && $style == 'icon-right-inline' ) {
		$heading_css = $desc_css = $button_wrap_css = 'padding-right:'. $content_right_padding .'px;';
	}

	if ( $heading_font_weight != 'Default' ) $heading_css .= 'font-weight:'. $heading_font_weight .';';
	if ( $heading_color ) $heading_a_css .= 'color:'. $heading_color .';';
	if ( $heading_font_size ) $heading_css .= 'font-size:'. $heading_font_size .'px;';
	if ( $heading_line_height ) $heading_css .= 'line-height:'. $heading_line_height .';';
	if ( $heading_top_margin ) $heading_css .= 'margin-top:'. $heading_top_margin .'px;';
	if ( $heading_bottom_margin ) $heading_css .= 'margin-bottom:'. $heading_bottom_margin .'px;';
	if ( $heading_font_family != 'Default' ) {
		$heading_css .= 'font-family:'. $heading_font_family .';';
	}

	if ( $desc_font_weight != 'Default' ) $desc_css .= 'font-weight:'. $desc_font_weight .';';
	if ( $desc_color ) $desc_css .= 'color:'. $desc_color .';';
	if ( $desc_font_size ) $desc_css .= 'font-size:'. $desc_font_size .'px;';
	if ( $desc_line_height ) $desc_css .= 'line-height:'. $desc_line_height .';';
	if ( $desc_top_margin ) $desc_css .= 'margin-top:'. $desc_top_margin .'px;';
	if ( $desc_bottom_margin ) $desc_css .= 'margin-bottom:'. $desc_bottom_margin .'px;';
	if ( $desc_padding ) $desc_css .= 'padding:'. $desc_padding .';';
	if ( $desc_font_family != 'Default' ) {
		$desc_css .= 'font-family:'. $desc_font_family .';';
	}

	if ( $button_font_weight != 'Default' ) $button_css .= 'font-weight:'. $button_font_weight .';';
	if ( $button_color ) $button_css .= 'color:'. $button_color .';';
	if ( $button_bg_color ) $button_css .= 'background:'. $button_bg_color .';';	
	if ( $button_font_size ) $button_css .= 'font-size:'. $button_font_size .'px;';
	if ( $button_rounded ) $button_css .= 'border-radius:'. $button_rounded .'px;';
	if ( $button_border_color && $button_border_width ) $button_css .= 'border-style:solid;border-width:'. $button_border_width .';border-color:'. $button_border_color .';';
	if ( $button_line_height ) $button_css .= 'line-height:'. $button_line_height .';';
	if ( $button_padding ) $button_css .= 'padding:'. $button_padding .';';
	if ( $button_font_family != 'Default' ) {
		$button_css .= 'font-family:'. $button_font_family .';';
	}

	if ( $style == 'icon-top2' ) {
		$icon_offset = $icon_width / 2;
		$icon_css .= 'margin-top:-'. $icon_offset .'px;margin-left:-'. $icon_offset .'px;';
		$inner_css .= 'padding-top:'. ( $icon_offset ) .'px;';

		if ( $wrap_background ) $text_css .= 'background-color:'. $wrap_background .';';		
	}

	if ( $wrap_padding ) $text_css .= 'padding:'. $wrap_padding .';';

	if ( $icon_display == 'icon-font' ) {
		$icon = themesflat_sc_vc_get_icon_class( $atts, 'icon' );
		if ( $icon && $icon_type != '' ) {
			vc_icon_element_fonts_enqueue( $icon_type );
				
			if ( $icon_font_size ) $icon_css .= 'font-size:'. $icon_font_size .'px;';
			if ( $icon_position ) $icon_css .= 'top:'. $icon_position .'px;';

			if ($icon_width) {
				$line_height = $icon_width;
				$icon_css .= 'line-height:'. $line_height .'px;';
				$icon_css .= 'width:'. $icon_width .'px;';
				$icon_css .= 'height:'. $icon_width .'px;';				
			}

			if ($icon_rounded) $icon_css .= 'border-radius:'. $icon_rounded .'px;';

			if ( $icon_border_color && $icon_border_width ) $icon_css .= 'border-style:solid;border-width:'. $icon_border_width .';border-color:'. $icon_border_color .';';
			
			if ($icon_wrap_background) $icon_css .= 'background:'. $icon_wrap_background .';';
			if ( $icon_color ) $icon_css .= 'color:'. $icon_color .';';
			
			if ($icon_color_hover) $icon_css_js .= 'data-color_hover="'. $icon_color_hover .'" ';
			if ($icon_color) $icon_css_js .= 'data-color="'. $icon_color .'" ';
			
			if ($icon_wrap_background_hover) $icon_css_js .= 'data-bgcolor_hover="'. $icon_wrap_background_hover .'" ';
			if ($icon_wrap_background) $icon_css_js .= 'data-bgcolor="'. $icon_wrap_background .'" ';

			if ($icon_border_color_hover) $icon_css_js .= 'data-bordercolor_hover="'. $icon_border_color_hover .'" ';
			if ($icon_border_color) $icon_css_js .= 'data-bordercolor="'. $icon_border_color .'" ';
			

			$icon_html = sprintf(
				'<div class="icon-wrap %3$s" style="%2$s" %4$s>
					<i class="%1$s"></i>
				</div>',
				$icon,
				$icon_css,
				$icon_cls,
				$icon_css_js
			);
		}
	} else {
		if ( $image_width ) $image_css = 'width:'. $image_width .'px;';
		if ( $image_rounded ) $image_css_rounded = 'border-radius:'. $image_rounded .'px;';
		if ( $icon_position ) $image_css .= 'top:'. $icon_position .'px;';

		if ( $image )
			$icon_html = sprintf(
				'<div class="image-wrap" style="%2$s">
					<img alt="image" src="%1$s" style="%3$s">
				</div>',
				wp_get_attachment_image_src( $image, 'full' )[0],
				$image_css,
				$image_css_rounded
			);
	}

	$new_tab = $new_tab == 'yes' ? '_blank' : '_self'; 

	if ( $heading_color ) $heading_css_js .= 'data-headingcolor="'. $heading_color .'" ';
	if ( $heading_color_hover ) $heading_css_js .= 'data-headingcolor_hover="'. $heading_color_hover .'" ';
			
	if ( $heading ) $content_html .= sprintf(
		'<%5$s class="heading %6$s" style="%2$s" %8$s>
			<a target="%4$s" href="%3$s" style="%7$s">
				<span>%1$s</span>
			</a>
		</%5$s>',
		$heading,
		esc_attr( $heading_css ),
	    esc_attr( $link_url ),
	    $new_tab,
		$tag,
		$heading_cls,
		$heading_a_css,
		$heading_css_js
	);

	if ( $description ) $content_html .= sprintf(
		'<p class="desc" style="%2$s">
			<span>%1$s</span>
		</p>',
		$description,
		esc_attr( $desc_css )
	);

	$button_cls = $button_size;
	if ( $show_button && $button_text && $link_url ) {
		if ( $button_style == 'simple_link' ) $button_cls .= ' simple-link font-heading';
		if ( $button_style == 'custom' ) $button_cls .= ' themesflat_sc_vc-button custom';
		if ( $button_style == 'accent' ) $button_cls .= ' themesflat_sc_vc-button accent';
		if ( $button_style == 'dark' ) $button_cls .= ' themesflat_sc_vc-button dark';
		if ( $button_style == 'light' ) $button_cls .= ' themesflat_sc_vc-button light';
		if ( $button_style == 'very-light' ) $button_cls .= ' themesflat_sc_vc-button very-light';
		if ( $button_style == 'white' ) $button_cls .= ' themesflat_sc_vc-button white';
		if ( $button_style == 'outline' ) $button_cls .= ' themesflat_sc_vc-button outline ol-accent';
		if ( $button_style == 'outline_dark' ) $button_cls .= ' themesflat_sc_vc-button outline dark';
		if ( $button_style == 'outline_light' ) $button_cls .= ' themesflat_sc_vc-button outline light';
		if ( $button_style == 'outline_very-light' ) $button_cls .= ' themesflat_sc_vc-button outline very-light';
		if ( $button_style == 'outline_white' ) $button_cls .= '  themesflat_sc_vc-button outline white';

		if( $add_icon_button ) {		
			$button_icon_html = '<i class="fa fa-angle-right"></i>';		
		}
		
		if ( $button_color_hover ) $button_css_js .= 'data-buttoncolor_hover="'. $button_color_hover .'" ';
		if ( $button_color ) $button_css_js .= 'data-buttoncolor="'. $button_color .'" ';
		if ( $button_bg_color_hover ) $button_css_js .= 'data-button_bgcolor_hover="'. $button_bg_color_hover .'" ';
		if ( $button_bg_color ) $button_css_js .= 'data-button_bgcolor="'. $button_bg_color .'" ';
		if ( $button_border_color_hover ) $button_css_js .= 'data-button_bordercolor_hover="'. $button_border_color_hover .'" ';
		if ( $button_border_color ) $button_css_js .= 'data-button_bordercolor="'. $button_border_color .'" ';

		$content_html .= sprintf(
			'<div class="btn" style="%6$s" %8$s>
				<a target="%5$s" class="%3$s" href="%2$s" style="%4$s">%1$s %7$s</a>
			</div>',
			esc_html( $button_text ),
			esc_attr( $link_url ),
			$button_cls,
			$button_css,
			$new_tab,
			$button_wrap_css,
			$button_icon_html,
			$button_css_js
		);
	}

	if ( $class ) {
		$cls .= ' ' . $class;
	}

	if ( $style == 'icon-top2' ) {
		return sprintf( '<div class="themesflat_sc_vc-icon-box clearfix %1$s">
			<div class="wrap-inner" style="%4$s">
				<div class="inner">
					<div class="text-wrap" style="%5$s">%2$s %3$s</div>
				</div>
			</div>
			</div>',
			esc_attr( $cls ),
			$icon_html,
			$content_html,
			$inner_css,
			$text_css
		);
	} else {
		return sprintf( '<div class="themesflat_sc_vc-icon-box clearfix %1$s" style="%5$s">%4$s %2$s %3$s</div>',
			esc_attr( $cls ),
			$icon_html,
			$content_html,
			$line_step_html,
			$text_css
		);
	}
}