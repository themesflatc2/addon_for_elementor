<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_advimage_shortcode_params' );

function themesflat_advimage_shortcode_params() {
	vc_map( array(
		'name'        => esc_html__( 'Image Advanced', 'evockans' ),
        'description' => esc_html__('Displaying a image with awesome options.', 'evockans'),
		'base'        => 'advimage',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
		'params'      => array(
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'evockans' ),
				'param_name' => 'style',
				'value'      => array(
					'Video Or Map Poppup' => 'style-1',
					'Text Hyperlink' => 'style-2',
					'Image' => 'style-3',
				),
				'std'		=> 'style-1',
			),
			array(
				'type' => 'attach_image',
				'holder' => 'img',
				'heading' => esc_html__('Image', 'evockans'),
				'param_name' => 'image',
				'value' => '',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Image Cropping', 'evockans' ),
				'param_name' => 'image_crop',
				'value'      => array(
					'Full' => 'full',
					'600 x 600' => 'square',
					'600 x 500' => 'rectangle',
					'600 x 390' => 'rectangle2',
					'Custom' => 'custom',
				),
				'std'		=> 'full',
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Image Size', 'evockans' ),
				'description'    => esc_html__( '( Alternatively enter size in pixels (Example: 750x750 (Width x Height)).', 'evockans' ),
				'param_name' => 'image_size',
				'value' => '750x750',
				'dependency' => array( 'element' => 'image_crop', 'value' => 'custom' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Link Youtube/Vimeo (URL)/Map (URL)', 'evockans'),
				'param_name' => 'video_url',
				'value' => '',
				'dependency' => array( 'element' => 'style', 'value' => 'style-1' ),
	        ),
	         array(
				'type' => 'textfield',
				'heading' => esc_html__('Rounded', 'evockans'),
				'param_name' => 'rounded',
				'value' => '',
				'description'	=> esc_html__('Ex: 5px or 5%', 'evockans'),
				'dependency' => array( 'element' => 'style', 'value' => array('style-1', 'style-3') ),
            ),
	        array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Image: Effect', 'evockans' ),
				'param_name' => 'image_effect',
				'value'      => array(
					'None' => '',
					'Zoom' => 'effect-zoom',
					'Zoom Out Left' => 'effect-zoom-out-left',
					'Zoom Out Right' => 'effect-zoom-out-right',
					'Zoom Out Flip Horiz' => 'effect-zoom-out-flip-horiz',
					'Flip Horiz' => 'effect-flip-horiz',
					'Flip Vert' => 'effect-flip-vert',
					'Fold Up' => 'effect-fold-up',
					'Fold Down' => 'effect-fold-down',
					'Blur' => 'effect-blur',
					'Flip Diag 1' => 'effect-flip-diag-1',
					'Flip Diag 2' => 'effect-flip-diag-2',
				),
				'std'		=> '',
				'dependency' => array( 'element' => 'style', 'value' => array('style-1', 'style-3') ),
			),			
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Heading', 'evockans' ),
				'param_name' => 'heading',
				'value' => 'Heading Text',
				'dependency' => array( 'element' => 'style', 'value' => 'style-2' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Heading: Color', 'evockans'),
				'param_name' => 'heading_color',
				'value' => '#ffffff',
				'dependency' => array( 'element' => 'style', 'value' => 'style-2' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Background Color Overlay', 'evockans'),
				'param_name' => 'background_color_overlay',
				'value' => '',
				'dependency' => array( 'element' => 'style', 'value' => array('style-1', 'style-3') ),
            ),
            array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Overlay Style', 'evockans' ),
				'param_name' => 'overlay_style',
				'value'      => array(
					'Display' => 'overlay_display',
					'Hover Display' => 'overlay_hover_display',
				),
				'std'		=> 'overlay_hover_display',
			),
            array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Box Shadow?', 'evockans' ),
				'param_name' => 'box_shadow',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'dependency' => array( 'element' => 'style', 'value' => array('style-1', 'style-3') ),
			),	        
	        // Hyperlink
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Link (URL):', 'evockans'),
				'param_name' => 'link_url',
				'value' => '',
				'dependency' => array( 'element' => 'style', 'value' => 'style-2' ),
            ),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Open Link in a new Tab', 'evockans' ),
				'param_name' => 'new_tab',
				'value' => array(
					'Yes' => 'yes',
					'No' => 'no',
				),
				'std'		=> 'yes',
				'dependency' => array( 'element' => 'style', 'value' => 'style-2' ),
			),
			// Icon
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Icon Display Style', 'evockans' ),
				'param_name' => 'icon_display',
				'value'      => array(
					'None' => '',
					'Display' => 'display',
					'Hover Display' => 'hover_display',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
				'std'		=> '',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Icon Animation', 'evockans' ),
				'param_name' => 'icon_animation',
				'value'      => array(
					'None' => '',
					'Pulse Box 1' => 'pulsebox-1',
					'Pulse Box 2' => 'pulsebox-2',
					'Pulse Box 3' => 'pulsebox-3',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
				'std'		=> '',
				'dependency' => array( 'element' => 'icon_display', 'value' => array('display', 'hover_display') ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'evockans' ),
				'param_name' => 'icon_type',
				'description' => esc_html__( 'Select icon library.', 'evockans' ),
				'value' => array(
					esc_html__( 'None', 'evockans' ) => '',
					esc_html__( 'Mono Social', 'evockans' ) => 'monosocial',
					esc_html__( 'Simple Line', 'evockans' ) => 'simpleline',
					esc_html__( 'Font Ionicons', 'evockans' ) => 'ionicons',
					esc_html__( 'Font Elegant', 'evockans' ) => 'eleganticons',
					esc_html__( 'Font Themify Icons', 'evockans' ) => 'themifyicons',
					esc_html__( 'Font Icomoon Icons', 'evockans' ) => 'icomoon',
					esc_html__( 'FontAwesome', 'evockans' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'evockans' ) => 'openiconic',
					esc_html__( 'Typicons', 'evockans' ) => 'typicons',
					esc_html__( 'Entypo', 'evockans' ) => 'entypo',
					esc_html__( 'Linecons', 'evockans' ) => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => array('display', 'hover_display') ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_monosocial',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'monosocial',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'monosocial',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_simpleline',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'simpleline',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'simpleline',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_ionicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'ionicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'ionicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_eleganticons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'eleganticons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'eleganticons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_themifyicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'themifyicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'themifyicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_icomoon',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'icomoon',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'icomoon',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon',
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'fontawesome',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_openiconic',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'openiconic',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'openiconic',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_typicons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'typicons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'typicons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_entypo',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'entypo',
					'iconsPerPage' => 300,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'entypo',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_linecons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'linecons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Icon Size', 'evockans' ),
				'param_name' => 'icon_size',
				'value'      => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => array('display', 'hover_display') ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Icon Font Size', 'evockans' ),
				'param_name' => 'icon_font_size',
				'value'      => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => array('display', 'hover_display') ),
			),
			array(
				'type'       => 'textfield',
				'heading' => esc_html__('Icon Rounded', 'evockans'),
				'param_name' => 'icon_rounded',
				'value'      => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => array('display', 'hover_display') ),
	        ),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon: Color', 'evockans'),
				'param_name' => 'icon_color',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => array('display', 'hover_display') ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon: Background Color', 'evockans'),
				'param_name' => 'icon_background_color',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => array('display', 'hover_display') ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon: Background Color Hover', 'evockans'),
				'param_name' => 'icon_background_color_hover',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => array('display', 'hover_display') ),
            ),
            // Image
            array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Image: Style', 'evockans' ),
				'param_name' => 'image_style',
				'value'      => array(
					'Single Image' => 'single-image',
					'Single Image Popup' => 'single-image-popup',
					'Image Gallery Popup' => 'image-gallery-popup',
				),
				'std'		=> '',
				'group' => esc_html__( 'Image Style', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'style-3' ),
			),			
	        // Typography
			array(
				'type' => 'headings',
				'text' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'style-2' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Tag', 'evockans' ),
				'param_name' => 'tag',
				'value'      => array(
					'H1' => 'h1',
					'H2' => 'h2',
					'H3' => 'h3',
					'H4' => 'h4',
					'H5' => 'h5',
					'H6' => 'h6',
				),
				'std'		=> 'h3',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'style-2' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading: Font Family', 'evockans' ),
				'param_name' => 'heading_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'style-2' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading: Font Weight', 'evockans' ),
				'param_name' => 'heading_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'style-2' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Font Size', 'evockans'),
				'param_name' => 'heading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'style-2' ),
	        ),
	        // Box Shadow
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Horizontal (Required)', 'evockans'),
				'param_name' => 'horizontal',
				'value' => '',
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
				'dependency' => array( 'element' => 'box_shadow', 'value' => 'yes' ),
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Vertical (Required)', 'evockans'),
				'param_name' => 'vertical',
				'value' => '',
				'description'	=> esc_html__('Ex: 20px', 'evockans'),
				'dependency' => array( 'element' => 'box_shadow', 'value' => 'yes' ),
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Blur (Required)', 'evockans'),
				'param_name' => 'blur',
				'value' => '',
				'description'	=> esc_html__('Ex: 40px', 'evockans'),
				'dependency' => array( 'element' => 'box_shadow', 'value' => 'yes' ),
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Spread (Required)', 'evockans'),
				'param_name' => 'spread',
				'value' => '',
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
				'dependency' => array( 'element' => 'box_shadow', 'value' => 'yes' ),
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Box Shadow Color (Required)', 'evockans'),
				'param_name' => 'shadow_color',
				'value' => '',
				'description'	=> esc_html__('Ex: #eeeeee', 'evockans'),
				'dependency' => array( 'element' => 'box_shadow', 'value' => 'yes' ),
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Extra class name', 'evockans'),
				'param_name' => 'class',
				'value' => '',
				'group' => esc_html__( 'Class', 'evockans' ),
	        ),
		)
	) );
}

add_shortcode( 'advimage', 'themesflat_shortcode_advimage' );

/**
 * advimage shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_advimage( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'style' => 'style-1',
		'image' => '',
		'image_crop' => 'full',
		'image_size' => '750x750',
		'video_url' => '',
		'rounded' => '',
		'image_effect' => '',
		'background_color_overlay' => '',
		'overlay_style' => 'overlay_hover_display',
		'box_shadow' => '',
        'horizontal' => '',
        'vertical' => '',
        'blur' => '',
        'spread' => '',
        'shadow_color' => '',
		'icon_display' => '',
		'icon_animation' => '',
		'icon_size' => '',
		'icon_font_size' => '',
		'icon_rounded' => '',
		'icon_color' => '',
		'icon_background_color' => '',
		'icon_background_color_hover' => '',		
		'link_url' => '',
		'new_tab' => 'yes',
		'tag' => 'h3',
		'heading' => 'Heading Text',
		'heading_color' => '#ffffff',
		'icon_type' => '',
		'icon' => '',
		'image_style' => 'single-image',
		'heading_font_family' => 'Default',
		'heading_font_weight' => 'Default',
		'heading_font_size' => '',
		'class' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'advimage', $atts ));

	wp_enqueue_script( 'themesflat_sc_vc-magnificpopup' );

	$heading_font_size = intval( $heading_font_size );
	$icon_size = intval( $icon_size );
	$icon_font_size = intval( $icon_font_size );
	$icon_rounded = intval( $icon_rounded );	

	$cls = $css = $icon_cls = $image_html = $image_css = $heading_html = $heading_css  = $icon_html = $icon_html_video_map = $icon_css = $icon_cls = $icon_css_js = $overlay_css = '';
 

	if ( $heading_font_weight != 'Default' ) $heading_css .= 'font-weight:'. $heading_font_weight .';';
	if ( $heading_color ) $heading_css .= 'color:'. $heading_color .';';
	if ( $heading_font_size ) $heading_css .= 'font-size:'. $heading_font_size .'px;';
	if ( $heading_font_family != 'Default' ) { $heading_css .= 'font-family:'. $heading_font_family .';'; }

	if ( $icon_size ) $icon_css .= 'width:'. $icon_size .'px;' . 'height:'. $icon_size .'px;';
	if ( $icon_font_size ) $icon_css .= 'font-size:'. $icon_font_size .'px;';
	if ( $icon_rounded ) $icon_css .= 'border-radius:'. $icon_rounded .'px;';
	if ( $icon_color ) $icon_css .= 'color:'. $icon_color .';';
	if ( $icon_background_color ) $icon_css .= 'background-color:'. $icon_background_color .';';
	if ( $icon_background_color_hover ) $icon_css_js .= 'data-bgcolor_hover="'. $icon_background_color_hover .'" ';
	if ( $icon_background_color ) $icon_css_js .= 'data-bgcolor="'. $icon_background_color .'" ';

	if ( $background_color_overlay ) $overlay_css .= 'background:'. $background_color_overlay .';';
	if ( $rounded ) $overlay_css .= 'border-radius:' . $rounded . ';';

	if ( $rounded ) { $image_css .= 'border-radius:' . $rounded . ';'; }
	if ( $box_shadow ) {    	
    	if ( $horizontal && $vertical && $blur && $spread && $shadow_color )
    	$image_css .= 'box-shadow:'. $horizontal .' '. $vertical .' '. $blur .' '. $spread .' '. $shadow_color .';';
    }

	if ( $image ) {
	    $img_size = 'full';
	    if ( $image_crop == 'square' ) $img_size = 'themesflat_sc_vc-square';
	    if ( $image_crop == 'rectangle' ) $img_size = 'themesflat_sc_vc-rectangle';
	    if ( $image_crop == 'rectangle2' ) $img_size = 'themesflat_sc_vc-rectangle2';
	    

	    $image_url = wp_get_attachment_image_src( $image, 'full' )[0];

		$image_html = sprintf(
			'<div class="wrap-image" ><img alt="image" src="%1$s" style="%2$s"/><span class="overlay" style="%3$s"></span></div>',
			wp_get_attachment_image_src( $image, $img_size )[0], $image_css, $overlay_css
		);

		if ( $image_crop == 'custom' && $image_size != '' ) { 			
	    	$image = wpb_getImageBySize( array( 'attach_id' => $image, 'thumb_size' => $image_size ) );
	  		$image = $image['thumbnail'];

	  		$src_pattern = '/<img.+src=[\'"]([^\'"]+)[\'"].*>/i';	    	
			$matches = array();
			if(preg_match($src_pattern, $image, $matches)) {
			    $image = $matches[1];
			    $image = trim($image);
			}

	  		$image_html = sprintf( '<div class="wrap-image"><img alt="image" src="%1$s" style="%2$s"/><span class="overlay" style="%3$s"></span></div>', $image, $image_css, $overlay_css );
	    }

	}


	$icon = themesflat_sc_vc_get_icon_class( $atts, 'icon' );

	if ( $image_style == 'single-image') {
		if ( $icon && $icon_type != '' ) {
			vc_icon_element_fonts_enqueue( $icon_type );
			$icon_html = sprintf(
				'<span class="icon-wrap" href="#" style="%2$s" %3$s>				
					<i class="%1$s"></i>
				</span>',
				$icon,
				$icon_css,
				$icon_css_js
			);
		}
	} else if ( $image_style == 'single-image-popup') {
		if ( $icon && $icon_type != '' ) {
			vc_icon_element_fonts_enqueue( $icon_type );
			$icon_html = sprintf(
				'<a class="icon-wrap image-popup" href="%2$s" style="%3$s" %4$s>				
					<i class="%1$s"></i>
				</a>',
				$icon,
				$image_url,
				$icon_css,
				$icon_css_js
			);
		}
	} else if ( $image_style == 'image-gallery-popup' ) {
		if ( $icon && $icon_type != '' ) {
			vc_icon_element_fonts_enqueue( $icon_type );
			$icon_html = sprintf(
				'<a class="icon-wrap image-popup-gallery" href="%2$s" style="%3$s" %4$s>				
					<i class="%1$s"></i>
				</a>',
				$icon,
				$image_url,
				$icon_css,
				$icon_css_js
			);
		}
	} 	
	
	$icon_cls = $icon_animation;

	$class .= ' '. $overlay_style .' ';
	
	if ( $style == 'style-1' ) {
		if ( $icon && $icon_type != '' ) {
			vc_icon_element_fonts_enqueue( $icon_type );
			$icon_html_video_map = sprintf(
				'<a class="icon-wrap popup-video popup-gmaps %5$s" href="%2$s" style="%3$s" %4$s>				
					<i class="%1$s"></i>
				</a>',
				$icon,
				$video_url,
				$icon_css,
				$icon_css_js,
				$icon_cls
			);
		}	
			
		return sprintf(
			'<div class="themesflat_sc_vc-image-video %3$s %4$s %5$s" style="%6$s">
				%1$s
				%2$s
			</div>',
			$image_html,	
			$icon_html_video_map,
			$icon_display,
			$image_effect,
			$class,
			$css
		);
	} else if ( $style == 'style-2' ) {
		$new_tab = $new_tab == 'yes' ? '_blank' : '_self';
		$heading_html = sprintf( '<%1$s class="heading" style="%3$s">%2$s</%1$s>', $tag, esc_html( $heading ), $heading_css );

		if ( $link_url )
			$heading_html = sprintf(
				'<%1$s class="heading"><a target="%3$s" href="%2$s" style="%5$s">
					%4$s
				</a></%1$s>',
				$tag,
				$link_url,
				$new_tab,
				esc_html( $heading ),
				$heading_css
			);

		return sprintf(
			'<div class="themesflat_sc_vc-image-heading %3$s">
				%1$s %2$s
			</div>',
			$image_html,
			$heading_html,
			$class
		);
	} else if ( $style == 'style-3' ) {		
		return sprintf(
			'<div class="themesflat_sc_vc-single-image %3$s %4$s %5$s" style="%6$s">
				%1$s
				%2$s
			</div>',
			$image_html,
			$icon_html,
			$icon_display,
			$image_effect,
			$class,
			$css
		);
	}
}