<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_lines_shortcode_params' );

function themesflat_lines_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Lines', 'evockans'),
        'description' => esc_html__('Displaying Lines with custom width and height.', 'evockans'),
        'base' => 'lines',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'params' => array(
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Alignment', 'evockans' ),
				'param_name' => 'alignment',
				'value'      => array(
					'Left' => 'left',
					'Center' => 'center',
					'Right' => 'right',
				),
				'std'		=> 'left',
			),
			array(
				'type' => 'headings',
				'text' => esc_html__('Line 1:', 'evockans'),
				'param_name' => 'line1_heading',
				'class' => '',
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line 1: Width', 'evockans'),
				'param_name' => 'line1_width',
				'value' => '100',
				'dependency' => array( 'element' => 'line_1_full', 'value' => 'no' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line 1: Height', 'evockans'),
				'param_name' => 'line1_height',
				'value' => '3',
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Line 1: Color', 'evockans'),
				'param_name' => 'line1_color',
				'value' => '#2387ea',
            ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Line 1: Full-width', 'evockans' ),
				'param_name' => 'line_1_full',
				'value'      => array(
					'No' => 'no',
					'Yes' => 'yes',
				),
				'std'		=> 'no',
			),
			array(
				'type' => 'headings',
				'text' => esc_html__('Line 2:', 'evockans'),
				'param_name' => 'line2_heading',
				'class' => '',
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line 2: Width', 'evockans'),
				'param_name' => 'line2_width',
				'value' => '',
				'dependency' => array( 'element' => 'line_2_full', 'value' => 'no' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line 2: Height', 'evockans'),
				'param_name' => 'line2_height',
				'value' => '',
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Line 2: Color', 'evockans'),
				'param_name' => 'line2_color',
				'value' => '',
            ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Line 2: Full-width', 'evockans' ),
				'param_name' => 'line_2_full',
				'value'      => array(
					'No' => 'no',
					'Yes' => 'yes',
				),
				'std'		=> 'no',
			),
	        // Spacing
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Top Margin', 'evockans'),
				'param_name' => 'top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
				'description' => esc_html__( 'Ex: 20px', 'evockans' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Bottom Margin', 'evockans'),
				'param_name' => 'bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
				'description' => esc_html__( 'Ex: 20px', 'evockans' ),
            ),
        )
    ) );
}

add_shortcode( 'lines', 'themesflat_shortcode_lines' );

/**
 * lines shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_lines( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'alignment' => 'left',
		'line1_height' => '3',
		'line2_height' => '',
		'line1_width' => '100',
		'line2_width' => '',
		'line1_color' => '#2387ea',
		'line2_color' => '',
		'line_1_full' => 'no',
		'line_2_full' => 'no',
		'top_margin' => '',
		'bottom_margin' => ''
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'lines', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$line1_html = $line2_html = '';

	$line1_height = intval( $line1_height );
	if ( empty( $line1_height ) ) $line1_height = 3;

	$line2_height = intval( $line2_height );

	$wrap_height = $line1_height > $line2_height ? $line1_height : $line2_height;

	$line1_width = intval( $line1_width );
	if ( empty( $line1_width ) ) $line1_width = 100;

	$line2_width = intval( $line2_width );

	$top_margin = intval( $top_margin );
	$bottom_margin = intval( $bottom_margin );

	$wrap_cls = '';
	$lines_cls = $alignment;
	if ( $line_1_full == 'yes' ) {
		$line1_width = '100%';
		$lines_cls .= ' line1-full';
	}

	if ( $line_2_full == 'yes' ) {
		$line2_width = '100%';
		$lines_cls .= ' line2-full';
	}

	$line_1_left = $line1_width / 2;
	$line_2_left = $line2_width / 2;

	$line_1_top = $line1_height / 2;
	$line_2_top = $line2_height / 2;

	$margin1_left = '';
	if ( $alignment == 'center' )
		$margin1_left = '; margin-left: -'. $line_1_left .'px';

	$margin2_left = '';
	if ( $alignment == 'center' )
		$margin2_left = '; margin-left: -'. $line_2_left .'px';

	$css = 'height: '. $wrap_height .'px;'; $css1 = $css2 = '';

	if ( $line_1_full == 'yes' ) {
		$css1 = 'height:' . $line1_height . 'px; width: '. $line1_width .'; background-color: '. $line1_color .'; margin-top: -'. $line_1_top .'px';
	} else {
		$css1 = 'height:' . $line1_height . 'px; width: '. $line1_width .'px; background-color: '. $line1_color . $margin1_left .'; margin-top: -'. $line_1_top .'px';
	}

	if ( $line_2_full == 'yes' ) {
		$css2 = 'height:' . $line2_height . 'px; width: '. $line2_width .'; background-color: '. $line2_color .'; margin-top: -'. $line_2_top .'px';
	} else {
		$css2 = 'height:' . $line2_height . 'px; width: '. $line2_width .'px; background-color: '. $line2_color . $margin2_left .'; margin-top: -'. $line_2_top .'px';
	}

	if ( $top_margin ) $css .= 'margin-top:'. $top_margin .'px;';
	if ( $bottom_margin ) $css .= 'margin-bottom:'. $bottom_margin .'px;';

	if ( $line1_width && $line1_height ) {
		$line1_html = sprintf('<div class="line-1" style="%1$s"></div>', $css1 );
	}
	
	if ( $line2_width && $line2_height ) {
		$line2_html = sprintf('<div class="line-2" style="%1$s"></div>', $css2 );
	}

	return sprintf(
		'<div class="themesflat_sc_vc-lines clearfix %4$s" style="%1$s">
			%2$s
			%3$s
		</div>', 
		$css, $line1_html, $line2_html, $lines_cls
	);
}