<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_divider_shortcode_params' );

function themesflat_divider_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Divider', 'evockans'),
        'description' => esc_html__('Displaying lines separator.', 'evockans'),
        'base' => 'divider',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'params' => array(
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Alignment', 'evockans' ),
				'param_name' => 'alignment',
				'value'      => array(
					'Left' => 'left',
					'Center' => 'center',
					'Right' => 'right',
				),
				'std'		=> 'center',
				'dependency' => array( 'element' => 'icon_display', 'value' => 'no-icon' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Line Style', 'evockans' ),
				'param_name' => 'style',
				'value'      => array(
					'Solid' => 'solid',
					'Dotted' => 'dotted',
					'Dashed' => 'dashed',
					'Double' => 'double',
				),
				'std'		=> 'solid',
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line: Width (Optional)', 'evockans'),
				'param_name' => 'width',
				'value' => '',
				'description' => esc_html__( 'Leave it empty to set full-width.', 'evockans' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line: Height', 'evockans'),
				'param_name' => 'height',
				'value' => '1px',
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Line: Color', 'evockans'),
				'param_name' => 'color',
				'value' => '',
            ),
			// Icon
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Icon to Display', 'evockans' ),
				'param_name' => 'icon_display',
				'value'      => array(
					'No Icon' => 'no-icon',
					'Icon Font' => 'icon-font',
					'Icon Image' => 'icon-image',
					'Text' 			=> 'text',
				),
				'std'		=> 'no-icon',
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'evockans' ),
				'param_name' => 'icon_type',
				'description' => esc_html__( 'Select icon library.', 'evockans' ),
				'value' => array(
					esc_html__( 'None', 'evockans' ) => '',
					esc_html__( 'Mono Social', 'evockans' ) => 'monosocial',
					esc_html__( 'Simple Line', 'evockans' ) => 'simpleline',
					esc_html__( 'Font Ionicons', 'evockans' ) => 'ionicons',
					esc_html__( 'Font Elegant', 'evockans' ) => 'eleganticons',
					esc_html__( 'Font Themify Icons', 'evockans' ) => 'themifyicons',
					esc_html__( 'Font Icomoon Icons', 'evockans' ) => 'icomoon',
					esc_html__( 'FontAwesome', 'evockans' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'evockans' ) => 'openiconic',
					esc_html__( 'Typicons', 'evockans' ) => 'typicons',
					esc_html__( 'Entypo', 'evockans' ) => 'entypo',
					esc_html__( 'Linecons', 'evockans' ) => 'linecons',
				),
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-font' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_monosocial',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'monosocial',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'monosocial',
			    ),
			    'group' => esc_html__( 'Icon Or Text', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_simpleline',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'simpleline',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'simpleline',
			    ),
			    'group' => esc_html__( 'Icon Or Text', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_ionicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'ionicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'ionicons',
			    ),
			    'group' => esc_html__( 'Icon Or Text', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_eleganticons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'eleganticons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'eleganticons',
			    ),
			    'group' => esc_html__( 'Icon Or Text', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_themifyicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'themifyicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'themifyicons',
			    ),
			    'group' => esc_html__( 'Icon Or Text', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_icomoon',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'icomoon',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'icomoon',
			    ),
			    'group' => esc_html__( 'Icon Or Text', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon',
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'fontawesome',
				),
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_openiconic',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'openiconic',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'openiconic',
				),
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_typicons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'typicons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'typicons',
				),
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_entypo',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'entypo',
					'iconsPerPage' => 300,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'entypo',
				),
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_linecons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'linecons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'linecons',
				),
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon: Color', 'evockans'),
				'param_name' => 'icon_color',
				'value' => '',
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-font' ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon: Font Size', 'evockans'),
				'param_name' => 'icon_font_size',
				'value' => '',
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-font' ),
	        ),
	        // Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Icon Image', 'evockans'),
				'param_name' => 'image',
				'value' => '',
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-image' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Image: Width', 'evockans'),
				'param_name' => 'image_width',
				'value' => '',
				'description'	=> esc_html__('Ex: 50px', 'evockans'),
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-image' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon, Image, Text: Padding', 'evockans'),
				'param_name' => 'icon_padding',
				'value' => '',
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 12px 0px 12px', 'evockans'),
				'dependency' => array( 'element' => 'icon_display', 'value' => array( 'icon-font', 'icon-image', 'text' ) ),
	        ),
	        //Text
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Text', 'evockans'),
				'param_name' => 'text_enter',
				'value' => '',
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'text' ),
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Text: Color', 'evockans'),
				'param_name' => 'text_color',
				'value' => '',
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'text' ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Text: Font Size', 'evockans'),
				'param_name' => 'text_font_size',
				'value' => '',
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'text' ),
	        ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Text: Font Family', 'evockans' ),
				'param_name' => 'text_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'text' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Text: Font Weight', 'evockans' ),
				'param_name' => 'text_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'100' => '100',
					'200' => '200',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Icon Or Text', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'text' ),
			),
	        // Spacing
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Line: Top Margin', 'evockans'),
				'param_name' => 'top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Line: Bottom Margin', 'evockans'),
				'param_name' => 'bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
        )
    ) );
}

add_shortcode( 'divider', 'themesflat_shortcode_divider' );

/**
 * divider shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_divider( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'alignment' => 'center',
		'style' => 'solid',
		'width' => '',
		'height' => '1px',
		'color' => '',
		'icon_display' => 'no-icon',
		'icon_type' => '',
		'icon' => '',
		'icon_color' => '',
		'icon_font_size' => '',
		'icon_padding' => '',
		'image' => '',
		'image_width' => '',
		'text_enter' => '',
		'text_color' => '',
		'text_font_size' => '',
		'text_font_family' => '',
		'text_font_weight' => '',
		'top_margin' => '',
		'bottom_margin' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'divider', $atts ));

	$width = intval( $width );
	$height = intval( $height );
	$icon_font_size = intval( $icon_font_size );
	$text_font_size = intval( $text_font_size );
	$image_width = intval( $image_width );
	$top_margin = intval( $top_margin );
	$bottom_margin = intval( $bottom_margin );


	$css1 =  $css2 = $icon_html = $icon_cls = $icon_css = $text_css = $icon_css2 = $border_html_before = $border_html_after = $border_css = '';
	$cls = 'divider-'. $alignment;

	if ( $style == 'solid') $cls .= ' divider-solid';
	if ( $style == 'dotted') $cls .= ' divider-dotted';
	if ( $style == 'dashed') $cls .= ' divider-dashed';
	if ( $style == 'double') $cls .= ' divider-double';

	if ( $icon_display == 'no-icon' ) {
		if ( $width ) $css1 .= 'width:'. $width .'px;';
		if ( $height ) $css1 .= 'border-width:'. $height .'px;';
		if ( $color == '#1a7dd7' ) {
			$cls .= ' accent';
		} else {
			if ( $color ) $css1 .= 'border-top-color:'. $color .';';	
		}

		if ( $top_margin ) $css1 .= 'margin-top:'. $top_margin .'px;';
		if ( $bottom_margin ) $css1 .= 'margin-bottom:'. $bottom_margin .'px;';

		printf( '<div class="themesflat_sc_vc-divider %1$s" style="%2$s"></div><div class="clearfix"></div>', $cls, $css1 );
	}

	if ( $icon_display == 'icon-font' || $icon_display == 'icon-image' || $icon_display == 'text' ) {
		$icon = themesflat_sc_vc_get_icon_class( $atts, 'icon' );

		if ( $width ) $css2 = 'width:'. $width .'px;';
		if ( $top_margin ) $css2 .= 'margin-top:'. $top_margin .'px;';
		if ( $bottom_margin ) $css2 .= 'margin-bottom:'. $bottom_margin .'px;';

		if ( $height ) $border_css = 'border-bottom-width:'. $height .'px;';
		if ( $color ) $border_css .= 'border-color:'. $color .';';
		if ( $icon_padding ) $icon_css2 = 'padding:'. $icon_padding .';';		

		if ( $icon_display == 'icon-font' && $icon && $icon_type != '' ) {
			vc_icon_element_fonts_enqueue( $icon_type );

			if ( $icon_font_size ) $icon_css .= 'font-size:'. $icon_font_size .'px;';

			if ( $icon_color == '#1a7dd7' ) {
				$icon_cls .= ' accent';
			} else {
				if ( $icon_color ) $icon_css .= 'color:'. $icon_color .';';	
			}

			$icon_html = sprintf('<span class="%1$s %3$s" style="%2$s"></span>', $icon, $icon_css, $icon_cls );
		}

		if ( $icon_display == 'icon-image' ) {
			if ( $image )
			$icon_html = sprintf(
				'<img alt="image" src="%1$s" width="%2$s">',
				wp_get_attachment_image_src( $image, 'full' )[0],
				$image_width
			);
		}

		if ( $icon_display == 'text' ) {
			if ( $text_enter )

			if ( $text_color ) $text_css .= 'color:'. $text_color .';';
			if ( $text_font_size ) $text_css .= 'font-size:'. $text_font_size .'px;';
			if ( $text_font_weight ) $text_css .= 'font-weight:'. $text_font_weight .';';
			if ( $text_font_family ) $text_css .= 'font-family:'. $text_font_family .';';

			$icon_html = sprintf('<span property="text" style="%2$s">%1$s</span>',$text_enter, $text_css );
		}

		if( $height ){
			$border_html_before = sprintf('<span class="divider-icon-before %2$s" style="%1$s"></span>', $border_css, $cls);
			$border_html_after = sprintf('<span class="divider-icon-after %2$s" style="%1$s"></span>', $border_css, $cls);
		}

		return sprintf(
			'<div class="themesflat_sc_vc-divider has-icon" style="%1$s">
				<div class="divider-icon">
					%4$s
					<span class="icon-wrap" style="%3$s">%2$s</span>
					%5$s
				</div>
			</div><div class="clearfix"></div>',
			$css2,
			$icon_html,
			$icon_css2,
			$border_html_before,
			$border_html_after
		);
	}
}