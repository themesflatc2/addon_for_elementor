<?php
/**
 * Register filter for append custom class name
 * that generated from visual-composer
 */
class WPBakeryShortCode_modalpopups extends WPBakeryShortCodesContainer {}
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_modalpopups_shortcode_params' );

function themesflat_modalpopups_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Modal popups', 'evockans'),
        'description' => esc_html__('Displaying Modal popups.', 'evockans'),
        'base' => 'modalpopups',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'as_parent' => array('except' => 'modalpopups'),
        'controls' => 'full',
		'show_settings_on_create' => true,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'js_view' => 'VcColumnView',
        'params' => array(
        	array(
				'type' => 'textfield',
				'heading' => esc_html__('Button: Text', 'evockans'),
				'param_name' => 'button_text',
				'value' => 'Open Modal',
				'group' => esc_html__( 'Button', 'evockans' ),
	        ),
        	array(
				'type' => 'textfield',
				'heading' => esc_html__('Element ID: Button & Popups', 'evockans'),
				'param_name' => 'element_id',
				'value' => 'modal1',
				'group' => esc_html__( 'Button', 'evockans' ),
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Button: Text Color', 'evockans'),
				'param_name' => 'button_text_color',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
            ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Button: Background Color', 'evockans'),
				'param_name' => 'button_background_color',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Button: Border Width', 'evockans'),
				'param_name' => 'border_width',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
				'group' => esc_html__( 'Button', 'evockans' ),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Button: Border Color', 'evockans'),
				'param_name' => 'border_color',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
            ),
        	array(
				'type' => 'textfield',
				'heading' => esc_html__('Button: Rounded', 'evockans'),
				'param_name' => 'button_rounded',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button: Padding', 'evockans'),
				'param_name' => 'button_padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Button', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button: Margin', 'evockans'),
				'param_name' => 'button_margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Button', 'evockans' ),
	        ),
	        //Popups
        	array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style Modal Popups', 'evockans' ),
				'param_name' => 'style_modal_popups',
				'value'      => array(
					'Default' => '',
					'Popup Fade' => 'popup-fade',
					'Popup Bounce' => 'popup-bounce',
					'Popup Flip' => 'popup-flip',
				),
				'std'		=> '',
				'group' => esc_html__( 'Popups', 'evockans' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Content: Width', 'evockans'),
				'param_name' => 'popups_content_width',
				'value' => '400',
				'group' => esc_html__( 'Popups', 'evockans' ),
	        ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Content Popups: Padding', 'evockans'),
				'param_name' => 'popups_content_padding',
				'value' => '30px 30px 30px 30px',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Popups', 'evockans' ),
	        ), 
	        // Typography
	        array(
				'type' => 'headings',
				'text' => esc_html__('Setting Button', 'evockans'),
				'param_name' => 'setting_typography_button',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'class' => '',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Family', 'evockans' ),
				'param_name' => 'font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Weight', 'evockans' ),
				'param_name' => 'font_weight',
				'value'      => array(
					'Default' => 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Font Size', 'evockans'),
				'param_name' => 'font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Line-Height', 'evockans'),
				'param_name' => 'line_height',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),       
	        
        )
    ) );
}

add_shortcode( 'modalpopups', 'themesflat_shortcode_modalpopups' );

/**
 * modalpopups shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_modalpopups( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'button_text' => 'Open Modal',
		'element_id' => 'modal1',
		'button_text_color' => '',
		'button_background_color' => '',
		'border_width' => '',
		'border_color' => '',
		'button_rounded' => '',
		'button_padding' => '',
		'button_margin' => '',
	    'style_modal_popups' => '',	    
	    'popups_content_width' => '400',
	    'popups_content_padding' => '30px 30px 30px 30px',
	    'font_family' => 'Default',
		'font_weight' => 'Default',
		'font_size' => '',
		'line_height' => '',
	), $atts ) );	
	//extract($atts = vc_map_get_attributes( 'modalpopups', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$css = $content_html = $button_html = $button_css = $popup_cls = $content_width = $popup_css = $transition_In = $transition_Out = '';
	
	$popups_content_width = intval($popups_content_width);
	$button_rounded = intval( $button_rounded );
	$font_size = intval( $font_size );
	$line_height = intval( $line_height );

	if ($popups_content_width) $content_width .= $popups_content_width . 'px';
	if ( $button_text_color ) $button_css .= 'color:'. $button_text_color .';';
	if ( $button_background_color ) $button_css .= 'background:'. $button_background_color .';';
	if ( $border_color && $border_width ) $button_css .= 'border-style:solid;border-width:'. $border_width .';border-color:'. $border_color .';';
	if ( $button_rounded ) $button_css .= 'border-radius:'. $button_rounded .'px;';
	if ( $button_padding ) $button_css .= 'padding:'. $button_padding .';';
	if ( $button_margin ) $button_css .= 'margin:'. $button_margin .';';
	if ( $font_family != 'Default' ) $button_css .= 'font-family:'. $font_family .';';
	if ( $font_weight != 'Default' ) $button_css .= 'font-weight:'. $font_weight .';';
	if ( $font_size ) $button_css .= 'font-size:'. $font_size .'px;';
	if ( $line_height ) $button_css .= 'line-height:'. $line_height .'px;';

	if ($popups_content_padding)  $popup_css .= 'padding:'. $popups_content_padding . ';';

	if ( $style_modal_popups == 'popup-fade' ) {
		$transition_In  = 'data-iziModal-transitionIn="fadeIn"';
		$transition_Out = 'data-iziModal-transitionOut="fadeOut"';
	} 
	else if ( $style_modal_popups == 'popup-bounce' ) {
		$transition_In = 'data-iziModal-transitionIn="bounceInDown"';
		$transition_Out = 'data-iziModal-transitionOut="bounceOutDown"';
	}
	else if ( $style_modal_popups == 'popup-flip' ) {
		$transition_In = 'data-iziModal-transitionIn="flipInX"';
		$transition_Out = 'data-iziModal-transitionOut="flipOutX"';
	}
	else {
		$transition_In = 'data-iziModal-transitionIn="comingIn"';
		$transition_Out = '';
	}

	if ( $button_text && $element_id ) {		
		$button_html .= sprintf('<button class="button-modal-popups" data-izimodal-open="#%2$s" style="%3$s">%1$s</button>', $button_text, $element_id, $button_css );

		$content_html = sprintf('
			<div class="izimodal" id="%4$s" data-iziModal-width="%7$s" data-iziModal-fullscreen="true" %5$s %6$s>
				<div class="close-modal">
					<button class="close-modal" data-izimodal-close><i class="ti-close"></i></button>
				</div>			
				
			    <div class="modal-inside %2$s" style="%3$s">
			    	%1$s					
			    </div>			    
			</div>',
			do_shortcode($content),
			$popup_cls,
			$popup_css,
			$element_id,
			$transition_In,
			$transition_Out,
			$content_width
		);
	}	

	return sprintf( '
		<div class="themesflat_sc_vc-modalpopups clearfix">
			%1$s
			%2$s
		</div>',
		$button_html,
		$content_html
	);
}