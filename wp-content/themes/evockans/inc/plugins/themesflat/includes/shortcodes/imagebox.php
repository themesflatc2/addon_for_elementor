<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_imagebox_shortcode_params' );

function themesflat_imagebox_shortcode_params() {
	vc_map( array(
	    'name' => esc_html__('Image Box', 'evockans'),
	    'description' => esc_html__('Displaying image box.', 'evockans'),
	    'base' => 'imagebox',
		'weight'	=>	180,
	    'icon' => THEMESFLAT_ICON,
	    'category' => esc_html__('THEMESFLAT', 'evockans'),
	    'params' => array(
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Alignment', 'evockans' ),
				'param_name' => 'alignment',
				'value'      => array(
					'Left' => '',
					'Center' => 'text-center',
					'Right' => 'text-right',
				),
				'std'		=> '',
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Background Color', 'evockans'),
				'param_name' => 'background_color',
				'value' => '',				
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Rounded', 'evockans'),
				'param_name' => 'rounded',
				'value' => '',
				'description'	=> esc_html__('ex: 5px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Border Width', 'evockans'),
				'param_name' => 'border_width',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Border Color', 'evockans'),
				'param_name' => 'border_color',
				'value' => '',
            ),
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Box Shadow?', 'evockans' ),
				'param_name' => 'box_shadow',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Horizontal (Required)', 'evockans'),
				'param_name' => 'horizontal',
				'value' => '',
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
				'dependency' => array( 'element' => 'box_shadow', 'value' => 'yes' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Vertical (Required)', 'evockans'),
				'param_name' => 'vertical',
				'value' => '',
				'description'	=> esc_html__('Ex: 20px', 'evockans'),
				'dependency' => array( 'element' => 'box_shadow', 'value' => 'yes' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Blur (Required)', 'evockans'),
				'param_name' => 'blur',
				'value' => '',
				'description'	=> esc_html__('Ex: 40px', 'evockans'),
				'dependency' => array( 'element' => 'box_shadow', 'value' => 'yes' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Spread (Required)', 'evockans'),
				'param_name' => 'spread',
				'value' => '',
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
				'dependency' => array( 'element' => 'box_shadow', 'value' => 'yes' ),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Box Shadow Color (Required)', 'evockans'),
				'param_name' => 'shadow_color',
				'value' => '',
				'description'	=> esc_html__('Ex: #eeeeee', 'evockans'),
				'dependency' => array( 'element' => 'box_shadow', 'value' => 'yes' ),
            ),
			// Image
			array(
				'type' => 'attach_image',
				'holder' => 'img',
				'heading' => esc_html__('Image', 'evockans'),
				'param_name' => 'image',
				'value' => '',
				'group' => esc_html__( 'Image', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Image Cropping', 'evockans' ),
				'param_name' => 'image_crop',
				'value'      => array(
					'Full' => 'full',
					'600 x 600' => 'square',
					'600 x 500' => 'rectangle',
					'600 x 390' => 'rectangle2',
					'870 x auto' => 'auto1',
					'600 x auto' => 'auto2',
					'480 x auto' => 'auto3',
					'Custom' => 'custom',
				),
				'std'		=> 'full',
				'description'	=> esc_html__('Select "auto" to keep the same aspect ratio when cropping.', 'evockans'),
				'group' => esc_html__( 'Image', 'evockans' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Image Size', 'evockans' ),
				'description'    => esc_html__( '( Alternatively enter size in pixels (Example: 750x750 (Width x Height)).', 'evockans' ),
				'param_name' => 'image_size',
				'value' => '750x750',
				'dependency' => array( 'element' => 'image_crop', 'value' => 'custom' ),
				'group' => esc_html__( 'Image', 'evockans' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Image Rounded', 'evockans'),
				'param_name' => 'image_rounded',
				'value' => '',
				'description'	=> esc_html__('ex: 5px', 'evockans'),
				'group' => esc_html__( 'Image', 'evockans' ),
	        ),
	        array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Add Overlay Image?', 'evockans' ),
				'param_name' => 'add_overlay_image',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Image', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Overlay Background Style', 'evockans' ),
				'param_name' => 'overlay_bg_style',
				'value'      => array(
					'Default' => 'default',
					'Gradient Top To Bottom' => 'gradient-top-bottom',
					'Gradient Left To Right' => 'gradient-left-right',
					'Gradient Radial' => 'radial-gradient',
				),
				'std'		=> '',
				'dependency' => array( 'element' => 'add_overlay_image', 'value' => 'yes' ),
				'group' => esc_html__( 'Image', 'evockans' ),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Color Overlay', 'evockans'),
				'param_name' => 'color_overlay_img',
				'value' => 'rgba(35,135,234,0.5)',
				'group' => esc_html__( 'Image', 'evockans' ),
				'dependency' => array( 'element' => 'overlay_bg_style', 'value' => 'default' ),			
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Color Gradient Start Overlay', 'evockans'),
				'param_name' => 'color_overlay_gradient_1',
				'value' => '',
				'group' => esc_html__( 'Image', 'evockans' ),
				'dependency' => array( 'element' => 'overlay_bg_style', 'value' => array('gradient-top-bottom','gradient-left-right','radial-gradient') ),			
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Color Gradient Last Overlay', 'evockans'),
				'param_name' => 'color_overlay_gradient_2',
				'value' => '',
				'group' => esc_html__( 'Image', 'evockans' ),
				'dependency' => array( 'element' => 'overlay_bg_style', 'value' => array('gradient-top-bottom','gradient-left-right','radial-gradient') ),			
            ),
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Overlay Image Hover?', 'evockans' ),
				'param_name' => 'overlay_image_hover',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Image', 'evockans' ),
				'dependency' => array( 'element' => 'add_overlay_image', 'value' => 'yes' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Overlay Hover Effect', 'evockans' ),
				'param_name' => 'overlay_hover_effect',
				'value'      => array(
					'Fade In' => 'fade-in',
					'Top To Bottom' => 'top-to-bottom',
					'Bottom To Top' => 'bottom-to-top',
					'Left To Right' => 'left-to-right',
					'Right To Left' => 'right-to-left',
					'Zoom In' => 'zoom-in',
				),
				'std'		=> 'fade-in',
				'dependency' => array( 'element' => 'overlay_image_hover', 'value' => 'yes' ),
				'group' => esc_html__( 'Image', 'evockans' ),
			),			
			// Content
			array(
				'type' => 'headings',
				'text' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading_content',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Tag', 'evockans' ),
				'param_name' => 'tag',
				'value'      => array(
					'H1' => 'h1',
					'H2' => 'h2',
					'H3' => 'h3',
					'H4' => 'h4',
					'H5' => 'h5',
					'H6' => 'h6',
				),
				'std'		=> 'h3',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
            array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading',
				'value' => 'Heading Text',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Heading Color', 'evockans'),
				'param_name' => 'heading_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),				
            ),
            array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Add Prefix Heading?', 'evockans' ),
				'param_name' => 'add_prefix_heading',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Prefix Heading Style', 'evockans' ),
				'param_name' => 'prefix_heading_style',
				'value'      => array(
					'Circle' => 'prefix-circle',
					'Disc' => 'prefix-disc',
					'Square' => 'prefix-square',
					'Decimal' => 'prefix-decimal',
					'Decimal Leading Zero' => 'prefix-decimal-leading-zero',
					'Lower Alpha' => 'prefix-lower-alpha',
					'Upper Alpha' => 'prefix-upper-alpha',
					'Upper Roman' => 'prefix-upper-roman',
					
				),
				'std'		=> 'prefix-decimal-leading-zero',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'add_prefix_heading', 'value' => 'yes' ),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Prefix Heading Color', 'evockans'),
				'param_name' => 'prefix_heading_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),	
				'dependency' => array( 'element' => 'add_prefix_heading', 'value' => 'yes' ),			
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Padding Left', 'evockans'),
				'param_name' => 'heading_padding_left',
				'value' => '20px',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'add_prefix_heading', 'value' => 'yes' ),
            ),
			/*Sub Heading*/
            array(
				'type' => 'headings',
				'text' => esc_html__('Sub Heading', 'evockans'),
				'param_name' => 'sub_heading_content',
				'group' => esc_html__( 'Content', 'evockans' ),
			),			
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Sub Heading', 'evockans'),
				'param_name' => 'sub_heading',
				'value' => 'Sub Heading Text',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Sub Heading Color', 'evockans'),
				'param_name' => 'sub_heading_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),				
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Sub Heading Background Color', 'evockans'),
				'param_name' => 'sub_heading_bg_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),				
            ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Separator', 'evockans'),
				'param_name' => 'separator_content',
				'group' => esc_html__( 'Content', 'evockans' ),				
			),			
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Separator', 'evockans' ),
				'param_name' => 'separator',
				'value'      => array(
					'No Separator' => '',
					'Line' => 'line'
				),
				'std'		=> '',
				'group' => esc_html__( 'Content', 'evockans' ),				
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Line: Full-width', 'evockans' ),
				'param_name' => 'line_full',
				'value'      => array(
					'No' => 'no',
					'Yes' => 'yes',
				),
				'std'		=> 'no',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line Width', 'evockans'),
				'param_name' => 'line_width',
				'value' => '50',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'line_full', 'value' => 'no' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line Height', 'evockans'),
				'param_name' => 'line_height',
				'value' => '2',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Line Color', 'evockans'),
				'param_name' => 'line_color',
				'value' => '#eee',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
            ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Description', 'evockans'),
				'param_name' => 'desc_content',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' 		=> 'textarea',
				'heading' 	=> esc_html__('Description', 'evockans'),
				'param_name' 	=> 'description',
				'value' 		=> '',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Description Color', 'evockans'),
				'param_name' => 'desc_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),				
            ),
            array(
				'type' => 'headings',
				'text' => esc_html__('Wrap Content', 'evockans'),
				'param_name' => 'wrap_content',
				'group' => esc_html__( 'Content', 'evockans' ),
			),			
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Content Padding', 'evockans'),
				'param_name' => 'content_padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 20px', 'evockans'),	
				'group' => esc_html__( 'Content', 'evockans' ),				
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content Margin', 'evockans'),
				'param_name' => 'content_margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),	
				'group' => esc_html__( 'Content', 'evockans' ),				
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Content: Background Color', 'evockans'),
				'param_name' => 'content_bg_color',
				'value' => '#ffffff',
				'group' => esc_html__( 'Content', 'evockans' ),				
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content Rounded', 'evockans'),
				'param_name' => 'content_rounded',
				'value' => '',
				'description'	=> esc_html__('ex: 5px', 'evockans'),
				'group' => esc_html__( 'Content', 'evockans' ),
	        ),
            array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Content: Position', 'evockans' ),
				'param_name' => 'position_content',
				'value'      => '',
				'description'	=> esc_html__('Top Or Bottom. Default: 0px ex: -45px or 45px', 'evockans'),
				'group' => esc_html__( 'Content', 'evockans' ),	
			),
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Content Hover?', 'evockans' ),
				'param_name' => 'content_hover',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Content Hover Offset: Horizontal', 'evockans' ),
				'param_name' => 'content_hover_offset',
				'value'      => array(
					'-40' => '-40',
					'-35' => '-35',
					'-30' => '-30',
					'-25' => '-25',
					'-20' => '-20',
					'-15' => '-15',
					'-10' => '-10',
					'0' => '0',
					'10' => '10',
					'15' => '15',
					'20' => '20',
					'25' => '25',
					'30' => '30',
					'35' => '35',
					'40' => '40',
				),
				'std'		=> '-20',
				'dependency' => array( 'element' => 'content_hover', 'value' => 'yes' ),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			// Content Box Shadow
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Content Box Shadow?', 'evockans' ),
				'param_name' => 'content_box_shadow',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Horizontal (Required)', 'evockans'),
				'param_name' => 'content_horizontal',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
				'dependency' => array( 'element' => 'content_box_shadow', 'value' => 'yes' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Vertical (Required)', 'evockans'),
				'param_name' => 'content_vertical',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
				'description'	=> esc_html__('Ex: 5px', 'evockans'),
				'dependency' => array( 'element' => 'content_box_shadow', 'value' => 'yes' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Blur (Required)', 'evockans'),
				'param_name' => 'content_blur',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
				'description'	=> esc_html__('Ex: 30px', 'evockans'),
				'dependency' => array( 'element' => 'content_box_shadow', 'value' => 'yes' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Spread (Required)', 'evockans'),
				'param_name' => 'content_spread',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
				'dependency' => array( 'element' => 'content_box_shadow', 'value' => 'yes' ),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Box Shadow Color (Required)', 'evockans'),
				'param_name' => 'content_shadow_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
				'description'	=> esc_html__('Ex: #eeeeee', 'evockans'),
				'dependency' => array( 'element' => 'content_box_shadow', 'value' => 'yes' ),
            ),
            // Content Add Icon
            array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Add Icon Content?', 'evockans' ),
				'param_name' => 'add_icon_content',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'evockans' ),
				'param_name' => 'icon_type',
				'description' => esc_html__( 'Select icon library.', 'evockans' ),
				'value' => array(
					esc_html__( 'None', 'evockans' ) => '',
					esc_html__( 'Mono Social', 'evockans' ) => 'monosocial',
					esc_html__( 'Simple Line', 'evockans' ) => 'simpleline',
					esc_html__( 'Font Ionicons', 'evockans' ) => 'ionicons',
					esc_html__( 'Font Elegant', 'evockans' ) => 'eleganticons',
					esc_html__( 'Font Themify Icons', 'evockans' ) => 'themifyicons',
					esc_html__( 'Font Icomoon Icons', 'evockans' ) => 'icomoon',
					esc_html__( 'FontAwesome', 'evockans' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'evockans' ) => 'openiconic',
					esc_html__( 'Typicons', 'evockans' ) => 'typicons',
					esc_html__( 'Entypo', 'evockans' ) => 'entypo',
					esc_html__( 'Linecons', 'evockans' ) => 'linecons',
				),
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'add_icon_content', 'value' => 'yes' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_monosocial',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'monosocial',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'monosocial',
			    ),
			    'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_simpleline',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'simpleline',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'simpleline',
			    ),
			    'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_ionicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'ionicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'ionicons',
			    ),
			    'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_eleganticons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'eleganticons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'eleganticons',
			    ),
			    'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_themifyicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'themifyicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'themifyicons',
			    ),
			    'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_icomoon',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'icomoon',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'icomoon',
			    ),
			    'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon',
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'fontawesome',
				),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_openiconic',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'openiconic',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'openiconic',
				),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_typicons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'typicons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'typicons',
				),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_entypo',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'entypo',
					'iconsPerPage' => 300,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'entypo',
				),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_linecons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'linecons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'linecons',
				),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon Color', 'evockans'),
				'param_name' => 'icon_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'add_icon_content', 'value' => 'yes' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Font Size', 'evockans'),
				'param_name' => 'icon_font_size',
				'value' => '50px',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'add_icon_content', 'value' => 'yes' ),
	        ),
	        array(
				'type' => 'number',
				'heading' => esc_html__('Icon Position Left', 'evockans'),
				'param_name' => 'icon_position_left',
				'value' => 20,
				'suffix' => 'px',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'add_icon_content', 'value' => 'yes' ),
		  	),
		  	array(
				'type' => 'number',
				'heading' => esc_html__('Icon Position Top', 'evockans'),
				'param_name' => 'icon_position_top',
				'value' => 20,
				'suffix' => 'px',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'add_icon_content', 'value' => 'yes' ),
		  	),
	        // Button
	        array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Content Text & Button Inline?', 'evockans' ),
				'param_name' => 'button_inline',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'description'	=> esc_html__('Displaying Content Text & Button Inline', 'evockans'),
				'group' => esc_html__( 'Button', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Text', 'evockans'),
				'param_name' => 'button_text',
				'value' => 'READ MORE',
				'group' => esc_html__( 'Button', 'evockans' ),
				'description'	=> esc_html__('Leave it empty to disable.', 'evockans'),
	        ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Link (URL):', 'evockans'),
				'param_name' => 'link_url',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
            ),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Open Link in a new Tab', 'evockans' ),
				'param_name' => 'new_tab',
				'value' => array(
					'Yes' => 'yes',
					'No' => 'no',
				),
				'std'		=> 'yes',
				'group' => esc_html__( 'Button', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Style', 'evockans' ),
				'param_name' => 'button_style',
				'value'      => array(
					'Simple Link' => 'simple_link',
					'Accent' => 'accent',
					'Dark' => 'dark',
					'Light' => 'light',
					'Very Light' => 'very-light',
					'White' => 'white',
					'Outline' => 'outline',
					'Outline Dark' => 'outline_dark',
					'Outline Light' => 'outline_light',
					'Outline Very light' => 'outline_very-light',
					'Outline White' => 'outline_white',
				),
				'std'		=> 'simple_link',
				'group' => esc_html__( 'Button', 'evockans' ),				
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Size', 'evockans' ),
				'param_name' => 'button_size',
				'value'      => array(
					'Default' => '',
					'Small' => 'small',
					'Very Small' => 'xsmall',
					'Big' => 'big',
				),
				'std'		=> 'small',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array(
					'element' => 'button_style',
					'value' => array (
						'accent',
						'dark',
						'light',
						'very-light',
						'white',
						'outline',
						'outline_dark',
						'outline_light',
						'outline_very-light',
						'outline_white'
					)
				),
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Rounded', 'evockans'),
				'param_name' => 'button_rounded',
				'value' => '',
				'description'	=> esc_html__('ex: 10px', 'evockans'),
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array(
					'element' => 'button_style',
					'value' => array (
						'accent',
						'dark',
						'light',
						'very-light',
						'white',
						'outline',
						'outline_dark',
						'outline_light',
						'outline_very-light',
						'outline_white'
					)
				),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Padding', 'evockans'),
				'param_name' => 'button_padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array(
					'element' => 'button_style',
					'value' => array (
						'accent',
						'dark',
						'light',
						'very-light',
						'white',
						'outline',
						'outline_dark',
						'outline_light',
						'outline_very-light',
						'outline_white'
					)
				),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Link Color', 'evockans'),
				'param_name' => 'link_color',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'button_style', 'value' => 'simple_link' ),
            ),
            array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Button Full Width', 'evockans' ),
				'param_name' => 'button_full',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Button', 'evockans' ),
			),
            /*Button Icon*/
            array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Add Icon Button', 'evockans' ),
				'param_name' => 'add_icon_button',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Button', 'evockans' ),
			),	        
			// Typography
			array(
				'type' => 'headings',
				'text' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Font Family', 'evockans' ),
				'param_name' => 'heading_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Font Weight', 'evockans' ),
				'param_name' => 'heading_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Font Size', 'evockans'),
				'param_name' => 'heading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Line-Height', 'evockans'),
				'param_name' => 'heading_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'headings',
				'text' => esc_html__('Prefix Heading', 'evockans'),
				'param_name' => 'prefix_heading_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Prefix Heading Font Size', 'evockans'),
				'param_name' => 'prefix_heading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'headings',
				'text' => esc_html__('Sub Heading', 'evockans'),
				'param_name' => 'sub_heading_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Sub Heading Font Family', 'evockans' ),
				'param_name' => 'sub_heading_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Sub Heading Font Weight', 'evockans' ),
				'param_name' => 'sub_heading_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Sub Heading Font Size', 'evockans'),
				'param_name' => 'sub_heading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Sub Heading Line-Height', 'evockans'),
				'param_name' => 'sub_heading_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Description', 'evockans'),
				'param_name' => 'desc_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Description Font Family', 'evockans' ),
				'param_name' => 'desc_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Description Font Weight', 'evockans' ),
				'param_name' => 'desc_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Description Font Size', 'evockans'),
				'param_name' => 'desc_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Description Line-Height', 'evockans'),
				'param_name' => 'desc_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Button', 'evockans'),
				'param_name' => 'btn_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Font Family', 'evockans' ),
				'param_name' => 'button_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Font Weight', 'evockans' ),
				'param_name' => 'button_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Font Size', 'evockans'),
				'param_name' => 'button_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Line-Height', 'evockans'),
				'param_name' => 'button_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        // Spacing
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Top Margin', 'evockans'),
				'param_name' => 'heading_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Bottom Margin', 'evockans'),
				'param_name' => 'heading_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Sub Heading: Margin', 'evockans'),
				'param_name' => 'sub_heading_margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Separator: Top Margin', 'evockans'),
				'param_name' => 'sep_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Separator: Bottom Margin', 'evockans'),
				'param_name' => 'sep_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Description: Top Margin', 'evockans'),
				'param_name' => 'desc_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Description: Bottom Margin', 'evockans'),
				'param_name' => 'desc_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button: Top Margin', 'evockans'),
				'param_name' => 'button_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button: Bottom Margin', 'evockans'),
				'param_name' => 'button_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	    )
	) );
}

add_shortcode( 'imagebox', 'themesflat_shortcode_imagebox' );

/**
 * imagebox shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_imagebox( $atts, $content = null ) {
	extract( shortcode_atts( array(
        'alignment' => '',
        'rounded' => '',
        'border_width' => '',
        'border_color' => '',
        'box_shadow' => '',
        'horizontal' => '',
        'vertical' => '',
        'blur' => '',
        'spread' => '',
        'shadow_color' => '',
        'image'    => '',
        'image_crop' => 'full',
        'image_size' => '750x750',
        'image_rounded' => '',        
        'add_overlay_image' => '',
        'overlay_bg_style' => 'default',
        'overlay_image_hover' => '',
        'overlay_hover_effect' => 'fade-in',
        'color_overlay_img' => 'rgba(35,135,234,0.5)',
        'color_overlay_gradient_1' => '',
        'color_overlay_gradient_2' => '',
        'content_bg_color' => '#ffffff',
        'content_padding' => '',
        'content_margin' => '',
        'content_rounded' => '',
        'content_box_shadow' => '',
        'content_horizontal' => '',
        'content_vertical' => '',
        'content_blur' => '',
        'content_spread' => '',
        'content_shadow_color' => '',
        'background_color' => '',
        'box_shadow' => '',
        'add_icon_content' => '',
        'icon_type' => '',
		'icon' => '',
		'icon_color' => '',
		'icon_font_size' => '50px',
		'icon_position_top' => 20,
		'icon_position_left' => 20,
        'tag' => 'h3',
        'heading' => 'Heading Text',
        'heading_color' => '',
        'add_prefix_heading' => '',
        'prefix_heading_style' => 'prefix-decimal-leading-zero',
        'prefix_heading_color' => '',
        'heading_padding_left' => '20px',
        'sub_heading' => 'Sub Heading Text',
        'sub_heading_color' => '',
        'sub_heading_bg_color' => '',
        'separator' => '',
        'line_full' => 'no',
        'line_width' => '50',
        'line_height' => '2',
        'line_color' => '#eee',
        'description' => '',
        'desc_color' => '',
        'position_content' => '',
        'content_hover' => '',
        'content_hover_offset' => '-20',
        'button_inline' => '',
        'button_text' => 'READ MORE',
        'button_style' => 'simple_link',
        'button_size' => 'small',
        'button_rounded' => '',
        'button_padding' => '',
        'link_color' => '',
        'button_full' => '',
        'add_icon_button' => '',
        'heading_font_family' => 'Default',
        'heading_font_weight' => 'Default',
        'heading_font_size' => '',
        'heading_line_height' => '',
        'prefix_heading_font_size' => '',
        'sub_heading_font_family' => 'Default',
        'sub_heading_font_weight' => 'Default',
        'sub_heading_font_size' => '',
        'sub_heading_line_height' => '',
        'desc_font_family' => 'Default',
        'desc_font_weight' => 'Default',
        'desc_font_size' => '',
        'desc_line_height' => '',
        'button_font_family' => 'Default',
        'button_font_weight' => 'Default',
        'button_font_size' => '',
        'button_line_height' => '',
        'heading_top_margin' => '',
        'heading_bottom_margin' => '',
        'sub_heading_margin' => '',
        'desc_top_margin' => '',
        'desc_bottom_margin' => '',
        'button_top_margin' => '',
        'button_bottom_margin' => '',
        'sep_top_margin' => '',
        'sep_bottom_margin' => '',
        'link_url' => '',
        'new_tab' => 'yes',
    ), $atts ) );
    //extract($atts = vc_map_get_attributes( 'imagebox', $atts ));
    $content = wpb_js_remove_wpautop($content, true);

    $line_height = intval( $line_height );
    $heading_font_size = intval( $heading_font_size );
    $desc_font_size = intval( $desc_font_size );
    $button_font_size = intval( $button_font_size );
    $button_rounded = intval( $button_rounded );
    $heading_top_margin = intval( $heading_top_margin );
    $heading_bottom_margin = intval( $heading_bottom_margin );
    $sep_top_margin = intval( $sep_top_margin );
    $sep_bottom_margin = intval( $sep_bottom_margin );
    $desc_top_margin = intval( $desc_top_margin );
    $desc_bottom_margin = intval( $desc_bottom_margin );    
    $button_bottom_margin = intval( $button_bottom_margin );
    $image_rounded = intval( $image_rounded );
    $position_content = intval( $position_content );
    $content_rounded = intval( $content_rounded );
    $rounded = intval( $rounded );
    if ( $button_top_margin != '' ) {
    	$button_top_margin = intval( $button_top_margin );
    }
    $icon_font_size = intval( $icon_font_size );
    $heading_padding_left = intval( $heading_padding_left );
    $prefix_heading_font_size = intval( $prefix_heading_font_size );
    

    $css = $image_css = $heading_css = $heading_a_css = $sep_css = $desc_css = $button_cls = $button_css = $inner_css = $thumb_css = $content_css = $overlay_image_html = $overlay_image_css = $icon_html = $icon_css = '';
    $image_html = $heading_html = $sep_html = $desc_html = $button_html = $sub_heading_html = $sub_heading_css = '';
    $button_icon_html = $button_icon_css = $prefix_heading_html = $prefix_heading_css = '';

    $cls = $alignment;

    if ( $content_hover ) {
    	$cls .= ' has-hover-content hover-offset'.$content_hover_offset;
    }
    
    if ( $overlay_image_hover ) $cls .= ' '.$overlay_hover_effect;
    if ( $add_icon_content ) $cls .= ' has-icon-content';    

    if ( $background_color ) $inner_css .= 'background-color:'. $background_color .';';
    if ( $rounded ) $inner_css .= 'border-radius:'. $rounded .'px; overflow: hidden;';
    if ( $border_color && $border_width ) $inner_css .= 'border-style:solid;border-width:'. $border_width .';border-color:'. $border_color .';';
    if ( $box_shadow ) {    	
    	if ( $horizontal && $vertical && $blur && $spread && $shadow_color )
    	$inner_css .= 'box-shadow:'. $horizontal .' '. $vertical .' '. $blur .' '. $spread .' '. $shadow_color .';';
    }


    if ( $content_bg_color ) $content_css .= 'background-color:'. $content_bg_color .';';
    if ( $content_padding ) $content_css .= 'padding:'. $content_padding .';';
    if ( $content_margin ) $content_css .= 'margin:'. $content_margin .';';
    if ( $content_rounded ) $content_css .= 'border-radius:'. $content_rounded .'px;';
    if ( $position_content ) $content_css .= 'transform: translateY('. $position_content .'px); position: absolute; left: 0; right: 0;';
    if ( $content_box_shadow ) {    	
    	if ( $content_horizontal && $content_vertical && $content_blur && $content_spread && $content_shadow_color )
    	$content_css .= 'box-shadow:'. $content_horizontal .' '. $content_vertical .' '. $content_blur .' '. $content_spread .' '. $content_shadow_color .';';
    } 


    if ( $heading_font_weight != 'Default' ) $heading_css .= 'font-weight:'. $heading_font_weight .';';
    if ( $heading_color ) $heading_a_css .= 'color:'. $heading_color .';';
    if ( $heading_color ) $heading_css .= 'color:'. $heading_color .';';
    if ( $heading_font_size ) $heading_css .= 'font-size:'. $heading_font_size .'px;';
    if ( $heading_line_height ) $heading_css .= 'line-height:'. $heading_line_height .';';
    if ( $heading_top_margin ) $heading_css .= 'margin-top:'. $heading_top_margin .'px;';
    if ( $heading_bottom_margin ) $heading_css .= 'margin-bottom:'. $heading_bottom_margin .'px;';
    if ( $heading_font_family != 'Default' ) {
        $heading_css .= 'font-family:'. $heading_font_family .';';
    }


    if ( $sub_heading_font_weight != 'Default' ) $sub_heading_css .= 'font-weight:'. $sub_heading_font_weight .';';
    if ( $sub_heading_color ) $sub_heading_css .= 'color:'. $sub_heading_color .';';
    if ( $sub_heading_bg_color ) $sub_heading_css .= 'background-color:'. $sub_heading_bg_color .';';
    if ( $sub_heading_font_size ) $sub_heading_css .= 'font-size:'. $sub_heading_font_size .'px;';
    if ( $sub_heading_line_height ) $sub_heading_css .= 'line-height:'. $sub_heading_line_height .';';
    if ( $sub_heading_margin ) $sub_heading_css .= 'margin:'. $sub_heading_margin .';';
    if ( $sub_heading_font_family != 'Default' ) {
        $sub_heading_css .= 'font-family:'. $sub_heading_font_family .';';
    }


    if ( $desc_font_weight != 'Default' ) $desc_css .= 'font-weight:'. $desc_font_weight .';';
    if ( $desc_color ) $desc_css .= 'color:'. $desc_color .';';
    if ( $desc_font_size ) $desc_css .= 'font-size:'. $desc_font_size .'px;';
    if ( $desc_line_height ) $desc_css .= 'line-height:'. $desc_line_height .';';
    if ( $desc_top_margin ) $desc_css .= 'margin-top:'. $desc_top_margin .'px;';
    if ( $desc_bottom_margin ) $desc_css .= 'margin-bottom:'. $desc_bottom_margin .'px;';
    if ( $desc_font_family != 'Default' ) {
        $desc_css .= 'font-family:'. $desc_font_family .';';
    }


    if ( $button_font_weight != 'Default' ) $button_css .= 'font-weight:'. $button_font_weight .';';
    if ( $link_color ) $button_css .= 'color:'. $link_color .';';
    if ( $button_font_size ) $button_css .= 'font-size:'. $button_font_size .'px;';
    if ( $button_rounded ) $button_css .= 'border-radius:'. $button_rounded .'px;';
    if ( $button_padding ) $button_css .= 'padding:'. $button_padding .';';
    if ( $button_top_margin || $button_top_margin == '0' ) $button_css .= 'margin-top:'. $button_top_margin .'px;';
    if ( $button_bottom_margin ) $button_css .= 'margin-bottom:'. $button_bottom_margin .'px;';
    if ( $button_line_height ) $button_css .= 'line-height:'. $button_line_height .';';
    if ( $button_font_family != 'Default' ) {
        $button_css .= 'font-family:'. $button_font_family .';';
    }
    if ( $button_full )  $button_css .= 'width: 100%; text-align: center;';

    
    if ( $overlay_bg_style == 'default' ) {
    	if ( $color_overlay_img ) $overlay_image_css .= 'background-color:' . $color_overlay_img . ';';
    } else if( $overlay_bg_style == 'gradient-top-bottom' ){
    	if ( $color_overlay_gradient_1 && $color_overlay_gradient_2 ) {
    		$overlay_image_css .= 'background: linear-gradient(' . $color_overlay_gradient_1 .','. $color_overlay_gradient_2 . ');';
    	}
    } else if( $overlay_bg_style == 'gradient-left-right' ){
    	if ( $color_overlay_gradient_1 && $color_overlay_gradient_2 ) {
    		$overlay_image_css .= 'background: linear-gradient( to right, ' . $color_overlay_gradient_1 .','. $color_overlay_gradient_2 . ' );';
    	}
    } else if( $overlay_bg_style == 'radial-gradient' ){
    	if ( $color_overlay_gradient_1 && $color_overlay_gradient_2 ) {
    		$overlay_image_css .= 'background: radial-gradient( circle, ' . $color_overlay_gradient_1 .','. $color_overlay_gradient_2 . ' );';
    	}
    }
    if( $add_overlay_image ){    	
    	$overlay_image_html = sprintf('<div class="overlay" style="%1$s"></div>', $overlay_image_css);
    }
    if ( $image ) {
        $img_size = 'full';
        if ( $image_crop == 'square' ) $img_size = 'themesflat_sc_vc-square';
        if ( $image_crop == 'rectangle' ) $img_size = 'themesflat_sc_vc-rectangle';
        if ( $image_crop == 'rectangle2' ) $img_size = 'themesflat_sc_vc-rectangle2';
        if ( $image_crop == 'auto1' ) $img_size = 'themesflat_sc_vc-medium-auto';
        if ( $image_crop == 'auto2' ) $img_size = 'themesflat_sc_vc-small-auto';
        if ( $image_crop == 'auto3' ) $img_size = 'themesflat_sc_vc-xsmall-auto';

        if ( $image_rounded ) $thumb_css .= 'border-radius:' . $image_rounded . 'px; overflow: hidden;';

        if ( $image_crop == 'custom' ) { 			
	    	$image = wpb_getImageBySize( array( 'attach_id' => $image, 'thumb_size' => $image_size ) );
	  		$image = $image['thumbnail'];
	  		$image_html = sprintf( '<div class="thumb" style="%2$s">%1$s %3$s</div>', $image, $thumb_css, $overlay_image_html );

	    } else {
	    	$image_html .= sprintf(
	            '<div class="thumb" style="%2$s">%1$s %3$s</div>',
	            wp_get_attachment_image( $image, $img_size ), $thumb_css, $overlay_image_html
	        );
	    }
    }

    if ( $add_icon_content ) {
		$icon = themesflat_sc_vc_get_icon_class( $atts, 'icon' );
		if ( $icon && $icon_type != '' ) {
			vc_icon_element_fonts_enqueue( $icon_type );
				
			if ( $icon_font_size ) $icon_css .= 'font-size:'. $icon_font_size .'px;';			
			if ( $icon_color ) $icon_css .= 'color:'. $icon_color .';';		
			if ( $icon_position_top ) $icon_css .= 'top:'. $icon_position_top .'px;';
			if ( $icon_position_left ) $icon_css .= 'left:'. $icon_position_left .'px;';	

			$icon_html = sprintf(
				'<div class="icon-wrap" style="%2$s">
					<i class="%1$s"></i>
				</div>',
				$icon,
				$icon_css
			);
		}
	}

    $new_tab = $new_tab == 'yes' ? '_blank' : '_self'; 

    if( $add_prefix_heading ) {
    	$cls .= ' has-prefix-heading';
    	$cls .= ' '.$prefix_heading_style;
    	if( $prefix_heading_color ) $prefix_heading_css .= 'color:' . $prefix_heading_color .';';
    	if( $heading_padding_left ) $heading_css .= 'padding-left:' . $heading_padding_left .'px;';
    	if( $prefix_heading_font_size ) $prefix_heading_css .= 'font-size:' . $prefix_heading_font_size .'px;';
    
    	$prefix_heading_html = sprintf( '<span style="%1$s"></span>' ,$prefix_heading_css );
    }

    if ( $heading ) {
        $heading_html .= sprintf(
	        '<div class="wrap-title">
		        <%5$s class="title" style="%2$s">
		        	%7$s	        
		            <a target="%4$s" href="%3$s" style="%6$s">%1$s</a>
		        </%5$s>
	        </div>',
	        esc_html( $heading ),
	        $heading_css,
	        esc_attr( $link_url ),
	        $new_tab,
	        $tag,
	        $heading_a_css,
	        $prefix_heading_html
	    );
    }

    if ( $sub_heading ) {
        $sub_heading_html .= sprintf(
	        '<div class="wrap-sub-title"><div class="sub-title" style="%2$s">%1$s</div></div>',
	        esc_html( $sub_heading ),
	        $sub_heading_css
	    );
    }

    if ($separator == 'line' ) {
        if ( $line_full == 'yes' ) {
            $sep_css = 'width:100%;';
        } else {
            $line_width = intval( $line_width );
            if ( $line_width ) $sep_css = 'width:'. $line_width .'px;';
        }

        if ( $alignment == 'text-center' ) $sep_css .= 'margin: 0 auto;';

        if ( $line_height ) $sep_css .= 'height:'. $line_height .'px;';
        if ( $line_color ) $sep_css .= 'background-color:'. $line_color .';';
        if ( $sep_top_margin ) $sep_css .= 'margin-top:'. $sep_top_margin .'px;';
        if ( $sep_bottom_margin ) $sep_css .= 'margin-bottom:'. $sep_bottom_margin .'px;';
        
        $sep_html .= sprintf( '<div class="clearfix"></div><div class="sep" style="%1$s"></div>', $sep_css );
    }

    if ( $description )
        $desc_html .= sprintf(
        '<div class="desc" style="%2$s">%1$s</div>',
        $description, $desc_css
    );

    if ( $button_inline ) {
    	$cls .= ' content_button_inline';
    }

    if( $add_icon_button ) {		
		$button_icon_html = '<i class="fa fa-angle-right"></i>';		
	}

    if ( $button_text || $add_icon_button ) {
        $button_cls = $button_size;
        if ( $button_style == 'simple_link' ) $button_cls .= ' simple-link font-heading';
        if ( $button_style == 'accent' ) $button_cls .= ' themesflat_sc_vc-button accent';
        if ( $button_style == 'dark' ) $button_cls .= ' themesflat_sc_vc-button dark';
        if ( $button_style == 'light' ) $button_cls .= ' themesflat_sc_vc-button light';
        if ( $button_style == 'very-light' ) $button_cls .= ' themesflat_sc_vc-button very-light';
        if ( $button_style == 'white' ) $button_cls .= ' themesflat_sc_vc-button white';
        if ( $button_style == 'outline' ) $button_cls .= ' themesflat_sc_vc-button outline ol-accent';
        if ( $button_style == 'outline_dark' ) $button_cls .= ' themesflat_sc_vc-button outline dark';
        if ( $button_style == 'outline_light' ) $button_cls .= ' themesflat_sc_vc-button outline light';
        if ( $button_style == 'outline_very-light' ) $button_cls .= ' themesflat_sc_vc-button outline very-light';
        if ( $button_style == 'outline_white' ) $button_cls .= '  themesflat_sc_vc-button outline white';

        $button_html .= sprintf(
            '<div class="btn">
                <a target="%4$s" class="%3$s" href="%2$s" style="%5$s">%1$s %6$s</a>
            </div>',
            esc_html( $button_text ),
            esc_attr( $link_url ),
            $button_cls,
            $new_tab,
            $button_css,
            $button_icon_html
        );
    }

    return sprintf(
        '<div class="themesflat_sc_vc-image-box clearfix %7$s" style="%8$s">
            <div class="item">
                <div class="inner" style="%5$s">
                    %1$s
                    <div class="text-wrap" style="%6$s">
                        %11$s %10$s %2$s %9$s %3$s %4$s
                    </div>
                </div>
            </div>
        </div>', 
        $image_html,
        $heading_html,
        $desc_html,
        $button_html,
        $inner_css,
        $content_css,
        $cls,
        $css,
        $sep_html,
        $sub_heading_html,
        $icon_html
    );
}