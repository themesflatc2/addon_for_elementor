<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_table_data_list_items_content_shortcode_params' );

function themesflat_table_data_list_items_content_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Table Data List Items', 'evockans'),
        'description' => esc_html__('Displaying table data list items content.', 'evockans'),
        'base' => 'table_data_list_items_content',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'as_child' => array( 'only' => 'table_data_list' ),
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'params' => array(
        	array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'evockans' ),
				'param_name' => 'style_content',
				'value'      => array(
					'Default' 	=> 'Default',
					'Custom' 	=> 'custom',
				),
				'std'		=> 'Default',
			),
        	array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Content Background Color', 'evockans'),
				'param_name' => 'content_background_color',
				'value' => '',
				'dependency' => array( 'element' => 'style_content', 'value' => 'custom' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Content Color', 'evockans'),
				'param_name' => 'content_color',
				'value' => '',
				'dependency' => array( 'element' => 'style_content', 'value' => 'custom' ),
            ),
             // Multiple Columns
			array(
				'type' => 'headings',
				'text' => esc_html__('Multiple Columns Content', 'evockans'),
				'param_name' => 'add_new_rows',
				'group' => esc_html__( 'Multiple Columns', 'evockans' ),
				'class' => '',
			),  
	        // params group
            array(
                'type' => 'param_group',
                'value' => '',
                'param_name' => 'contents',
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => 'Enter your content',
                        'param_name' => 'content',
                    )
                ),
                'group' => esc_html__( 'Multiple Columns', 'evockans' ),
            ),
            // Typography
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Content: Font Family', 'evockans' ),
				'param_name' => 'content_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style_content', 'value' => 'custom' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Content: Font Weight', 'evockans' ),
				'param_name' => 'content_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style_content', 'value' => 'custom' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Content: Font Size', 'evockans'),
				'param_name' => 'content_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style_content', 'value' => 'custom' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content: Line Height', 'evockans'),
				'param_name' => 'content_line_height',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style_content', 'value' => 'custom' ),
	        ),
        )
    ) );
}

add_shortcode( 'table_data_list_items_content', 'themesflat_shortcode_table_data_list_items_content' );

/**
 * table_data_list_items_content shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_table_data_list_items_content( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'style_content' => 'Default',
	    'content_background_color' => '',
	    'content_color'	=> '',
	    'content_font_family' => 'Default',
	    'content_font_weight' => 'Default',
	    'content_font_size' => '',
	    'content_line_height' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'table_data_list_items_content', $atts ));
	$content = wpb_js_remove_wpautop($content, false);

	$contents = vc_param_group_parse_atts( $atts['contents'] );

	$css_content = '';

	if ($content_background_color) $css_content .= 'background-color:' . $content_background_color . ';'; 
	if ($content_color) $css_content .= 'color:' . $content_color . ';';
	if ($content_font_family != 'Default') $css_content .= 'font-family:'. $content_font_family .';';
	if ($content_font_weight != 'Default') $css_content .= 'font-weight:'. $content_font_weight .';';
	if ($content_font_size) $css_content .= 'font-size:' . $content_font_size . 'px;';
	if ($content_line_height) $css_content .= 'line-height:' . $content_line_height . ';';

	$css = $array_contents = '';
	if ($contents) {	
		foreach ($contents as $key => $value) {
			$array_contents .= sprintf('<td>%1$s</td>', $value['content']);		
		}
	}

	return sprintf ('
		<tr style="%2$s">
		    %1$s
		</tr>', $array_contents , $css_content);	
}