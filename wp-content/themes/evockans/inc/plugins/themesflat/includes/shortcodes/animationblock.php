<?php
/**
 * Register filter for append custom class name
 * that generated from visual-composer
 */
class WPBakeryShortCode_animationblock extends WPBakeryShortCodesContainer {}
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_animationblock_shortcode_params' );

function themesflat_animationblock_shortcode_params() {
	vc_map( array(
		'name' => esc_html__('Animation Block', 'evockans'),
		'description' => esc_html__('Apply animations anywhere.', 'evockans'),
		'base' => 'animationblock',
		'weight'	=>	180,
		'icon' => THEMESFLAT_ICON,		
        'as_parent' => array('except' => 'animationblock'),
		'controls' => 'full',
		'show_settings_on_create' => true,
		'category' => esc_html__('THEMESFLAT', 'evockans'),
		'js_view' => 'VcColumnView',
		'params' => array(
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Animation', 'evockans' ),
				'param_name' => 'animation',
				'value'      =>  themesflat_animation(),
				'std'		 => 'fadeInLeft'
			),
			array(
				'type' => 'number',
				'heading' => esc_html__('Animation Duration', 'evockans'),
				'param_name' => 'duration',
				'value' => 1,
				'suffix' => 's'
		  	),
	        array(
				'type' => 'number',
				'heading' => esc_html__('Animation Delay', 'evockans'),
				'param_name' => 'delay',
				'value' => 0.6,
				'suffix' => 's'
	        ),
	        array(
				'type' => 'number',
				'heading' => esc_html__('Viewport Position', 'evockans'),
				'param_name' => 'position',
				'value' => 90,
				'suffix' => '%'
	        ),
         )
    ) );
}

add_shortcode( 'animationblock', 'themesflat_shortcode_animationblock' );

/**
 * animationblock shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_animationblock( $atts, $content = null ) {
	extract( shortcode_atts( array(
	    'animation' => 'fadeInLeft',
	    'duration' => '1',
	    'delay' => '0.6',
	    'position' => '90'
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'animationblock', $atts ));

	if ( $duration ) $duration = $duration .'s';
	if ( $delay ) $delay = $delay .'s';
	if ( $position ) $position = $position .'%';

	return sprintf(
		'<div class="themesflat_sc_vc-animation-block" data-animate="%2$s" data-duration="%3$s" data-delay="%4$s" data-position="%5$s">
			%1$s
		</div>',
		do_shortcode($content),
		esc_attr( $animation ),
		esc_attr( $duration ),
		esc_attr( $delay ),
		esc_attr( $position )
	);
}