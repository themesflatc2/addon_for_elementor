<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_icons_shortcode_params' );

function themesflat_icons_shortcode_params() {
	vc_map( array(
		'name'        => esc_html__( 'Icons', 'evockans' ),
        'description' => esc_html__('Displaying Icon lists with custom icon.', 'evockans'),
		'base'        => 'icons',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
		'params'      => array(
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Alignment', 'evockans' ),
				'param_name' => 'alignment',
				'value'      => array(
					'Left' => 'left',
					'Center' => 'center',
					'Right' => 'right',
					'Center Grid' => 'center2',
				),
				'std'		=> 'left',
			),			
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Width', 'evockans'),
				'param_name' => 'width',
				'value' => '80',
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Height', 'evockans'),
				'param_name' => 'height',
				'value' => '80',
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Rounded', 'evockans'),
				'param_name' => 'rounded',
				'value' => '',
				'description'	=> esc_html__('ex: 10px', 'evockans'),
            ),
   //          array(
			// 	'type'       => 'dropdown',
			// 	'heading'    => esc_html__( 'Style', 'evockans' ),
			// 	'param_name' => 'style',
			// 	'value'      => array(
			// 		'Simple' => 'simple',
			// 		'Ouline' => 'outline',
			// 		'Background' => 'background',
			// 	),
			// 	'std'		=> 'background',
			// ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Background Color', 'evockans'),
				'param_name' => 'background_color',
				'value' => '',
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Background Color Hover', 'evockans'),
				'param_name' => 'background_color_hover',
				'value' => '',
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Border Width', 'evockans'),
				'param_name' => 'border_width',
				'value' => '',
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Border Color', 'evockans'),
				'param_name' => 'border_color',
				'value' => '',
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Border Color Hover', 'evockans'),
				'param_name' => 'border_color_hover',
				'value' => '',
            ),	        
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Border Style', 'evockans' ),
				'param_name' => 'border_style',
				'value'      => array(
					'Solid' => 'solid',
					'Dotted' => 'dotted',
					'Dashed' => 'dashed',
					'Double' => 'double',
				),
				'std'		=> 'solid',
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Margin', 'evockans'),
				'param_name' => 'margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
	        ),
			// Icon
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Enable Pulse Effect Infinity?', 'evockans' ),
				'param_name' => 'icon_effect',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'value' => array(
					'Yes' => 'yes',
					'No' => 'no',
				),
				'std'		=> 'no',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Icon Animation', 'evockans' ),
				'param_name' => 'icon_animation',
				'value'      => array(
					'None' => '',					 
					'Pulse Box 1' => 'pulsebox-1',
					'Pulse Box 2' => 'pulsebox-2',
					'Pulse Box 3' => 'pulsebox-3',
					'Pulse Infinite' => 'pulseinfinite',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
				'std'		=> '',
				'dependency' => array( 'element' => 'icon_effect', 'value' => 'yes' ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'evockans' ),
				'param_name' => 'icon_type',
				'description' => esc_html__( 'Select icon library.', 'evockans' ),
				'value' => array(
					esc_html__( 'None', 'evockans' ) => '',
					esc_html__( 'Mono Social', 'evockans' ) => 'monosocial',
					esc_html__( 'Simple Line', 'evockans' ) => 'simpleline',
					esc_html__( 'Font Ionicons', 'evockans' ) => 'ionicons',
					esc_html__( 'Font Elegant', 'evockans' ) => 'eleganticons',
					esc_html__( 'Font Themify Icons', 'evockans' ) => 'themifyicons',
					esc_html__( 'Font Icomoon Icons', 'evockans' ) => 'icomoon',
					esc_html__( 'FontAwesome', 'evockans' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'evockans' ) => 'openiconic',
					esc_html__( 'Typicons', 'evockans' ) => 'typicons',
					esc_html__( 'Entypo', 'evockans' ) => 'entypo',
					esc_html__( 'Linecons', 'evockans' ) => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_monosocial',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'monosocial',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'monosocial',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_simpleline',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'simpleline',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'simpleline',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_ionicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'ionicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'ionicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_eleganticons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'eleganticons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'eleganticons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_themifyicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'themifyicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'themifyicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_icomoon',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'icomoon',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'icomoon',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon',
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'fontawesome',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_openiconic',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'openiconic',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'openiconic',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_typicons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'typicons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'typicons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_entypo',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'entypo',
					'iconsPerPage' => 300,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'entypo',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_linecons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'linecons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon: Color', 'evockans'),
				'param_name' => 'icon_color',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon: Color Hover', 'evockans'),
				'param_name' => 'icon_color_hover',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon: Font Size', 'evockans'),
				'param_name' => 'icon_font_size',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
	        ),
	        // Hyperlink
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Hyperlink Style', 'evockans' ),
				'param_name' => 'hyperlink',
				'value'      => array(
					'Simple Link' => 'simple',
					'Video Link' => 'video',
				),
				'std'		=> 'simple',
				'group' => esc_html__( 'Hyperlink', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Link Youtube/Vimeo (URL)', 'evockans'),
				'param_name' => 'video_url',
				'value' => '',
				'group' => esc_html__( 'Hyperlink', 'evockans' ),
				'dependency' => array( 'element' => 'hyperlink', 'value' => 'video' ),
	        ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Link (URL):', 'evockans'),
				'param_name' => 'link_url',
				'value' => '',
				'group' => esc_html__( 'Hyperlink', 'evockans' ),
				'dependency' => array( 'element' => 'hyperlink', 'value' => 'simple' ),
            ),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Open Link in a new Tab', 'evockans' ),
				'param_name' => 'new_tab',
				'group' => esc_html__( 'Hyperlink', 'evockans' ),
				'value' => array(
					'Yes' => 'yes',
					'No' => 'no',
				),
				'std'		=> 'yes',
				'dependency' => array( 'element' => 'hyperlink', 'value' => 'simple' ),
			),
		)
	) );
}

add_shortcode( 'icons', 'themesflat_shortcode_icons' );

/**
 * icons shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_icons( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'alignment' => 'left',
		//'style' => 'background',
		'width' => '80',
		'height' => '80',
		'rounded' => '',
		'background_color' => '',
		'background_color_hover' => '',
		'border_color' => '',
		'border_color_hover' => '',
		'border_width' => '',
		'border_style' => 'solid',
		'margin' => '',
		'icon_effect' => 'no',
		'icon_animation' => '',
		'icon_type' => '',
		'icon' => '',
		'icon_color' => '',
		'icon_color_hover' => '',
		'icon_font_size' => '',
		'color' => '',
		'font_size' => '',
		'hyperlink' => 'simple',
		'video_url' => '',
		'link_url' => '',
		'new_tab' => 'yes',
	), $atts ) );

	$icon_font_size = intval( $icon_font_size );
	$rounded = intval( $rounded );
	$width = intval( $width );
	$height = intval( $height );
	$border_width = intval( $border_width );

	$css = $icon_html = $icon_cls = $icon_css = $icon_data = $line_height = $color_css_js = '';
	$cls = '';

	if ( $icon_effect == 'yes' ) {
		if ( $icon_animation == 'pulseinfinite' ) $icon_cls .= ' icon-effect pulse infinite';
		if ( $icon_animation == 'pulsebox-1' ) $icon_cls .= ' pulsebox-1';
		if ( $icon_animation == 'pulsebox-2' ) $icon_cls .= ' pulsebox-2';
		if ( $icon_animation == 'pulsebox-3' ) $icon_cls .= ' pulsebox-3';
	}

	$line_height = $height - ( $border_width * 2 );

	$icon = themesflat_sc_vc_get_icon_class( $atts, 'icon' );
	if ( $icon && $icon_type != '' ) {
		vc_icon_element_fonts_enqueue( $icon_type );

		if ( $margin ) $css .= 'margin:'. $margin .';';

		if ( $icon_font_size ) $icon_css .= 'font-size:'. $icon_font_size .'px;';
		if ( $width ) $icon_css .= 'width:'. $width .'px;';
		if ( $height ) $icon_css .= 'height:'. $height .'px;';		
		if ( $icon_color ) $icon_css .= 'color:'. $icon_color .';';		
		if ( $rounded ) $icon_css .= 'border-radius:'. $rounded .'px;';
		if ( $background_color ) $icon_css .= 'background-color:'. $background_color .';';
		
		if ( $border_width ) {
			if ( $border_color ) $icon_css .= 'border-color:'. $border_color .';';
			$icon_css .= 'border-style:'. $border_style .';';			
			$icon_css .= 'border-width:'. $border_width .'px;';
			$icon_css .= 'line-height:'. $line_height .'px;';
		}else{
			$icon_css .= 'line-height:'. $height .'px;';
		}
		
		if ($icon_color) $color_css_js .= 'data-color="'. $icon_color .'" ';
		if ($icon_color_hover) $color_css_js .= 'data-color_hover="'. $icon_color_hover .'" ';
		if ($background_color) $color_css_js .= 'data-bgcolor="'. $background_color .'" ';
		if ($background_color_hover) $color_css_js .= 'data-bgcolor_hover="'. $background_color_hover .'" ';
		if ($border_color) $color_css_js .= 'data-bordercolor="'. $border_color .'" ';
		if ($border_color_hover) $color_css_js .= 'data-bordercolor_hover="'. $border_color_hover .'" ';		

		$icon_html = sprintf('<span class="icon %4$s" style="%2$s" %3$s><i class="%1$s"></i></span>', $icon, $icon_css, $icon_data, $icon_cls );
	}

	if ( $hyperlink == 'simple' ) {
		if ( $link_url ) {
			$new_tab = $new_tab == 'yes' ? '_blank' : '_self';
			$icon_html = sprintf( '<a class="icon-wrap" target="%3$s" href="%2$s">%1$s</a>', $icon_html, $link_url, $new_tab );
		}
	} else {
		if ( $video_url ) {
			wp_enqueue_script( 'themesflat_sc_vc-magnificpopup' );
			$icon_html = sprintf( '<a class="icon-wrap popup-video" href="%2$s">%1$s</a>', $icon_html, $video_url );
		}
	}

	if ( $alignment == 'center' ) $css .= 'display: block; text-align:center;';
	if ( $alignment == 'right' ) $css .= 'display: block; text-align:right;';
	if ( $alignment == 'center2' ) $css .= 'position: absolute; left: 50%; top: 50%; margin: -'. ( $height / 2 ) .'px 0 0 -'. ( $width / 2 ) .'px;';

	return sprintf( '<div class="themesflat_sc_vc-icon clearfix %1$s" style="%3$s" %4$s>%2$s</div>', $cls, $icon_html, $css, $color_css_js );
}