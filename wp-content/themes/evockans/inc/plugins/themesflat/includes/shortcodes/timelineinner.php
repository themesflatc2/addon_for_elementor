<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_inner_timeline_shortcode_params' );

function themesflat_inner_timeline_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Inner Time Line', 'evockans'),
        'description' => esc_html__('Displaying Tabs Inner.', 'evockans'),
        'base' => 'inner_timeline',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),		
		'as_child'    => array( 'only' => 'timeline' ),
		'as_parent' => array( 'except' => 'inner_timeline' ),
		'controls' => 'full',
        'params' => array(
	        array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Alignment', 'evockans' ),
				'param_name' => 'alignment',
				'value'      => array(
					'Left All' => 'timeline-left-all',
					'Right All' => 'timeline-right-all',
				),
				'std'		=> 'timeline-left-all',
				'dependency' => array( 'element' => 'icon_display', 'value' => 'no-icon' ),
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Time', 'evockans'),
				'param_name' => 'time',
				'value' => '11 Dec 2018',
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Time Color', 'evockans'),
				'param_name' => 'time_color',
				'value' => '#2387ea',
				'group' => esc_html__( 'Content', 'evockans' ),				
            ),            
            //Add Highlight
            array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Add Highlight', 'evockans' ),
				'param_name' => 'add_highlight',
				'value' => array(
					'Yes' => 'yes',
					'No' => 'no',
				),
				'std'	=> 'yes',
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Highlight Width', 'evockans'),
				'param_name' => 'highlight_w',
				'value' => '18px',
				'group' => esc_html__( 'Highlight', 'evockans' ),
				'dependency' => array( 'element' => 'add_highlight', 'value' => 'yes' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Highlight Height', 'evockans'),
				'param_name' => 'highlight_h',
				'value' => '18px',
				'group' => esc_html__( 'Highlight', 'evockans' ),
				'dependency' => array( 'element' => 'add_highlight', 'value' => 'yes' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Highlight Border Width', 'evockans'),
				'param_name' => 'highlight_border_width',
				'value' => '2px',
				'group' => esc_html__( 'Highlight', 'evockans' ),
				'dependency' => array( 'element' => 'add_highlight', 'value' => 'yes' ),
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Highlight Border Color', 'evockans'),
				'param_name' => 'highlight_border_color',
				'value' => '#2387ea',
				'group' => esc_html__( 'Highlight', 'evockans' ),
				'dependency' => array( 'element' => 'add_highlight', 'value' => 'yes' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Highlight Background', 'evockans'),
				'param_name' => 'highlight_background',
				'value' => '#ffffff',
				'group' => esc_html__( 'Highlight', 'evockans' ),
				'dependency' => array( 'element' => 'add_highlight', 'value' => 'yes' ),
            ),
            //Add Arrows
            array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Add Arrows', 'evockans' ),
				'param_name' => 'add_arrows',
				'value' => array(
					'Yes' => 'yes',
					'No' => 'no',
				),
				'std'	=> 'no',
			),
	        // Content
	        array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Heading Color', 'evockans'),
				'param_name' => 'heading_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),				
            ),
	        array(
				'type' => 'textarea',				
				'heading' => esc_html__( 'Description', 'evockans' ),
				'param_name' => 'desc_content',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Description Color', 'evockans'),
				'param_name' => 'desc_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),				
            ), 
            array(
				'type' => 'textarea',				
				'heading' => esc_html__( 'Wrap Padding', 'evockans' ),
				'param_name' => 'wrap_padding',
				'value' => '10px 40px 60px 40px',
				'group' => esc_html__( 'Content', 'evockans' ),
			),           
            array(
				'type' => 'textarea',				
				'heading' => esc_html__( 'Content Padding', 'evockans' ),
				'param_name' => 'content_padding',
				'value' => '20px 30px 30px 30px',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Content Background', 'evockans'),
				'param_name' => 'content_bg',
				'value' => '#f7f7f7',
				'group' => esc_html__( 'Content', 'evockans' ),				
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Content Rounded', 'evockans'),
				'param_name' => 'content_rounded',
				'value' => '10px',
				'description'	=> esc_html__('ex: 5px', 'evockans'),
	        ),
			// Typography
			array(
				'type' => 'headings',
				'text' => esc_html__('Time', 'evockans'),
				'param_name' => 'time_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Time Font Family', 'evockans' ),
				'param_name' => 'time_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Time Font Weight', 'evockans' ),
				'param_name' => 'time_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Time Font Size', 'evockans'),
				'param_name' => 'time_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Time Line-Height', 'evockans'),
				'param_name' => 'time_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Font Family', 'evockans' ),
				'param_name' => 'heading_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Font Weight', 'evockans' ),
				'param_name' => 'heading_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Font Size', 'evockans'),
				'param_name' => 'heading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Line-Height', 'evockans'),
				'param_name' => 'heading_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'headings',
				'text' => esc_html__('Description', 'evockans'),
				'param_name' => 'desc_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Description Font Family', 'evockans' ),
				'param_name' => 'desc_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Description Font Weight', 'evockans' ),
				'param_name' => 'desc_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Description Font Size', 'evockans'),
				'param_name' => 'desc_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Description Line-Height', 'evockans'),
				'param_name' => 'desc_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        // Spacing
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Time: Top Margin', 'evockans'),
				'param_name' => 'time_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Time: Bottom Margin', 'evockans'),
				'param_name' => 'time_bottom_margin',
				'value' => '-35px',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Top Margin', 'evockans'),
				'param_name' => 'heading_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Bottom Margin', 'evockans'),
				'param_name' => 'heading_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Description: Top Margin', 'evockans'),
				'param_name' => 'desc_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Description: Bottom Margin', 'evockans'),
				'param_name' => 'desc_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Extra class name', 'evockans'),
				'param_name' => 'class',
				'value' => '',
				'group' => esc_html__( 'Class', 'evockans' ),
	        ),
        )
    ) );
}

add_shortcode( 'inner_timeline', 'themesflat_shortcode_inner_timeline' );

/**
 * inner_timeline shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_inner_timeline( $atts, $content = null ) {
	extract( shortcode_atts( array(
	    'alignment' => 'timeline-left-all',
	    'wrap_padding' => '10px 40px 60px 40px',
	    'add_highlight' => 'yes',
	    'highlight_w' => '18px',
	    'highlight_h' => '18px',
	    'highlight_border_width' => '2px',
		'highlight_border_color' => '#2387ea',
		'highlight_background' => '#ffffff',
		'add_arrows' => 'no',
	    'time' => '11 Dec 2018',
	    'time_color' => '#2387ea',
	    'heading' => '',
	    'heading_color' => '',
	    'desc_content' => '',
	    'desc_color' => '',	    
	    'content_padding' => '20px 30px 30px 30px',
	    'content_bg' => '#f7f7f7',
	    'content_rounded' => '10px',
	    'time_font_family' => 'Default',
        'time_font_weight' => 'Default',
        'time_font_size' => '',
        'time_line_height' => '',
	    'heading_font_family' => 'Default',
        'heading_font_weight' => 'Default',
        'heading_font_size' => '',
        'heading_line_height' => '',
        'desc_font_family' => 'Default',
        'desc_font_weight' => 'Default',
        'desc_font_size' => '',
        'desc_line_height' => '',
        'time_top_margin' => '',
        'time_bottom_margin' => '-35px',
        'heading_top_margin' => '',
        'heading_bottom_margin' => '',
        'desc_top_margin' => '',
        'desc_bottom_margin' => '',
	), $atts ) );	
	//extract($atts = vc_map_get_attributes( 'inner_timeline', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$highlight_w = intval( $highlight_w );
	$highlight_h = intval( $highlight_h );
	$heading_font_size = intval( $heading_font_size );
    $desc_font_size = intval( $desc_font_size );
	$content_rounded = intval( $content_rounded );
	$time_top_margin = intval( $time_top_margin );
    $time_bottom_margin = intval( $time_bottom_margin );
	$heading_top_margin = intval( $heading_top_margin );
    $heading_bottom_margin = intval( $heading_bottom_margin );
	$desc_top_margin = intval( $desc_top_margin );
    $desc_bottom_margin = intval( $desc_bottom_margin );  

	$wrap_css = $time_html = $highlight_html = $highlight_css = $arrows_html = $arrows_css = $content_html = $content_css = $heading_html = $desc_content_html = $time_css = $heading_css = $desc_css = '';

	if ( $time_font_weight != 'Default' ) $time_css .= 'font-weight:'. $time_font_weight .';';
    if ( $time_color ) $time_css .= 'color:'. $time_color .';';
    if ( $time_font_size ) $time_css .= 'font-size:'. $time_font_size .'px;';
    if ( $time_line_height ) $time_css .= 'line-height:'. $time_line_height .';';
    if ( $time_top_margin ) $time_css .= 'margin-top:'. $time_top_margin .'px;';
    if ( $time_bottom_margin ) $time_css .= 'margin-bottom:'. $time_bottom_margin .'px;';
    if ( $time_font_family != 'Default' ) {
        $time_css .= 'font-family:'. $time_font_family .';';
    }
	if ( $time ) {
		$time_html = sprintf('<h2 class="time" style="%2$s">%1$s</h2>', $time, $time_css);
	}

	if ( $wrap_padding ) $wrap_css .= 'padding:'. $wrap_padding .';';
	if ( $content_padding ) $content_css .= 'padding:'. $content_padding .';';
	if ( $content_bg ) $content_css .= 'background:'. $content_bg .';';
	if ( $content_rounded ) $content_css .= 'border-radius:'. $content_rounded .'px;';

	if ( $heading_font_weight != 'Default' ) $heading_css .= 'font-weight:'. $heading_font_weight .';';
    if ( $heading_color ) $heading_css .= 'color:'. $heading_color .';';
    if ( $heading_font_size ) $heading_css .= 'font-size:'. $heading_font_size .'px;';
    if ( $heading_line_height ) $heading_css .= 'line-height:'. $heading_line_height .';';
    if ( $heading_top_margin ) $heading_css .= 'margin-top:'. $heading_top_margin .'px;';
    if ( $heading_bottom_margin ) $heading_css .= 'margin-bottom:'. $heading_bottom_margin .'px;';
    if ( $heading_font_family != 'Default' ) {
        $heading_css .= 'font-family:'. $heading_font_family .';';
    }
	if ( $heading ) {
		$heading_html = sprintf('<h3 style="%2$s">%1$s</h3>', $heading, $heading_css);
	}

	if ( $desc_font_weight != 'Default' ) $desc_css .= 'font-weight:'. $desc_font_weight .';';
    if ( $desc_color ) $desc_css .= 'color:'. $desc_color .';';
    if ( $desc_font_size ) $desc_css .= 'font-size:'. $desc_font_size .'px;';
    if ( $desc_line_height ) $desc_css .= 'line-height:'. $desc_line_height .';';
    if ( $desc_top_margin ) $desc_css .= 'margin-top:'. $desc_top_margin .'px;';
    if ( $desc_bottom_margin ) $desc_css .= 'margin-bottom:'. $desc_bottom_margin .'px;';
    if ( $desc_font_family != 'Default' ) {
        $desc_css .= 'font-family:'. $desc_font_family .';';
    }
	if ( $desc_content ) {
		$desc_content_html = sprintf('<div class="text" style="%2$s">%1$s</div>', $desc_content, $desc_css);
	}	
	

	if ( $highlight_w ) $highlight_css .= 'width:'. $highlight_w .'px;';
	if ( $highlight_h ) $highlight_css .= 'height:'. $highlight_h .'px;';
	if ( $highlight_border_color && $highlight_border_width ) $highlight_css .= 'border-style:solid;border-width:'. $highlight_border_width .';border-color:'. $highlight_border_color .';';
	if ( $highlight_background ) $highlight_css .= 'background:'. $highlight_background .';';
	if ( $add_highlight == 'yes' ) {
		$highlight_html = sprintf('<span class="highlight" style="%1$s"></span>', $highlight_css);
	}

	if( $alignment == 'timeline-left-all' ) {
		if ( $content_bg ) $arrows_css .= 'border-color: transparent transparent transparent '. $content_bg .';';
	} else {
		if ( $content_bg ) $arrows_css .= 'border-color: transparent ' . $content_bg . ' transparent transparent;';
	}
	if ( $add_arrows == 'yes' ) {
		$arrows_html = sprintf('<span class="arrows" style="%1$s"></span>', $arrows_css);
	}
	

	if( $alignment == 'timeline-left-all' ) {		
		$content_html = sprintf(
			'<div class="timeline-container timeline-left">
				<div class="timeline-content" style="%4$s">					
					<div class="timeline-content-inner" style="%3$s">
						%6$s
						%1$s
						%2$s
					</div>
					%5$s
				</div>
			</div>',
			$heading_html,
			$desc_content_html,
			$content_css,
			$wrap_css,
			$highlight_html,
			$arrows_html
		);		
	} else {
		$content_html = sprintf(
			'<div class="timeline-container timeline-right">
				<div class="timeline-content" style="%4$s">					
					<div class="timeline-content-inner" style="%3$s">
						%6$s
						%1$s
						%2$s
					</div>
					%5$s
				</div>
			</div>',
			$heading_html,
			$desc_content_html,
			$content_css,
			$wrap_css,
			$highlight_html,
			$arrows_html
		);
	}

	return sprintf( '
		<div class="themesflat_sc_vc-spacer clearfix">
			<div class="timeline">
				<div class="%1$s">
					%2$s
					%3$s
				</div>
			</div>
		</div>',
		$alignment,
		$time_html,		
		$content_html
	);
}