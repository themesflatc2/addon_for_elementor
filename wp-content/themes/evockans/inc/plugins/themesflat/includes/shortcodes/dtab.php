<?php
/**
 * Register filter for append custom class name
 * that generated from visual-composer
 */
class WPBakeryShortCode_dtab extends WPBakeryShortCodesContainer {}
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_dtab_shortcode_params' );

function themesflat_dtab_shortcode_params() {
	vc_map( array(
		'name'        => esc_html__( 'Tab', 'evockans' ),
	    'description' => esc_html__('Displaying Tab.', 'evockans'),
		'base'        => 'dtab',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'js_composer'),
		'as_child'    => array( 'only' => 'dtabs' ),
		'as_parent' => array( 'except' => 'dtab' ),
		'controls' => 'full',
		'show_settings_on_create' => true,
		'js_view' => 'VcColumnView',
		'params'      => array(
			// Title
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Title', 'evockans'),
				'param_name' => 'title',
				'value' => 'Tab',
				'group' => esc_html__( 'Title', 'evockans' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Title: Padding', 'evockans'),
				'param_name' => 'title_padding',
				'value' => '21px 24px 21px 24px',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Title', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Title: Margin', 'evockans'),
				'param_name' => 'title_margin',
				'value' => '0px 4px 4px 0px',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Title', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Title: Rounded', 'evockans'),
				'param_name' => 'title_rounded',
				'value' => '',
				'description'	=> esc_html__('ex: 5px', 'evockans'),
				'group' => esc_html__( 'Title', 'evockans' ),
	        ),
	        // Icon
	        array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Show Icon', 'evockans' ),
				'param_name' => 'show_icon',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Title', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Icon Position', 'evockans' ),
				'param_name' => 'icon_position',
				'value'      => array(
					'Left' => 'icon-left',
					'Top' => 'icon-top',
				),
				'std'		=> 'icon-left',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'evockans' ),
				'param_name' => 'icon_type',
				'description' => esc_html__( 'Select icon library.', 'evockans' ),
				'value' => array(
					esc_html__( 'None', 'evockans' ) => '',
					esc_html__( 'Mono Social', 'evockans' ) => 'monosocial',
					esc_html__( 'Simple Line', 'evockans' ) => 'simpleline',
					esc_html__( 'Font Ionicons', 'evockans' ) => 'ionicons',
					esc_html__( 'Font Elegant', 'evockans' ) => 'eleganticons',
					esc_html__( 'Font Themify Icons', 'evockans' ) => 'themifyicons',
					esc_html__( 'Font Icomoon Icons', 'evockans' ) => 'icomoon',
					esc_html__( 'FontAwesome', 'evockans' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'evockans' ) => 'openiconic',
					esc_html__( 'Typicons', 'evockans' ) => 'typicons',
					esc_html__( 'Entypo', 'evockans' ) => 'entypo',
					esc_html__( 'Linecons', 'evockans' ) => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_monosocial',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'monosocial',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'monosocial',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_simpleline',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'simpleline',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'simpleline',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_ionicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'ionicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'ionicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_eleganticons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'eleganticons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'eleganticons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_themifyicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'themifyicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'themifyicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_icomoon',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'icomoon',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'icomoon',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_fontawesome',
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'fontawesome',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_openiconic',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'openiconic',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'openiconic',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_typicons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'typicons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'typicons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_entypo',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'entypo',
					'iconsPerPage' => 300,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'entypo',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_linecons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'linecons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon Color', 'evockans'),
				'param_name' => 'icon_color',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
            ),
            array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Icon Wrap Width', 'evockans' ),
				'param_name' => 'icon_width',
				'value'      => '',
				'std'		=> '15px',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Font Size', 'evockans'),
				'param_name' => 'icon_font_size',
				'value' => '15px',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon: Margin', 'evockans'),
				'param_name' => 'icon_margin',
				'value' => '0px 10px 0px 0px',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
	        ),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Content Color', 'evockans'),
				'param_name' => 'content_color',
				'value' => '#999999',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Content Background Color', 'evockans'),
				'param_name' => 'content_bg_color',
				'value' => '#ffffff',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content Padding', 'evockans'),
				'param_name' => 'content_padding',
				'value' => '30px',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 30px 30px 30px 30px;', 'evockans'),
				'group' => esc_html__( 'Content', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content: Margin', 'evockans'),
				'param_name' => 'content_margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Content', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content: Rounded', 'evockans'),
				'param_name' => 'content_rounded',
				'value' => '',
				'description'	=> esc_html__('ex: 5px', 'evockans'),
				'group' => esc_html__( 'Content', 'evockans' ),
	        ),
	        // Typography
	        //title
	        array(
				'type' => 'headings',
				'text' => esc_html__('Title', 'evockans'),
				'param_name' => 'title_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Title Font Family', 'evockans' ),
				'param_name' => 'title_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Title Font Weight', 'evockans' ),
				'param_name' => 'title_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Title Font Size', 'evockans'),
				'param_name' => 'title_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Title Line-Height', 'evockans'),
				'param_name' => 'title_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        //Content
	        array(
				'type' => 'headings',
				'text' => esc_html__('Title', 'evockans'),
				'param_name' => 'content_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Content Font Family', 'evockans' ),
				'param_name' => 'content_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Content Font Weight', 'evockans' ),
				'param_name' => 'content_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content Font Size', 'evockans'),
				'param_name' => 'content_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content Line-Height', 'evockans'),
				'param_name' => 'content_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
		)
	) );
}

add_shortcode( 'dtab', 'themesflat_shortcode_dtab' );

/**
 * dtab shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_dtab( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'title' => 'Tab',
		'title_padding' => '21px 24px 21px 24px',
		'title_margin' => '0px 4px 4px 0px',
		'title_rounded' => '',
		'show_icon' => '',
		'icon_position' => 'icon-left',
		'icon_type' => '',
		'icon' => '',
		'icon_color' => '',
		'icon_width' => '15px',
		'icon_font_size' => '15px',
		'icon_margin' => '0px 10px 0px 0px',
		'content_padding' => '30px',
		'content_margin' => '',
		'content_rounded' => '',
		'content_color' => '#999999',
		'content_bg_color' => '#ffffff',
		'title_font_family' => 'Default',
		'title_font_weight' => 'Default',
		'title_font_size' => '',
		'title_line_height' => '',
		'content_font_family' => 'Default',
		'content_font_weight' => 'Default',
		'content_font_size' => '',
		'content_line_height' => '',		
		
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'dtab', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$title_font_size = intval( $title_font_size );	
	$icon_width = intval( $icon_width );
	$icon_font_size = intval( $icon_font_size );
	$title_rounded = intval( $title_rounded );
	$content_rounded = intval( $content_rounded );

	$cls = $css = $title_css = $title_css_js = $title_inner_css = $content_css = $html = $icon_html = $icon_css = '';

	//title
	if ( $title_font_weight != 'Default' ) $title_css .= 'font-weight:'. $title_font_weight .';';
	if ( $title_font_size ) $title_css .= 'font-size:'. $title_font_size .'px;';
	if ( $title_line_height ) $title_css .= 'line-height:'. $title_line_height .';';
	if ( $title_font_family != 'Default' ) {
		$title_css .= 'font-family:'. $title_font_family .';';
	}	
	if ( $title_margin ) $title_css .= 'margin:'. $title_margin .';';
	if ( $title_rounded ) $title_css .= 'border-radius:'. $title_rounded .'px;';
	if ( $title_rounded ) $title_inner_css .= 'border-radius:'. $title_rounded .'px;';
	if ( $title_padding ) $title_inner_css .= 'padding:'. $title_padding .';';
		
	//content
	if ( $content_color ) $content_css .= 'color:'. $content_color .';';
	if ( $content_bg_color ) $content_css .= 'background-color:'. $content_bg_color .';';	
	if ( $content_margin ) $content_css .= 'margin:'. $content_margin .';';
	if ( $content_padding ) $content_css .= 'padding:'. $content_padding .';';
	if ( $content_rounded ) $content_css .= 'border-radius:'. $content_rounded .'px;';

	if ( $content_font_weight != 'Default' ) $content_css .= 'font-weight:'. $content_font_weight .';';
	if ( $content_font_size ) $content_css .= 'font-size:'. $content_font_size .'px;';
	if ( $content_line_height ) $content_css .= 'line-height:'. $content_line_height .';';
	if ( $content_font_family != 'Default' ) {
		$content_css .= 'font-family:'. $content_font_family .';';
	}
	
	if ( $show_icon ) {
	
	$icon = themesflat_sc_vc_get_icon_class( $atts, 'icon' );	
		if ( $icon && $icon_type != '' ) {
			vc_icon_element_fonts_enqueue( $icon_type );

			if ($icon_width) {
				$line_height = $icon_width;
				$icon_css .= 'line-height:'. $line_height .'px;';
				$icon_css .= 'width:'. $icon_width .'px;';
				$icon_css .= 'height:'. $icon_width .'px;';				
			}				
			
			if ( $icon_font_size ) $icon_css .= 'font-size:'. $icon_font_size .'px;';
			if ( $icon_color ) $icon_css .= 'color:'. $icon_color .';';
			if ( $icon_margin ) $icon_css .= 'margin:'. $icon_margin .';';

			$icon_html = sprintf(
				'<div class="icon-wrap" style="%2$s">
					<i class="%1$s"></i>
				</div>',
				$icon,
				$icon_css			
			);
		}
	}

	if ( $title ) {		
		$html .= sprintf(
			'<li class="item-title %5$s" style="%1$s">
				<span class="inner" style="%2$s">
				%4$s %3$s
				</span>
			</li>',
			esc_attr( $title_css ),
			esc_attr( $title_inner_css ),
			esc_html( $title ),
			$icon_html,
			$icon_position
		);
	}	

	
	$html .= sprintf(
		'<div class="item-content" style="%1$s">%2$s</div>',
		$content_css,
		do_shortcode($content)
	);
	

	return sprintf( '<div class="tab-content %1$s" style="%3$s">%2$s</div>', $cls, $html, $css );
}