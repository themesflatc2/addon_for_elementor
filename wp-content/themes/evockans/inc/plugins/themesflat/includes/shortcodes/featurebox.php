<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_featurebox_shortcode_params' );

function themesflat_featurebox_shortcode_params() {
	vc_map( array(
		'name'        => esc_html__( 'Feature Box', 'evockans' ),
        'description' => esc_html__('Displaying a feature content box.', 'evockans'),
		'base'        => 'featurebox',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
		'params'      => array(
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Wrap Background', 'evockans'),
				'param_name' => 'wrap_background',
				'value' => '#f8f8f8',
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap Padding', 'evockans'),
				'param_name' => 'wrap_padding',
				'value' => '30px',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
            ),
	        // Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image', 'evockans'),
				'param_name' => 'image',
				'value' => '',
				'group' => esc_html__( 'Image', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Image Cropping', 'evockans' ),
				'param_name' => 'image_crop',
				'value'      => array(
					'Full' => 'full',
					'600 x 600' => 'square',
					'600 x 500' => 'rectangle',
					'600 x 390' => 'rectangle2',
					'870 x auto' => 'auto1',
					'600 x auto' => 'auto2',
					'480 x auto' => 'auto3',
				),
				'std'		=> 'rectangle2',
				'group' => esc_html__( 'Image', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Image Width', 'evockans'),
				'param_name' => 'image_width',
				'value' => '',
				'description'	=> esc_html__('Ex: 200px.', 'evockans'),
				'group' => esc_html__( 'Image', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Image: Right Margin', 'evockans'),
				'param_name' => 'image_right_margin',
				'value' => '30px',
				'group' => esc_html__( 'Image', 'evockans' ),
	        ),
	        // Content
			array(
				'type' => 'headings',
				'text' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading_content',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Tag', 'evockans' ),
				'param_name' => 'tag',
				'value'      => array(
					'H1' => 'h1',
					'H2' => 'h2',
					'H3' => 'h3',
					'H4' => 'h4',
					'H5' => 'h5',
					'H6' => 'h6',
				),
				'std'		=> 'h3',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Heading Text', 'evockans' ),
				'param_name' => 'heading',
				'std'		=> 'Heading Text',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Heading Color', 'evockans'),
				'param_name' => 'heading_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Separator', 'evockans'),
				'param_name' => 'separator_content',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Separator', 'evockans' ),
				'param_name' => 'separator',
				'value'      => array(
					'No Separator' => '',
					'Line' => 'line'
				),
				'std'		=> '',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Line: Full-width', 'evockans' ),
				'param_name' => 'line_full',
				'value'      => array(
					'No' => 'no',
					'Yes' => 'yes',
				),
				'std'		=> 'no',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line Width', 'evockans'),
				'param_name' => 'line_width',
				'value' => '50',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'line_full', 'value' => 'no' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line Height', 'evockans'),
				'param_name' => 'line_height',
				'value' => '2',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Line Color', 'evockans'),
				'param_name' => 'line_color',
				'value' => '#ffb500',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
            ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Description', 'evockans'),
				'param_name' => 'content_content',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'textarea_html',
				'holder' => 'div',
				'param_name' => 'content',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Content Color', 'evockans'),
				'param_name' => 'content_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
	        // Button
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Text', 'evockans'),
				'param_name' => 'button_text',
				'value' => 'READ MORE',
				'group' => esc_html__( 'Button', 'evockans' ),
				'description'	=> esc_html__('Leave it empty to disable.', 'evockans'),
	        ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Style', 'evockans' ),
				'param_name' => 'button_style',
				'value'      => array(
					'Simple Link' => 'simple_link',
					'Accent' => 'accent',
					'Dark' => 'dark',
					'Light' => 'light',
					'Very Light' => 'very-light',
					'White' => 'white',
					'Outline' => 'outline',
					'Outline Dark' => 'outline_dark',
					'Outline Light' => 'outline_light',
					'Outline Very light' => 'outline_very-light',
					'Outline White' => 'outline_white',
				),
				'std'		=> 'accent',
				'group' => esc_html__( 'Button', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Link Style', 'evockans' ),
				'param_name' => 'link_style',
				'value'      => array(
					'Style 1' => 'style-1',
					'Style 2' => 'style-2',
					'Style 3' => 'style-3',
					'Style 4' => 'style-4',
				),
				'std'		=> 'style-1',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'button_style', 'value' => 'simple_link' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Size', 'evockans' ),
				'param_name' => 'button_size',
				'value'      => array(
					'Medium' => '',
					'Small' => 'small',
					'Big' => 'big',
				),
				'std'		=> 'small',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array(
					'element' => 'button_style',
					'value' => array (
						'accent',
						'dark',
						'light',
						'very-light',
						'white',
						'outline',
						'outline_dark',
						'outline_light',
						'outline_very-light',
						'outline_white'
					)
				),
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Rounded', 'evockans'),
				'param_name' => 'button_rounded',
				'value' => '',
				'description'	=> esc_html__('Ex: 10px', 'evockans'),
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array(
					'element' => 'button_style',
					'value' => array (
						'accent',
						'dark',
						'light',
						'very-light',
						'white',
						'outline',
						'outline_dark',
						'outline_light',
						'outline_very-light',
						'outline_white'
					)
				),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Button (URL):', 'evockans'),
				'param_name' => 'button_url',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
            ),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Open Link in a new Tab', 'evockans' ),
				'param_name' => 'new_tab',
				'value' => array(
					'Yes' => 'yes',
					'No' => 'no',
				),
				'std'		=> 'yes',
				'group' => esc_html__( 'Button', 'evockans' ),
			),
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Button Icon?', 'evockans' ),
				'param_name' => 'show_icon',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Button', 'evockans' ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'evockans' ),
				'param_name' => 'icon_type',
				'description' => esc_html__( 'Select icon library.', 'evockans' ),
				'value' => array(
					esc_html__( '', 'evockans' ) => '',
					esc_html__( 'AutoSer Icons', 'evockans' ) => 'extraicon',
					esc_html__( 'FontAwesome', 'evockans' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'evockans' ) => 'openiconic',
					esc_html__( 'Typicons', 'evockans' ) => 'typicons',
					esc_html__( 'Entypo', 'evockans' ) => 'entypo',
					esc_html__( 'Linecons', 'evockans' ) => 'linecons',
				),
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_extraicon',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'extraicon',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'extraicon',
			    ),
			    'group' => esc_html__( 'Button', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon',
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'fontawesome',
				),
				'group' => esc_html__( 'Button', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_openiconic',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'openiconic',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'openiconic',
				),
				'group' => esc_html__( 'Button', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_typicons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'typicons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'typicons',
				),
				'group' => esc_html__( 'Button', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_entypo',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'entypo',
					'iconsPerPage' => 300,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'entypo',
				),
				'group' => esc_html__( 'Button', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_linecons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'linecons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'linecons',
				),
				'group' => esc_html__( 'Button', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Font Size', 'evockans'),
				'param_name' => 'icon_font_size',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon: Left Padding', 'evockans'),
				'param_name' => 'icon_left_padding',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'description'	=> esc_html__('Spacing between the icon and the link.', 'evockans'),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
	        ),
	        // Typography
			array(
				'type' => 'headings',
				'text' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading: Font Family', 'evockans' ),
				'param_name' => 'heading_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading: Font Weight', 'evockans' ),
				'param_name' => 'heading_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Font Size', 'evockans'),
				'param_name' => 'heading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Line Height', 'evockans'),
				'param_name' => 'heading_line_height',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        // Spacing
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Top Margin', 'evockans'),
				'param_name' => 'heading_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Bottom Margin', 'evockans'),
				'param_name' => 'heading_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Separator: Top Margin', 'evockans'),
				'param_name' => 'sep_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Separator: Bottom Margin', 'evockans'),
				'param_name' => 'sep_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button: Top Margin', 'evockans'),
				'param_name' => 'button_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button: Bottom Margin', 'evockans'),
				'param_name' => 'button_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
		)
	) );
}

add_shortcode( 'featurebox', 'themesflat_shortcode_featurebox' );

/**
 * featurebox shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_featurebox( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'wrap_background' => '#f8f8f8',
		'wrap_padding' => '30px',
		'image' => '',
		'image_crop' => 'full',
		'image_width' => '',
		'image_right_margin' => '30px',
	    'heading' => 'Heading Text',
	    'heading_color' => '',
	    'content_color' => '',
		'separator' => '',
		'line_full' => 'no',
		'line_width' => '50',
		'line_height' => '2',
		'line_color' => '#ffb500',
		'tag' => 'h3',
	    'button_text' => 'READ MORE',
	    'button_style' => 'accent',
	    'button_size' => 'small',
	    'link_style' => 'style-1',
	    'button_rounded' => '',
	    'button_url' => '',
	    'new_tab' => 'yes',
	    'show_icon' => '',
	    'icon_type' => '',
	    'icon' => '',
	    'icon_font_size' => '',
	    'icon_left_padding' => '',
		'heading_font_family' => 'Default',
		'heading_font_weight' => 'Default',
		'heading_font_size' => '',
		'heading_line_height' => '',
		'heading_top_margin' => '',
		'heading_bottom_margin' => '',
		'sep_top_margin' => '',
		'sep_bottom_margin' => '',
		'button_top_margin' => '',
		'button_bottom_margin' => ''
	), $atts ) );	
	//extract($atts = vc_map_get_attributes( 'featurebox', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$wrap_padding = intval( $wrap_padding );
	$image_width = intval( $image_width );
	$image_right_margin = intval( $image_right_margin );
	$line_height = intval( $line_height );

	$icon_font_size = intval( $icon_font_size );
	$icon_left_padding = intval( $icon_left_padding );

	$heading_font_size = intval( $heading_font_size );
	$heading_line_height = intval( $heading_line_height );

	$heading_top_margin = intval( $heading_top_margin );
	$heading_bottom_margin = intval( $heading_bottom_margin );
	$sep_top_margin = intval( $sep_top_margin );
	$sep_bottom_margin = intval( $sep_bottom_margin );
	$button_top_margin = intval( $button_top_margin );
	$button_bottom_margin = intval( $button_bottom_margin );
	$button_rounded = intval( $button_rounded );

	$cls = $button_cls = $inner_css = $thumb_css = $heading_css = $sep_css = $content_css = $button_wrap_css = $button_css = $icon_css = '';
	$image_html = $heading_html = $sep_html = $content_html = $button_html = $icon_html = '';

	if ( $wrap_padding ) $inner_css .= 'padding:'. $wrap_padding .'px;';
	if ( $wrap_background ) $inner_css .= 'background-color:'. $wrap_background .';';
	if ( $content_color ) $content_css .= 'color:'. $content_color .';';

	if ( $heading_font_weight != 'Default' ) $heading_css .= 'font-weight:'. $heading_font_weight .';';
	if ( $heading_color ) $heading_css .= 'color:'. $heading_color .';';
	if ( $heading_font_size ) $heading_css .= 'font-size:'. $heading_font_size .'px;';
	if ( $heading_line_height ) $heading_css .= 'line-height:'. $heading_line_height .'px;';
	if ( $heading_top_margin ) $heading_css .= 'margin-top:'. $heading_top_margin .'px;';
	if ( $heading_bottom_margin ) $heading_css .= 'margin-bottom:'. $heading_bottom_margin .'px;';
	if ( $heading_font_family != 'Default' ) {
		$heading_css .= 'font-family:'. $heading_font_family .';';
	}

	if ( $image ) {
	    $img_size = 'themesflat_sc_vc-rectangle2';
	    if ( $image_crop == 'square' ) $img_size = 'themesflat_sc_vc-square';
	    if ( $image_crop == 'rectangle' ) $img_size = 'themesflat_sc_vc-rectangle';
	    if ( $image_crop == 'auto1' ) $img_size = 'themesflat_sc_vc-medium-auto';
	    if ( $image_crop == 'auto2' ) $img_size = 'themesflat_sc_vc-small-auto';
	    if ( $image_crop == 'auto3' ) $img_size = 'themesflat_sc_vc-xsmall-auto';
	    if ( $image_crop == 'full' ) $img_size = 'full';

		if ( $image_width ) $thumb_css .= 'width:'. $image_width .'px;max-width:'. $image_width .'px;';
		if ( $image_right_margin ) $thumb_css .= 'margin-right:'. $image_right_margin .'px;';

	    $image_html .= sprintf(
	        '<div class="thumb" style="%2$s">%1$s</div>',
	         wp_get_attachment_image( $image, $img_size ), $thumb_css
	    );
	}

	if ( $heading ) $heading_html .= sprintf(
		'<%3$s class="heading" style="%2$s"> %1$s </%3$s>',
		$heading,
		$heading_css,
		$tag
	);

	if ($separator == 'line' ) {
		if ( $line_full == 'yes' ) {
			$sep_css = 'width:100%;';
		} else {
			$line_width = intval( $line_width );
			if ( $line_width ) $sep_css = 'width:'. $line_width .'px;';
		}

		if ( $line_height ) $sep_css .= 'height:'. $line_height .'px;';
		if ( $line_color ) $sep_css .= 'background-color:'. $line_color .';';
		if ( $sep_top_margin ) $sep_css .= 'margin-top:'. $sep_top_margin .'px;';
		if ( $sep_bottom_margin ) $sep_css .= 'margin-bottom:'. $sep_bottom_margin .'px;';
		
		$sep_html .= sprintf( '<div class="clearfix"></div><div class="sep" style="%1$s"></div>', $sep_css );
	}

	if ( $content )
		$content_html .= sprintf(
		'<div class="content-inner">%1$s</div>',
		$content
	);

	$new_tab = $new_tab == 'yes' ? '_blank' : '_self'; 

	if ( $button_text ) {
	    $button_cls = $button_size;
	    if ( $button_style == 'simple_link' ) $button_cls .= ' simple-link font-heading '. $link_style;
	    if ( $button_style == 'accent' ) $button_cls .= ' themesflat_sc_vc-button accent';
	    if ( $button_style == 'dark' ) $button_cls .= ' themesflat_sc_vc-button dark';
	    if ( $button_style == 'light' ) $button_cls .= ' themesflat_sc_vc-button light';
	    if ( $button_style == 'very-light' ) $button_cls .= ' themesflat_sc_vc-button very-light';
	    if ( $button_style == 'white' ) $button_cls .= ' themesflat_sc_vc-button white';
	    if ( $button_style == 'outline' ) $button_cls .= ' themesflat_sc_vc-button outline ol-accent';
	    if ( $button_style == 'outline_dark' ) $button_cls .= ' themesflat_sc_vc-button outline dark';
	    if ( $button_style == 'outline_light' ) $button_cls .= ' themesflat_sc_vc-button outline light';
	    if ( $button_style == 'outline_very-light' ) $button_cls .= ' themesflat_sc_vc-button outline very-light';
	    if ( $button_style == 'outline_white' ) $button_cls .= '  themesflat_sc_vc-button outline white';

	    if ( $show_icon ) {
	        $icon = themesflat_sc_vc_get_icon_class( $atts, 'icon' );
	        if ( $icon && $icon_type != '' ) {
	            vc_icon_element_fonts_enqueue( $icon_type );

	            if ( $icon_font_size ) $icon_css .= 'font-size:'. $icon_font_size .'px;';
	            if ( $icon_left_padding ) $icon_css .= 'padding-left:'. $icon_left_padding .'px;';

	            $icon_html = sprintf('<span class="icon" style="%2$s"><i class="%1$s"></i></span>', $icon, $icon_css );
	        }
	    }

	    if ( $button_top_margin ) $button_wrap_css .= 'margin-top:'. $button_top_margin .'px;';
		if ( $button_bottom_margin ) $button_wrap_css .= 'margin-bottom:'. $button_bottom_margin .'px;';
		if ( $button_rounded ) $button_css .= 'border-radius:'. $button_rounded .'px;';

	    $button_html .= sprintf(
	        '<div class="btn" style="%6$s">
	            <a target="%4$s" class="%3$s" href="%2$s" style="%7$s">%1$s %5$s</a>
	        </div>',
	        esc_html( $button_text ),
	        esc_attr( $button_url ),
	        $button_cls,
	        $new_tab,
	        $icon_html,
	        $button_wrap_css,
	        $button_css
	    );
	}

	return sprintf(
		'<div class="themesflat_sc_vc-feature-box clearfix %1$s">
			<div class="inner clearfix" style="%2$s">
				%4$s
				<div class="content-wrap" style="%3$s">
				%5$s %6$s %7$s %8$s
				</div>
			</div>
		</div>',
		$cls,
		$inner_css,
		$content_css,
		$image_html,
		$heading_html,
		$sep_html,
		$content_html,
		$button_html
	);
}