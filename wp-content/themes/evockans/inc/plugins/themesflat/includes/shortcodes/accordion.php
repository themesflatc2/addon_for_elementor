<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_accordion_shortcode_params' );

function themesflat_accordion_shortcode_params() {
	vc_map( array(
		'name'        => esc_html__( 'Accordion', 'evockans' ),
	    'description' => esc_html__('Displaying Accordion', 'evockans'),
		'base'        => 'accordion',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'js_composer'),
		'as_child'    => array( 'only' => 'accordions' ),
		'params'      => array(
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Active section', 'evockans' ),
				'param_name' => 'open',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Bottom Margin', 'evockans'),
				'param_name' => 'bottom_margin',
				'value' => '10',
				'description'	=> esc_html__('Default: 10px', 'evockans'),
		  	),	
		  	array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Background Color Accordion Item', 'evockans'),
				'param_name' => 'background_color',
				'value' => '',
            ),	  	
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Rounded Accordion Item', 'evockans'),
				'param_name' => 'accordion_item_rounded',
				'value' => '',
				'description'	=> esc_html__('Ex: 5px', 'evockans'),
            ), 
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Border Width', 'evockans'),
				'param_name' => 'border_width',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Border Color', 'evockans'),
				'param_name' => 'border_color',
				'value' => '',
            ),
			// Heading
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Tag', 'evockans' ),
				'param_name' => 'tag',
				'value'      => array(
					'H1' => 'h1',
					'H2' => 'h2',
					'H3' => 'h3',
					'H4' => 'h4',
					'H5' => 'h5',
					'H6' => 'h6',
				),
				'std'		=> 'h6',
				'group' => esc_html__( 'Heading', 'evockans' ),
			),
            array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading',
				'value' => '',
				'group' => esc_html__( 'Heading', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Heading Color', 'evockans'),
				'param_name' => 'heading_color',
				'value' => '',
				'group' => esc_html__( 'Heading', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Heading Background Color', 'evockans'),
				'param_name' => 'heading_background_color',
				'value' => '',
				'group' => esc_html__( 'Heading', 'evockans' ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Padding', 'evockans'),
				'param_name' => 'heading_padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 22px 20px 22px 20px', 'evockans'),
				'group' => esc_html__( 'Heading', 'evockans' ),
	        ),	        
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Rounded', 'evockans'),
				'param_name' => 'heading_rounded',
				'value' => '',
				'description'	=> esc_html__('Ex: 5px', 'evockans'),
				'group' => esc_html__( 'Heading', 'evockans' ),
            ),  
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Border Width', 'evockans'),
				'param_name' => 'heading_border_width',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
				'group' => esc_html__( 'Heading', 'evockans' ),
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Border Color', 'evockans'),
				'param_name' => 'heading_border_color',
				'value' => '',
				'group' => esc_html__( 'Heading', 'evockans' ),
            ),    
			// Icon
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Show Heading Icon?', 'evockans' ),
				'param_name' => 'show_icon',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Heading', 'evockans' ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'evockans' ),
				'param_name' => 'icon_type',
				'description' => esc_html__( 'Select icon library.', 'evockans' ),
				'value' => array(
					esc_html__( 'None', 'evockans' ) => '',
					esc_html__( 'Mono Social', 'evockans' ) => 'monosocial',
					esc_html__( 'Simple Line', 'evockans' ) => 'simpleline',
					esc_html__( 'Font Ionicons', 'evockans' ) => 'ionicons',
					esc_html__( 'Font Elegant', 'evockans' ) => 'eleganticons',
					esc_html__( 'Font Themify Icons', 'evockans' ) => 'themifyicons',
					esc_html__( 'Font Icomoon Icons', 'evockans' ) => 'icomoon',
					esc_html__( 'FontAwesome', 'evockans' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'evockans' ) => 'openiconic',
					esc_html__( 'Typicons', 'evockans' ) => 'typicons',
					esc_html__( 'Entypo', 'evockans' ) => 'entypo',
					esc_html__( 'Linecons', 'evockans' ) => 'linecons',
				),
				'group' => esc_html__( 'Heading', 'evockans' ),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_monosocial',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'monosocial',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'monosocial',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_simpleline',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'simpleline',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'simpleline',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_ionicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'ionicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'ionicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_eleganticons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'eleganticons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'eleganticons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_themifyicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'themifyicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'themifyicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_icomoon',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'icomoon',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'icomoon',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon',
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'fontawesome',
				),
				'group' => esc_html__( 'Heading', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_openiconic',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'openiconic',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'openiconic',
				),
				'group' => esc_html__( 'Heading', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_typicons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'typicons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'typicons',
				),
				'group' => esc_html__( 'Heading', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_entypo',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'entypo',
					'iconsPerPage' => 300,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'entypo',
				),
				'group' => esc_html__( 'Heading', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_linecons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'linecons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'linecons',
				),
				'group' => esc_html__( 'Heading', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Font Size', 'evockans'),
				'param_name' => 'icon_font_size',
				'value' => '',
				'group' => esc_html__( 'Heading', 'evockans' ),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Spacing between the heading and the icon', 'evockans'),
				'param_name' => 'heading_left_padding',
				'value' => '30',
				'group' => esc_html__( 'Heading', 'evockans' ),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
	        ),
	        // Content
			array(
				'type' 		=> 'textarea_html',
				'heading' 	=> esc_html__('Content', 'evockans'),
				'param_name' 	=> 'content',
				'value' 		=> '',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Content Color', 'evockans'),
				'param_name' => 'content_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Content: Background Color', 'evockans'),
				'param_name' => 'content_background_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content Padding', 'evockans'),
				'param_name' => 'content_padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 5px 20px 20px 20px', 'evockans'),
				'group' => esc_html__( 'Content', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content Margin', 'evockans'),
				'param_name' => 'content_margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 22px 20px 22px 20px', 'evockans'),
				'group' => esc_html__( 'Content', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Rounded', 'evockans'),
				'param_name' => 'content_rounded',
				'value' => '',
				'description'	=> esc_html__('Ex: 5px', 'evockans'),
				'group' => esc_html__( 'Content', 'evockans' ),
            ), 
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Border Width', 'evockans'),
				'param_name' => 'content_border_width',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
				'group' => esc_html__( 'Content', 'evockans' ),
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Border Color', 'evockans'),
				'param_name' => 'content_border_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
	        // Typography
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Font Family', 'evockans' ),
				'param_name' => 'heading_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Font Weight', 'evockans' ),
				'param_name' => 'heading_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Font Size', 'evockans'),
				'param_name' => 'heading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Line-Height', 'evockans'),
				'param_name' => 'heading_line_height',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        // Box Shadow
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Horizontal (Required)', 'evockans'),
				'param_name' => 'horizontal',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Vertical (Required)', 'evockans'),
				'param_name' => 'vertical',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 20px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Blur (Required)', 'evockans'),
				'param_name' => 'blur',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 40px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Spread (Required)', 'evockans'),
				'param_name' => 'spread',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Box Shadow Color (Required)', 'evockans'),
				'param_name' => 'shadow_color',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: #eeeeee', 'evockans'),
            ),
		)
	) );
}

add_shortcode( 'accordion', 'themesflat_shortcode_accordion' );

/**
 * accordion shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_accordion( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'open' => '',
		'bottom_margin' => '10',
		'border_color'	=> '',
		'background_color' => '',
		'accordion_item_rounded' => '',
		'border_width'	=> '',
		'tag' => 'h6',
		'heading' => '',
		'heading_color' => '',
		'heading_padding' => '',
		'heading_rounded' => '',
		'heading_border_width' => '',
		'heading_border_color' => '',
		'heading_background_color'	=> '',
		'show_icon' => '',
		'icon_type' => '',
		'icon' => '',
		'icon_font_size' => '',
		'heading_left_padding' => '',
		'content_color'	=> '',		
		'content_background_color' => '',		
		'content_padding' => '',
		'content_margin' => '',
		'content_rounded' => '',
		'content_border_width' => '',
		'content_border_color' => '',
		'heading_font_family' => 'Default',
		'heading_font_weight' => 'Default',
		'heading_font_size' => '',
		'heading_line_height' => '',
		'horizontal' => '',
	    'vertical' => '',
	    'blur' => '',
	    'spread' => '',
	    'shadow_color' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'accordion', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$icon_font_size = intval( $icon_font_size );
	$heading_left_padding = intval( $heading_left_padding );
	$heading_font_size = intval( $heading_font_size );
	$heading_line_height = intval( $heading_line_height );
	$heading_rounded = intval( $heading_rounded );
	$content_rounded = intval( $content_rounded );
	$accordion_item_rounded = intval( $accordion_item_rounded );
	$bottom_margin = intval( $bottom_margin );

	$cls = $css = $icon_wrap_css = $icon_css = $heading_css = $heading_inner_css = $content_css = $html = $icon_html = '';
	if ( $open ) $cls = ' active';
	$css .= $bottom_margin == '0'
		? 'margin-bottom:0;'
		: 'margin-bottom:'. $bottom_margin .'px;';
	

	if ( $horizontal && $vertical && $blur && $spread && $shadow_color ){
		$css .= 'box-shadow:'. $horizontal .' '. $vertical .' '. $blur .' '. $spread .' '. $shadow_color .';';
	}

	if ( $background_color ) $css .= 'background-color:' . $background_color . ';';
	
	if ( $accordion_item_rounded ) $css .= 'border-radius:'. $accordion_item_rounded .'px;';

	if ( $border_color && $border_width ) $css .= 'border-style:solid;border-width:'. $border_width .';border-color:'. $border_color .';';

	if ( $show_icon ) {
		$icon = themesflat_sc_vc_get_icon_class( $atts, 'icon' );
		if ( $icon && $icon_type != '' ) {
			$cls .= ' has-icon';
			vc_icon_element_fonts_enqueue( $icon_type );
			if ( $icon_font_size ) $icon_css .= 'font-size:'. $icon_font_size .'px;';
			$icon_html = sprintf('<span class="icon-wrap"><i class="%1$s" style="%2$s"></i></span>', $icon, $icon_css );
		}
	} else { $cls .= ' no-icon'; }

	if ( $heading_font_weight != 'Default' ) $heading_css .= 'font-weight:'. $heading_font_weight .';';
	if ( $heading_font_size ) $heading_css .= 'font-size:'. $heading_font_size .'px;';
	if ( $heading_line_height ) $heading_css .= 'line-height:'. $heading_line_height .'px;';
	if ( $heading_padding ) $heading_css .= 'padding:'. $heading_padding .';';
	if ( $heading_color ) $heading_css .= 'color:'. $heading_color .';';
	if ( $heading_rounded ) $heading_css .= 'border-radius:'. $heading_rounded .'px;';
	if ( $heading_border_color && $heading_border_width ) $heading_css .= 'border-style:solid;border-width:'. $heading_border_width .';border-color:'. $heading_border_color .';';
	if ( $heading_background_color ) $heading_css .= 'background-color:' . $heading_background_color . ';'; 
	if ( $heading_font_family != 'Default' ) {
		$heading_css .= 'font-family:'. $heading_font_family .';';
	}

	if ( $heading_left_padding ) $heading_inner_css .= 'padding-left:'. $heading_left_padding .'px;';
	if ( $content_color ) $content_css .= 'color:'. $content_color .';';
	if ( $content_background_color ) $content_css .= 'background:'. $content_background_color .';';
	if ( $content_padding ) $content_css .= 'padding:'. $content_padding .';';
	if ( $content_margin ) $content_css .= 'margin:'. $content_margin .';';
	if ( $content_rounded ) $content_css .= 'border-radius:'. $content_rounded .'px;';
	if ( $content_border_color && $content_border_width ) $content_css .= 'border-style:solid;border-width:'. $content_border_width .';border-color:'. $content_border_color .';';

	if ( $heading )
		$html .= sprintf(
			'<%4$s class="accordion-heading" style="%1$s">
				<span class="inner" style="%2$s">
					%5$s
					%3$s
				</span>
			</%4$s>',
			esc_attr( $heading_css ),
			esc_attr( $heading_inner_css ),
			esc_html( $heading ),
			$tag,
			$icon_html
		);

	if ( $content )
		$html .= sprintf(
			'<div class="accordion-content" style="%1$s">%2$s</div>',
			$content_css,
			$content
		);

	return sprintf( '<div class="accordion-item %1$s" style="%3$s">%2$s</div>', esc_attr( $cls ), $html, esc_attr( $css ) );
	
}