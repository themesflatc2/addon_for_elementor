<?php
/**
 * Register filter for append custom class name
 * that generated from visual-composer
 */
class WPBakeryShortCode_sectionadvanced extends WPBakeryShortCodesContainer {}
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_sectionadvanced_shortcode_params' );

function themesflat_sectionadvanced_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Section Advanced', 'evockans'),
        'description' => esc_html__('Section Advanced', 'evockans'),
        'base' => 'sectionadvanced',
		'weight'	=>	180,
		'icon' => THEMESFLAT_ICON,
		'as_parent' => array('except' => 'sectionadvanced'),
		'controls' => 'full',
		'show_settings_on_create' => true,
		'category' => esc_html__('THEMESFLAT', 'evockans'),
		'js_view' => 'VcColumnView',
		'params' => array(
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Section Height', 'evockans' ),
				'param_name' => 'adv_height',
				'value'      => array(
					'Full Height' => 'full-height',
					'Custom Height' => 'custom-height',
				),
				'std'		=> 'full-height',
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Section Custom Height', 'evockans'),
				'param_name' => 'adv_custom_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 700px.', 'evockans'),
				'dependency' => array( 'element' => 'adv_height', 'value' => 'custom-height' ),
            ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Showcase', 'evockans' ),
				'param_name' => 'showcase',
				'value'      => array(
					'Background Slideshow' => 'slideshow',
					'Background Video' => 'video',
				),
				'std'		=> 'slideshow',
			),
			array(
				'type' => 'attach_images',
				'heading' => esc_html__('Background Images', 'evockans'),
				'param_name' => 'images',
				'value' => '',
				'description' => esc_html__('Choose multi-images for background slideshow.', 'evockans'),
				'dependency' => array( 'element' => 'showcase', 'value' => 'slideshow' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Effects', 'evockans' ),
				'param_name' => 'effect',
				'value'      => array(
					'fade' => 'fade',
					'fade2' => 'fade2',
					'slideLeft' => 'slideLeft',
					'slideLeft2' => 'slideLeft2',
					'slideRight' => 'slideRight',
					'slideRight2' => 'slideRight2',
					'slideUp' => 'slideUp',
					'slideDown' => 'slideDown',
					'slideDown2' => 'slideDown2',
					'zoomIn' => 'zoomIn',
					'zoomIn2' => 'zoomIn2',
					'zoomOut' => 'zoomOut',
					'zoomOut2' => 'zoomOut2',
					'swirlLeft' => 'swirlLeft',
					'swirlLeft2' => 'swirlLeft2',
					'swirlRight' => 'swirlRight',
					'swirlRight2' => 'swirlRight2',
					'burn' => 'burn',
					'burn2' => 'burn2',
					'blur' => 'blur',
					'blur2' => 'blur2',
					'flash' => 'flash',
					'flash2' => 'flash2'
				),
				'std'		=> 'fade',
				'dependency' => array( 'element' => 'showcase', 'value' => 'slideshow' ),
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Youtube link (URL)', 'evockans'),
				'param_name' => 'video_link',
				'value' => '',
				'description' => esc_html__('Youtube link or ID. Ex: https://www.youtube.com/watch?time_continue=1&v=eJxA7s9si4A', 'evockans'),
				'dependency' => array( 'element' => 'showcase', 'value' => 'video' ),
            ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Pattern Overlay', 'evockans' ),
				'param_name' => 'pattern_overlay',
				'value'      => array(
					'No Parttern' => '',
					'Style 1' => 'style-1',
					'Style 2' => 'style-2',
					'Style 3' => 'style-3',
					'Style 4' => 'style-4',
					'Style 5' => 'style-5',
					'Style 6' => 'style-6',
					'Style 7' => 'style-7',
					'Style 8' => 'style-8',
					'Style 9' => 'style-9',
				),
				'std'		=> '',
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Color Overlay', 'evockans'),
				'param_name' => 'color_overlay',
				'value' => '',
            ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Content Alignment', 'evockans' ),
				'param_name' => 'alignment',
				'value'      => array(
					'Left' => 'text-left',
					'Center' => 'text-center',
					'Right' => 'text-right',
				),
				'std'		=> 'text-center',
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Content: Top Margin', 'evockans'),
				'param_name' => 'content_top',
				'value' => '',
				'description'	=> esc_html__('Ex: 50px. In case you want to set a spacing above the content area.', 'evockans'),
            ),
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Content area into Grid?', 'evockans' ),
				'param_name' => 'grid',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
			),
			// Arrow
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Show arrow?', 'evockans' ),
				'param_name' => 'scroll',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Arrow', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Arrow Style', 'evockans' ),
				'param_name' => 'arrow_style',
				'value'      => array(
					'Style 1' => 'style-1',
					'Style 2' => 'style-2',
				),
				'std'		=> 'style-1',
				'group' => esc_html__( 'Arrow', 'evockans' ),
				'dependency' => array( 'element' => 'scroll', 'value' => 'yes' ),
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Scroll to Row (ID)', 'evockans'),
				'param_name' => 'scroll_id',
				'value' => '',
				'description' => esc_html__('Enter the anchor ID you assigned to the row.', 'evockans'),
				'group' => esc_html__( 'Arrow', 'evockans' ),
				'dependency' => array( 'element' => 'scroll', 'value' => 'yes' ),
            ),
        )
    ) );
}

add_shortcode( 'sectionadvanced', 'themesflat_shortcode_sectionadvanced' );

/**
 * sectionadvanced shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_sectionadvanced( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'adv_height' => 'full-height',
		'adv_custom_height' => '',
		'showcase' => 'slideshow',
	    'images' => '',
	    'effect' => 'fade',
	    'color_overlay' => '',
	    'pattern_overlay' => '',
	    'alignment' => 'text-center',
	    'content_top' => '',
	    'grid' => '',
	    'video_link' => '',
	    'scroll' => '',
	    'arrow_style' => 'style-1',
	    'scroll_id' => '',
	), $atts ) );
	$content = wpb_js_remove_wpautop($content, true);

	$content_top = intval( $content_top );

	$count = $img_str = '';
	$content_html = $arrow_html = '';
	if ( ! empty( $images ) ) {
		$imgs = explode( ',', trim( $images ) );
		$count = count( $imgs );
		for ( $i=0; $i<$count; $i++ ) {
			$img_str .= wp_get_attachment_image_src( $imgs[$i], 'full' )[0] .'|';
		}
		$img_str = substr( $img_str, 0, -1 );
	}

	if ( $grid ) {
		$content_html = sprintf( '<div class="themesflat_sc_vc-container"><div class="adv-content">%1$s</div></div>', do_shortcode($content) );
	} else {
		$content_html = sprintf( '<div class="adv-content">%1$s</div>', do_shortcode($content) );
	}

	if ( $scroll ) {
		$arrow_html .= sprintf( '<a href="#%2$s" class="bounce infinite adv-arrow scroll-target %1$s"></a>', $arrow_style, esc_html( $scroll_id ) );
	}

	if ( $adv_height = 'custom-height' && $adv_custom_height )
		$adv_custom_height = intval( $adv_custom_height );

	if ( $showcase == 'slideshow') {
		wp_enqueue_script( 'themesflat_sc_vc-vegas' );
		return sprintf(
			'<div class="adv-section slideshow %6$s" data-count="%2$s" data-image="%1$s" data-effect="%5$s" data-overlay="%3$s" data-poverlay="%4$s" data-content="%9$s" data-height="%10$s">
		    	%7$s %8$s
			</div>',
			$img_str,
			$count,
			$color_overlay,
			$pattern_overlay,
			$effect,
			$alignment,
			$content_html,
			$arrow_html,
			$content_top,
			$adv_custom_height
		);
	}

	if ( $showcase == 'video') {
		wp_enqueue_script( 'themesflat_sc_vc-ytplayer' );

		$property = "{videoURL:'$video_link',containment:'.player', showControls:false, autoPlay:true, loop:true, mute:false, startAt:0, opacity:1, addRaster:'$pattern_overlay', quality:'default'}";

		return sprintf(
			'<div class="video-area">
				<div class="adv-section video player %3$s" data-property="%1$s" data-overlay="%2$s" data-content="%6$s" data-height="%7$s">
				    %4$s %5$s
			    </div>
		    </div>',
			$property,
			$color_overlay,
			$alignment,
			$content_html,
			$arrow_html,
			$content_top,
			$adv_custom_height
		);
	}
}