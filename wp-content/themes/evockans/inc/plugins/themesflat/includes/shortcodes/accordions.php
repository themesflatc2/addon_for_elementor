<?php
/**
 * Register filter for append custom class name
 * that generated from visual-composer
 */
class WPBakeryShortCode_accordions extends WPBakeryShortCodesContainer {}
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_accordions_shortcode_params' );

function themesflat_accordions_shortcode_params() {
	vc_map( array(
		'name'        => esc_html__( 'Accordions', 'evockans' ),
        'description' => esc_html__('Displaying Accordions', 'evockans'),
		'base'        => 'accordions',
		'weight'	=>	180,
		'icon' => THEMESFLAT_ICON,
		'as_parent' => array( 'only' => 'accordion' ),
		'controls' => 'full',
		'show_settings_on_create' => true,
		'category' => esc_html__('THEMESFLAT', 'evockans'),
		'js_view' => 'VcColumnView',
		'params' => array(
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Accordion Icon', 'evockans' ),
				'param_name' => 'accordion_icon',
				'value'      => array(
					'None' => 'accordion_icon_none',
					'Plus' => 'accordion_icon_plus',
				),
				'std'		=> 'accordion_icon_plus',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Accordion Icon Position', 'evockans' ),
				'param_name' => 'accordion_icon_position',
				'value'      => array(
					'Left' 		=> 'accordion_icon_left',
					'Right' 	=> 'accordion_icon_right',
				),
				'std'		=> 'accordion_icon_right',
				'dependency' => array( 'element' => 'accordion_icon', 'value' => 'accordion_icon_plus' ),
			),
			array(
				'type' => 'css_editor',
				'param_name' => 'css',
				'group' => esc_html__( 'Design Options', 'gravita' )
			),

		)
	) );
}

add_shortcode( 'accordions', 'themesflat_shortcode_accordions' );

/**
 * accordions shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_accordions( $atts, $content = null ) {
	extract( shortcode_atts( array(
	    'accordion_icon' => 'accordion_icon_plus',
	    'accordion_icon_position' => 'accordion_icon_right',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'accordions', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	return sprintf( '
		<div class="themesflat_sc_vc-accordions %2$s %3$s">%1$s</div>',
		do_shortcode($content),
		esc_attr( $accordion_icon ),
		esc_attr( $accordion_icon_position )
	);
}