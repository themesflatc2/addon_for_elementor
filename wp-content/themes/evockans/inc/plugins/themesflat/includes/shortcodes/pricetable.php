<?php
/**
 * Register filter for append custom class name
 * that generated from visual-composer
 */
class WPBakeryShortCode_pricetable extends WPBakeryShortCodesContainer {}
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_pricetable_shortcode_params' );

function themesflat_pricetable_shortcode_params() {
	vc_map( array(
		'name'        => esc_html__( 'Price Table', 'evockans' ),
        'description' => esc_html__('Displaying Price Tables.', 'evockans'),
		'base'        => 'pricetable',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'as_parent' => array('except' => 'pricetable'),
		'controls' => 'full',
		'show_settings_on_create' => true,
		'js_view' => 'VcColumnView',
		'params'      => array(	
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Price Table Style', 'evockans' ),
				'param_name' => 'price_table_style',
				'value'      => array(
					'Style 1' => 'style-1',
					'Style 2' => 'style-2',
					'Style 3' => 'style-3',					
				),
				'std'		=> 'style-1',
			),
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Price Table: Active', 'evockans' ),
				'param_name' => 'active',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Price Table: Active Style', 'evockans' ),
				'param_name' => 'style_active',
				'value'      => array(
					'Style 1' => 'active-style-1',
					'Style 2' => 'active-style-2',					
				),
				'std'		=> 'style-1',
				'dependency' => array( 'element' => 'active', 'value' => 'yes' ),
			),			
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Wrap: Alignment', 'evockans' ),
				'param_name' => 'alignment',
				'value'      => array(
					'Left' => '',
					'Center' => 'text-center',
					'Right' => 'text-right',					
				),
				'std'		=> 'text-center',
			),	
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Wrap: Background Color', 'evockans'),
				'param_name' => 'background_color',
				'value' => '#ffffff',
            ),	
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Padding', 'evockans'),
				'param_name' => 'padding',
				'value' => '40px 40px 40px 40px',
				'description'	=> esc_html__('Top Right Bottom Left. You can use % or px value.', 'evockans'),
	        ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap: Border Width', 'evockans'),
				'param_name' => 'border_width',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Wrap: Border Color', 'evockans'),
				'param_name' => 'border_color',
				'value' => '',
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap: Rounded', 'evockans'),
				'param_name' => 'rounded',
				'value' => '10px',
				'description'	=> esc_html__('ex: 10px', 'evockans'),
            ),
            // Box Shadow
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Horizontal (Required)', 'evockans'),
				'param_name' => 'horizontal',
				'value' => '0px',				
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Vertical (Required)', 'evockans'),
				'param_name' => 'vertical',
				'value' => '0px',				
				'description'	=> esc_html__('Ex: 20px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Blur (Required)', 'evockans'),
				'param_name' => 'blur',
				'value' => '57px',				
				'description'	=> esc_html__('Ex: 40px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Spread (Required)', 'evockans'),
				'param_name' => 'spread',
				'value' => '0px',				
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Box Shadow Color (Required)', 'evockans'),
				'param_name' => 'shadow_color',
				'value' => 'rgba(0,0,0,.08)',				
				'description'	=> esc_html__('Ex: #eeeeee', 'evockans'),
            ),
            // Icon
            array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Icon to Display', 'evockans' ),
				'param_name' => 'icon_display',
				'value'      => array(
					'Icon Font' => 'icon-font',
					'Icon Image' => 'icon-image',
				),
				'std'		=> 'icon-image',
				'dependency' => array( 'element' => 'price_table_style', 'value' => array('style-2','style-3') ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'evockans' ),
				'param_name' => 'icon_type',
				'description' => esc_html__( 'Select icon library.', 'evockans' ),
				'value' => array(
					esc_html__( 'None', 'evockans' ) => '',
					esc_html__( 'Mono Social', 'evockans' ) => 'monosocial',
					esc_html__( 'Simple Line', 'evockans' ) => 'simpleline',
					esc_html__( 'Font Ionicons', 'evockans' ) => 'ionicons',
					esc_html__( 'Font Elegant', 'evockans' ) => 'eleganticons',
					esc_html__( 'Font Themify Icons', 'evockans' ) => 'themifyicons',
					esc_html__( 'Font Icomoon Icons', 'evockans' ) => 'icomoon',
					esc_html__( 'FontAwesome', 'evockans' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'evockans' ) => 'openiconic',
					esc_html__( 'Typicons', 'evockans' ) => 'typicons',
					esc_html__( 'Entypo', 'evockans' ) => 'entypo',
					esc_html__( 'Linecons', 'evockans' ) => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-font' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_monosocial',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'monosocial',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'monosocial',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_simpleline',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'simpleline',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'simpleline',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_ionicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'ionicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'ionicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_eleganticons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'eleganticons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'eleganticons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_themifyicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'themifyicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'themifyicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_icomoon',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'icomoon',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'icomoon',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon',
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'fontawesome',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_openiconic',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'openiconic',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'openiconic',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_typicons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'typicons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'typicons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_entypo',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'entypo',
					'iconsPerPage' => 300,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'entypo',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_linecons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'linecons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon Color', 'evockans'),
				'param_name' => 'icon_color',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => array( 'icon-font' ) ),
            ),
            array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Icon Wrap Width', 'evockans' ),
				'param_name' => 'icon_width',
				'value'      => '',
				'std'		=> '80',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => array( 'icon-font' ) ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Font Size', 'evockans'),
				'param_name' => 'icon_font_size',
				'value' => '50px',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-font' ),
	        ),	        
			// Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image', 'evockans'),
				'param_name' => 'image',
				'value' => '',
				'group' => esc_html__( 'Image', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-image' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Image Width', 'evockans'),
				'param_name' => 'image_width',
				'value' => '80',
				'description'	=> esc_html__('Ex: 100px', 'evockans'),
				'group' => esc_html__( 'Image', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-image' ),
	        ),
	        array(
				'type'       => 'textfield',
				'heading' => esc_html__('Image Rounded', 'evockans'),
				'param_name' => 'image_rounded',
				'value'      => '',
				'description'	=> esc_html__('Ex: 100px', 'evockans'),
				'group' => esc_html__( 'Image', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-image' ),
	        ),	 

	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap Icon & Image: Padding', 'evockans'),
				'param_name' => 'icon_padding',
				'value' => '25px 0px',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'dependency' => array( 'element' => 'icon_display', 'value' => array ('icon-image' , 'icon-font') ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap Icon & Image: Margin', 'evockans'),
				'param_name' => 'icon_margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'dependency' => array( 'element' => 'icon_display', 'value' => array ('icon-image' , 'icon-font') ),
	        ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap Icon & Image: Border Width', 'evockans'),
				'param_name' => 'icon_border_width',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
				'dependency' => array( 'element' => 'icon_display', 'value' => array ('icon-image' , 'icon-font') ),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Wrap Icon & Image: Border Color', 'evockans'),
				'param_name' => 'icon_border_color',
				'value' => '',
				'dependency' => array( 'element' => 'icon_display', 'value' => array ('icon-image' , 'icon-font') ),
            ),       
			// Heading
            array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading',
				'value' => 'Advanced Package',
				'group' => esc_html__( 'Heading', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Heading Color', 'evockans'),
				'param_name' => 'heading_color',
				'value' => '#222222',
				'group' => esc_html__( 'Heading', 'evockans' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Heading: Padding', 'evockans' ),
				'param_name' => 'heading_padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Heading', 'evockans' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Heading: Margin', 'evockans' ),
				'param_name' => 'heading_margin',
				'value' => '0px 0px 3px 0px',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Heading', 'evockans' ),
			),
            array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Sub Heading', 'evockans'),
				'param_name' => 'sub_heading',
				'value' => 'Per Month',
				'group' => esc_html__( 'Heading', 'evockans' ),
            ),            
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Sub Heading Color', 'evockans'),
				'param_name' => 'sub_heading_color',
				'value' => '#999999',
				'group' => esc_html__( 'Heading', 'evockans' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Sub Heading: Padding', 'evockans' ),
				'param_name' => 'sub_heading_padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Heading', 'evockans' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Sub Heading: Margin', 'evockans' ),
				'param_name' => 'sub_heading_margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Heading', 'evockans' ),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Heading Background Color', 'evockans'),
				'param_name' => 'heading_background_color',
				'value' => '',
				'group' => esc_html__( 'Heading', 'evockans' ),
            ),
			// Price
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Currency', 'evockans'),
				'param_name' => 'currency',
				'value' => '$',
				'description'	=> esc_html__('The currency for this package.', 'evockans'),
				'group' => esc_html__( 'Price', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Currency Color', 'evockans'),
				'param_name' => 'currency_color',
				'value' => '',
				'group' => esc_html__( 'Price', 'evockans' ),
            ),
            array(
				'type' => 'number',
				'heading' => esc_html__('Currency Position Top', 'evockans'),
				'param_name' => 'currency_position_top',
				'value' => 12,
				'suffix' => 'px',
				'group' => esc_html__( 'Price', 'evockans' ),
		  	),
		  	array(
				'type' => 'number',
				'heading' => esc_html__('Currency Position Lett', 'evockans'),
				'param_name' => 'currency_position_left',
				'value' => 0,
				'suffix' => 'px',
				'group' => esc_html__( 'Price', 'evockans' ),
		  	),
            array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Price', 'evockans'),
				'param_name' => 'price',
				'value' => '179',
				'description'	=> esc_html__('The price for this package.', 'evockans'),
				'group' => esc_html__( 'Price', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Price Color', 'evockans'),
				'param_name' => 'price_color',
				'value' => '',
				'group' => esc_html__( 'Price', 'evockans' ),
            ),
            //Unit
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Price Unit', 'evockans' ),
				'param_name' => 'price_unit',
				'value' => '/Month',
				'description'	=> esc_html__('The price unit for this package.', 'evockans'),
				'group' => esc_html__( 'Price', 'evockans' ), 
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Price Unit Color', 'evockans'),
				'param_name' => 'unit_color',
				'value' => '',
				'group' => esc_html__( 'Price', 'evockans' ),
            ),
            array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Price Unit Block', 'evockans' ),
				'param_name' => 'price_unit_block',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Price', 'evockans' ),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Price Unit Background Color', 'evockans'),
				'param_name' => 'unit_bg_color',
				'value' => '#222222',
				'group' => esc_html__( 'Price', 'evockans' ),
				'dependency' => array( 'element' => 'price_unit_block', 'value' => 'yes' ),
            ),
            //Price Wrap
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Price Wrap: Background', 'evockans'),
				'param_name' => 'price_background',
				'value' => '',
				'group' => esc_html__( 'Price', 'evockans' ),
            ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Price Wrap: Padding', 'evockans' ),
				'param_name' => 'price_padding',
				'value' => '0px 0px 24px 0px',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Price', 'evockans' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Price Wrap: Margin', 'evockans' ),
				'param_name' => 'price_margin',
				'value' => '23px 0px 23px 0px',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Price', 'evockans' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Price Wrap: Border Width', 'evockans'),
				'param_name' => 'price_border_width',
				'value' => '0px 0px 1px 0px',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
				'group' => esc_html__( 'Price', 'evockans' ),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Price Wrap: Border Color', 'evockans'),
				'param_name' => 'price_border_color',
				'value' => 'rgba(0, 0, 0, .1)',
				'group' => esc_html__( 'Price', 'evockans' ),
            ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Content Wrap: Alignment', 'evockans' ),
				'param_name' => 'content_alignment',
				'value'      => array(
					'Left' => 'text-left',
					'Center' => 'text-center',
					'Right' => 'text-right',					
				),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Content Wrap: Background', 'evockans'),
				'param_name' => 'content_background',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Content Color', 'evockans'),
				'param_name' => 'content_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Content Wrap: Padding', 'evockans' ),
				'param_name' => 'content_padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Price Wrap: Border Width', 'evockans'),
				'param_name' => 'content_border_width',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
				'group' => esc_html__( 'Content', 'evockans' ),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Price Wrap: Border Color', 'evockans'),
				'param_name' => 'content_border_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
			// Button
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Style', 'evockans' ),
				'param_name' => 'button_style',
				'value'      => array(
					'Accent' => 'accent',
					'Dark' => 'dark',
					'Light' => 'light',
					'Very Light' => 'very-light',
					'White' => 'white',
					'Outline' => 'outline',
					'Outline Dark' => 'outline_dark',
					'Outline Light' => 'outline_light',
					'Outline Very light' => 'outline_very-light',
					'Outline White' => 'outline_white',
					'Custom' => 'custom',
				),
				'std'		=> '',
				'group' => esc_html__( 'Button', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Size', 'evockans' ),
				'param_name' => 'button_size',
				'value'      => array(
					'Default' => '',
					'Small' => 'small',
					'Very Small' => 'xsmall',
					'Big' => 'big',
				),
				'std'		=> '',
				'group' => esc_html__( 'Button', 'evockans' ),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Color', 'evockans'),
				'param_name' => 'button_color',
				'value' => '#ffffff',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'button_style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Hover Color', 'evockans'),
				'param_name' => 'button_color_hover',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'button_style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Background Color', 'evockans'),
				'param_name' => 'button_background_color',
				'value' => '#2387ea',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'button_style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Hover Background Color', 'evockans'),
				'param_name' => 'button_background_color_hover',
				'value' => '#222222',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'button_style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Border Width', 'evockans'),
				'param_name' => 'button_border_width',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'button_style', 'value' => 'custom' ),
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Border Color', 'evockans'),
				'param_name' => 'button_border_color',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'button_style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Hover Border Color', 'evockans'),
				'param_name' => 'button_border_color_hover',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'button_style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Rounded', 'evockans'),
				'param_name' => 'btn_rounded',
				'value' => '',
				'description'	=> esc_html__('Ex: 10px', 'evockans'),
				'group' => esc_html__( 'Button', 'evockans' ),
            ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Button: Padding', 'evockans' ),
				'param_name' => 'btn_padding',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Button', 'evockans' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Button: Margin', 'evockans' ),
				'param_name' => 'btn_margin',
				'value' => '20px 0px 0px 0px',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Button', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Text', 'evockans'),
				'param_name' => 'button_text',
				'value' => 'Purchase Now',
				'group' => esc_html__( 'Button', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Link (URL)', 'evockans'),
				'param_name' => 'button_url',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
	        ),
	        array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Full-width Button?', 'evockans' ),
				'param_name' => 'full_width',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Button', 'evockans' ),
			),
	        // Typography
	        //Heading
			array(
				'type' => 'headings',
				'text' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Font Family', 'evockans' ),
				'param_name' => 'heading_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Font Weight', 'evockans' ),
				'param_name' => 'heading_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Font Size', 'evockans'),
				'param_name' => 'heading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Line-Height', 'evockans'),
				'param_name' => 'heading_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        //Sub Heading
			array(
				'type' => 'headings',
				'text' => esc_html__('Sub Heading', 'evockans'),
				'param_name' => 'sub_heading_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Sub Heading Font Family', 'evockans' ),
				'param_name' => 'sub_heading_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Sub Heading Font Weight', 'evockans' ),
				'param_name' => 'sub_heading_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Sub Heading Font Size', 'evockans'),
				'param_name' => 'sub_heading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Sub Heading Line-Height', 'evockans'),
				'param_name' => 'sub_heading_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        //Currency
	        array(
				'type' => 'headings',
				'text' => esc_html__('Currency', 'evockans'),
				'param_name' => 'currency_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Currency Font Family', 'evockans' ),
				'param_name' => 'currency_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Currency Font Weight', 'evockans' ),
				'param_name' => 'currency_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Currency Font Size', 'evockans'),
				'param_name' => 'currency_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Currency Line-Height', 'evockans'),
				'param_name' => 'currency_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        //Price
			array(
				'type' => 'headings',
				'text' => esc_html__('Price', 'evockans'),
				'param_name' => 'price_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Price Font Family', 'evockans' ),
				'param_name' => 'price_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Price Font Weight', 'evockans' ),
				'param_name' => 'price_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Price Font Size', 'evockans'),
				'param_name' => 'price_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Price Line-Height', 'evockans'),
				'param_name' => 'price_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        //Unit
			array(
				'type' => 'headings',
				'text' => esc_html__('Unit', 'evockans'),
				'param_name' => 'unit_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Unit Font Family', 'evockans' ),
				'param_name' => 'unit_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Unit Font Weight', 'evockans' ),
				'param_name' => 'unit_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Unit Font Size', 'evockans'),
				'param_name' => 'unit_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Unit Line-Height', 'evockans'),
				'param_name' => 'unit_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        //Button
			array(
				'type' => 'headings',
				'text' => esc_html__('Button', 'evockans'),
				'param_name' => 'button_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Font Family', 'evockans' ),
				'param_name' => 'button_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Font Weight', 'evockans' ),
				'param_name' => 'button_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Font Size', 'evockans'),
				'param_name' => 'button_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Line-Height', 'evockans'),
				'param_name' => 'button_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
		)
	) );
}

add_shortcode( 'pricetable', 'themesflat_shortcode_pricetable' );

/**
 * pricetable shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_pricetable( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'price_table_style' => 'style-1',
		'active' => '',
		'style_active' => 'active-style-1',
		'alignment' => 'text-center',
		'background_color' => '#ffffff',
		'padding' => '40px 40px 40px 40px',
		'border_width' => '',
		'border_color' => '',
		'rounded' => '10',
		'horizontal' => '0px',
        'vertical' => '0px',
        'blur' => '57px',
        'spread' => '0px',
        'shadow_color' => 'rgba(0,0,0,.08)',
        'icon_display' => 'icon-image',
		'image' => '',
		'image_width' => '80',
		'image_rounded' => '',
		'icon_type' => '',
		'icon' => '',
		'icon_color' => '',
		'icon_width' => '80',
		'icon_font_size' => '50px',
		'icon_padding' => '25px 0px',
		'icon_margin' => '',
		'icon_border_width' => '',
		'icon_border_color' => '',
		'heading' => 'Advanced Package',		
		'heading_color' => '#222222',
		'heading_padding' => '',
		'heading_margin' => '0px 0px 3px 0px',
		'sub_heading' => 'Per Month',
		'sub_heading_color' => '#999999',
		'sub_heading_padding' => '',
		'sub_heading_margin' => '',
		'heading_background_color' => '',
		'currency' => '$',
		'currency_color' => '',
		'currency_position_top' => 12,
		'currency_position_left' => 0,
		'price' => '179',
		'price_color' => '',
		'price_unit' => '/Month',
		'unit_color' => '',
		'price_unit_block' => '',
		'unit_bg_color' => '#222222',
		'price_padding' => '0px 0px 24px 0px',
		'price_margin' => '23px 0px 23px 0px',
		'price_border_width' => '0px 0px 1px 0px',
		'price_border_color' => 'rgba(0, 0, 0, .1)',
		'price_background' => '',		
		'content_background' => '',
		'content_alignment' => 'text-left',
		'content_color' => '',
		'content_padding' => '',
		'content_border_width' => '',
		'content_border_color' => '',
		'btn_padding' => '',
		'btn_margin' => '20px 0px 0px 0px',
		'button_style' => 'accent',
		'button_size' => '',
		'button_color' => '#ffffff',
		'button_color_hover' => '',
		'button_background_color' => '#2387ea',
		'button_background_color_hover' => '#222222',
		'button_border_width' => '',
		'button_border_color' => '',
		'button_border_color_hover' => '',
		'btn_rounded' => '',
		'button_text' => 'Purchase Now',
		'button_url' => '',
		'full_width' => '',
		'heading_font_family' => 'Default',
		'heading_font_weight' => 'Default',
		'heading_font_size' => '',
		'heading_line_height' => '',
		'sub_heading_font_family' => 'Default',
		'sub_heading_font_weight' => 'Default',
		'sub_heading_font_size' => '',
		'sub_heading_line_height' => '',
		'currency_font_family' => 'Default',
		'currency_font_weight' => 'Default',
		'currency_font_size' => '',
		'currency_line_height' => '',
		'price_font_family' => 'Default',
		'price_font_weight' => 'Default',
		'price_font_size' => '',
		'price_line_height' => '',
		'unit_font_family' => 'Default',
		'unit_font_weight' => 'Default',
		'unit_font_size' => '',
		'unit_line_height' => '',
		'button_font_family' => 'Default',
		'button_font_weight' => 'Default',
		'button_font_size' => '',
		'button_line_height' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'pricetable', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$rounded = intval( $rounded );
	
	$heading_font_size = intval( $heading_font_size );
	$sub_heading_font_size = intval( $sub_heading_font_size );

	$price_font_size = intval( $price_font_size );
	$unit_font_size = intval( $unit_font_size );
	$btn_rounded = intval( $btn_rounded );
	$icon_width = intval( $icon_width );
	$icon_font_size = intval( $icon_font_size );

	$cls = $css = $btn_css = $btn_css_js = $h1_css = $h2_css = $p_css = $c_css = $pu_css = '';
	$heading_cls = $heading_css = $price_css = $content_css = $button_css = $button_cls = '';
	$h_html = $p_html = $b_html = '';
	$h1_html = $p1_html = $p2_html = $p3_html = $h2_html = '';
	$icon_css = $icon_line_height = $image_css = $wrap_icon_img_css = $icon_html = $image_css_rounded = '';

	$cls .= ' ' . $alignment;  

	if ( $price_unit_block == 'yes') {
		$cls .= ' term-block';
		if ( $unit_bg_color ) $pu_css .= 'background-color:'. $unit_bg_color .';';
	}

	if ($active) $cls .= ' ' . $style_active;  

	//Wrap
	if ( $border_color && $border_width ) $css .= 'border-style:solid;border-width:'. $border_width .';border-color:'. $border_color .';';
	if ( $rounded ) $css .= 'border-radius:'. $rounded .'px;';
	if ( $horizontal && $vertical && $blur && $spread && $shadow_color )
    	$css .= 'box-shadow:'. $horizontal .' '. $vertical .' '. $blur .' '. $spread .' '. $shadow_color .';';
    if ( $padding ) $css .= 'padding:'. $padding .';';
    if ( $background_color ) $css .= 'background-color:'. $background_color .';';

    if ($heading_background_color) $heading_css .= 'background-color:'. $heading_background_color .';';
    //heading
	if ( $heading_font_weight != 'Default' ) $h1_css .= 'font-weight:'. $heading_font_weight .';';
	if ( $heading_color ) $h1_css .= 'color:'. $heading_color .';';
	if ( $heading_font_size ) $h1_css .= 'font-size:'. $heading_font_size .'px;';
	if ( $heading_line_height ) $h1_css .= 'line-height:'. $heading_line_height .';';
	if ( $heading_font_family != 'Default' ) {
		$h1_css .= 'font-family:'. $heading_font_family .';';
	}
	if ( $heading_padding ) $h1_css .= 'padding:'. $heading_padding .';';
	if ( $heading_margin ) $h1_css .= 'margin:'. $heading_margin .';';	

	//sub_heading
	if ( $sub_heading_font_weight != 'Default' ) $h2_css .= 'font-weight:'. $sub_heading_font_weight .';';
	if ( $sub_heading_color ) $h2_css .= 'color:'. $sub_heading_color .';';
	if ( $sub_heading_font_size ) $h2_css .= 'font-size:'. $sub_heading_font_size .'px;';
	if ( $sub_heading_line_height ) $h2_css .= 'line-height:'. $sub_heading_line_height .';';
	if ( $sub_heading_font_family != 'Default' ) {
		$h2_css .= 'font-family:'. $sub_heading_font_family .';';
	}
	if ( $sub_heading_padding ) $h2_css .= 'padding:'. $sub_heading_padding .';';
	if ( $sub_heading_margin ) $h2_css .= 'margin:'. $sub_heading_margin .';';

	//currency
	if ( $currency_color ) $c_css .= 'color:' . $currency_color . ';';
	if ( $currency_position_top != '' ) $c_css .= 'top:' . $currency_position_top . 'px;';
	if ( $currency_position_left != '' ) $c_css .= 'left:' . $currency_position_left . 'px;';
	if ( $currency_font_weight != 'Default' ) $c_css .= 'font-weight:'. $currency_font_weight .';';
	if ( $currency_font_size ) $c_css .= 'font-size:'. $currency_font_size .'px;';
	if ( $currency_line_height ) $c_css .= 'line-height:'. $currency_line_height .';';
	if ( $currency_font_family != 'Default' ) {
		$c_css .= 'font-family:'. $currency_font_family .';';
	}

	//price
	if ( $price_font_weight != 'Default' ) $p_css .= 'font-weight:'. $price_font_weight .';';
	if ( $price_font_size ) $p_css .= 'font-size:'. $price_font_size .'px;';
	if ( $price_line_height ) $p_css .= 'line-height:'. $price_line_height .';';
	if ( $price_font_family != 'Default' ) {
		$p_css .= 'font-family:'. $price_font_family .';';
	}

	//unit
	if ( $unit_font_weight != 'Default' ) $pu_css .= 'font-weight:'. $unit_font_weight .';';
	if ( $unit_color ) $pu_css .= 'color:'. $unit_color .';';
	if ( $unit_font_size ) $pu_css .= 'font-size:'. $unit_font_size .'px;';
	if ( $unit_line_height ) $pu_css .= 'line-height:'. $unit_line_height .';';
	if ( $unit_font_family != 'Default' ) {
		$pu_css .= 'font-family:'. $unit_font_family .';';
	}

	//price
	if ( $price_padding ) $price_css .= 'padding:'. $price_padding .';';
	if ( $price_margin ) $price_css .= 'margin:'. $price_margin .';';
	if ( $price_border_color && $price_border_width ) $price_css .= 'border-style:solid;border-width:'. $price_border_width .';border-color:'. $price_border_color .';';
	if ( $price_color ) $p_css .= 'color:'. $price_color .';';
	if ( $price_background ) $price_css .= 'background-color:'. $price_background .';';

	//content
	if ( $content_padding ) $content_css .= 'padding:'. $content_padding .';';
	if ( $content_background ) $content_css .= 'background-color:'. $content_background .';';
	if ( $content_color ) $content_css .= 'color:'. $content_color .';';
	if ( $content_border_color && $content_border_width ) $content_css .= 'border-style:solid;border-width:'. $content_border_width .';border-color:'. $content_border_color .';';

	//button
	if ( $btn_padding ) $button_css .= 'padding:'. $btn_padding .';';
	if ( $btn_margin ) $button_css .= 'margin:'. $btn_margin .';';
	if ( $btn_rounded ) $button_css .= 'border-radius:'. $btn_rounded .'px;';
	if ( $full_width ) $button_css .= 'display:block; text-align:center;';
	if ( $button_background_color ) $button_css .= 'background-color:'. $button_background_color .';';
	if ( $button_color ) $button_css .= 'color:'. $button_color .';';
	if ( $button_border_color && $button_border_width ) $button_css .= 'border-style:solid;border-width:'. $button_border_width .';border-color:'. $button_border_color .';';
	if ( $button_font_weight != 'Default' ) $button_css .= 'font-weight:'. $button_font_weight .';';
	if ( $button_font_size ) $button_css .= 'font-size:'. $button_font_size .'px;';
	if ( $button_line_height ) $button_css .= 'line-height:'. $button_line_height .';';
	if ( $button_font_family != 'Default' ) { 
		$button_css .= 'font-family:'. $button_font_family .';'; 
	}

	if ( $sub_heading ) {
		$h2_html .= sprintf( '<div class="sub-title" style="%2$s">%1$s</div>', $sub_heading, $h2_css );
	}

	if ( $heading ) {
		$h1_html .= sprintf( '<div class="title %3$s" style="%2$s">%1$s</div>', $heading, $h1_css, $heading_cls );
		$h_html .= sprintf(
			'<div class="price-table-name" style="%3$s">
				%1$s
				%2$s
			</div>',
			$h1_html,
			$h2_html,
			$heading_css
		);
	}

		
	if ( $currency )  $p3_html .= sprintf( '<span class="currency" style="%2$s">%1$s</span>', $currency, $c_css );
	if ( $price ) $p1_html .= sprintf( '<span class="price" style="%2$s">%1$s</span>', $price, $p_css );
	if ( $price_unit ) $p2_html .= sprintf( '<span class="term" style="%2$s">%1$s</span>', $price_unit, $pu_css );
	if ( $price || $price_unit )
		$p_html .= sprintf(
			'<div class="price-table-price" style="%3$s">
				<div class="price-wrap">%4$s %1$s %2$s</div>
			</div>',
			$p1_html,
			$p2_html,
			$price_css,
			$p3_html
		);

	if ( $button_text ) {
		$button_cls = $button_size;
	    if ( $button_style == 'accent' ) $button_cls .= ' themesflat_sc_vc-button accent';
	    if ( $button_style == 'dark' ) $button_cls .= ' themesflat_sc_vc-button dark';
	    if ( $button_style == 'light' ) $button_cls .= ' themesflat_sc_vc-button light';
	    if ( $button_style == 'very-light' ) $button_cls .= ' themesflat_sc_vc-button very-light';
	    if ( $button_style == 'white' ) $button_cls .= ' themesflat_sc_vc-button white';
	    if ( $button_style == 'outline' ) $button_cls .= ' themesflat_sc_vc-button outline ol-accent';
	    if ( $button_style == 'outline_dark' ) $button_cls .= ' themesflat_sc_vc-button outline dark';
	    if ( $button_style == 'outline_light' ) $button_cls .= ' themesflat_sc_vc-button outline light';
	    if ( $button_style == 'outline_very-light' ) $button_cls .= ' themesflat_sc_vc-button outline very-light';
	    if ( $button_style == 'outline_white' ) $button_cls .= '  themesflat_sc_vc-button outline white';  	

	    if ( $button_style == 'custom' ) $button_cls .= ' themesflat_sc_vc-button custom';

	    if ( $button_background_color_hover ) $btn_css_js .= 'data-btn_bgcolor_hover="'. $button_background_color_hover .'" ';
		if ( $button_background_color ) $btn_css_js .= 'data-btn_bgcolor="'. $button_background_color .'" ';
		if ( $button_color ) $btn_css_js .= 'data-btn_color="'. $button_color .'" ';
		if ( $button_color_hover ) $btn_css_js .= 'data-btn_color_hover="'. $button_color_hover .'" ';
		if ( $button_border_color_hover ) $btn_css_js .= 'data-btn_bordercolor_hover="'. $button_border_color_hover .'" ';
		if ( $button_border_color ) $btn_css_js .= 'data-btn_bordercolor="'. $button_border_color .'" ';   

	    $b_html .= sprintf(
	    	'<div class="price-table-button" style="%1$s">
	    		<a target="_blank" class="%2$s" href="%3$s" style="%4$s" %6$s>%5$s</a>
	    	</div>',
	    	$btn_css,
	    	$button_cls,
	    	$button_url,
	    	$button_css,
	    	$button_text,
	    	$btn_css_js
	    );
	}

	//Icon
	if ( $icon_border_color && $icon_border_width ) $wrap_icon_img_css .= 'border-style:solid;border-width:'. $icon_border_width .';border-color:'. $icon_border_color .';';
	if ( $icon_padding ) $wrap_icon_img_css .= 'padding:'. $icon_padding .';';
	if ( $icon_margin ) $wrap_icon_img_css .= 'margin:'. $icon_margin .';';	

	if ( $icon_display == 'icon-font' ) {
		$icon = themesflat_sc_vc_get_icon_class( $atts, 'icon' );
		if ( $icon && $icon_type != '' ) {
			vc_icon_element_fonts_enqueue( $icon_type );
				
			if ( $icon_font_size ) $icon_css .= 'font-size:'. $icon_font_size .'px;';
			if ( $icon_color ) $icon_css .= 'color:'. $icon_color .';';

			if ($icon_width) {
				$icon_line_height = $icon_width;
				$icon_css .= 'line-height:'. $icon_line_height .'px;';
				$icon_css .= 'width:'. $icon_width .'px;';
				$icon_css .= 'height:'. $icon_width .'px;';				
			}		

			$icon_html = sprintf(
				'<div class="icon-wrap" style="%2$s">
					<i class="%1$s"></i>
				</div>',
				$icon,
				$icon_css
			);
		}
	} else {
		if ( $image_width ) $image_css = 'width:'. $image_width .'px;';
		if ( $image_rounded ) $image_css_rounded = 'border-radius:'. $image_rounded .'px;';

		if ( $image ){
			$icon_html = sprintf(
				'<div class="image-wrap" style="%2$s">
					<img alt="image" src="%1$s" style="%3$s">
				</div>',
				wp_get_attachment_image_src( $image, 'full' )[0],
				$image_css,
				$image_css_rounded
			);
			
		}
	}

	if ( $price_table_style == 'style-1' ) {
		return sprintf(
			'<div class="themesflat_sc_vc-price-table %1$s" style="%2$s">
				%5$s %6$s
				<div class="price-table-content %8$s" style="%3$s">%4$s</div>
				%7$s
			</div>',
			$cls,
			$css,
			$content_css,
			do_shortcode($content),
			$h_html,
			$p_html,
			$b_html,
			$content_alignment
		);
	} else if ( $price_table_style == 'style-2' ) {
		return sprintf(
			'<div class="themesflat_sc_vc-price-table %1$s" style="%2$s">
				%5$s 
				<div class="price-table-icon" style="%10$s">%9$s</div>
				<div class="price-table-content %8$s" style="%3$s">%4$s</div>
				%6$s
				%7$s
			</div>',
			$cls,
			$css,
			$content_css,
			do_shortcode($content),
			$h_html,
			$p_html,
			$b_html,
			$content_alignment,
			$icon_html,
			$wrap_icon_img_css
		);
	} else if ( $price_table_style == 'style-3' ) {
		return sprintf(
			'<div class="themesflat_sc_vc-price-table %1$s" style="%2$s">
				<div class="price-table-icon" style="%10$s">%9$s</div>
				%5$s %6$s
				<div class="price-table-content %8$s" style="%3$s">%4$s</div>				
				%7$s
			</div>',
			$cls,
			$css,
			$content_css,
			do_shortcode($content),
			$h_html,
			$p_html,
			$b_html,
			$content_alignment,
			$icon_html,
			$wrap_icon_img_css
		);
	}

	
}