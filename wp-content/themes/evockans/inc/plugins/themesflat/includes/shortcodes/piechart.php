<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_piechart_shortcode_params' );

function themesflat_piechart_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Pie Chart', 'evockans'),
        'description' => esc_html__('Displaying Pie Chart.', 'evockans'),
        'base' => 'piechart',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'params' => array(
	        array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Percent', 'evockans'),
				'param_name' => 'percent',
				'value' => '70',
				'group' => esc_html__( 'Pie Chart', 'evockans' ),
	        ),	                   
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Width', 'evockans'),
				'param_name' => 'width',
				'value' => '7',
				'group' => esc_html__( 'Pie Chart', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Size', 'evockans'),
				'param_name' => 'size',
				'value' => '150',
				'group' => esc_html__( 'Pie Chart', 'evockans' ),
	        ),	 
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Percent Color', 'evockans'),
				'param_name' => 'color',
				'value' => '#2387ea',
				'group' => esc_html__( 'Pie Chart', 'evockans' ),
            ), 
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Track Color', 'evockans'),
				'param_name' => 'track_color',
				'value' => 'rgba(0,0,0,0.1)',
				'group' => esc_html__( 'Pie Chart', 'evockans' ),
            ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Percent Text Color', 'evockans'),
				'param_name' => 'percent_color',
				'value' => '#222222',
				'group' => esc_html__( 'Pie Chart', 'evockans' ),
            ),   
            //Spacing                	
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Padding', 'evockans'),
				'param_name' => 'padding',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Margin', 'evockans'),
				'param_name' => 'margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),	
	        // Typography
	        array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Family', 'evockans' ),
				'param_name' => 'font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Weight', 'evockans' ),
				'param_name' => 'font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Font Size', 'evockans'),
				'param_name' => 'font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),               
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Extra class name', 'evockans'),
				'param_name' => 'class',
				'value' => '',
				'group' => esc_html__( 'Class', 'evockans' ),
	        ),
        )
    ) );
}

add_shortcode( 'piechart', 'themesflat_shortcode_piechart' );

/**
 * piechart shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_piechart( $atts, $content = null ) {
	extract( shortcode_atts( array(
	    'percent' => '70',
	    'percent_color' => '#222222',
	    'track_color' => 'rgba(0,0,0,0.1)',
	    'width' => '7',
	    'size' => '150',
	    'color' => '#2387ea',
	    'margin' => '',
	    'padding' => '',
	    'font_family' => 'Default',
		'font_weight' => 'Default',
		'font_size' => '',
	    'class' => '',
	), $atts ) );	
	//extract($atts = vc_map_get_attributes( 'piechart', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$cls = $css = $piechart_html = $percent_css = '';

	$percent = intval( $percent );
	$width = intval( $width );
	$size = intval( $size );
	$font_size = intval( $font_size );

	if ( $class ) $cls .= ' ' . $class;

	if ( $percent == '' ) { $percent = 100; }
	if ( $width == '' ) { $width = 7; }
	if ( $size == '' ) { $size = 150; }

	if ( $padding ) $css .= 'padding:' . $padding . ';';
	if ( $margin ) $css .= 'margin:' . $margin . ';';
	if ( $percent_color ) $percent_css .= 'color:' . $percent_color . ';';

	if ( $font_family != 'Default' ) $percent_css .= 'font-family:'. $font_family .';';
	if ( $font_weight != 'Default' ) $percent_css .= 'font-weight:'. $font_weight .';';
	if ( $font_size ) $percent_css .= 'font-size:'. $font_size .'px;';

	wp_enqueue_script( 'themesflat_sc_vc-piechart' );
	wp_enqueue_script( 'themesflat_sc_vc-appear' );

	$piechart_html .= sprintf('
		<div class="pie-chart">
			<div class="chart-percent">
				<span class="chart" data-percent="%1$s" data-width="%2$s" data-size="%3$s" data-color="%4$s" data-trackcolor="%5$s" style="%6$s">
					<span class="percent"></span>
				</span>
			</div>
		</div>',
		$percent,
		$width,
		$size,
		$color,
		$track_color,
		$percent_css	
		);

	
	return sprintf( '
		<div class="themesflat_sc_vc-piechart clearfix %1$s" style="%3$s">%2$s</div>',
		$cls,
		$piechart_html,
		$css
	);
}