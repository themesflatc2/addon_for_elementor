<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_advlist_shortcode_params' );

function themesflat_advlist_shortcode_params() {
	vc_map( array(
		'name'        => esc_html__( 'List Advanced', 'evockans' ),
        'description' => esc_html__('Displaying Icon lists with custom icon.', 'evockans'),
		'base'        => 'advlist',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
		'params'      => array(
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'evockans' ),
				'param_name' => 'style',
				'value'      => array(
					'Icon Left' => 'icon-left',
					'Icon Right' => 'icon-right',
				),
				'std'		=> 'icon-left',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Icon to Display', 'evockans' ),
				'param_name' => 'icon_display',
				'value'      => array(
					'Icon Font' => 'icon-font',
					'Icon Image' => 'icon-image',
				),
				'std'		=> 'icon-font',
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Background Color', 'evockans'),
				'param_name' => 'content_background_color',
				'value' => '',
            ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Border Style', 'evockans' ),
				'param_name' => 'content_border_style',
				'value'      => array(
					'Solid' => 'solid',
					'Dotted' => 'dotted',
					'Dashed' => 'dashed',
				),
				'std'		=> 'solid',
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Border Color', 'evockans'),
				'param_name' => 'content_border_color',
				'value' => '',
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Border Width', 'evockans'),
				'param_name' => 'content_border_width',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Ex: 1px 1px 1px 1px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Padding', 'evockans'),
				'param_name' => 'content_padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Bottom Margin', 'evockans'),
				'param_name' => 'content_bottom_margin',
				'value' => '10',
	        ),
	        array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Full Width', 'evockans' ),
				'param_name' => 'full_width',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'std'	=> 'yes'
			),
	        // Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image', 'evockans'),
				'param_name' => 'image',
				'value' => '',
				'group' => esc_html__( 'Image', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-image' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Image Width', 'evockans'),
				'param_name' => 'image_width',
				'value' => '',
				'description'	=> esc_html__('Ex: 50px', 'evockans'),
				'group' => esc_html__( 'Image', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-image' ),
	        ),
			// Icon
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Icon Style', 'evockans' ),
				'param_name' => 'icon_style',
				'value'      => array(
					'Simple' => 'simple',
					'Circle Accent' => 'circle-accent',
					'Circle Light' => 'circle-light',
					'Circle Dark' => 'circle-dark',
					'Square Accent' => 'square-accent',
					'Square Light' => 'square-light',
					'Square Dark' => 'square-dark',
				),
				'std'		=> 'circle',
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'evockans' ),
				'param_name' => 'icon_type',
				'description' => esc_html__( 'Select icon library.', 'evockans' ),
				'value' => array(
					esc_html__( 'None', 'evockans' ) => '',
					esc_html__( 'Mono Social', 'evockans' ) => 'monosocial',
					esc_html__( 'Simple Line', 'evockans' ) => 'simpleline',
					esc_html__( 'Font Ionicons', 'evockans' ) => 'ionicons',
					esc_html__( 'Font Elegant', 'evockans' ) => 'eleganticons',
					esc_html__( 'Font Themify Icons', 'evockans' ) => 'themifyicons',
					esc_html__( 'Font Icomoon Icons', 'evockans' ) => 'icomoon',
					esc_html__( 'FontAwesome', 'evockans' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'evockans' ) => 'openiconic',
					esc_html__( 'Typicons', 'evockans' ) => 'typicons',
					esc_html__( 'Entypo', 'evockans' ) => 'entypo',
					esc_html__( 'Linecons', 'evockans' ) => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-font' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_monosocial',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'monosocial',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'monosocial',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_simpleline',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'simpleline',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'simpleline',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_ionicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'ionicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'ionicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_eleganticons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'eleganticons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'eleganticons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_themifyicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'themifyicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'themifyicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_icomoon',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'icomoon',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'icomoon',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon',
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'fontawesome',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_openiconic',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'openiconic',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'openiconic',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_typicons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'typicons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'typicons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_entypo',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'entypo',
					'iconsPerPage' => 300,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'entypo',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_linecons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'linecons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon Color', 'evockans'),
				'param_name' => 'icon_color',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-font' ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Font Size', 'evockans'),
				'param_name' => 'icon_font_size',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-font' ),
	        ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Icon Position', 'evockans' ),
				'param_name' => 'icon_position',
				'value'      => array(
					'Middle' => 'middle',
					'Top' => 'top',
				),
				'std'		=> 'middle',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'icon_display', 'value' => 'icon-font' ),
			),
			// Content
			array(
				'type' => 'textarea',
				'holder' => 'div',
				'heading' => esc_html__( 'Content', 'evockans' ),
				'param_name' => 'content',
				'group' => esc_html__( 'Content', 'evockans' ), 
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Content Color', 'evockans'),
				'param_name' => 'content_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content Left Padding', 'evockans'),
				'param_name' => 'content_left_padding',
				'value' => '28',
				'group' => esc_html__( 'Content', 'evockans' ),
				'description'	=> esc_html__('Spacing between the icon and the content', 'evockans'),
				'dependency' => array( 'element' => 'style', 'value' => 'icon-left' ),
	        ),
	        // Hyperlink
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Enable Hyperlink on text?', 'evockans' ),
				'param_name' => 'hyperlink',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Hyperlink', 'evockans' ),
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Link (URL):', 'evockans'),
				'param_name' => 'link_url',
				'value' => '',
				'group' => esc_html__( 'Hyperlink', 'evockans' ),
				'dependency' => array( 'element' => 'hyperlink', 'value' => 'yes' ),
            ),
	        // Typography
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Content Font Family', 'evockans' ),
				'param_name' => 'content_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Content Font Weight', 'evockans' ),
				'param_name' => 'content_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content Font Size', 'evockans'),
				'param_name' => 'content_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content Line-Height', 'evockans'),
				'param_name' => 'content_line_height',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
		)
	) );
}

add_shortcode( 'advlist', 'themesflat_shortcode_advlist' );

/**
 * advlist shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_advlist( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'style' => 'icon-left',
		'icon_display' => 'icon-font',
		'image' => '',
		'image_width' => '',
		'icon_style' => 'circle-accent',
		'icon_type' => '',
		'icon' => '',
		'icon_position' => 'middle',
		'icon_color' => '',
		'icon_font_size' => '',
		'content_color' => '',
		'content_left_padding' => '28',
		'content_padding' => '',
		'content_bottom_margin' => '10',
		'full_width' => 'yes',
		'content_background_color' => '',
		'content_border_style' => 'solid',
		'content_border_color' => '',
		'content_border_width' => '',
		'hyperlink' => '',
		'link_url' => '',
		'content_font_family' => 'Default',
		'content_font_weight' => 'Default',
		'content_font_size' => '',
		'content_line_height' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'advlist', $atts ));

	$icon_font_size = intval( $icon_font_size );
	$content_left_padding = intval( $content_left_padding );
	$content_font_size = intval( $content_font_size );
	$content_line_height = intval( $content_line_height );
	$content_bottom_margin = intval( $content_bottom_margin );
	$image_width = intval( $image_width );

	$css = $list_css = $inner_css = $inner_content_css = $icon_cls = $icon_css = $link_css = '';
	$icon_html = $content_html = $image_css = '';

	$cls = $style .' icon-'. $icon_position;
	$icon_cls = $icon_style;

	if ( $content_background_color ) $cls .= ' has-content-background' ;
	if ( $content_border_color || $content_border_width ) $cls .= ' has-content-border' ;

	if ( $content_font_weight != 'Default' ) $inner_content_css .= 'font-weight:'. $content_font_weight .';';
	if ( $content_font_size ) $inner_content_css .= 'font-size:'. $content_font_size .'px;';
	if ( $content_line_height ) $inner_content_css .= 'line-height:'. $content_line_height .'px;';
	if ( $content_font_family != 'Default' ) {
		$inner_content_css .= 'font-family:'. $content_font_family .';';
	}

	if ( $style == 'icon-left' && isset( $content_left_padding ) )
		$inner_css .= 'padding-left:'. $content_left_padding .'px;';
	if ( $content_color ) {
		$inner_css .= 'color:'. $content_color;
		$link_css .= 'color:'. $content_color;
	}



	if ( $content_padding ) $list_css .= 'padding:'. $content_padding .';';
	if ( $content_bottom_margin ) $list_css .= 'margin-bottom:'. $content_bottom_margin .'px;';

	if ( $content_background_color ) $list_css .= 'background-color:'. $content_background_color .';';
	if ( $content_border_color ) $list_css .= 'border-color:'. $content_border_color .';';
	if ( $content_border_width ) $list_css .= 'border-style: solid; border-width:'. $content_border_width .';border-style:'. $content_border_style .';';

	if ( $icon_display == 'icon-font' ) {
		vc_icon_element_fonts_enqueue( $icon_type );
		$icon = themesflat_sc_vc_get_icon_class( $atts, 'icon' );

		if ( $icon_color == '#1a7dd7' ) {
			$icon_cls .= ' accent';
		} else {
			if ( $icon_color ) $icon_css .= 'color:'. $icon_color .';';
		}
		
		if ( $icon_font_size ) $icon_css .= 'font-size:'. $icon_font_size .'px;';

		$icon_html = sprintf(
			'<span class="icon %3$s" style="%2$s">
				<i class="%1$s"></i>
			</span>',
			$icon,
			$icon_css,
			$icon_cls
		);
	} else {
		if ( $image_width ) $image_css = 'width:'. $image_width .'px;';

		if ( $image )
		$icon_html = sprintf(
			'<span class="image" style="%2$s">
				<img alt="image" src="%1$s">
			</span>',
			wp_get_attachment_image_src( $image, 'full' )[0],
			$image_css
		);
	}

	if ( $content )
		$content_html = sprintf(
			'<span style="%2$s">%1$s</span>',
			$content,
			$inner_content_css
		);

	if ( $hyperlink && $link_url ) {
		$icon_html = sprintf( '<a target="_blank" href="%2$s">%1$s</a>', $icon_html, $link_url );
		$content_html = sprintf( '<a target="_blank" href="%2$s" style="%3$s">%1$s</a>', $content_html, $link_url, $link_css );
	}

	if( $full_width != 'yes' ){
		$css = 'display: inline-block;';
	}

	return sprintf(
		'<div class="themesflat_sc_vc-list clearfix %1$s" style="%6$s">
			<div style="%3$s">
				<span style="%2$s">%4$s %5$s</span>
			</div>
		</div>',
		esc_attr( $cls ),
		$inner_css,
		$list_css,
		$icon_html,
		$content_html,
		$css
	);
}