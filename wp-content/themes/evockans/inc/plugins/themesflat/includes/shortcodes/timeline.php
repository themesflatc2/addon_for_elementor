<?php
/**
 * Register filter for append custom class name
 * that generated from visual-composer
 */
class WPBakeryShortCode_timeline extends WPBakeryShortCodesContainer {}
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_timeline_shortcode_params' );

function themesflat_timeline_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Time Line', 'evockans'),
        'description' => esc_html__('Displaying Time Line.', 'evockans'),
        'base' => 'timeline',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'as_parent' => array( 'only' => 'inner_timeline' ),
		'controls' => 'full',
		'show_settings_on_create' => true,		
		'js_view' => 'VcColumnView',
        'params' => array(
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Extra class name', 'evockans'),
				'param_name' => 'class',
				'value' => '',
				'group' => esc_html__( 'Class', 'evockans' ),
	        ),
        )
    ) );
}

add_shortcode( 'timeline', 'themesflat_shortcode_timeline' );

/**
 * timeline shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_timeline( $atts, $content = null ) {
	extract( shortcode_atts( array(
	    'class' => '',
	), $atts ) );	
	//extract($atts = vc_map_get_attributes( 'timeline', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	return sprintf( '
		<div class="themesflat_sc_vc-timeline %1$s clearfix">%2$s</div>',
		$class,
		do_shortcode($content)
	);
}