<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_testimonials_shortcode_params' );

function themesflat_testimonials_shortcode_params() {
	vc_map( array(
	    'name' => esc_html__('Testimonials', 'evockans'),
	    'description' => esc_html__('Displaying testimonials box.', 'evockans'),
	    'base' => 'testimonials',
		'weight'	=>	180,
	    'icon' => THEMESFLAT_ICON,
	    'category' => esc_html__('THEMESFLAT', 'evockans'),
	    'params' => array(
	    	array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'evockans' ),
				'param_name' => 'style',
				'value'      => array(
					'Style 1' => 'style-1',
					'Style 2' => 'style-2',
					'Style 3' => 'style-3',
					'Style 4' => 'style-4',
					'Style 5' => 'style-5',
				),
				'std'		=> 'style-2',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Align', 'evockans' ),
				'param_name' => 'align',
				'value' => array(
					'Left' => '',
					'Center' => 'text-center',
				),
				'std'		=> '',
			),
	        // Image
			array(
				'type' => 'attach_image',
				'holder' => 'img',
				'heading' => esc_html__('Image', 'evockans'),
				'param_name' => 'image',
				'value' => '',
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Image Width (Optional)', 'evockans'),
				'param_name' => 'image_width',
				'value' => '',
				'description'	=> esc_html__('Default: 70px.', 'evockans'),
	        ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Image Style', 'evockans' ),
				'param_name' => 'image_style',
				'value'      => array(
					'Square' => 'image-square',
					'Circle' => 'image-circle',
				),
				'std'		=> 'image-circle',
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Wrap: Background', 'evockans'),
				'param_name' => 'text_background',
				'value' => '',
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap: Padding', 'evockans'),
				'param_name' => 'text_padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap: Margin', 'evockans'),
				'param_name' => 'wrap_text_margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
	        ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap: Rounded', 'evockans'),
				'param_name' => 'text_rounded',
				'value' => '',
				'description'	=> esc_html__('Ex: 6px', 'evockans'),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap: Border Width', 'evockans'),
				'param_name' => 'text_border_width',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Wrap: Border Color', 'evockans'),
				'param_name' => 'text_border_color',
				'value' => '',
            ),
	        // Text
			array(
				'type' 		=> 'textarea',
				'heading' 	=> esc_html__('Text', 'evockans'),
				'param_name' 	=> 'text',
				'value' 		=> '',
				'group' => esc_html__( 'Text', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Text Color', 'evockans'),
				'param_name' => 'text_color',
				'value' => '',
				'group' => esc_html__( 'Text', 'evockans' ),
            ),
            // Name
            array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Name', 'evockans'),
				'param_name' => 'name',
				'value' => 'Bruno Strong',
				'group' => esc_html__( 'Name & Position', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Name Color', 'evockans'),
				'param_name' => 'name_color',
				'value' => '',
				'group' => esc_html__( 'Name & Position', 'evockans' ),
            ),
            // Position
            array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Position', 'evockans'),
				'param_name' => 'position',
				'value' => 'Envato User',
				'group' => esc_html__( 'Name & Position', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Position Color', 'evockans'),
				'param_name' => 'position_color',
				'value' => '',
				'group' => esc_html__( 'Name & Position', 'evockans' ),
            ),
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Name & Position Inline?', 'evockans' ),
				'param_name' => 'inline',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'description'	=> esc_html__('Displaying Name & Position Inline', 'evockans'),
				'group' => esc_html__( 'Name & Position', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Name & Position: Space Between', 'evockans'),
				'param_name' => 'gap',
				'value' => '5px',
				'group' => esc_html__( 'Name & Position', 'evockans' ),
				'dependency' => array( 'element' => 'inline', 'value' => 'yes' ),
	        ),
	        // Rating
	        array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Display Title Rating', 'evockans' ),
				'param_name' => 'display_title_rating',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Rating', 'evockans' ),				
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Title Rating', 'evockans'),
				'param_name' => 'title_rating',
				'value' => '',
				'group' => esc_html__( 'Rating', 'evockans' ),
				'dependency' => array( 'element' => 'display_title_rating', 'value' => 'yes' ),
            ),
            array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Display Title Rating', 'evockans' ),
				'param_name' => 'display_rating',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Rating', 'evockans' ),
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Number Star', 'evockans'),
				'param_name' => 'num_rating',
				'value' => '5',
				'group' => esc_html__( 'Rating', 'evockans' ),
				'dependency' => array( 'element' => 'display_rating', 'value' => 'yes' ),
            ), 
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Star Color', 'evockans'),
				'param_name' => 'star_color',
				'value' => '#2387ea',
				'group' => esc_html__( 'Rating', 'evockans' ),
				'dependency' => array( 'element' => 'display_rating', 'value' => 'yes' ),
            ),  
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Star Font Size', 'evockans'),
				'param_name' => 'star_font_size',
				'value' => '13px',
				'group' => esc_html__( 'Rating', 'evockans' ),
				'dependency' => array( 'element' => 'display_rating', 'value' => 'yes' ),
	        ),         
			// Typography
			array(
				'type' => 'headings',
				'text' => esc_html__('Text', 'evockans'),
				'param_name' => 'text_typography',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Text Font Family', 'evockans' ),
				'param_name' => 'text_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Text Font Weight', 'evockans' ),
				'param_name' => 'text_font_weight',
				'value'      => array(
					'Default' => 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Text Font Size', 'evockans'),
				'param_name' => 'text_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Text Font Style', 'evockans' ),
				'param_name' => 'text_font_style',
				'value'      => array(
					'Normal' => 'normal',
					'Italic' => 'italic',
				),
				'std'		=> 'normal',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Text Line-height', 'evockans'),
				'param_name' => 'text_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        //Name
			array(
				'type' => 'headings',
				'text' => esc_html__('Name', 'evockans'),
				'param_name' => 'name_typography',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Name Font Family', 'evockans' ),
				'param_name' => 'name_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Name Font Weight', 'evockans' ),
				'param_name' => 'name_font_weight',
				'value'      => array(
					'Default' => 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Name Font Size', 'evockans'),
				'param_name' => 'name_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Name Line-height', 'evockans'),
				'param_name' => 'name_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Name: Letter Spacing', 'evockans'),
				'param_name' => 'name_letter_spacing',
				'value' => '',
				'description'	=> esc_html__('Ex: 1px', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Name: Word Spacing', 'evockans'),
				'param_name' => 'name_word_spacing',
				'value' => '',
				'description'	=> esc_html__('Ex: 1px', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        //Position
			array(
				'type' => 'headings',
				'text' => esc_html__('Position', 'evockans'),
				'param_name' => 'position_typography',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Position Font Family', 'evockans' ),
				'param_name' => 'position_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Position Font Weight', 'evockans' ),
				'param_name' => 'company_font_weight',
				'value'      => array(
					'Default' => 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Position Font Size', 'evockans'),
				'param_name' => 'position_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Position Line-height', 'evockans'),
				'param_name' => 'position_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Position Font Style', 'evockans' ),
				'param_name' => 'position_font_style',
				'value'      => array(
					'Normal' => 'normal',
					'Italic' => 'italic',
				),
				'std'		=> 'normal',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        // Spacing
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Image: Margin', 'evockans'),
				'param_name' => 'image_margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Name & Position: Top Margin', 'evockans'),
				'param_name' => 'name_post_top',
				'value' => '',
				'description'	=> esc_html__('Ex: 20px', 'evockans'),
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Text: Margin', 'evockans'),
				'param_name' => 'text_margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Box Rating: Margin', 'evockans'),
				'param_name' => 'box_rating_margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        // Box Shadow
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Horizontal (Required)', 'evockans'),
				'param_name' => 'horizontal',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Vertical (Required)', 'evockans'),
				'param_name' => 'vertical',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 20px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Blur (Required)', 'evockans'),
				'param_name' => 'blur',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 40px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Spread (Required)', 'evockans'),
				'param_name' => 'spread',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Box Shadow Color (Required)', 'evockans'),
				'param_name' => 'shadow_color',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: #eeeeee', 'evockans'),
            ),
	    )
	) );
}

add_shortcode( 'testimonials', 'themesflat_shortcode_testimonials' );

/**
 * testimonials shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_testimonials( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'style' => 'style-2',
		'align' => '',
		'image' => '',
		'image_width' => '',
		'image_style' => 'image-circle',
		'text' => '',
		'text_color' => '',
		'text_background' => '',
		'text_padding' => '',
		'wrap_text_margin' => '',
		'text_rounded' => '',
		'text_border_width' => '',
		'text_border_color' => '',
		'name' => 'Bruno Strong',
		'name_color' => '',
		'position' => 'Envato User',
		'position_color' => '',
		'inline' => '',
		'gap' => '5px',
		'display_title_rating' => '',
		'title_rating' => '',
		'display_rating' => '',
		'num_rating' => '5',
		'star_color' => '#2387ea',
		'star_font_size' => '13px',
		'text_font_family' => 'Default',
		'text_font_weight' => 'Default',
		'text_font_size' => '',
		'text_font_style' => 'normal',
		'text_line_height' => '',
		'name_font_family' => 'Default',
		'name_font_weight' => 'Default',
		'name_font_size' => '',
		'name_line_height' => '',
		'name_letter_spacing' => '',
		'name_word_spacing' => '',
		'position_font_family' => 'Default',
		'position_font_weight' => 'Default',
		'position_font_size' => '',
		'position_line_height' => '',
		'position_font_style' => 'normal',
		'image_margin' => '',
		'name_post_top' => '',
		'text_margin' => '',
		'box_rating_margin' => '',
		'horizontal' => '',
        'vertical' => '',
        'blur' => '',
        'spread' => '',
        'shadow_color' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'testimonials', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$text_font_size = intval( $text_font_size );
	$name_font_size = intval( $name_font_size );
	$position_font_size = intval( $position_font_size );

	$image_width = intval( $image_width );
	$name_post_top = intval( $name_post_top );

	$text_rounded = intval( $text_rounded );
	$gap = intval( $gap );
	$num_rating = intval( $num_rating );
	$star_font_size = intval( $star_font_size );

	$cls = $image_style;
	$cls .= ' ' . $style . ' ' .$align;
	$item_css = $text_css = $name_css = $position_css = $thumb_css = $name_pos_css = $text_p_css = $box_rating_css = '';
	$inner_html = $image_html = $text_html = $name_html = $position_html = $rating_html = $title_rating_html = $star_html = $star_css = '';

	if ( $inline ) $cls .= ' name-inline';

	$text_css .= 'font-style:'. $text_font_style .';';

	if ( $text_background ) {
		$text_css .= 'background-color: '. $text_background .';';
		
	} else {
		$text_css .= 'background-color: transparent;';
	}

	if ( $display_title_rating ) $cls .= ' has-title-rating';
	if ( $display_rating ) $cls .= ' has-rating';

	if ( $horizontal && $vertical && $blur && $spread && $shadow_color )
    	$text_css .= 'box-shadow:'. $horizontal .' '. $vertical .' '. $blur .' '. $spread .' '. $shadow_color .';';

	if ( $text_font_weight != 'Default' ) $text_css .= 'font-weight:'. $text_font_weight .';';
	if ( $text_color ) $text_css .= 'color: '. $text_color .';';
	if ( $text_padding ) $text_css .= 'padding:'. $text_padding .';';
	if ( $wrap_text_margin ) $text_css .= 'margin:'. $wrap_text_margin .';';
	if ( $text_rounded ) $text_css .= 'border-radius:'. $text_rounded .'px;';
	if ( $text_font_size ) $text_css .= 'font-size:'. $text_font_size .'px;';
	if ( $text_line_height ) $text_css .= 'line-height:'. $text_line_height .';';
	if ( $text_font_family != 'Default' ) {
	    $text_css .= 'font-family:'. $text_font_family .';';
	}
	if ( $text_border_color && $text_border_width ) $text_css .= 'border-style:solid;border-width:'. $text_border_width .';border-color:'. $text_border_color .';';

	if ( $name_font_weight != 'Default' ) $name_css .= 'font-weight:'. $name_font_weight .';';
	if ( $name_color ) $name_css .= 'color: '. $name_color .';';
	if ( $name_font_size ) $name_css .= 'font-size:'. $name_font_size .'px;';
	if ( $name_line_height ) $name_css .= 'line-height:'. $name_line_height .';';
	if ( $name_font_family != 'Default' ) {
	    $name_css .= 'font-family:'. $name_font_family .';';
	}
	if ( $name_letter_spacing ) $name_css .= 'letter-spacing:'. $name_letter_spacing .';';
	if ( $name_word_spacing ) $name_css .= 'word-spacing:'. $name_word_spacing .';';

	if ( $position_font_weight != 'Default' ) $position_css .= 'font-weight:'. $position_font_weight .';';
	if ( $position_color ) $position_css .= 'color: '. $position_color .';';
	if ( $inline ) {
		if ( $gap ) $position_css .= 'padding-left: '. $gap .'px;';
	}
	
	if ( $position_font_size ) $position_css .= 'font-size:'. $position_font_size .'px;';
	if ( $position_line_height ) $position_css .= 'line-height:'. $position_line_height .';';
	if ( $position_font_family != 'Default' ) {
	    $position_css .= 'font-family:'. $position_font_family .';';
	}
	$position_css .= 'font-style:'. $position_font_style .';';

	if ( $image_width ) {
		$thumb_css .= 'width:'. $image_width .'px; height:'. $image_width .'px;';
	}

	if ( $style == 'style-1' ) {
		if ( $image_width ) {
			$thumb_css .= 'margin-left:-'. ( $image_width / 2 ) .'px;margin-top:-'. ( $image_width / 2 ) .'px;';
			$item_css .= 'padding-top:'. ( $image_width / 2 ) .'px;';
		}else {
			$thumb_css .= 'margin-left:-35px;margin-top:-35px;';
			$item_css .= 'padding-top:35px;';
		}
	}

	if ( $image_margin ) $thumb_css .= 'margin:' . $image_margin . ';';


	if ( $image ) {
	    $image_html .= sprintf(
	        '<div class="thumb" style="%2$s">%1$s</div>',
	         wp_get_attachment_image( $image, 'full' ), $thumb_css
	    );
	}

	if ( $text )
	$text_html .= sprintf(
		'<blockquote class="text" style="%2$s">
		%1$s
		</blockquote>',
		$text, $text_css
	);

	if ( $name || $position ) {
		if ( $name ) {
		    $name_html .= sprintf(
		    '<h6 class="name" style="%2$s">%1$s</h6>',
		    $name, $name_css
		    );
		}

		if ( $position ) {
		    $position_html .= sprintf(
		    '<span class="position" style="%2$s">%1$s</span>',
		    $position, $position_css
		    );
		}
	}

	if ( $name_post_top ) $name_pos_css .= 'margin-top:'. $name_post_top .'px;';
	if ( $display_title_rating ){
		if ( $title_rating ) $title_rating_html = sprintf( '<h6 class="title_rating">%1$s</h6>', $title_rating );
	}

	if ( $display_rating ){
		if ( $num_rating ) {
			if ( $num_rating > 5 ) {
				$num_rating = 5;
			} else if ( $num_rating < 1 ) {
				$num_rating = 5;
			}

			if ( $num_rating == 1 ) {
				$star_html =   '<i class="fas fa-star"></i>
							    <i class="far fa-star"></i>
							    <i class="far fa-star"></i>
							    <i class="far fa-star"></i>
							    <i class="far fa-star"></i>';
			} else if ( $num_rating == 2 ) {
				$star_html =   '<i class="fas fa-star"></i>
							    <i class="fas fa-star"></i>
							    <i class="far fa-star"></i>
							    <i class="far fa-star"></i>
							    <i class="far fa-star"></i>';
			} else if ( $num_rating == 3 ) {
				$star_html =   '<i class="fas fa-star"></i>
							    <i class="fas fa-star"></i>
							    <i class="fas fa-star"></i>
							    <i class="far fa-star"></i>
							    <i class="far fa-star"></i>';
			} else if ( $num_rating == 4 ) {
				$star_html =   '<i class="fas fa-star"></i>
							    <i class="fas fa-star"></i>
							    <i class="fas fa-star"></i>
							    <i class="fas fa-star"></i>
							    <i class="far fa-star"></i>';
			} else if ( $num_rating == 5 ) {
				$star_html =   '<i class="fas fa-star"></i>
							    <i class="fas fa-star"></i>
							    <i class="fas fa-star"></i>
							    <i class="fas fa-star"></i>
							    <i class="fas fa-star"></i>';
			}
		}
	}

	if ( $box_rating_margin ) $box_rating_css .= 'margin:' . $box_rating_margin . ';';

	if ( $star_color ) $star_css .= 'color:' . $star_color . ';';
	if ( $star_font_size ) $star_css .= 'font-size:' . $star_font_size . 'px;';

	$rating_html = sprintf('
		<div class="box-rating" style="%4$s">
		    %1$s
		    <div class="rating" style="%3$s">%2$s</div>
		</div>',
		$title_rating_html,
		$star_html,
		$star_css,
		$box_rating_css
	);

	if ( $text_margin ) $text_p_css .= 'margin:' . $text_margin . ';';	

	if ( $style == 'style-1' ) {
		$text_html = sprintf(
			'<blockquote class="text" style="%2$s">
			<p style="%7$s">%1$s</p><div class="name-pos" style="%5$s">%3$s %4$s</div>%6$s
			</blockquote>',
			$text, $text_css, $name_html, $position_html, $name_pos_css, $rating_html, $text_p_css
		);

		$inner_html .= sprintf(
		    '<div class="inner">%1$s %2$s</div>',
		    $image_html, $text_html
		);
	} else if ( $style == 'style-2' ){
		$text_html = sprintf(
			'<blockquote class="text" style="%2$s">
			%6$s			
			<div class="name-pos" style="%5$s">%3$s %4$s</div>
			%7$s
			<p style="%8$s">%1$s</p>
			</blockquote>',
			$text, $text_css, $name_html, $position_html, $name_pos_css, $image_html, $rating_html, $text_p_css
		);
		
		$inner_html .= sprintf(
		    '<div class="inner">%1$s</div>',
		    $text_html
		);
	} else if ( $style == 'style-3' ){
		$text_html = sprintf(
			'<blockquote class="text" style="%2$s">
			%6$s
			<p style="%8$s">%1$s</p>
			%7$s				
			<div class="name-pos" style="%5$s">%3$s %4$s</div>					
			</blockquote>',
			$text, $text_css, $name_html, $position_html, $name_pos_css, $image_html, $rating_html, $text_p_css
		);
		
		$inner_html .= sprintf(
		    '<div class="inner">%1$s</div>',
		    $text_html
		);
	} else if ( $style == 'style-4' ){
		$text_html = sprintf(
			'<blockquote class="text" style="%2$s">
			<p style="%8$s">%1$s</p>						
			%7$s	
			%6$s			
			<div class="name-pos" style="%5$s">%3$s %4$s</div>					
			</blockquote>',
			$text, $text_css, $name_html, $position_html, $name_pos_css, $image_html, $rating_html, $text_p_css
		);
		
		$inner_html .= sprintf(
		    '<div class="inner">%1$s</div>',
		    $text_html
		);
	} else if ( $style == 'style-5' ){
		$text_html = sprintf(
			'<blockquote class="text" style="%2$s">
			%6$s
			<div class="name-pos" style="%5$s">%3$s %4$s</div>
			<p style="%8$s">%1$s</p>						
			%7$s				
			</blockquote>',
			$text, $text_css, $name_html, $position_html, $name_pos_css, $image_html, $rating_html, $text_p_css
		);
		
		$inner_html .= sprintf(
		    '<div class="inner">%1$s</div>',
		    $text_html
		);
	}

	
	return sprintf(
	    '<div class="themesflat_sc_vc-testimonials clearfix %2$s">
	        <div class="item" style="%3$s">
	            %1$s
	        </div>
	    </div>', 
	    $inner_html,
	    $cls,
	    $item_css
	);

	
}