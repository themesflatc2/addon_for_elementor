<?php
/**
 * Register filter for append custom class name
 * that generated from visual-composer
 */
class WPBakeryShortCode_rotateboxprimary extends WPBakeryShortCodesContainer {}
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_rotateboxprimary_shortcode_params' );

function themesflat_rotateboxprimary_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Rotate Box Primary', 'evockans'),
        'description' => esc_html__('Empty space with custom height.', 'evockans'),
        'base' => 'rotateboxprimary',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'as_child' => array( 'only' => 'rotatebox' ),
        'as_parent' => array('except' => 'rotateboxprimary'),
        'controls' => 'full',
		'show_settings_on_create' => true,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'js_view' => 'VcColumnView',
        'params' => array(
        	array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image', 'evockans'),
				'param_name' => 'image',
				'value' => '',
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Rounded', 'evockans'),
				'param_name' => 'rounded',
				'value' => '',
				'description'	=> esc_html__('Ex: 5px', 'evockans'),
	        ),
	        array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Padding', 'evockans' ),
				'param_name'  => 'padding',
				'value'	=> '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
			),        
        )
    ) );
}

add_shortcode( 'rotateboxprimary', 'themesflat_shortcode_rotateboxprimary' );

/**
 * rotateboxprimary shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_rotateboxprimary( $atts, $content = null ) {
	extract( shortcode_atts( array(	
		'image' => '',
	    'rounded' => '',
	    'padding' => '',	    
	), $atts ) );	
	//extract($atts = vc_map_get_attributes( 'rotateboxprimary', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$inner_css = '';

	if ( $padding ) $inner_css .= 'padding:' . $padding . ';';

	if ( $image ) {
	    $image_url = wp_get_attachment_image_src( $image, 'full' )[0];
	    $image_html = 'background-image: url(' . $image_url . ');';
	} 

	$rounded = intval( $rounded );
	if ( $rounded ) $image_html .= 'border-radius:' . $rounded . 'px;';

	return sprintf( '		
		<div class="front" style="%1$s">
			<div class="inner" style="%3$s">%2$s</div>			  	
		</div>',
		$image_html,
		do_shortcode($content),
		$inner_css
	);
}