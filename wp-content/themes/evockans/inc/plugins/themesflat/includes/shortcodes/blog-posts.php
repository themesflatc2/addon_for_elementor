<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_blogposts_shortcode_params' );

function themesflat_blogposts_shortcode_params() {
	$category_posts = get_terms( 'category' );
	$category_posts_name[] = 'All';
	foreach ( $category_posts as $category_post ) {
		$category_posts_name[$category_post->name] = $category_post->slug;		
	}
	vc_map( array(
	    'name' => esc_html__('Blog Posts', 'evockans'),
	    'description' => esc_html__('Displaying blog posts in carousel.', 'evockans'),
	    'base' => 'blogposts',
		'weight'	=>	180,
	    'icon' => THEMESFLAT_ICON,
	    'category' => esc_html__('THEMESFLAT', 'evockans'),
	    'params' => array(	    	
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Layout', 'evockans' ),
				'param_name' => 'layout',
				'value'      => array(
					'Grid Classic' => 'blog-grid',
					'Grid Simple' => 'blog-grid-simple',	
					'Grid Style 1' => 'blog-grid-s1',
					'Grid Style 2' => 'blog-grid-s2',
					'List Small' => 'blog-list-small',							
				),
				'std'		=> 'blog-grid',				
			),
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Enable Carousel?', 'evockans' ),
				'param_name' => 'enable_carousel',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Thumbnail Cropping', 'evockans' ),
				'param_name' => 'image_crop',
				'value'      => array(
					'Full' => 'full',
					'600 x 600' => 'square',
					'600 x 500' => 'rectangle',
					'600 x 390' => 'rectangle2',
					'Custom' => 'custom',
				),
				'std'		=> 'custom',
			),			
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Image Size', 'evockans' ),
				'description'    => esc_html__( '( Alternatively enter size in pixels (Example: 750x750 (Width x Height)).', 'evockans' ),
				'param_name' => 'image_size',
				'value' => '750x525',
				'dependency' => array( 'element' => 'image_crop', 'value' => 'custom' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Text Alignment', 'evockans' ),
				'param_name' => 'alignment',
				'value'      => array(
					'Left' => '',
					'Center' => 'text-center',
					'Right' => 'text-right',
				),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap Padding.', 'evockans'),
				'param_name' => 'wrap_padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
	        ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap Margin.', 'evockans'),
				'param_name' => 'wrap_margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap Rounded.', 'evockans'),
				'param_name' => 'wrap_rounded',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Content Padding.', 'evockans'),
				'param_name' => 'content_padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Content Background', 'evockans'),
				'param_name' => 'content_background',
				'value' => '',
            ),
            array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Hide Image Post?', 'evockans' ),
				'param_name' => 'hide_image',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
			),
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Hide Meta Post?', 'evockans' ),
				'param_name' => 'hide_meta',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
			),			
	        // Query
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Number of items', 'evockans'),
				'param_name' => 'items',
				'value' => '3',
				'group' => esc_html__( 'Query', 'evockans' ),
	        ),	        
	        array(
				'type'        => 'dropdown',
				'heading' => esc_html__('Category', 'evockans'),
				'param_name' => 'cat_slug',
				'value'       => $category_posts_name,
				'description' => esc_html__('Display Posts From Some Categories.', 'evockans'),
				'group' => esc_html__( 'Query', 'evockans' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'My Custom Ids', 'evockans' ),
				'param_name'  => 'post_in',
				'value'		  => '',				
				'description' => esc_html__( 'Just Show these Blog Post by IDs EX:1,2,3', 'evockans' ),
				'group' => esc_html__( 'Query', 'evockans' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Post Ids Will Be Inorged. Ex: 1,2,3', 'evockans' ),
				'param_name'  => 'exclude',
				'value'       => '',
				'group' => esc_html__( 'Query', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Excerpt Length', 'evockans'),
				'param_name' => 'excerpt_lenght',
				'value' => '15',
				'description' => esc_html('The number of words you wish to display in the excerpt. Enter "0" to hide the excerpt.', 'evockans'),
				'group' => esc_html__( 'Query', 'evockans' ),
	        ),	        
			// Button
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Text', 'evockans'),
				'param_name' => 'button_text',
				'value' => 'Read More',
				'group' => esc_html__( 'Button', 'evockans' ),
	        ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Style', 'evockans' ),
				'param_name' => 'button_style',
				'value'      => array(
					'Simple Link' => 'simple_link',
					'Accent' => 'accent',
					'Dark' => 'dark',
					'Light' => 'light',
					'Very Light' => 'very-light',
					'White' => 'white',
					'Outline' => 'outline',
					'Outline Dark' => 'outline_dark',
					'Outline Light' => 'outline_light',
					'Outline Very light' => 'outline_very-light',
					'Outline White' => 'outline_white',
				),
				'std'		=> 'simple_link',
				'group' => esc_html__( 'Button', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Size', 'evockans' ),
				'param_name' => 'button_size',
				'value'      => array(
					'Medium' => '',
					'Small' => 'small',
					'Very mall' => 'xsmall',
					'Big' => 'big',
				),
				'std'		=> 'small',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array(
					'element' => 'button_style',
					'value' => array (
						'accent',
						'dark',
						'light',
						'very-light',
						'white',
						'outline',
						'outline_dark',
						'outline_light',
						'outline_very-light',
						'outline_white'
					)
				),
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Rounded', 'evockans'),
				'param_name' => 'button_rounded',
				'value' => '',
				'description'	=> esc_html__('ex: 10px', 'evockans'),
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array(
					'element' => 'button_style',
					'value' => array (
						'accent',
						'dark',
						'light',
						'very-light',
						'white',
						'outline',
						'outline_dark',
						'outline_light',
						'outline_very-light',
						'outline_white'
					)
				),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Padding', 'evockans'),
				'param_name' => 'button_padding',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Link Color', 'evockans'),
				'param_name' => 'link_color',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
				'dependency' => array( 'element' => 'button_style', 'value' => 'simple_link' ),
            ),
            /*Button Icon*/
            array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Add Icon Button', 'evockans' ),
				'param_name' => 'add_icon_button',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Button', 'evockans' ),
			),	  
	        // Controls
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Item: Space Between', 'evockans'),
				'param_name' => 'gap',
				'value' => '30',
				'group' => esc_html__( 'Controls', 'evockans' ),
				'description'	=> esc_html__('Important! Include the blur distance of the shadow.', 'evockans'),
				'dependency' => array( 'element' => 'enable_carousel', 'value' => 'yes' ),
	        ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Item: Auto Scroll?', 'evockans' ),
				'param_name' => 'auto_scroll',
				'value'      => array(
					'No' => 'false',
					'Yes' => 'true',
				),
				'std'		=> 'false',
				'group' => esc_html__( 'Controls', 'evockans' ),
				'dependency' => array( 'element' => 'enable_carousel', 'value' => 'yes' ),
			),
			array(
				'type' => 'headings',
				'text' => esc_html__('Bullets', 'evockans'),
				'param_name' => 'bullets_heading',
				'group' => esc_html__( 'Controls', 'evockans' ),
				'dependency' => array( 'element' => 'enable_carousel', 'value' => 'yes' ),
			),
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Show Bullets?', 'evockans' ),
				'param_name' => 'show_bullets',
				'group' => esc_html__( 'Controls', 'evockans' ),
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'dependency' => array( 'element' => 'enable_carousel', 'value' => 'yes' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Space between Bullets & Images', 'evockans' ),
				'param_name' => 'bullet_between',
				'value'      => array(
					'50px' => '50',
					'45px' => '45',
					'40px' => '40',
					'35px' => '35',
					'30px' => '30',
					'25px' => '25',
					'20px' => '20',
					'15px' => '15',
					'10px' => '10',
				),
				'std'		=> '50',
				'dependency' => array( 'element' => 'show_bullets', 'value' => 'yes' ),
				'group' => esc_html__( 'Controls', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Bullets Show', 'evockans' ),
				'param_name' => 'bullet_show',
				'group' => esc_html__( 'Controls', 'evockans' ),
				'value'      => array(
					'Square' => 'bullet-square',
					'Circle' => 'bullet-circle',
				),
				'std'		=> 'bullet-square',
				'dependency' => array( 'element' => 'show_bullets', 'value' => 'yes' ),
			),
			array(
				'type' => 'headings',
				'text' => esc_html__('Arrows', 'evockans'),
				'param_name' => 'arrows_heading',
				'group' => esc_html__( 'Controls', 'evockans' ),
				'dependency' => array( 'element' => 'enable_carousel', 'value' => 'yes' ),
			),
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Show Arrows?', 'evockans' ),
				'param_name' => 'show_arrows',
				'group' => esc_html__( 'Controls', 'evockans' ),
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'dependency' => array( 'element' => 'enable_carousel', 'value' => 'yes' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Arrows Offset: Horizontal', 'evockans' ),
				'param_name' => 'arrow_offset',
				'group' => esc_html__( 'Controls', 'evockans' ),
				'value'      => array(
					'-40' => '-40',
					'-35' => '-35',
					'-30' => '-30',
					'-25' => '-25',
					'-20' => '-20',
					'-15' => '-15',
					'-10' => '-10',
					'0 - IN' => '0i',
					'Center' => 'center',
					'0 - OUT' => '0o',
					'10' => '10',
					'15' => '15',
					'20' => '20',
					'25' => '25',
					'30' => '30',
					'35' => '35',
					'40' => '40',
				),
				'std'		=> 'center',
				'dependency' => array( 'element' => 'show_arrows', 'value' => 'yes' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Arrows Offset: Vertical', 'evockans' ),
				'param_name' => 'arrow_offset_v',
				'group' => esc_html__( 'Controls', 'evockans' ),
				'value'      => array(
					'-120' => '-120',
					'-110' => '-110',
					'-100' => '-100',
					'-90' => '-90',
					'-80' => '-80',
					'-70' => '-70',
					'-60' => '-60',
					'-50' => '-50',
					'-40' => '-40',
					'-30' => '-30',
					'-20' => '-20',
					'0' => '0',
					'20' => '20',
					'30' => '30',
					'40' => '40',
					'50' => '50',
					'60' => '60',
					'70' => '70',
					'80' => '80',
					'90' => '90',
					'100' => '100',
					'110' => '110',
					'120' => '120',
				),
				'std'		=> '0',
				'dependency' => array( 'element' => 'show_arrows', 'value' => 'yes' ),
			),
			// Columns
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Screen > 1000px.', 'evockans' ),
				'param_name' => 'column',
				'group'      => esc_html__( 'Columns', 'evockans' ),
				'value'      => array(
					'1 Column' => '1c',
					'2 Columns' => '2c',
					'3 Columns' => '3c',
					'4 Columns' => '4c',
					'5 Columns' => '5c',
					'6 Columns' => '6c',
					'7 Columns' => '7c',
				),
				'std'		=> '3c',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Screen from 600px to 1000px.', 'evockans' ),
				'param_name' => 'column2',
				'group'      => esc_html__( 'Columns', 'evockans' ),
				'value'      => array(
					'1 Column' => '1c',
					'2 Columns' => '2c',
					'3 Columns' => '3c',
					'4 Columns' => '4c',
					'5 Columns' => '5c',
				),
				'std'		=> '2c',
				'dependency' => array( 'element' => 'enable_carousel', 'value' => 'yes' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Screen < 600px.', 'evockans' ),
				'param_name' => 'column3',
				'group'      => esc_html__( 'Columns', 'evockans' ),
				'value'      => array(
					'1 Column' => '1c',
					'2 Columns' => '2c',
					'3 Columns' => '3c',
					'4 Columns' => '4c',
				),
				'std'		=> '1c',
				'dependency' => array( 'element' => 'enable_carousel', 'value' => 'yes' ),
			),
			// Typography
			array(
				'type' => 'headings',
				'text' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Font Family', 'evockans' ),
				'param_name' => 'heading_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Font Weight', 'evockans' ),
				'param_name' => 'heading_font_weight',
				'value'      => array(
					'Default' => 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Heading Color', 'evockans'),
				'param_name' => 'heading_color',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Font Size', 'evockans'),
				'param_name' => 'heading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Line-Height', 'evockans'),
				'param_name' => 'heading_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Excerpt', 'evockans'),
				'param_name' => 'desc_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Excerpt Font Family', 'evockans' ),
				'param_name' => 'desc_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Excerpt Font Weight', 'evockans' ),
				'param_name' => 'desc_font_weight',
				'value'      => array(
					'Default' => 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Excerpt Color', 'evockans'),
				'param_name' => 'desc_color',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Excerpt Font Size', 'evockans'),
				'param_name' => 'desc_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Excerpt Line-height', 'evockans'),
				'param_name' => 'desc_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Button', 'evockans'),
				'param_name' => 'button_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Font Family', 'evockans' ),
				'param_name' => 'button_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Font Weight', 'evockans' ),
				'param_name' => 'button_font_weight',
				'value'      => array(
					'Default' => 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Font Size', 'evockans'),
				'param_name' => 'button_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Line-Height', 'evockans'),
				'param_name' => 'button_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        // Spacing
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Top Margin', 'evockans'),
				'param_name' => 'heading_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Bottom Margin', 'evockans'),
				'param_name' => 'heading_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Meta Post: Top Margin', 'evockans'),
				'param_name' => 'meta_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Meta Post: Bottom Margin', 'evockans'),
				'param_name' => 'meta_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Description: Top Margin', 'evockans'),
				'param_name' => 'desc_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Description: Bottom Margin', 'evockans'),
				'param_name' => 'desc_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	    )
	) );
}

add_shortcode( 'blogposts', 'themesflat_shortcode_blogposts' );

/**
 * blogposts shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_blogposts( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'layout' => 'blog-grid',
		'enable_carousel' => '',
		'alignment' => '',
		'wrap_padding' => '',
		'wrap_margin' => '',
		'wrap_rounded' => '',
		'image_crop' => 'custom',
		'image_size' => '750x525',
		'content_padding' => '',
		'content_background' => '',
		'hide_image' => '',
		'hide_meta' => '',
		'excerpt_lenght' => '15',
		'column'		=> '3c',
		'column2'		=> '2c',
		'column3'		=> '1c',
		'items'		=> '3',
		'post_in' => '',
		'exclude'	=> '',
		'gap'		=> '30',
		'auto_scroll' => 'false',
		'show_bullets' => '',
		'show_arrows' => '',
		'bullet_show' => 'bullet-square',
		'bullet_between' => '50',
	    'arrow_offset' => 'center',
	    'arrow_offset_v' => '0',
		'cat_slug' => 'All',
		'heading_font_family' => 'Default',
		'heading_font_weight' => 'Default',
		'heading_color' => '',
		'heading_font_size' => '',
		'heading_line_height' => '',
		'desc_font_family' => 'Default',
		'desc_font_weight' => 'Default',
		'desc_color' => '',
		'desc_font_size' => '',
		'desc_line_height' => '',
		'button_font_family' => 'Default',
		'button_font_weight' => 'Default',
		'button_font_size' => '',
		'button_line_height' => '',
		'button_style' => 'simple_link',
		'button_size' => 'small',
		'button_rounded' => '',
		'button_padding' => '',
		'link_color' => '',
		'button_text' => 'Read More',
		'add_icon_button' => '', 
		'heading_top_margin' => '',
		'heading_bottom_margin' => '',
		'meta_top_margin' => '',
		'meta_bottom_margin' => '',
		'desc_top_margin' => '',
		'desc_bottom_margin' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'blogposts', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$items = intval( $items );
	$gap = intval( $gap );
	$column = intval( $column );
	$column2 = intval( $column2 );
	$column3 = intval( $column3 );

	$excerpt_lenght = intval( $excerpt_lenght );
	$button_rounded = intval( $button_rounded );

	$heading_font_size = intval( $heading_font_size );
	$desc_font_size = intval( $desc_font_size );
	$button_font_size = intval( $button_font_size );

	$heading_top_margin = intval( $heading_top_margin );
	$heading_bottom_margin = intval( $heading_bottom_margin );
	$meta_top_margin = intval( $meta_top_margin );
	$meta_bottom_margin = intval( $meta_bottom_margin );
	$desc_top_margin = intval( $desc_top_margin );
	$desc_bottom_margin = intval( $desc_bottom_margin );
	$wrap_rounded = intval( $wrap_rounded );

	$item_css = $content_css = $heading_css = $desc_css = $meta_css = $button_css = $has_carousel = $button_icon_html = $button_icon_css = '';
	$author_html = $comment_html = '';
	if ( empty( $items ) )
		return;

	$cls = 'arrow-center '. $alignment .' '. $bullet_show .' ';
	$cls .= 'offset'. $arrow_offset .' offset-v'. $arrow_offset_v;

	if ( $content_padding ) $content_css .= 'padding:'. $content_padding .';';
	if ( $content_background ) $item_css .= 'background-color:'. $content_background .';';
	if ( $wrap_padding ) $item_css .= 'padding:'. $wrap_padding .';';
	if ( $wrap_margin ) $item_css .= 'margin:'. $wrap_margin .';';
	if ( $wrap_rounded ) $item_css .= 'border-radius:'. $wrap_rounded .'px;';

	if ( $show_bullets ) $cls .= ' has-bullets'; 
	if ( $show_arrows ) $cls .= ' has-arrows';

	if ( $bullet_between == '45' ) $cls .= ' bullet45';
	if ( $bullet_between == '40' ) $cls .= ' bullet40';
	if ( $bullet_between == '35' ) $cls .= ' bullet35';
	if ( $bullet_between == '30' ) $cls .= ' bullet30';
	if ( $bullet_between == '25' ) $cls .= ' bullet25';
	if ( $bullet_between == '20' ) $cls .= ' bullet20';
	if ( $bullet_between == '15' ) $cls .= ' bullet15';
	if ( $bullet_between == '10' ) $cls .= ' bullet10';

	if ( $heading_font_weight != 'Default' ) $heading_css .= 'font-weight:'. $heading_font_weight .';';
	if ( $heading_color ) $heading_css .= 'color:'. $heading_color .';';
	if ( $heading_font_size ) $heading_css .= 'font-size:'. $heading_font_size .'px;';
	if ( $heading_line_height ) $heading_css .= 'line-height:'. $heading_line_height .';';
	if ( $heading_top_margin ) $heading_css .= 'margin-top:'. $heading_top_margin .'px;';
	if ( $heading_bottom_margin ) $heading_css .= 'margin-bottom:'. $heading_bottom_margin .'px;';
	if ( $heading_font_family != 'Default' ) {
		$heading_css .= 'font-family:'. $heading_font_family .';';
	}

	if ( $desc_font_weight != 'Default' ) $desc_css .= 'font-weight:'. $desc_font_weight .';';
	if ( $desc_color ) $desc_css .= 'color:'. $desc_color .';';
	if ( $desc_font_size ) $desc_css .= 'font-size:'. $desc_font_size .'px;';
	if ( $desc_line_height ) $desc_css .= 'line-height:'. $desc_line_height .';';
	if ( $desc_top_margin ) $desc_css .= 'margin-top:'. $desc_top_margin .'px;';
	if ( $desc_bottom_margin ) $desc_css .= 'margin-bottom:'. $desc_bottom_margin .'px;';
	if ( $desc_font_family != 'Default' ) {
		$desc_css .= 'font-family:'. $desc_font_family .';';
	}

	if ( $meta_top_margin ) $meta_css .= 'margin-top:'. $meta_top_margin .'px;';
	if ( $meta_bottom_margin ) $meta_css .= 'margin-bottom:'. $meta_bottom_margin .'px;';

	if ( $button_font_weight != 'Default' ) $button_css .= 'font-weight:'. $button_font_weight .';';
	if ( $link_color ) $button_css .= 'color:'. $link_color .';';
	if ( $button_rounded ) $button_css .= 'border-radius:'. $button_rounded .'px;';
	if ( $button_padding ) $button_css .= 'padding:'. $button_padding .';';
	if ( $button_font_size ) $button_css .= 'font-size:'. $button_font_size .'px;';
	if ( $button_line_height ) $button_css .= 'line-height:'. $button_line_height .';';
	if ( $button_font_family != 'Default' ) {
		$button_css .= 'font-family:'. $button_font_family .';';
	}

	$button_cls = $button_size;
	if ( $button_style == 'simple_link' ) $button_cls .= ' simple-link';
	if ( $button_style == 'accent' ) $button_cls .= ' themesflat_sc_vc-button accent';
	if ( $button_style == 'dark' ) $button_cls .= ' themesflat_sc_vc-button dark';
	if ( $button_style == 'light' ) $button_cls .= ' themesflat_sc_vc-button light';
	if ( $button_style == 'very-light' ) $button_cls .= ' themesflat_sc_vc-button very-light';
	if ( $button_style == 'white' ) $button_cls .= ' themesflat_sc_vc-button white';
	if ( $button_style == 'outline' ) $button_cls .= ' themesflat_sc_vc-button outline ol-accent';
	if ( $button_style == 'outline_dark' ) $button_cls .= ' themesflat_sc_vc-button outline dark';
	if ( $button_style == 'outline_light' ) $button_cls .= ' themesflat_sc_vc-button outline light';
	if ( $button_style == 'outline_very-light' ) $button_cls .= ' themesflat_sc_vc-button outline very-light';
	if ( $button_style == 'outline_white' ) $button_cls .= '  themesflat_sc_vc-button outline white';

	if( $add_icon_button ) {
		$button_icon_html = '<i class="fa fa-angle-right"></i>';
	}

	if ( $enable_carousel == 'yes' ) {
		$has_carousel = 'class="owl-carousel owl-theme"';
		$cls .= ' has-carousel';
	}else {
		$has_carousel = 'class="template"';
	}

	$query_args = array(
	    'post_type' => 'post',
	    'posts_per_page' => $items,
	);
	
	if ( $cat_slug != 'All' ) $query_args['category_name'] = $cat_slug;

	if ( $post_in ) {	
		if ( trim($post_in) != '') {
			$query_args['post__in'] = explode(',', trim($post_in));
	        $query_args['orderby'] = 'post__in';
		}
	}

	if ( ! empty( $exclude ) ) {				
		if ( ! is_array( $exclude ) )
			$exclude = explode( ',', $exclude );

		$query_args['post__not_in'] = $exclude;
	}

	$query = new WP_Query( $query_args );	
	if ( ! $query->have_posts() ) { return; }
	ob_start(); ?>

	<div class="themesflat_sc_vc-blog-post <?php echo esc_attr( $cls ); ?>" data-auto="<?php echo esc_attr( $auto_scroll ); ?>" data-column="<?php echo esc_attr( $column ); ?>" data-column2="<?php echo esc_attr( $column2 ); ?>" data-column3="<?php echo esc_attr( $column3 ); ?>" data-gap="<?php echo esc_html( $gap ); ?>">
	<?php if ( $query->have_posts() ) : ?>
		<?php wp_enqueue_script( 'themesflat_sc_vc-owlcarousel' ); ?>
		<div class="wrap-blog-article <?php echo esc_attr($layout) ?> <?php echo ($enable_carousel == '' ? 'columns-'.esc_attr( $column ) : ''); ?>">
			<div <?php echo $has_carousel ?>>			
			    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<div class="item">
			    	<article class="entry blog-post-item clearfix post-<?php the_ID(); ?>">
			    		<div class="entry-border"  style="<?php echo esc_attr( $item_css ); ?>">
							
							<?php if ( $layout == 'blog-grid-s1' ): ?>
							<div class="post-date">
								<?php
								$archive_year  = get_the_time('Y'); 
								$archive_month = get_the_time('m'); 
								$archive_day   = get_the_time('d'); 
								?>
								<a href="<?php echo get_day_link( $archive_year, $archive_month, $archive_day); ?>"><?php echo get_the_date();?></a>			
							</div>
							<?php endif; ?>

							<?php 
								if ($layout == 'blog-grid-s1') {								
									if ( get_the_title() ) {
										echo '<h3 class="entry-title" style="'. esc_attr( $heading_css ) .'"><a href="'. esc_url( get_the_permalink() ) .'">'. get_the_title() .'</a></h3>';
									}

									if ( empty( $hide_meta ) ) {
										?>
										<div class="entry-meta" style="<?php echo esc_attr( $meta_css ); ?>"><?php themesflat_posted_on($layout); ?></div>
										<?php
									}
								}
							?>

		    				<!-- Featured Post -->			    					    				
			    			<?php 
			    			if( empty( $hide_image ) ) {
			    				$feature_post = '';
								global $themesflat_thumbnail;
    							$themesflat_thumbnail = 'themesflat-blog';
			    				switch ( get_post_format() ) {	
									case 'gallery':
									$images = themesflat_decode(themesflat_meta( 'gallery_images'));									

									if ( empty( $images ) )
										break;
									?>		
									<div class="featured-post">		
										<div class="customizable-carousel owl-carousel owl-theme" data-loop="true" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-space="15" data-autoplay="true" data-autospeed="4000" data-nav-dots="false" data-nav-arrows="true">
											<?php 
												if ( !empty( $images ) && is_array( $images ) ) {
													foreach ( $images as $image ) { ?>
														<div class="item-gallery">  
														    <?php 														    
														    if ( has_post_thumbnail() ) {
											    				if ( $image_crop == 'custom' && $image_size != '' ) {
							  										echo wpb_getImageBySize(
								  											array(
								  												'attach_id' => $image,
								  												'thumb_size' => $image_size
								  											)
								  										)['thumbnail'];
											    				} else {
															    	$img_size = 'full';
																	if ( $image_crop == 'square' ) $img_size = 'themesflat_sc_vc-square';
																	if ( $image_crop == 'rectangle' ) $img_size = 'themesflat_sc_vc-rectangle';
																	if ( $image_crop == 'rectangle2' ) $img_size = 'themesflat_sc_vc-rectangle2';

																	echo wp_get_attachment_image( $image, $img_size );
																}
											    			}
														    ?>

														</div>
													<?php }
												} 
											?> 
										</div>		
									</div><!-- /.feature-post -->
									<?php 
									break;
									case 'video':	
									$video = themesflat_meta('video_url');
									if ( !$video ) 
										break;
									global $_wp_additional_image_sizes;
									$video_size = array( 
										'height' => $_wp_additional_image_sizes[$themesflat_thumbnail]['height'],
										'width' => $_wp_additional_image_sizes[$themesflat_thumbnail]['width']
										);

										if ( has_post_thumbnail() ) {
											?>
											<div class="themesflat_video_embed">
												<?php
							    				if ( $image_crop == 'custom' && $image_size != '' ) {
			  										echo wpb_getImageBySize(
				  											array(
				  												'attach_id' => get_post_thumbnail_id(get_the_ID()),
				  												'thumb_size' => $image_size
				  											)
				  										)['thumbnail'];
							    				} else {
											    	$img_size = 'full';
													if ( $image_crop == 'square' ) $img_size = 'themesflat_sc_vc-square';
													if ( $image_crop == 'rectangle' ) $img_size = 'themesflat_sc_vc-rectangle';
													if ( $image_crop == 'rectangle2' ) $img_size = 'themesflat_sc_vc-rectangle2';

													echo get_the_post_thumbnail( get_the_ID(), $img_size );
												}
												?>
												<div class="video-video-box-overlay">
													<div class="video-video-box-button-sm">					
														<button class="video-video-play-icon" data-izimodal-open="#format-video">
															<i class="fa fa-play"></i>
														</button>
													</div>					
												</div>
											</div>
											<div class="izimodal" id="format-video" data-izimodal-width="850px" data-iziModal-fullscreen="true">
											    <iframe height="430" src="<?php echo esc_url($video) ?>" class="full-width shadow-primary" style="width: 100%; max-width: 100%;"></iframe>
											</div>
												<?php
						    			}
									break;
									default:

									?>							

									<div class="featured-post">			    				
						    			<?php 
						    			if ( has_post_thumbnail() ) {
						    				if ( $image_crop == 'custom' && $image_size != '' ) {
		  										echo wpb_getImageBySize(
			  											array(
			  												'attach_id' => get_post_thumbnail_id(get_the_ID()),
			  												'thumb_size' => $image_size
			  											)
			  										)['thumbnail'];
						    				} else {
										    	$img_size = 'full';
												if ( $image_crop == 'square' ) $img_size = 'themesflat_sc_vc-square';
												if ( $image_crop == 'rectangle' ) $img_size = 'themesflat_sc_vc-rectangle';
												if ( $image_crop == 'rectangle2' ) $img_size = 'themesflat_sc_vc-rectangle2';

												echo get_the_post_thumbnail( get_the_ID(), $img_size );
											}
						    			}							    			
						    			?>				    			
					    			</div>
									<?php
								}
							}
			    			?>
							<!-- Content Post -->
			                <div class="content-post" style="<?php echo esc_attr( $content_css ); ?>">
								<?php

								if ($layout != 'blog-grid-s1') {								
									if ( get_the_title() ) {
										echo '<h3 class="entry-title" style="'. esc_attr( $heading_css ) .'"><a href="'. esc_url( get_the_permalink() ) .'">'. get_the_title() .'</a></h3>';
									}
								}

								if( $layout == 'blog-grid' ){
									if ( empty( $hide_meta ) ) {
										?>
										<div class="entry-meta" style="<?php echo esc_attr( $meta_css ); ?>"><?php themesflat_posted_on($layout); ?></div>
										<?php
									}
								}

								if ( $excerpt_lenght ) {
									echo '<p class="entry-content" style="'. esc_attr( $desc_css ) .'">'. wp_trim_words( get_the_content(), $excerpt_lenght, '&hellip;' ) .'</p>';
								}

								if ( $layout == 'blog-grid-s2' || $layout == 'blog-list-small' ): ?>
								<div class="post-date">
									<?php
									$archive_year  = get_the_time('Y'); 
									$archive_month = get_the_time('m'); 
									$archive_day   = get_the_time('d'); 
									?>
									Posted <a href="<?php echo get_day_link( $archive_year, $archive_month, $archive_day); ?>"><?php echo get_the_date();?></a>			
								</div>
								<?php endif; 

								if ( $button_text ) {
									?>
									<div class="wrap-meta-button">
										<?php 
											if( $layout == 'blog-grid-simple' ){
												if ( empty( $hide_meta ) ) {
													?>
													<div class="entry-meta"><?php themesflat_posted_on($layout); ?></div>
													<?php
												}
											}
										?>
										<div class="themesflat-button-container">
											<a href="<?php echo esc_url( get_permalink() ) ?>" class="<?php echo esc_attr( $button_cls ) ?>" style="<?php echo esc_attr( $button_css ); ?>"> <?php echo esc_html( $button_text ); echo $button_icon_html; ?> </a>
										</div>
									</div>
									<?php
								}
								?>
			                </div><!-- /.content-post -->
			            </div>
			        </article><!-- /.news-item -->
				</div>
				<?php endwhile; ?>			
			</div><!-- /.owl-carousel -->
		</div>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
	</div><!-- /.themesflat_sc_vc-news -->
	<?php
	$return = ob_get_clean();
	return $return;
}