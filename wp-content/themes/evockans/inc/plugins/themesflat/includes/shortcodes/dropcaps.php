<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_dropcaps_shortcode_params' );

function themesflat_dropcaps_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Dropcaps', 'evockans'),
        'description' => esc_html__('Displaying Dropcaps.', 'evockans'),
        'base' => 'dropcaps',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'params' => array(
        	array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'evockans' ),
				'param_name' => 'style',
				'value'      => array(
					'Style 1' 	=> 'dropcap-1',
					'Style 2' 	=> 'dropcap-2',
					'Style 3' 	=> 'dropcap-3',
					'Style 4' 	=> 'dropcap-4',
					'Style 5' 	=> 'dropcap-5',
					'Custom' 	=> 'custom',
				),
				'std'		=> 'dropcap-1',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Dropcaps Size', 'evockans' ),
				'param_name' => 'size',
				'value'      => array(
					'Dropcap size sm' 	=> 'dropcap-sm',
					'Dropcap size md' 	=> 'dropcap-md',
					'Dropcap size lg' 	=> 'dropcap-lg',
				),
				'std'		=> 'dropcap-sm',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Dropcaps Content Size', 'evockans' ),
				'param_name' => 'content_size',
				'value'      => array(
					'Content size small' 	=> 'text-content-small',
					'Content size medium' 	=> 'text-content',
					'Content size big' 	=> 'text-content-big',
				),
				'std'		=> 'text-content-big',
			),
	        // Text
			array(
				'type' 		=> 'textarea',
				'heading' 	=> esc_html__('Text', 'evockans'),
				'param_name' 	=> 'text',
				'value' 		=> '',
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Text Color', 'evockans'),
				'param_name' => 'text_color',
				'value' => '',				
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
            ),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('First Letter Text Color', 'evockans'),
				'param_name' => 'first_letter_color',
				'value' => '',
				'group' => esc_html__( 'First Letter Text', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
            ),                
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('First Letter Text Background Color', 'evockans'),
				'param_name' => 'first_letter_background_color',
				'value' => '',
				'group' => esc_html__( 'First Letter Text', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
            ),   
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Rounded', 'evockans'),
				'param_name' => 'rounded',
				'value' => '',
				'description'	=> esc_html__('Ex: 5px', 'evockans'),
				'group' => esc_html__( 'First Letter Text', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
            ), 
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Border Width', 'evockans'),
				'param_name' => 'border_width',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
				'group' => esc_html__( 'First Letter Text', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Border Color', 'evockans'),
				'param_name' => 'border_color',
				'value' => '',
				'group' => esc_html__( 'First Letter Text', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
            ), 
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Padding', 'evockans'),
				'param_name' => 'padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 12px 12px 12px 12px', 'evockans'),
				'group' => esc_html__( 'First Letter Text', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
	        ), 
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Margin', 'evockans'),
				'param_name' => 'margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 15px 0px 0px', 'evockans'),
				'group' => esc_html__( 'First Letter Text', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
	        ),        
			// Typography
			array(
				'type' => 'headings',
				'text' => esc_html__('Setting First Letter', 'evockans'),
				'param_name' => 'setting_typography_first_letter',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'class' => '',
			),  
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'First Letter: Font Family', 'evockans' ),
				'param_name' => 'first_letter_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'First Letter: Font Weight', 'evockans' ),
				'param_name' => 'first_letter_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('First Letter: Font Size', 'evockans'),
				'param_name' => 'first_letter_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('First Letter: Line Height', 'evockans'),
				'param_name' => 'first_letter_line_height',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
	        ),
	        array(
				'type' => 'headings',
				'text' => esc_html__('Setting Text', 'evockans'),
				'param_name' => 'setting_typography_text',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'class' => '',
			),  
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Text: Font Family', 'evockans' ),
				'param_name' => 'text_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Text: Font Weight', 'evockans' ),
				'param_name' => 'text_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Text: Font Size', 'evockans'),
				'param_name' => 'text_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Text: Line Height', 'evockans'),
				'param_name' => 'text_line_height',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
	        ),
        )
    ) );
}

add_shortcode( 'dropcaps', 'themesflat_shortcode_dropcaps' );

/**
 * dropcaps shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_dropcaps( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'style' => 'dropcap-1',
		'size' => 'dropcap-sm',
		'content_size' => 'text-content-big',
	    'text' => '',
	    'first_letter_font_family' => 'Default',
	    'first_letter_font_weight' => 'Default',
	    'first_letter_font_size' => '',
	    'first_letter_line_height' => '',
	    'first_letter_color' => '',
 	    'first_letter_background_color' => '',
 	    'border_color'	=> '',
 	    'border_width'	=> '',
 	    'rounded'	=> '', 	    
 	    'padding'	=> '',
 	    'margin'	=> '',
 	    'text_font_family' => 'Default',
	    'text_font_weight' => 'Default',
	    'text_font_size' => '',
	    'text_line_height' => '',
	    'text_color' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'dropcaps', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$cls = $css = $content_text = $css_content = $css_inline = '';	

	if ($style) $cls .=  $style;
	if ($size) $cls .=  ' ' .$size;
	if ($content_size) $cls .=  ' ' .$content_size;

	if ( $text ) $content_text .= sprintf( '<p style="%2$s">%1$s</p>', $text, $css_content );

	

	if ($style == 'custom') {
		$text_font_size = intval($text_font_size);
		$first_letter_font_size = intval($first_letter_font_size);
		$rounded = intval($rounded);

		if ($text_font_family != 'Default') $css_content .= 'font-family:'. $text_font_family .';';
		if ($text_font_weight != 'Default') $css_content .= 'font-weight:'. $text_font_weight .';';
		if ($text_font_size) $css_content .= 'font-size:' . $text_font_size . 'px;';
		if ($text_line_height) $css_content .= 'line-height:' . $text_line_height . ';';
		if ($text_color) $css_content .= 'color:' . $text_color . ';';

		if ($first_letter_color) $css_inline .= 'color:' . $first_letter_color .';';
		if ($first_letter_background_color) $css_inline .= 'background:' . $first_letter_background_color .';';
		if ($first_letter_font_family != 'Default') $css_inline .= 'font-family:'. $first_letter_font_family .';';
		if ($first_letter_font_weight != 'Default') $css_inline .= 'font-weight:'. $first_letter_font_weight .';';		
		if ($first_letter_font_size) $css_inline .= 'font-size:' . $first_letter_font_size . 'px;';
		if ($first_letter_line_height) $css_inline .= 'line-height:' . $first_letter_line_height . ';';
		if ( $border_color && $border_width ) $css_inline .= 'border-style:solid;border-width:'. $border_width .';border-color:'. $border_color .';';
		if ( $rounded ) $css_inline .= 'border-radius:'. $rounded .'px;';
		if ( $padding ) $css_inline .= 'padding:'. $padding .';';
		if ( $margin ) $css_inline .= 'margin:'. $margin .';';

		add_action('wp_head', printf('<style type="text/css">.dropcap-custom p::first-letter { %1$s; }</style>', $css_inline ) , 100);	
		return sprintf( '
			<div class="dropcap dropcap-custom" style="%2$s">
				%1$s
			</div>', $content_text, $css_content );
	}else {
		return sprintf( '
			<div class="%1$s">				
				%2$s
			</div>', $cls, $content_text );
	}
}
