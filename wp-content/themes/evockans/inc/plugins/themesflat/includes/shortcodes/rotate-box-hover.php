<?php
/**
 * Register filter for append custom class name
 * that generated from visual-composer
 */
class WPBakeryShortCode_rotateboxhover extends WPBakeryShortCodesContainer {}
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_rotateboxhover_shortcode_params' );

function themesflat_rotateboxhover_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Rotate Box Hover', 'evockans'),
        'description' => esc_html__('Empty space with custom height.', 'evockans'),
        'base' => 'rotateboxhover',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'as_child' => array( 'only' => 'rotatebox' ),
        'as_parent' => array('except' => 'rotateboxhover'),
        'controls' => 'full',
		'show_settings_on_create' => true,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'js_view' => 'VcColumnView',
        'params' => array(  
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Rounded', 'evockans'),
				'param_name' => 'rounded',
				'value' => '',
				'description'	=> esc_html__('Ex: 5px', 'evockans'),
	        ),
	        array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Padding', 'evockans' ),
				'param_name'  => 'padding',
				'value'	=> '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
			),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Background', 'evockans'),
				'param_name' => 'background',
				'value' => '#2387ea',
            ),
        )
    ) );
}

add_shortcode( 'rotateboxhover', 'themesflat_shortcode_rotateboxhover' );

/**
 * rotateboxhover shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_rotateboxhover( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'rounded' => '',
		'background' => '#2387ea',
		'padding' => '',
	), $atts ) );	
	//extract($atts = vc_map_get_attributes( 'rotateboxhover', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$css = $inner_css = '';

	if ( $padding ) $inner_css .= 'padding:' . $padding . ';';

	$rounded = intval( $rounded );
	if ( $rounded ) $css .= 'border-radius:' . $rounded . 'px;';
	if ( $background ) $css .= 'background-color:' . $background . ';';	


	return sprintf( '
		<div class="back" style="%1$s">
			<div class="inner" style="%3$s">%2$s</div>
		</div>',
		$css,
		do_shortcode($content),
		$inner_css
	);
}