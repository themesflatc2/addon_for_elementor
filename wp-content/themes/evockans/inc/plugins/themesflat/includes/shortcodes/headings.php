<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_headings_shortcode_params' );

function themesflat_headings_shortcode_params() {
	vc_map( array(
		'name'        => esc_html__( 'Headings', 'evockans' ),
        'description' => esc_html__('Displaying awesome heading styles.', 'evockans'),
		'base'        => 'headings',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
		'params'      => array(
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Alignment', 'evockans' ),
				'param_name' => 'alignment',
				'value'      => array(
					'Left' => '',
					'Right' => 'text-right',
					'Center' => 'text-center',
				),
				'std'		=> 'text-center',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Position', 'evockans' ),
				'param_name' => 'heading_position',
				'value'      => array(
					'Top' => 'heading_top',
					'Bottom' => 'heading_bottom',
				),
				'std'		=> 'heading_top',
			),
			 array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Style Input', 'evockans' ),
				'param_name' => 'heading_style_input',
				'value'      => array(
					'Text Area' => 'heading_textarea',
					'Raw HTML' => 'heading_textarea_raw_html',
				),
				'std'		=> 'textarea',
			),
			array(
				'type' => 'textarea',
				'holder' => 'div',
				'heading' => esc_html__( 'Heading Text', 'evockans' ),
				'param_name' => 'heading',
				'dependency' => array( 'element' => 'heading_style_input', 'value' => 'heading_textarea' ),
			),
			array(
				'type' => 'textarea_raw_html',
				'holder' => 'div',
				'heading' => esc_html__( 'Heading Text', 'evockans' ),
				'param_name' => 'heading_raw_html',
				'dependency' => array( 'element' => 'heading_style_input', 'value' => 'heading_textarea_raw_html' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Heading Color', 'evockans'),
				'param_name' => 'heading_color',
				'value' => '',
            ),
			array(
				'type' => 'textarea',
				'holder' => 'div',
				'heading' => esc_html__( 'Sub-Heading (Optional)', 'evockans' ),
				'param_name' => 'subheading',
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Sub-Heading Color', 'evockans'),
				'param_name' => 'subheading_color',
				'value' => '',
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Sub-Heading Max-Width', 'evockans'),
				'param_name' => 'subheading_width',
				'value' => '',
            ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Separator', 'evockans' ),
				'param_name' => 'separator',
				'value'      => array(
					'No Separator' => '',
					'Line' => 'line',
					'Image' => 'image',
				),
				'std'		=> '',
				'group' => esc_html__( 'Separator', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Separator Position', 'evockans' ),
				'param_name' => 'sep_position',
				'value'      => array(
					'Top' => 'top',
					'Between Heading & Sub-Heading' => 'between',
					'Bottom' => 'bottom',
					'Left' => 'left',
					'Right' => 'right',
				),
				'std'		=> 'between',
				'group' => esc_html__( 'Separator', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => array( 'line', 'image' ) ),
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line Width', 'evockans'),
				'param_name' => 'line_width',
				'value' => '100px',
				'description'	=> esc_html__('Ex: 100% or 100px', 'evockans'),
				'group' => esc_html__( 'Separator', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line Height', 'evockans'),
				'param_name' => 'line_height',
				'value' => '3',
				'group' => esc_html__( 'Separator', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line: Right Margin', 'evockans'),
				'param_name' => 'line_right_margin',
				'value' => '100',
				'group' => esc_html__( 'Separator', 'evockans' ),
				'dependency' => array( 'element' => 'sep_position', 'value' => 'left' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line: Left Margin', 'evockans'),
				'param_name' => 'line_left_margin',
				'value' => '100',
				'group' => esc_html__( 'Separator', 'evockans' ),
				'dependency' => array( 'element' => 'sep_position', 'value' => 'right' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Line Color', 'evockans'),
				'param_name' => 'line_color',
				'value' => '#2387ea',
				'group' => esc_html__( 'Separator', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
            ),
	        // Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image', 'evockans'),
				'param_name' => 'image',
				'value' => '',
				'group' => esc_html__( 'Separator', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'image' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Image Width (Optional)', 'evockans'),
				'param_name' => 'image_width',
				'value' => '',
				'group' => esc_html__( 'Separator', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'image' ),
	        ),
	        // Typography
			array(
				'type' => 'headings',
				'text' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Tag', 'evockans' ),
				'param_name' => 'tag',
				'value'      => array(
					'H1' => 'h1',
					'H2' => 'h2',
					'H3' => 'h3',
					'H4' => 'h4',
					'H5' => 'h5',
					'H6' => 'h6',
				),
				'std'		=> 'h2',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading: Font Family', 'evockans' ),
				'param_name' => 'heading_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading: Font Weight', 'evockans' ),
				'param_name' => 'heading_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'100' => '100',
					'200' => '200',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Font Size', 'evockans'),
				'param_name' => 'heading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Font Size on Mobile', 'evockans'),
				'param_name' => 'heading_font_size_mobile',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Line Height', 'evockans'),
				'param_name' => 'heading_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Sub-Heading', 'evockans'),
				'param_name' => 'subheading_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Sub-Heading: Font Family', 'evockans' ),
				'param_name' => 'subheading_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Sub-Heading: Font Weight', 'evockans' ),
				'param_name' => 'subheading_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Sub-Heading: Font Size', 'evockans'),
				'param_name' => 'subheading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Sub-Heading: Line Height', 'evockans'),
				'param_name' => 'subheading_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Sub-Heading: Font Style', 'evockans' ),
				'param_name' => 'subheading_font_style',
				'value'      => array(
					'Normal' => 'normal',
					'Italic' => 'italic',
				),
				'std'		=> 'normal',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        // Spacing
	        array(
				'type' => 'headings',
				'text' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading_spacing',
				'group' => esc_html__( 'Spacing', 'evockans' ),
				'class' => '',
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Top Margin', 'evockans'),
				'param_name' => 'heading_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Bottom Margin', 'evockans'),
				'param_name' => 'heading_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Letter Spacing', 'evockans'),
				'param_name' => 'heading_letter_spacing',
				'value' => '',
				'description'	=> esc_html__('Ex: 1px', 'evockans'),
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Word Spacing', 'evockans'),
				'param_name' => 'heading_word_spacing',
				'value' => '',
				'description'	=> esc_html__('Ex: 1px', 'evockans'),
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'headings',
				'text' => esc_html__('Sub-Heading', 'evockans'),
				'param_name' => 'subheading_spacing',
				'group' => esc_html__( 'Spacing', 'evockans' ),
				'class' => '',
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Sub-Heading: Top Margin', 'evockans'),
				'param_name' => 'subheading_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Sub-Heading: Bottom Margin', 'evockans'),
				'param_name' => 'subheading_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Sub-Heading: Letter Spacing', 'evockans'),
				'param_name' => 'subheading_letter_spacing',
				'value' => '',
				'description'	=> esc_html__('Ex: 1px', 'evockans'),
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Sub-Heading: Word Spacing', 'evockans'),
				'param_name' => 'subheading_word_spacing',
				'value' => '',
				'description'	=> esc_html__('Ex: 1px', 'evockans'),
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Extra class name', 'evockans'),
				'param_name' => 'class',
				'value' => '',
				'group' => esc_html__( 'Class', 'evockans' ),
	        ),
		)
	) );
}

add_shortcode( 'headings', 'themesflat_shortcode_headings' );

/**
 * headings shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_headings( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'alignment' => 'text-center',
		'heading_position' => 'heading_top',
		'heading_style_input' => 'heading_textarea',		
	    'heading' => '',
	    'heading_raw_html' => '',
	    'heading_color' => '',
	    'subheading' => '',
	    'subheading_color' => '',
	    'subheading_width' => '',
		'separator' => '',
		'sep_position' => 'between',
		'line_width' => '100px',
		'line_height' => '3',
		'line_right_margin' => '100',
		'line_left_margin' => '100',
		'line_color' => '#2387ea',
		'image' => '',
		'image_width' => '',
		'tag' => 'h2',
		'heading_font_family' => 'Default',
		'heading_font_weight' => 'Default',
		'heading_font_size' => '',
	    'heading_font_size_mobile' => '',
		'heading_line_height' => '',
		'subheading_font_family' => 'Default',
		'subheading_font_weight' => 'Default',
		'subheading_font_size' => '',
		'subheading_line_height' => '',
		'subheading_font_style' => 'normal',
		'heading_top_margin' => '',
		'heading_bottom_margin' => '',
		'heading_letter_spacing' => '',
		'heading_word_spacing' => '',
		'subheading_top_margin' => '',
		'subheading_bottom_margin' => '',
		'subheading_letter_spacing' => '',
		'subheading_word_spacing' => '',
		'class' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'headings', $atts ));

	$image_width = intval( $image_width );
	$subheading_width = intval( $subheading_width );
	$line_height = intval( $line_height );
	$line_right_margin = intval( $line_right_margin );

	$heading_font_size = intval( $heading_font_size );
	$heading_font_size_mobile = intval( $heading_font_size_mobile );
	$subheading_font_size = intval( $subheading_font_size );

	$heading_top_margin = intval( $heading_top_margin );
	$heading_bottom_margin = intval( $heading_bottom_margin );
	$subheading_top_margin = intval( $subheading_top_margin );
	$subheading_bottom_margin = intval( $subheading_bottom_margin );
	$heading_letter_spacing = intval( $heading_letter_spacing );
	$heading_word_spacing = intval( $heading_word_spacing );

	$cls = $alignment;
	$css = $data = $heading_css = $subheading_css = '';
	$subheading_css .= 'font-style:'. $subheading_font_style .';';

	if ( $class ) {
		$cls .= ' ' . $class;
	}

	if ( $heading_font_weight != 'Default' ) $heading_css .= 'font-weight:'. $heading_font_weight .';';
	if ( $heading_color ) $heading_css .= 'color:'. $heading_color .';';
	if ( $heading_line_height ) $heading_css .= 'line-height:'. $heading_line_height .';';
	if ( $heading_top_margin ) $heading_css .= 'margin-top:'. $heading_top_margin .'px;';
	if ( $heading_bottom_margin ) $heading_css .= 'margin-bottom:'. $heading_bottom_margin .'px;';
	if ( $heading_letter_spacing ) $heading_css .= 'letter-spacing:'. $heading_letter_spacing .'px;';
	if ( $heading_word_spacing ) $heading_css .= 'word-spacing:'. $heading_word_spacing .'px;';
	if ( $heading_font_family != 'Default' ) {
		$heading_css .= 'font-family:'. $heading_font_family .';';
	}

	if ( $heading_font_size ) {
		$heading_css .= 'font-size:'. $heading_font_size .'px;';
	    $data .= 'data-font='. $heading_font_size;
	}
	if ( $heading_font_size_mobile ) $data .= ' data-mfont='. $heading_font_size_mobile;

	if ( $subheading_font_weight != 'Default' ) $subheading_css .= 'font-weight:'. $subheading_font_weight .';';
	if ( $subheading_color ) $subheading_css .= 'color:'. $subheading_color .';';
	if ( $subheading_font_size ) $subheading_css .= 'font-size:'. $subheading_font_size .'px;';
	if ( $subheading_line_height ) $subheading_css .= 'line-height:'. $subheading_line_height .';';
	if ( $subheading_top_margin ) $subheading_css .= 'margin-top:'. $subheading_top_margin .'px;';
	if ( $subheading_bottom_margin ) $subheading_css .= 'margin-bottom:'. $subheading_bottom_margin .'px;';
	if ( $subheading_letter_spacing ) $subheading_css .= 'letter-spacing:'. $subheading_letter_spacing .';';
	if ( $subheading_word_spacing ) $subheading_css .= 'word-spacing:'. $subheading_word_spacing .';';
	if ( $subheading_width ) $subheading_css .= 'max-width:'. $subheading_width .'px; margin-left: auto; margin-right: auto;';
	if ( $subheading_font_family != 'Default' ) {
		$subheading_css .= 'font-family:'. $subheading_font_family .';';
	}

	if ($heading_style_input == 'heading_textarea_raw_html') {
		$heading = rawurldecode( base64_decode( $heading_raw_html ) );
	}

	$html = $heading_html = $sub_html = $sep_html = '';
	if ( $heading ) $heading_html .= sprintf(
		'<%3$s class="heading clearfix" style="%2$s">
			%1$s
		</%3$s>',
		$heading,
		$heading_css,
		$tag
	);

	if ( $subheading ) $sub_html .= sprintf(
		'<p class="sub-heading clearfix" style="%2$s">
			%1$s
		</p>',
		$subheading,
		$subheading_css
	);

	$line_cls = $line_css = $image_css = '';
	if ( $separator == 'line' ) {
		if ( empty( $line_width ) ) $line_width = '100px';
		if ( empty( $line_height ) ) $line_height = 3;

		if ( $line_width ) { $line_css .= 'width:'. $line_width .';'; } 

		$line_css .= 'height:'. $line_height .'px;';

		if ( $line_color == '#1a7dd7' ) {
			$line_cls .= 'accent';
		} else {
			if ( $line_color ) $line_css .= 'background-color:'. $line_color .';';
		}

		$sep_html .= sprintf( '<div class="sep %2$s clearfix" style="%1$s"></div>', $line_css, $line_cls );
	}

	if ( $separator == 'image' ) {
		if ( $image_width ) $image_css = 'width:'. $image_width .'px;';

		if ( $image )
		$sep_html = sprintf(
			'<div class="sep clearfix" style="%2$s">
				<img alt="image" src="%1$s">
			</div>',
			wp_get_attachment_image_src( $image, 'full' )[0],
			$image_css
		);
	}

	if ( $heading_position == 'heading_bottom' ) {

		if ( $sep_position == 'between' ) {
			$html = $sub_html . $sep_html . $heading_html ;
		} elseif ( $sep_position == 'top' ) {
			$html = $sep_html . $sub_html . $heading_html;
		} else { $html = $sub_html . $heading_html . $sep_html; }

	}else{
		if ( $sep_position == 'between' ) {
			$html = $heading_html . $sep_html . $sub_html;
		} elseif ( $sep_position == 'top' ) {
			$html = $sep_html . $heading_html . $sub_html;
		} else { $html = $heading_html . $sub_html . $sep_html; }
	}

		

	if ( $line_right_margin && $sep_position == 'left' ) {
		$css .= 'padding-left:'. $line_right_margin .'px';
		$cls .= ' left-sep';
	}

	if ( $line_left_margin && $sep_position == 'right' ) {
		$css .= 'padding-right:'. $line_left_margin .'px';
		$cls .= ' right-sep';
	}

	return sprintf( '<div class="themesflat_sc_vc-headings clearfix %2$s" %3$s style="%4$s">%1$s</div>',
		$html,
		$cls,
		$data,
		$css
	);
}