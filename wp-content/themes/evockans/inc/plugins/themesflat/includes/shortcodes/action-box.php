<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_actionbox_shortcode_params' );

function themesflat_actionbox_shortcode_params() {
	vc_map( array(
	    'name' => esc_html__('Action Box', 'evockans'),
	    'description' => esc_html__('Displaying Action-Box or Promo-Box.', 'evockans'),
	    'base' => 'actionbox',
		'weight'	=>	180,
	    'icon' => THEMESFLAT_ICON,
	    'category' => esc_html__('THEMESFLAT', 'evockans'),
	    'params' => array(
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap Padding', 'evockans'),
				'param_name' => 'padding',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
	        ),
	        array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'evockans' ),
				'param_name' => 'style',
				'value'      => array(
					'Default'		=> '',
					'Center'		=> 'style-center',
				)
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap Rounded', 'evockans'),
				'param_name' => 'rounded',
				'value' => '',
				'description'	=> esc_html__('ex: 10px', 'evockans'),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Wrap Background Color', 'evockans'),
				'param_name' => 'background',
				'value' => '',
            ),
            // Content
			array(
				'type' => 'headings',
				'text' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading_content',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Tag', 'evockans' ),
				'param_name' => 'heading_tag',
				'value'      => array(
					'H1' => 'h1',
					'H2' => 'h2',
					'H3' => 'h3',
					'H4' => 'h4',
					'H5' => 'h5',
					'H6' => 'h6',
				),
				'std'		=> 'h2',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
            array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Heading Text', 'evockans'),
				'param_name' => 'heading_text',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Heading Color', 'evockans'),
				'param_name' => 'heading_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
			// Icon
			array(
				'type'       => 'checkbox',
				'heading'    => esc_html__( 'Show Heading Icon?', 'evockans' ),
				'param_name' => 'show_icon',
				'value'      => array( esc_html__( 'Yes, please.', 'evockans' ) => 'yes' ),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'evockans' ),
				'param_name' => 'icon_type',
				'description' => esc_html__( 'Select icon library.', 'evockans' ),
				'value' => array(
					esc_html__( 'None', 'evockans' ) => '',
					esc_html__( 'Mono Social', 'evockans' ) => 'monosocial',
					esc_html__( 'Simple Line', 'evockans' ) => 'simpleline',
					esc_html__( 'Font Ionicons', 'evockans' ) => 'ionicons',
					esc_html__( 'Font Elegant', 'evockans' ) => 'eleganticons',
					esc_html__( 'Font Themify Icons', 'evockans' ) => 'themifyicons',
					esc_html__( 'Font Icomoon Icons', 'evockans' ) => 'icomoon',
					esc_html__( 'FontAwesome', 'evockans' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'evockans' ) => 'openiconic',
					esc_html__( 'Typicons', 'evockans' ) => 'typicons',
					esc_html__( 'Entypo', 'evockans' ) => 'entypo',
					esc_html__( 'Linecons', 'evockans' ) => 'linecons',
				),
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_monosocial',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'monosocial',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'monosocial',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_simpleline',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'simpleline',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'simpleline',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_ionicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'ionicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'ionicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_eleganticons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'eleganticons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'eleganticons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_themifyicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'themifyicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'themifyicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_icomoon',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'icomoon',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'icomoon',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon',
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'fontawesome',
				),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_openiconic',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'openiconic',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'openiconic',
				),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_typicons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'typicons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'typicons',
				),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_entypo',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'entypo',
					'iconsPerPage' => 300,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'entypo',
				),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_linecons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'linecons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'linecons',
				),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon Color', 'evockans'),
				'param_name' => 'icon_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Font Size', 'evockans'),
				'param_name' => 'icon_font_size',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon: Right Padding', 'evockans'),
				'param_name' => 'icon_right_padding',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
				'description'	=> esc_html__('Spacing between the icon and the heading. Default: 20px.', 'evockans'),
				'dependency' => array( 'element' => 'show_icon', 'value' => 'yes' ),
	        ),
			// Sub-Heading
			array(
				'type' => 'headings',
				'text' => esc_html__('Sub-Heading', 'evockans'),
				'param_name' => 'subheading_content',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'textarea',
				'holder' => 'div',
				'heading' => esc_html__( 'Sub-Heading (Optional)', 'evockans' ),
				'param_name' => 'subheading_text',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Sub-Heading Color', 'evockans'),
				'param_name' => 'subheading_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
            // Button
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Size', 'evockans' ),
				'param_name' => 'button_size',
				'value'      => array(
					'Medium' => '',
					'Small' => 'small',
					'Big' => 'big',
				),
				'std'		=> '',
				'group' => esc_html__( 'Button', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Style', 'evockans' ),
				'param_name' => 'button_style',
				'value'      => array(
					'Accent' => 'accent',
					'Dark' => 'dark',
					'Light' => 'light',
					'Very Light' => 'very-light',
					'White' => 'white',
					'Outline' => 'outline',
					'Outline Dark' => 'outline_dark',
					'Outline Light' => 'outline_light',
					'Outline Very light' => 'outline_very-light',
					'Outline White' => 'outline_white',
				),
				'std'		=> '',
				'group' => esc_html__( 'Button', 'evockans' ),
			),
            array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Button Text', 'evockans'),
				'param_name' => 'button_text',
				'value' => 'READ MORE',
				'group' => esc_html__( 'Button', 'evockans' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Rounded', 'evockans'),
				'param_name' => 'button_rounded',
				'value' => '',
				'description'	=> esc_html__('ex: 10px', 'evockans'),
				'group' => esc_html__( 'Button', 'evockans' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Button link (URL):', 'evockans'),
				'param_name' => 'button_url',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
            ),
            array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Button Padding', 'evockans'),
				'param_name' => 'button_padding',
				'value' => '',
				'group' => esc_html__( 'Button', 'evockans' ),
            ),
            // Typography
			array(
				'type' => 'headings',
				'text' => esc_html__('Heading', 'evockans'),
				'param_name' => 'heading_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Font Family', 'evockans' ),
				'param_name' => 'heading_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Font Weight', 'evockans' ),
				'param_name' => 'heading_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Font Size', 'evockans'),
				'param_name' => 'heading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading Line-Height', 'evockans'),
				'description' => esc_html('ex: 20px or 1.3 or 150%', 'evockans'),
				'param_name' => 'heading_line_height',				
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Sub-Heading', 'evockans'),
				'param_name' => 'subheading_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Sub-Heading Font Family', 'evockans' ),
				'param_name' => 'subheading_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Sub-Heading Font Weight', 'evockans' ),
				'param_name' => 'subheading_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Sub-Heading Font Size', 'evockans'),
				'param_name' => 'subheading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Sub-Heading Line-Height', 'evockans'),
				'description' => esc_html('ex: 20px or 1.3 or 150%', 'evockans'),
				'param_name' => 'subheading_line_height',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Button', 'evockans'),
				'param_name' => 'button_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Font Family', 'evockans' ),
				'param_name' => 'button_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Button Font Weight', 'evockans' ),
				'param_name' => 'button_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Font Size', 'evockans'),
				'param_name' => 'button_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button Line-Height', 'evockans'),
				'description' => esc_html('ex: 20px or 1.3 or 150%', 'evockans'),
				'param_name' => 'button_line_height',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Top Margin', 'evockans'),
				'param_name' => 'heading_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Bottom Margin', 'evockans'),
				'param_name' => 'heading_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button: Top Margin', 'evockans'),
				'param_name' => 'button_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Button: Bottom Margin', 'evockans'),
				'param_name' => 'button_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Letter Spacing', 'evockans'),
				'param_name' => 'heading_letter_spacing',
				'value' => '-2px',
				'description'	=> esc_html__('Ex: 1px', 'evockans'),
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Word Spacing', 'evockans'),
				'param_name' => 'heading_word_spacing',
				'value' => '2px',
				'description'	=> esc_html__('Ex: 1px', 'evockans'),
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        // Box Shadow
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Horizontal (Required)', 'evockans'),
				'param_name' => 'horizontal',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Vertical (Required)', 'evockans'),
				'param_name' => 'vertical',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 20px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Blur (Required)', 'evockans'),
				'param_name' => 'blur',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 40px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Spread (Required)', 'evockans'),
				'param_name' => 'spread',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Box Shadow Color (Required)', 'evockans'),
				'param_name' => 'shadow_color',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: #eeeeee', 'evockans'),
            ),
	    )
	) );
}

add_shortcode( 'actionbox', 'themesflat_shortcode_actionbox' );

/**
 * actionbox shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_actionbox( $atts, $content = null ) {	
	extract( shortcode_atts( array(
		'heading_text' => '',
		'style'	=> '',
		'rounded' => '',
		'heading_color' => '',
		'heading_tag' => 'h2',
		'subheading_text' => '',
		'subheading_color' => '',
		'padding' => '',
		'background' => '',
		'button_size' => '',
		'button_style' => 'accent',
		'button_rounded' => '',
		'button_text' => 'READ MORE',
		'button_url' => '',
		'button_padding' => '',
		'heading_font_family' => 'Default',
		'heading_font_weight' => 'Default',
		'heading_font_size' => '',
		'heading_line_height' => '',
		'subheading_font_family' => 'Default',
		'subheading_font_weight' => 'Default',
		'subheading_font_size' => '',
		'subheading_line_height' => '',
		'button_font_family' => 'Default',
		'button_font_weight' => 'Default',
		'button_font_size' => '',
		'button_line_height' => '',
		'heading_top_margin' => '',
		'heading_bottom_margin' => '',
		'heading_letter_spacing' => '-2px',
		'heading_word_spacing' => '2px',
		'button_top_margin' => '',
		'button_bottom_margin' => '',
		'show_icon' => '',
		'icon_type' => '',
		'icon' => '',
		'icon_color' => '',
		'icon_font_size' => '',
		'icon_right_padding' => '',
		'horizontal' => '',
        'vertical' => '',
        'blur' => '',
        'spread' => '',
        'shadow_color' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'actionbox', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$heading_font_size = intval( $heading_font_size );
	$subheading_font_size = intval( $subheading_font_size );
	$icon_font_size = intval( $icon_font_size );
	$icon_right_padding = intval( $icon_right_padding );
	$button_font_size = intval( $button_font_size );
	$heading_top_margin = intval( $heading_top_margin );
	$heading_bottom_margin = intval( $heading_bottom_margin );
	$button_top_margin = intval( $button_top_margin );
	$button_bottom_margin = intval( $button_bottom_margin );
	$button_rounded = intval( $button_rounded );
	$heading_letter_spacing = intval( $heading_letter_spacing );
	$heading_word_spacing = intval( $heading_word_spacing );

	$heading_wrap_cls = '';
	$button_wrap_cls = '';

	$cls = $css = $heading_css = $subheading_css = $button_css = '';
	$icon_wrap_css = $icon_cls = $icon_css = $sub_html = $html = $icon_html = '';

	if ( $horizontal && $vertical && $blur && $spread && $shadow_color )
    	$css .= 'box-shadow:'. $horizontal .' '. $vertical .' '. $blur .' '. $spread .' '. $shadow_color .';';
	if ( $padding ) $css .= 'padding:'. $padding .';';
	if ( $rounded ) $css .= 'border-radius:'. $rounded .';';
	if ( $background == '#1a7dd7') { $cls .= ' accent'; }
	else { if ( $background ) $css .= 'background-color:'. $background .';'; }

	if ( $heading_font_weight != 'Default' ) $heading_css .= 'font-weight:'. $heading_font_weight .';';
	if ( $heading_color ) $heading_css .= 'color:'. $heading_color .';';
	if ( $heading_font_size ) $heading_css .= 'font-size:'. $heading_font_size .'px;';
	if ( $heading_line_height ) $heading_css .= 'line-height:'. $heading_line_height .';';
	if ( $heading_top_margin ) $heading_css .= 'margin-top:'. $heading_top_margin .'px;';
	if ( $heading_bottom_margin ) $heading_css .= 'margin-bottom:'. $heading_bottom_margin .'px;';
	if ( $heading_letter_spacing ) $heading_css .= 'letter-spacing:'. $heading_letter_spacing .'px;';
	if ( $heading_word_spacing ) $heading_css .= 'word-spacing:'. $heading_word_spacing .'px;';
	if ( $heading_font_family != 'Default' ) {
		$heading_css .= 'font-family:'. $heading_font_family .';';
	}

	if ( $subheading_font_weight != 'Default' ) $subheading_css .= 'font-weight:'. $subheading_font_weight .';';
	if ( $subheading_color ) $subheading_css .= 'color:'. $subheading_color .';';
	if ( $subheading_font_size ) $subheading_css .= 'font-size:'. $subheading_font_size .'px;';
	if ( $subheading_line_height ) $subheading_css .= 'line-height:'. $subheading_line_height .';';
	if ( $subheading_font_family != 'Default' ) {
		$subheading_css .= 'font-family:'. $subheading_font_family .';';
	}

	if ( $button_font_weight != 'Default' ) $button_css .= 'font-weight:'. $button_font_weight .';';
	if ( $button_font_size ) $button_css .= 'font-size:'. $button_font_size .'px;';
	if ( $button_rounded ) $button_css .= 'border-radius:'. $button_rounded .'px;';
	if ( $button_line_height ) $button_css .= 'line-height:'. $button_line_height .';';
	if ( $button_top_margin ) $button_css .= 'margin-top:'. $button_top_margin .'px;';
	if ( $button_bottom_margin ) $button_css .= 'margin-bottom:'. $button_bottom_margin .'px;';
	if ( $button_padding ) $button_css .= 'padding:'. $button_padding .';';
	if ( $button_font_family != 'Default' ) {
		$button_css .= 'font-family:'. $button_font_family .';';
	}

	$button_cls = $button_size;
	if ( $button_style == 'accent' ) $button_cls .= ' themesflat_sc_vc-button accent';
	if ( $button_style == 'dark' ) $button_cls .= ' themesflat_sc_vc-button dark';
	if ( $button_style == 'light' ) $button_cls .= ' themesflat_sc_vc-button light';
	if ( $button_style == 'very-light' ) $button_cls .= ' themesflat_sc_vc-button very-light';
	if ( $button_style == 'white' ) $button_cls .= ' themesflat_sc_vc-button white';
	if ( $button_style == 'outline' ) $button_cls .= ' themesflat_sc_vc-button outline ol-accent';
	if ( $button_style == 'outline_dark' ) $button_cls .= ' themesflat_sc_vc-button outline dark';
	if ( $button_style == 'outline_light' ) $button_cls .= ' themesflat_sc_vc-button outline light';
	if ( $button_style == 'outline_very-light' ) $button_cls .= ' themesflat_sc_vc-button outline very-light';
	if ( $button_style == 'outline_white' ) $button_cls .= '  themesflat_sc_vc-button outline white';

	if ( $show_icon ) {
		$icon = themesflat_sc_vc_get_icon_class( $atts, 'icon' );
		if ( $icon && $icon_type != '' ) {
			$cls .= ' has-icon';
			vc_icon_element_fonts_enqueue( $icon_type );

			if ( $icon_font_size ) $icon_css .= 'font-size:'. $icon_font_size .'px;';
			
			if ( $icon_right_padding ) $icon_wrap_css .= 'padding-left:'. $icon_right_padding .'px;';

			if ( $icon_color == '#1a7dd7' ) $icon_cls .= ' accent';
			else { if ( $icon_color ) $icon_css .= 'color:'. $icon_color .';'; }

			$icon_html = sprintf('<span class="icon %3$s" style="%2$s"><i class="%1$s"></i></span>', $icon, $icon_css, $icon_cls );
		}
	}

	if ( $subheading_text ) {
		$sub_html .= sprintf('
			<div class="sub-heading" style="%1$s">
				%2$s
			</div>',
			$subheading_css,
			$subheading_text
		);	
	}

	if ( $heading_text ) {
		$html .= sprintf('
			<div class="heading-wrap %1$s">
				<div class="text-wrap" style="%6$s">
					%5$s
					<%4$s class="heading" style="%2$s">
						%3$s
					</%4$s>
					%7$s
				</div>
			</div>',
			$heading_wrap_cls,
			$heading_css,
			$heading_text,
			$heading_tag,
			$icon_html,
			$icon_wrap_css,
			$sub_html
		);
	}

	$button_link = $button_url ? $button_url : '';
	if ( $button_text ) {
		$html .= sprintf('
			<div class="button-wrap %1$s">
				<a href="%5$s" class="%2$s" style="%3$s">
					%4$s
				</a>
			</div>',
			$button_wrap_cls,
			$button_cls,
			$button_css,
			esc_html( $button_text ),
			$button_link
		);
	}

	return sprintf(
		'<div class="themesflat_sc_vc-action-box %3$s %4$s" style="%2$s">
			<div class="inner">%s</div>
		</div>', $html, $css, $cls, $style );
	
}