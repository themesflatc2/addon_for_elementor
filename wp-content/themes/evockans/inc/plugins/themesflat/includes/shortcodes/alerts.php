<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_alerts_shortcode_params' );

function themesflat_alerts_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Alerts', 'evockans'),
        'description' => esc_html__('Displaying Alerts.', 'evockans'),
        'base' => 'alerts',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'params' => array(			
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'evockans' ),
				'param_name' => 'style',
				'value'      => array(
					'Alert Success' 	=> 'alert-success',
					'Alert Info' 		=> 'alert-info',
					'Alert Warning' 	=> 'alert-warning',
					'Alert Danger' 		=> 'alert-danger',
					'Alert Primary' 	=> 'alert-primary',
					'Alert Secondary' 	=> 'alert-secondary',
					'Alert Dark' 		=> 'alert-dark',
					'Alert Light' 		=> 'alert-light',
				),
			),			
			array(
				'type' => 'textfield',
				'holder' => 'strong',
				'heading' => esc_html__('Status', 'evockans'),
				'param_name' => 'status_enter',
				'value' => '',
	        ),
	        array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Text', 'evockans'),
				'param_name' => 'text_enter',
				'value' => '',
	        ),	
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Status And Text: Color', 'evockans'),
				'param_name' => 'text_color',
				'value' => '',
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Background Color', 'evockans'),
				'param_name' => 'background_color',
				'value' => '',
            ),
	        //Typography        
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Font Size', 'evockans'),
				'param_name' => 'font_size',
				'value' => '',
				'std'	=> '1rem',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Line Height', 'evockans'),
				'param_name' => 'line_height',
				'value' => '',
				'std'	=> '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Family', 'evockans' ),
				'param_name' => 'font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Weight', 'evockans' ),
				'param_name' => 'font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'100' => '100',
					'200' => '200',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        // Spacing
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Top Margin', 'evockans'),
				'param_name' => 'top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Bottom Margin', 'evockans'),
				'param_name' => 'bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
        )
    ) );
}

add_shortcode( 'alerts', 'themesflat_shortcode_alerts' );

/**
 * alerts shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_alerts( $atts, $content = null ) {
	extract( shortcode_atts( array(		
		'style' => 'alert-success',	
		'status_enter' => '',
		'text_enter' => '',	
		'text_color' => '',
		'background_color' => '',
		'font_size' => '1rem',
		'line_height' => '',
		'font_family' => '',
		'font_weight' => '',
		'top_margin' => '',
		'bottom_margin' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'alerts', $atts ));

	$top_margin = intval( $top_margin );
	$bottom_margin = intval( $bottom_margin );


	$css =  $text_css =  '';		

	if ( $status_enter || $text_enter ) {		
		
		if ( $text_color ) $css .= 'color:'. $text_color .';';
		if ( $background_color ) $css .= 'background-color:'. $background_color .';';
		if ( $font_size ) $css .= 'font-size:'. $font_size .';';
		if ( $line_height ) $css .= 'line-height:'. $line_height .';';
		if ( $font_weight ) $css .= 'font-weight:'. $font_weight .';';
		if ( $font_family ) $css .= 'font-family:'. $font_family .';';
		if ( $top_margin ) $css .= 'margin-top:'. $top_margin .'px;';
		if ( $bottom_margin ) $css .= 'margin-bottom:'. $bottom_margin .'px;';		

		return sprintf(
			'<div class="alert %1$s alert-dismissible" style="%2$s">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>%3$s</strong> %4$s
			</div>',
			$style,
			$css,
			$status_enter,
			$text_enter
			
		);
	}
}