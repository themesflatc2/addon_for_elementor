<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_teammembers_shortcode_params' );

function themesflat_teammembers_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Team Members', 'evockans'),
        'description' => esc_html__('Empty space with custom height.', 'evockans'),
        'base' => 'teammembers',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'params' => array(
        	array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'evockans' ),
				'param_name' => 'style',
				'value'      => array(
					'Style 1' => 'style-1',
					'Style 2' => 'style-2',
					'Custom' => 'custom',
				),
				'std'		=> 'style-1',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Align', 'evockans' ),
				'param_name' => 'align',
				'value' => array(
					'Left' => '',					
					'Right' => 'text-right',
					'Center' => 'text-center',
				),
				'std'		=> 'text-center',
			),
        	array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Wrap: Padding', 'evockans' ),
				'param_name'  => 'wrap_padding',
				'value'	=> '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Wrap: Margin', 'evockans' ),
				'param_name'  => 'wrap_margin',
				'value'	=> '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Wrap: Rounded', 'evockans' ),
				'param_name'  => 'wrap_rounded',
				'value'	=> '',
				'description'	=> esc_html__('Ex: 5px', 'evockans'),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Wrap: Backround Color', 'evockans'),
				'param_name' => 'wrap_background_color',
				'value' => '',
            ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Wrap: Border Width', 'evockans'),
				'param_name' => 'wrap_border_width',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left. Default: 0px 0px 0px 0px', 'evockans'),
	        ),
	        array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Wrap: Border Color', 'evockans'),
				'param_name' => 'wrap_border_color',
				'value' => '',
            ),
            //Image
	        array(
				'type'       => 'attach_image',
				'holder' => 'img',
				'heading'    => esc_html__( 'Image', 'evockans' ),
				'param_name' => 'image',
				'group' => esc_html__( 'Image', 'evockans' ),
			),
	        array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Image Cropping', 'evockans' ),
				'param_name' => 'image_crop',
				'value'      => array(
					'Full' => 'full',
					'600 x 600' => 'square',
					'600 x 500' => 'rectangle',
					'600 x 390' => 'rectangle2',
					'Custom' => 'custom',
				),
				'std'		=> 'full',
				'group' => esc_html__( 'Image', 'evockans' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Image Size', 'evockans' ),
				'description'    => esc_html__( '( Alternatively enter size in pixels (Example: 750x750 (Width x Height)).', 'evockans' ),
				'param_name' => 'image_size',
				'value' => '750x750',
				'dependency' => array( 'element' => 'image_crop', 'value' => 'custom' ),
				'group' => esc_html__( 'Image', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Image Align', 'evockans' ),
				'param_name' => 'image_align',
				'value' => array(
					'top' => 'image-top',
					'Left' => 'image-left',					
					'Right' => 'image-right',
				),
				'std'		=> 'image-top',
				'group' => esc_html__( 'Image', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Image Rounded', 'evockans' ),
				'param_name'  => 'image_rounded',
				'value'	=> '',
				'description'	=> esc_html__('Ex: 50% or 200px', 'evockans'),
				'group' => esc_html__( 'Image', 'evockans' ),
			),
			//Name
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Tag', 'evockans' ),
				'param_name' => 'name_tag',
				'value'      => array(
					'H1' => 'h1',
					'H2' => 'h2',
					'H3' => 'h3',
					'H4' => 'h4',
					'H5' => 'h5',
					'H6' => 'h6',
				),
				'std'		=> 'h4',
				'group' => esc_html__( 'Name', 'evockans' ),
			),
			array(
				'type'        => 'textfield',
				'holder' => 'div',
				'heading'     => esc_html__( 'Name', 'evockans' ),
				'param_name'  => 'name',
				'value'	=> '',
				'group' => esc_html__( 'Name', 'evockans' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Name: Margin', 'evockans' ),
				'param_name'  => 'name_margin',
				'value'	=> '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Name', 'evockans' ),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Name: Color', 'evockans'),
				'param_name' => 'name_color',
				'value' => '',
				'group' => esc_html__( 'Name', 'evockans' ),
            ),
            //Position
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Position', 'evockans' ),
				'param_name'  => 'position',
				'value'	=> '',
				'group' => esc_html__( 'Position', 'evockans' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Position: Margin', 'evockans' ),
				'param_name'  => 'position_margin',
				'value'	=> '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Position', 'evockans' ),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Position: Color', 'evockans'),
				'param_name' => 'position_color',
				'value' => '',
				'group' => esc_html__( 'Position', 'evockans' ),
            ),
            //Content
			array(
				'type'        => 'textarea',
				'heading'     => esc_html__( 'Content', 'evockans' ),
				'param_name'  => 'description',
				'value'	=> '',
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Content: Margin', 'evockans' ),
				'param_name'  => 'content_margin',
				'value'	=> '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Content', 'evockans' ),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Content: Color', 'evockans'),
				'param_name' => 'content_color',
				'value' => '',
				'group' => esc_html__( 'Content', 'evockans' ),
            ),
            //Social            
            array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Facebook URL', 'redbiz' ),
				'param_name' => 'facebook',
				'value' => '',
				'group' => esc_html__( 'Social', 'evockans' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Twitter URL', 'redbiz' ),
				'param_name' => 'twitter',
				'value' => '',
				'group' => esc_html__( 'Social', 'evockans' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Pinterest URL', 'redbiz' ),
				'param_name' => 'pinterest',
				'value' => '',
				'group' => esc_html__( 'Social', 'evockans' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'LikeIn URL', 'redbiz' ),
				'param_name' => 'likedin',
				'value' => '',
				'group' => esc_html__( 'Social', 'evockans' ),
			),			
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Skype URL', 'redbiz' ),
				'param_name' => 'skype',
				'value' => '',
				'group' => esc_html__( 'Social', 'evockans' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Instagram URL', 'redbiz' ),
				'param_name' => 'instagram',
				'value' => '',
				'group' => esc_html__( 'Social', 'evockans' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Vimeo URL', 'redbiz' ),
				'param_name' => 'vimeo',
				'value' => '',
				'group' => esc_html__( 'Social', 'evockans' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Google URL', 'redbiz' ),
				'param_name' => 'google',
				'value' => '',
				'group' => esc_html__( 'Social', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Social: Color', 'evockans'),
				'param_name' => 'social_color',
				'value' => '',
				'group' => esc_html__( 'Social', 'evockans' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Social: Backround Color', 'evockans'),
				'param_name' => 'social_bg_color',
				'value' => '',
				'group' => esc_html__( 'Social', 'evockans' ),
            ),
            array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Icon Social: Padding', 'evockans' ),
				'param_name'  => 'social_padding',
				'value'	=> '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Social', 'evockans' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Icon Social: Margin', 'evockans' ),
				'param_name'  => 'social_margin',
				'value'	=> '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
				'group' => esc_html__( 'Social', 'evockans' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Icon Social: Rounded', 'evockans' ),
				'param_name'  => 'social_rounded',
				'value'	=> '',
				'description'	=> esc_html__('Ex: 5px', 'evockans'),
				'group' => esc_html__( 'Social', 'evockans' ),
			),
            // Typography
            //name
			array(
				'type' => 'headings',
				'text' => esc_html__('Name', 'evockans'),
				'param_name' => 'name_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Family', 'evockans' ),
				'param_name' => 'name_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Weight', 'evockans' ),
				'param_name' => 'name_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Font Size', 'evockans'),
				'param_name' => 'name_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Line Height', 'evockans'),
				'param_name' => 'name_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        //position
	        array(
				'type' => 'headings',
				'text' => esc_html__('Position', 'evockans'),
				'param_name' => 'position_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Family', 'evockans' ),
				'param_name' => 'position_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Weight', 'evockans' ),
				'param_name' => 'position_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Font Size', 'evockans'),
				'param_name' => 'position_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Line Height', 'evockans'),
				'param_name' => 'position_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Style', 'evockans' ),
				'param_name' => 'position_font_style',
				'value'      => array(
					'Normal' => '',
					'Italic' => 'italic',
				),
				'std'		=> 'normal',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        //content
	        array(
				'type' => 'headings',
				'text' => esc_html__('Content', 'evockans'),
				'param_name' => 'content_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Family', 'evockans' ),
				'param_name' => 'content_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Weight', 'evockans' ),
				'param_name' => 'content_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Font Size', 'evockans'),
				'param_name' => 'content_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Line Height', 'evockans'),
				'param_name' => 'content_line_height',
				'value' => '',
				'description'	=> esc_html__('Ex: 24px or 1.3 or 150%', 'evockans'),
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Font Style', 'evockans' ),
				'param_name' => 'content_font_style',
				'value'      => array(
					'Normal' => '',
					'Italic' => 'italic',
				),
				'std'		=> 'normal',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			//Social
	        array(
				'type' => 'headings',
				'text' => esc_html__('Social', 'evockans'),
				'param_name' => 'social_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Font Size', 'evockans'),
				'param_name' => 'social_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        // Box Shadow
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Horizontal (Required)', 'evockans'),
				'param_name' => 'horizontal',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Vertical (Required)', 'evockans'),
				'param_name' => 'vertical',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 20px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Blur (Required)', 'evockans'),
				'param_name' => 'blur',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 40px', 'evockans'),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Spread (Required)', 'evockans'),
				'param_name' => 'spread',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: 0px', 'evockans'),
	        ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Box Shadow Color (Required)', 'evockans'),
				'param_name' => 'shadow_color',
				'value' => '',
				'group' => esc_html__( 'Box Shadow', 'evockans' ),
				'description'	=> esc_html__('Ex: #eeeeee', 'evockans'),
            ),
        )
    ) );
}

add_shortcode( 'teammembers', 'themesflat_shortcode_teammembers' );

/**
 * teammembers shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_teammembers( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'style' => 'style-1',
		'align' => 'text-center',
		'wrap_padding' => '',
		'wrap_margin' => '',
		'wrap_rounded' => '',
		'wrap_background_color' => '',
		'wrap_border_width' => '',
		'wrap_border_color' => '',
	    'image' => '',
		'image_crop' => 'full',
		'image_size' => '750x750',
		'image_align' => 'image-top',
		'image_rounded' => '',
		'name_tag' => 'h4',
		'name' => '',
		'name_margin' => '',
		'name_color' => '',
		'name_font_family' => 'Default',
		'name_font_weight' => 'Default',
		'name_font_size' => '',
		'name_line_height' => '',
		'position' => '',
		'position_margin' => '',
		'position_color' => '',
		'position_font_family' => 'Default',
		'position_font_weight' => 'Default',
		'position_font_size' => '',
		'position_line_height' => '',
		'position_font_style' => '',
		'description' => '',
		'content_margin' => '',
		'content_color' => '',
		'content_font_family' => 'Default',
		'content_font_weight' => 'Default',
		'content_font_size' => '',
		'content_line_height' => '',
		'content_font_style' => '',
		'social_padding' => '',
		'social_margin' => '',
		'social_rounded' => '',
		'facebook' => '',
		'twitter' => '',
		'vimeo' => '',
		'google' => '',
		'likedin' => '',
		'instagram' => '',
		'pinterest' => '',
		'skype' => '',
		'social_color'=> '',
		'social_bg_color' => '',
		'social_font_size' => '',
		'horizontal' => '',
        'vertical' => '',
        'blur' => '',
        'spread' => '',
        'shadow_color' => '',
	), $atts ) );	
	//extract($atts = vc_map_get_attributes( 'teammembers', $atts ));
	$content = wpb_js_remove_wpautop($content, true);

	$cls = $css = $image_html = $image_css = $name_html = $name_css = $position_html =  $position_css = $description_html =  $description_css = '';
	$social_html = $social_css = $box_social_html = '';

	$cls .= $style .' '. $align;

	if ( $style == 'custom') {
		$cls .= ' '.$image_align;
	}

	$wrap_rounded = intval( $wrap_rounded );
	$name_font_size = intval( $name_font_size );
	$position_font_size = intval( $position_font_size );
	$content_font_size = intval( $content_font_size );
	$social_font_size = intval( $social_font_size );
	$social_rounded = intval( $social_rounded );

	if ( $wrap_padding ) $css .= 'padding:' . $wrap_padding . ';';
	if ( $wrap_margin ) $css .= 'margin:' . $wrap_margin . ';';
	if ( $wrap_rounded ) $css .= 'border-radius:' . $wrap_rounded . 'px; overflow: hidden;';
	if ( $wrap_background_color ) $css .= 'background-color:' . $wrap_background_color . ';';
	if ( $wrap_border_color && $wrap_border_width ) $css .= 'border-style:solid;border-width:'. $wrap_border_width .';border-color:'. $wrap_border_color .';';
	if ( $horizontal && $vertical && $blur && $spread && $shadow_color )
    	$css .= 'box-shadow:'. $horizontal .' '. $vertical .' '. $blur .' '. $spread .' '. $shadow_color .';';
	
	//image 
	if ( $image_rounded ) $image_css .= 'border-radius:' . $image_rounded . ';';
	if ( $image ) {
	    $img_size = 'full';
	    if ( $image_crop == 'square' ) $img_size = 'themesflat_sc_vc-square';
	    if ( $image_crop == 'rectangle' ) $img_size = 'themesflat_sc_vc-rectangle';
	    if ( $image_crop == 'rectangle2' ) $img_size = 'themesflat_sc_vc-rectangle2';

		$image_html = sprintf(
			'<div class="team-image"><img alt="image" src="%1$s" style="%2$s"/></div>',
			wp_get_attachment_image_src( $image, $img_size )[0], $image_css );

		if ( $image_crop == 'custom' ) { 
	  		$image = wpb_getImageBySize( array( 'attach_id' => $image, 'thumb_size' => $image_size ) );
	  		$image = $image['thumbnail'];

	  		$src_pattern = '/<img.+src=[\'"]([^\'"]+)[\'"].*>/i';	    	
			$matches = array();
			if(preg_match($src_pattern, $image, $matches)) {
			    $image = $matches[1];
			    $image = trim($image);
			}

	  		$image_html = sprintf( '<div class="team-image"><img alt="image" src="%1$s" style="%2$s"/></div>', $image , $image_css );
	    }

	} else { return; }

	//name css
	if ( $name_margin ) $name_css .= 'margin:' . $name_margin . ';';
	if ( $name_color ) $name_css .= 'color:' . $name_color . ';';
	if ( $name_font_size ) $name_css .= 'font-size:'. $name_font_size .'px;';
	if ( $name_font_weight != 'Default' ) $name_css .= 'font-weight:'. $name_font_weight .';';
	if ( $name_line_height ) $name_css .= 'line-height:'. $name_line_height .';';
	if ( $name_font_family != 'Default' ) {
		$name_css .= 'font-family:'. $name_font_family .';';
	}


	if ( $name ) {
		$name_html .= sprintf( '<%1$s class="team-name" style="%3$s">%2$s</%1$s>', $name_tag, $name, $name_css );
	}	

	//position css
	if ( $position_margin ) $position_css .= 'margin:' . $position_margin . ';';
	if ( $position_color ) $position_css .= 'color:' . $position_color . ';';
	if ( $position_font_style == 'italic' ) { $position_css .= 'font-style: italic;'; }
	if ( $position_font_size ) $position_css .= 'font-size:'. $position_font_size .'px;';
	if ( $position_font_weight != 'Default' ) $position_css .= 'font-weight:'. $position_font_weight .';';
	if ( $position_line_height ) $position_css .= 'line-height:'. $position_line_height .';';
	if ( $position_font_family != 'Default' ) {
		$position_css .= 'font-family:'. $position_font_family .';';
	}

	if ( $position ) {
		$position_html = sprintf( '<div class="team-position" style="%2$s">%1$s</div>', $position , $position_css );
	}	

	//Content
	if ( $content_margin ) $description_css .= 'margin:' . $content_margin . ';';
	if ( $content_color ) $description_css .= 'color:' . $content_color . ';';
	if ( $content_font_size ) $description_css .= 'font-size:'. $content_font_size .'px;';
	if ( $content_font_weight != 'Default' ) $description_css .= 'font-weight:'. $content_font_weight .';';
	if ( $content_line_height ) $description_css .= 'line-height:'. $content_line_height .';';
	if ( $content_font_style == 'italic' ) { $description_css .= 'font-style: italic;'; }
	if ( $content_font_family != 'Default' ) {
		$description_css .= 'font-family:'. $content_font_family .';';
	}
	if ( $description ) {
		$description_html .= sprintf( '<div class="team-desc" style="%2$s">%1$s</div>', $description , $description_css );
	}

	//social
	if( $social_padding ) $social_css .= 'padding:' . $social_padding . ';' ;
	if( $social_margin ) $social_css .= 'margin:' . $social_margin . ';' ;
	if( $social_color ) $social_css .= 'color:' . $social_color . ';' ;
	if( $social_rounded ) $social_css .= 'border-radius:' . $social_rounded . 'px;' ;
	if( $social_font_size ) $social_css .= 'font-size:' . $social_font_size . 'px;' ;
	if( $social_bg_color ) {
		$social_css .= 'background-color:' . $social_bg_color . ';' ;
		$cls .= ' social-has-bg';
	}


	if ( $facebook )
		$social_html .= sprintf( '<a href="%1$s" style="%2$s" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>', esc_url( $facebook ), $social_css );
	if ( $twitter )
		$social_html .= sprintf( '<a href="%1$s" style="%2$s" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>', esc_url( $twitter ), $social_css );
	if ( $vimeo )
		$social_html .= sprintf( '<a href="%1$s" style="%2$s" target="_blank" class="vimeo"><i class="fa fa-vimeo"></i></a>', esc_url( $vimeo ), $social_css );		
	if ( $instagram )
		$social_html .= sprintf( '<a href="%1$s" style="%2$s" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>', esc_url( $instagram ), $social_css );
	if ( $pinterest )
		$social_html .= sprintf( ' <a href="%1$s" style="%2$s" target="_blank" class="pinterest"><i class="fa fa-pinterest-p"></i></a>', esc_url( $pinterest ), $social_css );
	if ( $likedin )
		$social_html .= sprintf( '<a href="%1$s" style="%2$s" target="_blank" class="likedin"><i class="fa fa-linkedin"></i></a>', esc_url( $likedin ), $social_css );
	if ( $skype )
		$social_html .= sprintf( ' <a href="%1$s" style="%2$s" target="_blank" class="skype"><i class="fa fa-skype"></i></a>', esc_url( $skype ), $social_css );
	if ( $google )
		$social_html .= sprintf( '<a href="%1$s" style="%2$s" target="_blank" class="google"><i class="fa fa-google"></i></a>', esc_url( $google ), $social_css );

	$box_social_html .= sprintf( '<div class="team-box-social">%1$s</div>', $social_html );

	if ( $style == 'style-1' ) {
		//image 
		if ( $image_rounded ) $image_css .= 'border-radius:' . $image_rounded . ';';
		if ( $image ) {
		    $img_size = 'full';
		    if ( $image_crop == 'square' ) $img_size = 'themesflat_sc_vc-square';
		    if ( $image_crop == 'rectangle' ) $img_size = 'themesflat_sc_vc-rectangle';
		    if ( $image_crop == 'rectangle2' ) $img_size = 'themesflat_sc_vc-rectangle2';

			$image_html = sprintf(
				'<div class="team-image"><img alt="image" src="%1$s" style="%2$s"/>%3$s</div>',
				wp_get_attachment_image_src( $image, $img_size )[0], $image_css, $box_social_html );

			if ( $image_crop == 'custom' ) { 
		  		$image = wpb_getImageBySize( array( 'attach_id' => $image, 'thumb_size' => $image_size ) );
		  		$image = $image['thumbnail'];

		  		$src_pattern = '/<img.+src=[\'"]([^\'"]+)[\'"].*>/i';	    	
				$matches = array();
				if(preg_match($src_pattern, $image, $matches)) {
				    $image = $matches[1];
				    $image = trim($image);
				}

		  		$image_html = sprintf( '<div class="team-image"><img alt="image" src="%1$s" style="%2$s"/>%3$s</div>', $image , $image_css, $box_social_html );
		    }

		} else { return; }

		return sprintf( '
			<div class="themesflat_sc_vc-teammembers %1$s" style="%2$s">
				%3$s
				%4$s				
				%5$s
				%6$s				
			</div>',
			$cls,
			$css,
			$image_html,
			$name_html,
			$position_html,
			$description_html
		);
	} else if ( $style == 'style-2' ) {
		return sprintf( '
			<div class="themesflat_sc_vc-teammembers %1$s" style="%2$s">
				%3$s
				<div class="wrap-team-name">%4$s %5$s</div>				
				%6$s
				%7$s
			</div>',
			$cls,
			$css,
			$image_html,
			$name_html,
			$position_html,
			$description_html,
			$box_social_html
		);
	} else if ( $style == 'custom' ) {
		return sprintf( '
			<div class="themesflat_sc_vc-teammembers %1$s" style="%2$s">
				<div class="inner">
					<div class="inner-image">
						%3$s
					</div>
					<div class="inner-content">
						<div class="wrap-content">				
							%4$s
							%5$s
							%6$s
							%7$s
						</div>
					</div>
				</div>				
			</div>',
			$cls,
			$css,
			$image_html,
			$name_html,
			$position_html,
			$description_html,
			$box_social_html
		);
	}
	
}