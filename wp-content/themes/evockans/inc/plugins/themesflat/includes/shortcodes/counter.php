<?php
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_counter_shortcode_params' );

function themesflat_counter_shortcode_params() {
	vc_map( array(
		'name'        => esc_html__( 'Counter', 'evockans' ),
        'description' => esc_html__('Displaying a running counter.', 'evockans'),
		'base'        => 'counter',
		'weight'	  =>	180,
        'icon' 		  => THEMESFLAT_ICON,
        'category'    => esc_html__('THEMESFLAT', 'evockans'),
		'params'      => array(
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Order', 'evockans' ),
				'param_name' => 'style',
				'value'      => array(
					'No Icon' => 'no-icon',
					'Icon Top' => 'icon-top',
					'Icon Left' => 'icon-left',
				),
				'std'		=> '',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Order', 'evockans' ),
				'param_name' => 'order',
				'value'      => array(
					'Number & Heading' => 'nh',
					'Heading & Number' => 'hn',
				),
				'std'		=> 'nh',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Alignment', 'evockans' ),
				'param_name' => 'alignment',
				'value'      => array(
					'Left' => '',
					'Center' => 'text-center',
					'Right' => 'text-right',
				),
				'std'		=> 'text-center',
				'dependency' => array( 'element' => 'style', 'value' => array('no-icon','icon-top')  ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Wrap: Margin', 'evockans' ),
				'param_name' => 'wrap_margin',
				'value' => '',
				'description'	=> esc_html__('Top Right Bottom Left.', 'evockans'),
			),
			// Icon
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'evockans' ),
				'param_name' => 'icon_type',
				'description' => esc_html__( 'Select icon library.', 'evockans' ),
				'value' => array(
					esc_html__( 'None', 'evockans' ) => '',
					esc_html__( 'Mono Social', 'evockans' ) => 'monosocial',
					esc_html__( 'Simple Line', 'evockans' ) => 'simpleline',
					esc_html__( 'Font Ionicons', 'evockans' ) => 'ionicons',
					esc_html__( 'Font Elegant', 'evockans' ) => 'eleganticons',
					esc_html__( 'Font Themify Icons', 'evockans' ) => 'themifyicons',
					esc_html__( 'Font Icomoon Icons', 'evockans' ) => 'icomoon',
					esc_html__( 'FontAwesome', 'evockans' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'evockans' ) => 'openiconic',
					esc_html__( 'Typicons', 'evockans' ) => 'typicons',
					esc_html__( 'Entypo', 'evockans' ) => 'entypo',
					esc_html__( 'Linecons', 'evockans' ) => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => array('icon-top','icon-left')  ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_monosocial',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'monosocial',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'monosocial',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_simpleline',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'simpleline',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'simpleline',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_ionicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'ionicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'ionicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_eleganticons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'eleganticons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'eleganticons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_themifyicons',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'themifyicons',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'themifyicons',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
			    'type' => 'iconpicker',
			    'heading' => esc_html__( 'Icon', 'evockans' ),
			    'param_name' => 'icon_icomoon',
			    'settings' => array(
			        'emptyIcon' => true,
			        'type' => 'icomoon',
			        'iconsPerPage' => 200,
			    ),
			    'dependency' => array(
			        'element' => 'icon_type',
			        'value' => 'icomoon',
			    ),
			    'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon',
				'settings' => array(
					'emptyIcon' => true,
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'fontawesome',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_openiconic',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'openiconic',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'openiconic',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_typicons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'typicons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'typicons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_entypo',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'entypo',
					'iconsPerPage' => 300,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'entypo',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'evockans' ),
				'param_name' => 'icon_linecons',
				'settings' => array(
					'emptyIcon' => true,
					'type' => 'linecons',
					'iconsPerPage' => 200,
				),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'linecons',
				),
				'group' => esc_html__( 'Icon', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Icon Color', 'evockans'),
				'param_name' => 'icon_color',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => array('icon-top','icon-left')  ),
            ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Font Size', 'evockans'),
				'param_name' => 'icon_font_size',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => array('icon-top','icon-left')  ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Line-height', 'evockans'),
				'param_name' => 'icon_line_height',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => array('icon-top','icon-left')  ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon: Right Margin', 'evockans'),
				'param_name' => 'icon_right_margin',
				'value' => '',
				'group' => esc_html__( 'Icon', 'evockans' ),
				'description'	=> esc_html__('Spacing between the icon and the heading.', 'evockans'),
				'dependency' => array( 'element' => 'style', 'value' => array('icon-left')  ),
	        ),
			// Number
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Number', 'evockans' ),
				'param_name' => 'number',
				'group' => esc_html__( 'Number', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Number Color', 'evockans'),
				'param_name' => 'number_color',
				'value' => '',
				'group' => esc_html__( 'Number', 'evockans' ),
            ),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Rolling Time', 'evockans'),
				'param_name' => 'time',
				'value' => '5000',
				'suffix' => 'ms',
				'group' => esc_html__( 'Number', 'evockans' ),
		  	),
		  	array(
				'type' => 'textfield',
				'heading' => esc_html__('Delay Time', 'evockans'),
				'param_name' => 'delaytime',
				'value' => '25',
				'group' => esc_html__( 'Number', 'evockans' ),
		  	),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Prefix (Optional)', 'evockans' ),
				'param_name' => 'number_prefix',
				'group' => esc_html__( 'Number', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Prefix Color', 'evockans'),
				'param_name' => 'prefix_color',
				'value' => '',
				'group' => esc_html__( 'Number', 'evockans' ),
            ),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Suffix (Optional)', 'evockans' ),
				'param_name' => 'number_suffix',
				'group' => esc_html__( 'Number', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Suffix Color', 'evockans'),
				'param_name' => 'suffix_color',
				'value' => '',
				'group' => esc_html__( 'Number', 'evockans' ),
            ),
		  	// Heading
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading Tag', 'evockans' ),
				'param_name' => 'heading_tag',
				'value'      => array(
					'H1' => 'h1',
					'H2' => 'h2',
					'H3' => 'h3',
					'H4' => 'h4',
					'H5' => 'h5',
					'H6' => 'h6',
					'div' => 'div',
				),
				'std'		=> 'div',
				'group' => esc_html__( 'Heading', 'evockans' ),
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__( 'Heading', 'evockans' ),
				'param_name' => 'heading',
				'group' => esc_html__( 'Heading', 'evockans' ),
			),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Heading Color', 'evockans'),
				'param_name' => 'heading_color',
				'value' => '',
				'group' => esc_html__( 'Heading', 'evockans' ),
            ),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Separator', 'evockans' ),
				'param_name' => 'separator',
				'value'      => array(
					'No Separator' => '',
					'Line' => 'line',
					'Image' => 'image',
				),
				'std'		=> '',
				'group' => esc_html__( 'Separator', 'evockans' ),
			),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line Width', 'evockans'),
				'param_name' => 'line_width',
				'value' => '50',
				'group' => esc_html__( 'Separator', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
            ),
            array(
				'type' => 'textfield',
				'heading' => esc_html__('Line Height', 'evockans'),
				'param_name' => 'line_height',
				'value' => '2',
				'group' => esc_html__( 'Separator', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Line Color', 'evockans'),
				'param_name' => 'line_color',
				'value' => '',
				'group' => esc_html__( 'Separator', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'line' ),
            ),
	        // Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image', 'evockans'),
				'param_name' => 'image',
				'value' => '',
				'group' => esc_html__( 'Separator', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'image' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Image Width (Optional)', 'evockans'),
				'param_name' => 'image_width',
				'value' => '',
				'group' => esc_html__( 'Separator', 'evockans' ),
				'dependency' => array( 'element' => 'separator', 'value' => 'image' ),
	        ),
	        // Typography
			array(
				'type' => 'headings',
				'text' => esc_html__('Number Settings', 'evockans'),
				'param_name' => 'number_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'class' => '',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Number: Font Family', 'evockans' ),
				'param_name' => 'number_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Number: Font Weight', 'evockans' ),
				'param_name' => 'number_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Number: Font Style', 'evockans' ),
				'param_name' => 'number_font_style',
				'value'      => array(
					'Normal' => 'normal',
					'Italic' 	 => 'italic',
				),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Number: Font Size', 'evockans'),
				'param_name' => 'number_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Number: Line Height', 'evockans'),
				'param_name' => 'number_line_height',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Heading Settings', 'evockans'),
				'param_name' => 'heading_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'class' => '',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading: Font Family', 'evockans' ),
				'param_name' => 'heading_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading: Font Weight', 'evockans' ),
				'param_name' => 'heading_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Heading: Font Style', 'evockans' ),
				'param_name' => 'heading_font_style',
				'value'      => array(
					'Normal' => 'normal',
					'Italic' 	 => 'italic',
				),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Font Size', 'evockans'),
				'param_name' => 'heading_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Line Height', 'evockans'),
				'param_name' => 'heading_line_height',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Prefix Settings', 'evockans'),
				'param_name' => 'prefix_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'class' => '',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Prefix: Font Family', 'evockans' ),
				'param_name' => 'prefix_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Prefix: Font Weight', 'evockans' ),
				'param_name' => 'prefix_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Prefix: Font Size', 'evockans'),
				'param_name' => 'prefix_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Prefix: Line Height', 'evockans'),
				'param_name' => 'prefix_line_height',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
			array(
				'type' => 'headings',
				'text' => esc_html__('Suffix Settings', 'evockans'),
				'param_name' => 'suffix_typograpy',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'class' => '',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Suffix: Font Family', 'evockans' ),
				'param_name' => 'suffix_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Suffix: Font Weight', 'evockans' ),
				'param_name' => 'suffix_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
			),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Suffix: Font Size', 'evockans'),
				'param_name' => 'suffix_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Suffix: Line Height', 'evockans'),
				'param_name' => 'suffix_line_height',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Number: Top Margin', 'evockans'),
				'param_name' => 'number_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Number: Bottom Margin', 'evockans'),
				'param_name' => 'number_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Prefix: Right Margin', 'evockans'),
				'param_name' => 'prefix_right_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Suffix: Left Margin', 'evockans'),
				'param_name' => 'suffix_left_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Top Margin', 'evockans'),
				'param_name' => 'heading_top_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Heading: Bottom Margin', 'evockans'),
				'param_name' => 'heading_bottom_margin',
				'value' => '',
				'group' => esc_html__( 'Spacing', 'evockans' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Extra class name', 'evockans'),
				'param_name' => 'class',
				'value' => '',
				'group' => esc_html__( 'Class', 'evockans' ),
	        ),
		)
	) );
}

add_shortcode( 'counter', 'themesflat_shortcode_counter' );

/**
 * counter shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_counter( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'style' => '',
		'order' => 'nh',
		'alignment' => 'text-center',
		'number' => '',
		'number_color' => '',
		'number_prefix' => '',
		'prefix_color' => '',
		'number_suffix' => '',
		'suffix_color' => '',
		'time' => '5000',
		'delaytime' => '25',
		'heading_tag' => 'div',
	    'heading' => '',
	    'heading_color' => '',
		'separator' => '',
		'line_width' => '50',
		'line_height' => '2',
		'line_color' => '',
		'image' => '',
		'image_width' => '',
		'number_font_family' => 'Default',		
		'number_font_weight' => 'Default',
		'number_font_style' => 'normal',
		'number_font_size' => '',
		'number_line_height' => '',
		'heading_font_family' => 'Default',
		'heading_font_weight' => 'Default',
		'heading_font_style' => 'normal',
		'heading_font_size' => '',
		'heading_line_height' => '',
		'prefix_font_family' => 'Default',
		'prefix_font_weight' => 'Default',
		'prefix_font_size' => '',
		'prefix_line_height' => '',
		'suffix_font_family' => 'Default',
		'suffix_font_weight' => 'Default',		
		'suffix_font_size' => '',
		'suffix_line_height' => '',
		'number_top_margin' => '',
		'number_bottom_margin' => '',
		'prefix_right_margin' => '',
		'suffix_left_margin' => '',
		'heading_top_margin' => '',
		'heading_bottom_margin' => '',
		'icon_type' => '',
		'icon' => '',
		'icon_color' => '',
		'icon_font_size' => '',
		'icon_right_margin' => '',
		'class' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'counter', $atts ));

	$time = intval( $time );
	$delaytime = intval( $delaytime );
	$image_width = intval( $image_width );
	$line_width = intval( $line_width );
	$line_height = intval( $line_height );
	$icon_font_size = intval( $icon_font_size );
	$icon_right_margin = intval( $icon_right_margin );

	$heading_font_size = intval( $heading_font_size );
	$heading_line_height = intval( $heading_line_height );
	$number_font_size = intval( $number_font_size );
	$number_line_height = intval( $number_line_height );

	$prefix_font_size = intval( $prefix_font_size );
	$prefix_line_height = intval( $prefix_line_height );
	$suffix_font_size = intval( $suffix_font_size );
	$suffix_line_height = intval( $suffix_line_height );

	$heading_top_margin = intval( $heading_top_margin );
	$heading_bottom_margin = intval( $heading_bottom_margin );
	$number_top_margin = intval( $number_top_margin );
	$number_bottom_margin = intval( $number_bottom_margin );
	$prefix_right_margin = intval( $prefix_right_margin );
	$suffix_left_margin = intval( $suffix_left_margin );

	$cls = $style .' '. $alignment;
	$icon_cls = $icon_css = $heading_css = $number_css = $suffix_cls = $prefix_cls = $suffix_css = $prefix_css = '';
	$html = $icon_html = $heading_html = $number_html = $sep_html = '';
	$line_cls = $line_css = $image_css = '';
	$number_cls = '';

	if ( $number_color == '#1a7dd7' ) {
		$number_cls .= 'accent';
	} else {
		if ( $number_color ) $number_css .= 'color:'. $number_color .';';
	}

	if ( $prefix_color == '#1a7dd7' ) {
		$prefix_cls .= 'accent';
	} else {
		if ( $prefix_color ) $prefix_css .= 'color:'. $prefix_color .';';
	}

	if ( $suffix_color == '#1a7dd7' ) {
		$suffix_cls .= 'accent';
	} else {
		if ( $suffix_color ) $suffix_css .= 'color:'. $suffix_color .';';
	}

	if ( $number_font_weight != 'Default' ) $number_css .= 'font-weight:'. $number_font_weight .';';
	if ( $number_font_style ) $number_css .= 'font-style:'. $number_font_style .';';
	if ( $number_font_size ) $number_css .= 'font-size:'. $number_font_size .'px;';
	if ( $number_line_height ) $number_css .= 'line-height:'. $number_line_height .'px;';
	if ( $number_top_margin ) $number_css .= 'margin-top:'. $number_top_margin .'px;';
	if ( $number_bottom_margin ) $number_css .= 'margin-bottom:'. $number_bottom_margin .'px;';	
	if ( $number_font_family != 'Default' ) {
		$number_css .= 'font-family:'. $number_font_family .';';
	}

	if ( $heading_font_weight != 'Default' ) $heading_css .= 'font-weight:'. $heading_font_weight .';';
	if ( $heading_font_style ) $heading_css .= 'font-style:'. $heading_font_style .';';
	if ( $heading_color ) $heading_css .= 'color:'. $heading_color .';';
	if ( $heading_font_size ) $heading_css .= 'font-size:'. $heading_font_size .'px;';
	if ( $heading_line_height ) $heading_css .= 'line-height:'. $heading_line_height .'px;';
	if ( $heading_top_margin ) $heading_css .= 'margin-top:'. $heading_top_margin .'px;';
	if ( $heading_bottom_margin ) $heading_css .= 'margin-bottom:'. $heading_bottom_margin .'px;';
	if ( $heading_font_family != 'Default' ) {
		$heading_css .= 'font-family:'. $heading_font_family .';';
	}

	if ( $prefix_font_weight != 'Default' ) $prefix_css .= 'font-weight:'. $prefix_font_weight .';';
	if ( $prefix_font_size ) $prefix_css .= 'font-size:'. $prefix_font_size .'px;';
	if ( $prefix_line_height ) $prefix_css .= 'line-height:'. $prefix_line_height .'px;';
	if ( $prefix_right_margin ) $prefix_css .= 'margin-right:'. $prefix_right_margin .'px;';	
	if ( $prefix_font_family != 'Default' ) {
		$prefix_css .= 'font-family:'. $prefix_font_family .';';
	}

	if ( $suffix_font_weight != 'Default' ) $suffix_css .= 'font-weight:'. $suffix_font_weight .';';
	if ( $suffix_font_size ) $suffix_css .= 'font-size:'. $suffix_font_size .'px;';
	if ( $suffix_line_height ) $suffix_css .= 'line-height:'. $suffix_line_height .'px;';
	if ( $suffix_left_margin ) $suffix_css .= 'margin-left:'. $suffix_left_margin .'px;';
	if ( $suffix_font_family != 'Default' ) {
		$suffix_css .= 'font-family:'. $suffix_font_family .';';
	}

	if ( $number )
		wp_enqueue_script( 'themesflat_sc_vc-appear' );
		wp_enqueue_script( 'themesflat_sc_vc-counterup' );
		$number_html .= sprintf(
		'<div class="number-wrap font-heading" style="%2$s"><span class="prefix %10$s" style="%3$s">%5$s</span><span class="number %9$s" data-speed="%7$s" data-delay="%12$s" data-from="0" data-to="%8$s"> %1$s</span><span class="suffix %11$s" style="%4$s">%6$s</span></div>',
		$number,
		$number_css,
		$prefix_css,
		$suffix_css,
		$number_prefix,
		$number_suffix,
		$time,
		$number,
		$number_cls,
		$prefix_cls,
		$suffix_cls,
		$delaytime
	);

	if ( $heading ) $heading_html .= sprintf(
		'<%3$s class="heading" style="%2$s">
			%1$s
		</%3$s>',
		$heading,
		$heading_css,
		$heading_tag
	);

	if ( $separator == 'line' ) {
		if ( empty( $line_width ) ) $line_width = 50;
		if ( empty( $line_height ) ) $line_height = 2;

		if ( $line_color == '#1a7dd7' ) {
			$line_cls .= 'accent';
		} else {
			if ( $line_color ) $line_css .= 'background-color:'. $line_color .';';
		}

		$line_css .= 'width:'. $line_width .'px;height:'. $line_height .'px;';

		$sep_html .= sprintf( '<div class="sep %2$s" style="%1$s"></div>', $line_css, $line_cls );
	}

	if ( $separator == 'image' ) {
		if ( $image_width ) $image_css = 'width:'. $image_width .'px;';

		if ( $image )
		$sep_html = sprintf(
			'<div class="sep image" style="%2$s">
				<img alt="image" src="%1$s">
			</div>',
			wp_get_attachment_image_src( $image, 'full' )[0],
			$image_css
		);
	}

	if ( $order == 'nh' ) {
		$html = $number_html . $sep_html . $heading_html;
	} else {
		$html = $heading_html . $sep_html . $number_html;
	}

	if ( $style == 'icon-top' || $style == 'icon-left' ) {
		$icon = themesflat_sc_vc_get_icon_class( $atts, 'icon' );
		if ( $icon && $icon_type != '' ) {
			vc_icon_element_fonts_enqueue( $icon_type );

			if ( $icon_font_size ) $icon_css .= 'font-size:'. $icon_font_size .'px; line-height: normal;';

			if ( $icon_color == '#1a7dd7' ) {
				$icon_cls .= ' accent';
			} else {
				if ( $icon_color ) $icon_css .= 'color:'. $icon_color .';';
			}
			
			if ( $icon_right_margin ) $icon_css .= 'margin-right:'. $icon_right_margin .'px;';

			$icon_html = sprintf('<div class="icon %3$s" style="%2$s"><i class="%1$s"></i></div>', $icon, $icon_css, $icon_cls );
		}

		$html = '<div class="inner"><div class="icon-wrap">'. $icon_html .'</div><div class="text-wrap">'. $html .'</div></div>';
	}

	return sprintf( '<div class="themesflat_sc_vc-counter clearfix %2$s %3$s">%1$s</div>',
		$html,
		$cls,
		$class
	);
}