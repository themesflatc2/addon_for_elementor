<?php
/**
 * Register filter for append custom class name
 * that generated from visual-composer
 */
class WPBakeryShortCode_table_data_list extends WPBakeryShortCodesContainer {}
/**
 * Register shortcode into Visual Composer
 */
add_action( 'vc_before_init', 'themesflat_table_data_list_shortcode_params' );

function themesflat_table_data_list_shortcode_params() {
	vc_map( array(
        'name' => esc_html__('Table Data List', 'evockans'),
        'description' => esc_html__('Displaying table data list.', 'evockans'),
        'base' => 'table_data_list',
		'weight'	=>	180,
        'icon' => THEMESFLAT_ICON,
        'category' => esc_html__('THEMESFLAT', 'evockans'),
        'js_view' => 'VcColumnView',
        'as_parent'    => array( 'only' => 'table_data_list_items_content' ),
        'params' => array(
        	array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'evockans' ),
				'param_name' => 'align',
				'value'      => array(
					'Left' 	=> 'left',
					'Center' => 'center',
					'Right' => 'right',
				),
				'std'		=> 'text-left',
			),
	        array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'evockans' ),
				'param_name' => 'style',
				'value'      => array(
					'Table Hover' 	=> 'table-hover',
					'Table Striped' => 'table-striped',
					'Table Striped Bordered' => 'table-striped table-bordered',
					'Table Dark' 	=> 'table-dark',
					'Table Dark Bordered' 	=> 'table-dark table-bordered',
					'Table Dark Striped' 	=> 'table-dark table-striped',
					'Table Dark Striped Bordered' 	=> 'table-dark table-striped table-bordered',
					'Custom' 	=> 'custom',
				),
				'std'		=> 'table-hover',
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Wrap Background Color', 'evockans'),
				'param_name' => 'wrap_background_color',
				'value' => '',
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Title Background Color', 'evockans'),
				'param_name' => 'title_background_color',
				'value' => '',
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
            ),
            array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Title Color', 'evockans'),
				'param_name' => 'title_color',
				'value' => '',
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
            ),  
            // Multiple Columns
			array(
				'type' => 'headings',
				'text' => esc_html__('Multiple Columns Title', 'evockans'),
				'param_name' => 'add_new_columns',
				'group' => esc_html__( 'Multiple Columns', 'evockans' ),
				'class' => '',
			),          
			// params group
            array(
                'type' => 'param_group',
                'value' => '',
                'param_name' => 'titles',
                // Note params is mapped inside param-group:
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => 'Enter your title',
                        'param_name' => 'title',
                    )
                ),
                'group' => esc_html__( 'Multiple Columns', 'evockans' ),
            ),
            // Typography
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Title: Font Family', 'evockans' ),
				'param_name' => 'title_font_family',
				'value'      =>  themesflat_plugin_google_font(),
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Title: Font Weight', 'evockans' ),
				'param_name' => 'title_font_weight',
				'value'      => array(
					'Default'		=> 'Default',
					'300' => '300',
					'400' => '400',
					'500' => '500',
					'600' => '600',
					'700' => '700',
					'800' => '800',
					'900' => '900',
				),
				'std'		=> 'Default',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Title: Font Size', 'evockans'),
				'param_name' => 'title_font_size',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
	        ),
	        array(
				'type' => 'textfield',
				'heading' => esc_html__('Title: Line Height', 'evockans'),
				'param_name' => 'title_line_height',
				'value' => '',
				'group' => esc_html__( 'Typography', 'evockans' ),
				'dependency' => array( 'element' => 'style', 'value' => 'custom' ),
	        ),
        )
    ) );
}

add_shortcode( 'table_data_list', 'themesflat_shortcode_table_data_list' );

/**
 * table_data_list shortcode handle
 * 
 * @param   array  $atts  Shortcode attributes
 * @return  void
 */
function themesflat_shortcode_table_data_list( $atts, $content = null ) {	
	extract( shortcode_atts( array(
		'align' => 'left',
	    'style' => 'table-hover',
	    'wrap_background_color' => '',
	    'title_background_color' => '',
	    'title_color' => '',
	    'title_font_family' => 'Default',
	    'title_font_weight' => 'Default',
	    'title_font_size' => '',
	    'title_line_height' => '',
	), $atts ) );
	//extract($atts = vc_map_get_attributes( 'table_data_list', $atts ));
	$content = wpb_js_remove_wpautop($content, false);

	$titles = vc_param_group_parse_atts( $atts['titles'] );

	$cls = $style;
	$css = $array_title = $css_title = '';

	$title_font_size = intval($title_font_size);

	if ($wrap_background_color) $css .= 'background-color:' . $wrap_background_color . ';';
	if ($align) $css .= 'text-align:' . $align . ';';

	if ($title_background_color) $css_title .= 'background-color:' . $title_background_color . ';'; 
	if ($title_color) $css_title .= 'color:' . $title_color . ';';
	if ($title_font_family != 'Default') $css_title .= 'font-family:'. $title_font_family .';';
	if ($title_font_weight != 'Default') $css_title .= 'font-weight:'. $title_font_weight .';';
	if ($title_font_size) $css_title .= 'font-size:' . $title_font_size . 'px;';
	if ($title_line_height) $css_title .= 'line-height:' . $title_line_height . ';';

	if ($titles) {	
		foreach ($titles as $key => $value) {
			$array_title .= sprintf('<th>%1$s</th>', $value['title']);		
		}
	}

	return sprintf( '
		<div class="themesflat_sc_vc-table table-responsive" style="%5$s">
			<table class="table %1$s">
				<thead>
				  <tr style="%4$s">%2$s </tr>
				</thead>
				<tbody>%3$s</tbody>
			</table>	
		</div>', $cls, $array_title, do_shortcode($content), $css_title ,$css );
}