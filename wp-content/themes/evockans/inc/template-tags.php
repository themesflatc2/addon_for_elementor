<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package evockans
 */

if ( ! function_exists( 'themesflat_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time, post categories and author.
 */

function themesflat_widget_layout($columns) {
	$layout = array();
	switch ($columns) {
		case 1:
			$layout = array(12);
			break;
		case 2:
			$layout = array(6,6);
			break;
		case 3:
			$layout = array(4,4,4);
			break;
		case 4:
			$layout = array(4,3,3,2);
			break;
		case 5:
			$layout = array(2,2,2,2,2);
			break;
		default:
			$layout = array(12);
			break;
		
	}
	return $layout;
}

function themesflat_widget_layout_s2($columns) {
	$layout = array();
	switch ($columns) {
		case 1:
			$layout = array(12);
			break;
		case 2:
			$layout = array(6,6);
			break;
		case 3:
			$layout = array(5,4,3);
			break;
		case 4:
			$layout = array(3,3,3,3);
			break;
		case 5:
			$layout = array(2,2,2,2,2);
			break;
		default:
			$layout = array(12);
			break;
		
	}
	return $layout;
}

function themesflat_widget_layout_s3($columns) {
	$layout = array();
	switch ($columns) {
		case 1:
			$layout = array(12);
			break;
		case 2:
			$layout = array(6,6);
			break;
		case 3:
			$layout = array(4,4,4);
			break;
		case 4:
			$layout = array(2,2,3,5);
			break;
		case 5:
			$layout = array(2,2,2,2,2);
			break;
		default:
			$layout = array(12);
			break;
		
	}
	return $layout;
}

function themesflat_widget_layout_s4($columns) {
	$layout = array();
	switch ($columns) {
		case 1:
			$layout = array(12);
			break;
		case 2:
			$layout = array(6,6);
			break;
		case 3:
			$layout = array(4,4,4);
			break;
		case 4:
			$layout = array(3,3,3,3);
			break;
		case 5:
			$layout = array(2,2,2,2,2);
			break;
		default:
			$layout = array(12);
			break;
		
	}
	return $layout;
}

function themesflat_posted_on( $layout = '' ) { ?>	
	<ul class="meta-left">
		<?php if ( $layout == '' || $layout == 'blog-list' ) {
			printf(
				'<li class="post-author"><i class="far fa-user"></i><span class="author"><a href="%s" title="%s" rel="author">%s</a></span></li>',
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) )),
				esc_attr( sprintf( esc_html__( 'View all posts by %s', 'evockans' ), get_the_author() ) ),
				get_the_author()
			);
		} ?>	

		<?php if ( $layout == 'blog-grid-s1' ) {
			printf(
				'<li class="post-author">By <span class="author"><a href="%s" title="%s" rel="author">%s</a></span></li>',
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) )),
				esc_attr( sprintf( esc_html__( 'View all posts by %s', 'evockans' ), get_the_author() ) ),
				get_the_author()
			);
		} ?>

		<?php if ( $layout != 'blog-grid-s1' ): ?>
		<li class="post-date"><i class="far fa-calendar-alt"></i>
			<?php
			$archive_year  = get_the_time('Y'); 
			$archive_month = get_the_time('m'); 
			$archive_day   = get_the_time('d'); 
			?>
			<a href="<?php echo get_day_link( $archive_year, $archive_month, $archive_day); ?>"><?php echo get_the_date();?></a>			
		</li>
		<?php endif; ?>

		<?php if ( $layout == 'blog-grid' ) {
			echo'<li class="post-comments"><i class="fas fa-comments"></i>';
				comments_popup_link( esc_html__( 'Comment (0)', 'evockans' ), esc_html__(  'Comment (1)', 'evockans' ), esc_html__( 'Comments (%)', 'evockans' ) );
			echo '</li>';
		}?>
				
	</ul>
<?php
}
endif;

if ( ! function_exists( 'themesflat_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function themesflat_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' == get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', ', ' );
		if ( $tags_list && is_single() ) {
			printf( '<div class="tags-links"><strong>Tags:</strong>' . esc_html__( ' %1$s', 'evockans' ) . '</div>', 
				$tags_list );
		}
	}
	if ( themesflat_get_opt('show_social_share') == 1 ) {
		echo '<div class="wrap-social-share-article">';
			get_template_part('tpl/social-share');
		echo '</div>';
	}
	
	edit_post_link( esc_html__( 'Edit', 'evockans' ), '<div class="clearboth"><span class="edit-link">', '</span></div>' );
}
endif;

if ( ! function_exists( 'themesflat_post_navigation' ) ) :
function themesflat_post_navigation() {
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous ) {
		return;
	}
	?>
	<nav class="navigation posts-navigation" role="navigation">
		<h2 class="screen-reader-text"><?php esc_html_e( 'Post navigation', 'evockans' ); ?></h2>
		<ul class="nav-links clearfix">
			<?php
			if ( is_attachment() ) :
				previous_post_link( '<li>%link</li>', sprintf( '<span class="meta-nav">%s</span> %%title', esc_html__( 'Published In', 'evockans' ) ) );
			else :
				previous_post_link( '<li class="previous-post">%link</li>', sprintf( '<span class="meta-nav">%s</span> %%title', esc_html__( 'Previous Post', 'evockans' ) ) );
				next_post_link( '<li class="next-post">%link</li>', sprintf( '<span class="meta-nav">%s</span> %%title', esc_html__( 'Next Post', 'evockans' ) ) );
			endif;
			?>
		</ul><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;