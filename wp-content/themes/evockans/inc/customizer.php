<?php
/**
 * evockans Theme Customizer
 *
 * @package evockans
 */

function themesflat_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
    $wp_customize->get_section( 'header_image' )->title = esc_html('Backgound PageTitle', 'evockans');
    $wp_customize->get_section( 'header_image' )->priority = '22';   
    $wp_customize->get_section( 'title_tagline' )->priority = '1';
    $wp_customize->get_section( 'title_tagline' )->title = esc_html('General', 'evockans');
    $wp_customize->get_section( 'colors' )->title = esc_html('Layout Style', 'evockans');  
    $wp_customize->remove_control('display_header_text');
    $wp_customize->remove_control('header_textcolor');
    $wp_customize->remove_control('background_color');
    remove_theme_support( 'custom-header' );
  
    //Heading
    class themesflat_Info extends WP_Customize_Control {
        public $type = 'heading';
        public $label = '';
        public function render_content() {
        ?>
            <h3 class="themesflat-title-control"><?php echo esc_html( $this->label ); ?></h3>
        <?php
        }
    }    

    //Title
    class themesflat_Title_Info extends WP_Customize_Control {
        public $type = 'title';
        public $label = '';
        public function render_content() {
        ?>
            <h4><?php echo esc_html( $this->label ); ?></h4>
        <?php
        }
    }    

    //Desc
    class themesflat_Theme_Info extends WP_Customize_Control {
        public $type = 'info';
        public $label = '';
        public function render_content() {
        ?>
            <h3><?php echo esc_html( $this->label ); ?></h3>
        <?php
        }
    }    

    //Desc
    class themesflat_Desc_Info extends WP_Customize_Control {
        public $type = 'desc';
        public $label = '';
        public function render_content() {
        ?>
            <p class="themesflat-desc-control"><?php echo esc_html( $this->label ); ?></p>
        <?php
        }
    }       

    //___General___//
    $wp_customize->add_setting('themesflat_options[info]', array(
            'type'              => 'info_control',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'esc_attr',            
        )
    );

    // Heading site infomation
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-stie-infomation', array(
        'label' => esc_html('SITE INFORMATION', 'evockans'),
        'section' => 'title_tagline',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );    

    // Desc site infomaton
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_siteinfomation', array(
        'label' => esc_html('This section have basic information of your site, just change it to match with you need.', 'evockans'),
        'section' => 'title_tagline',
        'settings' => 'themesflat_options[info]',
        'priority' => 2
        ) )
    ); 

    // Enable Smooth Scroll
    $wp_customize->add_setting(
      'enable_smooth_scroll',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('enable_smooth_scroll'),     
        )   
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'enable_smooth_scroll',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Enable Smooth Scroll', 'evockans'),
            'section' => 'title_tagline',
            'priority' => 10,
        ))
    );

    // Enable Preload
    $wp_customize->add_setting(
      'enable_preload',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('enable_preload'),     
        )   
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'enable_preload',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Enable Preload', 'evockans'),
            'section' => 'title_tagline',
            'priority' => 11,
        ))
    );

    // Key Google Api
    $wp_customize->add_setting(
        'key_google_api',
        array(
            'default' => themesflat_customize_default('key_google_api'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control(
        'key_google_api',
        array(
            'label' => esc_html( 'Key Google Api', 'evockans' ),
            'section' => 'title_tagline',
            'type' => 'text',
            'priority' => 12
        )
    );

    // Preload
    $wp_customize->add_setting(
        'preload',
        array(
            'default'           => themesflat_customize_default('preload'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( new themesflat_RadioImages($wp_customize,
        'preload',
        array (
            'type'      => 'radio-images',           
            'section'   => 'title_tagline',
            'priority'  => 13,
            'label'         => esc_html('Preload', 'evockans'),
            'choices'   => array (
                'preload-1' => array (
                    'tooltip'   => esc_html( 'Circle Loaders 1','evockans' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/preload-1.png'
                ) ,
                'preload-2'=>  array (
                    'tooltip'   => esc_html( 'Circle Loaders 2','evockans' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/preload-2.png'
                ) ,
                'preload-3'=>  array (
                    'tooltip'   => esc_html( 'Circle Loaders 3','evockans' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/preload-3.png'
                ) ,
                'preload-4'=>  array (
                    'tooltip'   => esc_html( 'Circle Loaders 4','evockans' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/preload-4.png'
                ) ,
                'preload-5'=>  array (
                    'tooltip'   => esc_html( 'Spinner Loaders','evockans' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/preload-5.png'
                ) ,
                'preload-6'=>  array (
                    'tooltip'   => esc_html( 'Pulse Loaders','evockans' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/preload-6.png'
                ) ,
                'preload-7'=>  array (
                    'tooltip'   => esc_html( 'Square Loaders','evockans' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/preload-7.png'
                ) ,
                'preload-8'=>  array (
                    'tooltip'   => esc_html( 'Line Loaders','evockans' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/preload-8.png'
                ) ,
            ),
        ))
    ); 
  
    //__social links__//
    $wp_customize->add_section(
        'themesflat_socials',
        array(
            'title'         => esc_html('Socials', 'evockans'),
            'priority'      => 2,
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );

    //Socials
    $wp_customize->add_setting(
      'social_links',
      array(
        'sanitize_callback' => 'esc_attr',
        'default' => themesflat_customize_default('social_links'),     
      )   
    );
    $wp_customize->add_control( new themesflat_SocialIcons($wp_customize,
        'social_links',
        array(
            'type' => 'social-icons',
            'label' => esc_html('Social', 'evockans'),
            'section' => 'themesflat_socials',
            'priority' => 1,
        ))
    );      

    //___Header___//
    $wp_customize->add_section(
        'themesflat_header',
        array(
            'title'         => esc_html('Header', 'evockans'),
            'priority'      => 2,
        )
    );

    // Heading custom logo
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-logo', array(
        'label' => esc_html('Custom Logo', 'evockans'),
        'section' => 'themesflat_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 2
        ) )
    );   
    // Desc custon logo
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_logo', array(
        'label' => esc_html('In this section You can upload your own custom logo, change the way your logo can be displayed', 'evockans'),
        'section' => 'themesflat_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 3
        ) )
    );    

    //Logo
    $wp_customize->add_setting(
        'site_logo',
        array(
            'default' => themesflat_customize_default('site_logo'),
            'sanitize_callback' => 'esc_url_raw',
        )
    );    
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'site_logo',
            array(
               'label'          => esc_html( 'Upload Your Logo ', 'evockans' ),
               'description'    => esc_html( 'The best size is 190x51px ( If you don\'t display logo please remove it your website display 
                Site Title default in General )', 'evockans' ),
               'type'           => 'image',
               'section'        => 'themesflat_header',
               'priority'       => 5,
            )
        )
    );

    // Logo Retina
    $wp_customize->add_setting(
        'site_retina_logo',
        array(
            'default'           => themesflat_customize_default('site_retina_logo'),
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'site_retina_logo',
            array(
               'label'          => esc_html( 'Upload Your Logo Retina', 'evockans' ),
               'description'    => esc_html( 'The best size is 372x90px', 'evockans' ),
               'type'           => 'image',
               'section'        => 'themesflat_header',
               'priority'       => 6,
            )
        )
    );

    //Logo Sticky
    $wp_customize->add_setting(
        'site_logo_sticky',
        array(
            'default' => themesflat_customize_default('site_logo'),
            'sanitize_callback' => 'esc_url_raw',
        )
    );    
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'site_logo_sticky',
            array(
               'label'          => esc_html( 'Upload Your Logo Sticky Header', 'evockans' ),
               'description'    => esc_html( 'The best size is 190x51px ( If you don\'t display logo please remove it your website display 
                Site Title default in General )', 'evockans' ),
               'type'           => 'image',
               'section'        => 'themesflat_header',
               'priority'       => 6,
            )
        )
    );

    // Logo Retina Sticky
    $wp_customize->add_setting(
        'site_retina_logo_sticky',
        array(
            'default'           => themesflat_customize_default('site_retina_logo'),
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'site_retina_logo_sticky',
            array(
               'label'          => esc_html( 'Upload Your Logo Retina Sticky Header', 'evockans' ),
               'description'    => esc_html( 'The best size is 372x90px', 'evockans' ),
               'type'           => 'image',
               'section'        => 'themesflat_header',
               'priority'       => 6,
            )
        )
    );

    // Logo Size    
    $wp_customize->add_setting(
        'logo_width',
        array(
            'default'   =>  themesflat_customize_default('logo_width'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( new themesflat_Slide_Control( $wp_customize,
        'logo_width',
            array(
                'type'      =>  'slide-control',
                'section'   =>  'themesflat_header',
                'label'     =>  'Logo Size Width(px)',
                'priority'  => 7,
                'input_attrs' => array(
                    'min' => 0,
                    'max' => 500,
                    'step' => 1,
                ),
            )

        )
    ); 

    // Box control
    $wp_customize->add_setting(
        'logo_controls',
        array(
            'default' => themesflat_customize_default('logo_controls'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control( new themesflat_BoxControls($wp_customize,
        'logo_controls',
        array(
            'label' => esc_html( 'Logo Box Controls (px)', 'evockans' ),
            'section' => 'themesflat_header',
            'type' => 'box-controls',
            'priority' => 10
        ))
    );  

    // Heading Header Style     
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'style_header', array(
        'label' => esc_html('Header Style', 'evockans'),
        'section' => 'themesflat_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 12
        ) )
    );    

    $wp_customize->add_setting(
        'style_header',
        array(
            'default'           => themesflat_customize_default('style_header'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( new themesflat_RadioImages($wp_customize,
        'style_header',
        array (
            'type'      => 'radio-images',           
            'section'   => 'themesflat_header',
            'priority'  => 13,
            'label'         => esc_html('Header Style', 'evockans'),
            'choices'   => array (
                'header-style1' => array (
                    'tooltip'   => esc_html( 'Header Style 1','evockans' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/header1.jpg'
                ),
                'header-style2'=>  array (
                    'tooltip'   => esc_html( 'Header Style 2','evockans' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/header2.jpg'
                ),
                'header-style3'=>  array (
                    'tooltip'   => esc_html( 'Header Style 3','evockans' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/header3.jpg'
                ),
                'header-style4'=>  array (
                    'tooltip'   => esc_html( 'Header Style 4','evockans' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/header4.jpg'
                ),
            ),
        ))
    ); 

    // Enable Topbar Absolute
    $wp_customize->add_setting(
      'header_absolute',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('header_absolute'),     
        )   
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'header_absolute',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Enable Header Absolute', 'evockans'),
            'section' => 'themesflat_header',
            'priority' => 15,
        ))
    );

    // Show search 
    $wp_customize->add_setting(
      'header_search_box',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('header_search_box'),     
        )   
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'header_search_box',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Show Search Header', 'evockans'),
            'section' => 'themesflat_header',
            'priority' => 16,
        ))
    );  

    // Header Sticky
    $wp_customize->add_setting(
      'header_sticky',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('header_sticky'),     
        )   
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'header_sticky',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Enable Sticky Header', 'evockans'),
            'section' => 'themesflat_header',
            'priority' => 17,
        ))
    ); 

    // Heading Top Bar 
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'top-bar', array(
        'label' => esc_html('Top Bar', 'evockans'),
        'section' => 'themesflat_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 18
        ) )
    );    

    // Desc Top Bar 
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc', array(
        'label' => esc_html('Turn on/off the top bar and change it styles', 'evockans'),
        'section' => 'themesflat_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 19
        ) )
    );  

    // Top bar
    $wp_customize->add_setting( 
      'topbar_show',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('topbar_show'),     
        )   
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'topbar_show',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Show Topbar', 'evockans'),
            'section' => 'themesflat_header',
            'priority' => 20,
        ))
    ); 

    // Enable Topbar Absolute
    $wp_customize->add_setting(
      'topbar_absolute',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('topbar_absolute'),     
        )   
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'topbar_absolute',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Enable Topbar Absolute', 'evockans'),
            'section' => 'themesflat_header',
            'priority' => 21,
        ))
    ); 

    // Enable Socials Top
    $wp_customize->add_setting(
      'enable_socials_link_top',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('enable_socials_link_top'),     
        )   
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'enable_socials_link_top',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Enable Socials Top', 'evockans'),
            'section' => 'themesflat_header',
            'priority' => 21,
        ))
    );

    // Top Content
    $wp_customize->add_setting(
        'top_content',
        array(
            'default' => themesflat_customize_default('top_content'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'top_content',
        array(
            'label' => esc_html( 'Content Left', 'evockans' ),
            'section' => 'themesflat_header',
            'type' => 'textarea',
            'priority' => 26
        )
    );

   //___Footer___//
    $wp_customize->add_section(
        'flat_footer',
        array(
            'title'         => esc_html('Footer', 'evockans'),
            'priority'      => 4,
        )
    );        

    $wp_customize->remove_control('display_header_text');
    $wp_customize->remove_control('header_textcolor');

     // Footer widget
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-action-box-footer', array(
        'label' => esc_html('Action Box', 'evockans'),
        'section' => 'flat_footer',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    ); 
    
    $wp_customize->add_setting(
        'show_action_box',
        array(
            'default'   => themesflat_customize_default('show_action_box'),
            'sanitize_callback'  => 'themesflat_sanitize_checkbox',
        )
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
            'show_action_box',
            array(
                'type'      => 'checkbox',
                'label'     => 'Show Action Box',
                'section'   => 'flat_footer',
                'priority'  => 2
            )
        )
    );

    $wp_customize->add_setting(
        'text_action_box',
        array(
            'default'   =>  themesflat_customize_default('text_action_box'),
            'sanitize_callback'  =>  'themesflat_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'text_action_box',
        array(
            'type'      =>  'textarea',
            'label'     =>  'Text Action Box',
            'section'   =>  'flat_footer',
            'priority'  =>  3
        )
    );

    $wp_customize->add_setting(
        'text_button_action_box',
        array(
            'default'   =>  themesflat_customize_default('text_button_action_box'),
            'sanitize_callback'  =>  'themesflat_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'text_button_action_box',
        array(
            'type'      =>  'text',
            'section'   =>  'flat_footer',
            'label'     =>  'Text Button Action Box',
            'priority'  => 4
        )
    );

    // Footer widget
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-widget-footer', array(
        'label' => esc_html('footer widgets', 'evockans'),
        'section' => 'flat_footer',
        'settings' => 'themesflat_options[info]',
        'priority' => 9
        ) )
    );    

    // Desc
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_widget_footer', array(
        'label' => esc_html('This section allow to change the layout and styles of footer widgets to match as you need', 'evockans'),
        'section' => 'flat_footer',
        'settings' => 'themesflat_options[info]',
        'priority' => 10
        ) )
    );

    // Footer Style
    $wp_customize->add_setting(
        'footer_style',
        array(
            'default'           => themesflat_customize_default('footer_style'),
            'sanitize_callback' => 'themesflat_sanitize_grid_post_related',
        )
    );
    $wp_customize->add_control(
        'footer_style',
        array(
            'type'      => 'select',           
            'section'   => 'flat_footer',
            'priority'  => 14,
            'label'     => esc_html('Footer Style', 'evockans'),
            'choices'   => array(                
                1     => esc_html( 'Footer S1', 'evockans' ),
                2     => esc_html( 'Footer S2', 'evockans' ),
                3     => esc_html( 'Footer S3', 'evockans' ),
                4     => esc_html( 'Footer S4', 'evockans' ),                  
            )
        )
    );  

    // Columns Footer
    $wp_customize->add_setting(
        'footer_widget_areas',
        array(
            'default'           => themesflat_customize_default('footer_widget_areas'),
            'sanitize_callback' => 'themesflat_sanitize_grid_post_related',
        )
    );
    $wp_customize->add_control(
        'footer_widget_areas',
        array(
            'type'      => 'select',           
            'section'   => 'flat_footer',
            'priority'  => 14,
            'label'     => esc_html('Columns Footer', 'evockans'),
            'choices'   => array(                
                1     => esc_html( '1 Columns', 'evockans' ),
                2     => esc_html( '2 Columns', 'evockans' ),
                3     => esc_html( '3 Columns', 'evockans' ),
                4     => esc_html( '4 Columns', 'evockans' ), 
                5     => esc_html( '5 Columns', 'evockans' ),                 
            )
        )
    );      

    // Footer text color
    $wp_customize->add_setting(
        'footer_text_color',
        array(
            'default'           => themesflat_customize_default('footer_text_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'footer_text_color',
            array(
                'label'         => esc_html('Footer Text Color', 'evockans'),
                'section'       => 'flat_footer',
                'settings'      => 'footer_text_color',
                'priority'      => 13
            )
        )
    ); 

    // Footer title
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-footer-content', array(
        'label' => esc_html('CUSTOM FOOTER', 'evockans'),
        'section' => 'flat_footer',
        'settings' => 'themesflat_options[info]',
        'priority' => 14
        ) )
    );    

    // Desc
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_footer', array(
        'label' => esc_html('You can change the copyright text on the footer.', 'evockans'),
        'section' => 'flat_footer',
        'settings' => 'themesflat_options[info]',
        'priority' => 15
        ) )
    );

    // Show Footer
    $wp_customize->add_setting ( 
        'show_footer',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox' ,
            'default' => themesflat_customize_default('show_footer'),     
        )
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'show_footer',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show Footer', 'evockans'),
            'section'   => 'flat_footer',
            'priority'  => 18
        ))
    );  

    // Footer Box control
    $wp_customize->add_setting(
        'footer_controls',
        array(
            'default' => themesflat_customize_default('footer_controls'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control( new themesflat_BoxControls($wp_customize,
        'footer_controls',
        array(
            'label' => esc_html( 'Footer Box Controls (px)', 'evockans' ),
            'section' => 'flat_footer',
            'type' => 'box-controls',
            'priority' => 18
        ))
    );

    // Footer backgound image
    $wp_customize->add_setting(
        'footer_background_image',
        array(
            'default'           => themesflat_customize_default('footer_background_image'),
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'footer_background_image',
            array(
               'label'          => esc_html( 'Upload Your Footer Background Image', 'evockans' ),
               'type'           => 'image',
               'section'        => 'flat_footer',
               'priority'       => 18,
            )
        )
    );
   
    // Footer Copyright
    $wp_customize->add_setting(
        'footer_copyright',
        array(
            'default' => themesflat_customize_default('footer_copyright'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control(
        'footer_copyright',
        array(
            'label' => esc_html( 'Copyright', 'evockans' ),
            'section' => 'flat_footer',
            'type' => 'textarea',
            'priority' => 19
        )
    );

    // Bottom text color
    $wp_customize->add_setting(
        'bottom_text_color',
        array(
            'default'           => themesflat_customize_default('bottom_text_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'bottom_text_color',
            array(
                'label'         => esc_html('Bottom Text Color', 'evockans'),
                'section'       => 'flat_footer',
                'settings'      => 'bottom_text_color',
                'priority'      => 20
            )
        )
    );

    // Show Footer
    $wp_customize->add_setting ( 
        'show_menu_bottom',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox' ,
            'default' => themesflat_customize_default('show_menu_bottom'),     
        )
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'show_menu_bottom',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show Menu Bottom', 'evockans'),
            'section'   => 'flat_footer',
            'priority'  => 21
        ))
    );

    //__Color__//
    $wp_customize->add_panel('color_panel',array(
        'title'         => 'Color',
        'description'   => 'This is panel Description',
        'priority'      => 10,
    ));

    // ADD SECTION GENERAL
    $wp_customize->add_section('color_general',array(
        'title'         => 'General',
        'priority'      => 10,
        'panel'         => 'color_panel',
    ));

    // Heading Color Scheme
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'color_scheme', array(
        'label' => esc_html('SCHEME COLOR', 'evockans'),
        'section' => 'color_general',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );    

    // Desc color scheme
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_color_schemer', array(
        'label' => esc_html('Select the color that will be used for theme color.','evockans'),
        'section' => 'color_general',
        'settings' => 'themesflat_options[info]',
        'priority' => 2
        ) )
    ); 

    $wp_customize->add_setting(
        'primary_color',
        array(
            'default'           => themesflat_customize_default('primary_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'primary_color',
            array(
                'label'         => esc_html('Scheme color', 'evockans'),
                'section'       => 'color_general',
                'settings'      => 'primary_color',
                'priority'      => 3
            )
        )
    );    

    // Body Color
    $wp_customize->add_setting(
        'body_text_color',
        array(
            'default'           => themesflat_customize_default('body_text_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'body_text_color',
            array(
                'label'         => esc_html('Body Color', 'evockans'),
                'section'       => 'color_general',
                'settings'      => 'body_text_color',
                'priority'      => 4
            )
        )
    ); 

     // ADD SECTION HEADER COLOR
    $wp_customize->add_section('color_header',array(
        'title'=>'Header',
        'priority'=>11,
        'panel'=>'color_panel',
    ));     

    // ADD SECTION HEADER COLOR
    $wp_customize->add_section('color_header',array(
        'title'=>'Header',
        'priority'=>11,
        'panel'=>'color_panel',
    ));

    // TITLE SECTION TOP COLOR
    $wp_customize->add_setting('themesflat_options[info]', array(
        'type'              => 'info_control',
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'esc_attr',            
        )
    );
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'topbar_color', array(
        'label' => esc_html('Top Color', 'evockans'),
        'section' => 'color_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );  

    // Top bar background color
    $wp_customize->add_setting(
        'top_background_color',
        array(
            'default'           => themesflat_customize_default('top_background_color'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new themesflat_ColorOverlay(
            $wp_customize,
            'top_background_color',
            array(
                'label'         => esc_html('Topbar Backgound', 'evockans'),
                'description'   => esc_html__(' Opacity =1 for Background Color', 'evockans'),
                'section'       => 'color_header',
                'settings'      => 'top_background_color',
                'priority'      => 2
            )
        )
    );

    // Top bar text color
    $wp_customize->add_setting(
        'topbar_textcolor',
        array(
            'default'           => themesflat_customize_default('topbar_textcolor'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new themesflat_ColorOverlay(
            $wp_customize,
            'topbar_textcolor',
            array(
                'label'         => esc_html('Topbar Text Color', 'evockans'),
                'section'       => 'color_header',
                'settings'      => 'topbar_textcolor',
                'priority'      => 3
            )
        )
    );

    // MENU COLOR
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'menu_color', array(
        'label' => esc_html('MENU COLOR', 'evockans'),
        'section' => 'color_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 3
        ) )
    );    

    // Desc
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_menu_color', array(
        'label' => esc_html('Select color for background menu, background submenu color menu a, color menu a:hover, background menu a:hover...','evockans'),
        'section' => 'color_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 4
        ) )
    );   

    // Menu Background
    $wp_customize->add_setting(
        'mainnav_backgroundcolor',
        array(
            'default'           => themesflat_customize_default('mainnav_backgroundcolor'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( new themesflat_ColorOverlay(
            $wp_customize,
            'mainnav_backgroundcolor',
            array(
                'label'         => esc_html__('Mainnav Background', 'evockans'),
                'description'   => esc_html__(' Opacity =1 for Background Color', 'evockans'),
                'section'       => 'color_header',
                'priority'      => 5
            )
        )
    );   

    // Menu a color
    $wp_customize->add_setting(
        'mainnav_color',
        array(
            'default'           => themesflat_customize_default('mainnav_color'),
            'sanitize_callback' => 'esc_attr'
        )
    );
    $wp_customize->add_control(
        new themesflat_ColorOverlay(
            $wp_customize,
            'mainnav_color',
            array(
                'label' => esc_html('Mainnav a color', 'evockans'),
                'section' => 'color_header',
                'priority' => 6
            )
        )
    );

    // Menu a:hover color
    $wp_customize->add_setting(
        'mainnav_hover_color',
        array(
            'default'           => themesflat_customize_default('mainnav_hover_color'),
            'sanitize_callback' => 'esc_attr'
        )
    );
    $wp_customize->add_control(
        new themesflat_ColorOverlay(
            $wp_customize,
            'mainnav_hover_color',
            array(
                'label' => esc_html('Mainnav a:hover color & active', 'evockans'),
                'section' => 'color_header',
                'priority' => 7
            )
        )
    );

    // Sub menu a color
    $wp_customize->add_setting(
        'sub_nav_color',
        array(
            'default'           => themesflat_customize_default('sub_nav_color'),
            'sanitize_callback' => 'esc_attr'
        )
    );
    $wp_customize->add_control(
        new themesflat_ColorOverlay(
            $wp_customize,
            'sub_nav_color',
            array(
                'label' => esc_html('Sub nav a color', 'evockans'),
                'section' => 'color_header',
                'priority' => 8
            )
        )
    );

    // Sub nav background
    $wp_customize->add_setting(
        'sub_nav_background',
        array(
            'default'           => themesflat_customize_default('sub_nav_background'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new themesflat_ColorOverlay(
            $wp_customize,
            'sub_nav_background',
            array(
                'label' => esc_html('Sub nav background color', 'evockans'),
                'section' => 'color_header',
                'priority' => 9
            )
        )
    );

    // Border color li sub nav
    $wp_customize->add_setting(
        'border_color_sub_nav',
        array(
            'default'           => themesflat_customize_default('border_color_sub_nav'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new themesflat_ColorOverlay(
            $wp_customize,
            'border_color_sub_nav',
            array(
                'label' => esc_html('Border color sub nav', 'evockans'),
                'section' => 'color_header',
                'priority' => 10
            )
        )
    );

    // Sub nav background hover
    $wp_customize->add_setting(
        'sub_nav_color_hover',
        array(
            'default'   => themesflat_customize_default('sub_nav_color_hover'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new themesflat_ColorOverlay(
            $wp_customize,
            'sub_nav_color_hover',
            array(
                'label' => esc_html('Sub-menu color hover', 'evockans'),
                'section' => 'color_header',
                'priority' => 11
            )
        )
    );

    // Sub nav background hover
    $wp_customize->add_setting(
        'sub_nav_background_hover',
        array(
            'default'   => themesflat_customize_default('sub_nav_background_hover'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new themesflat_ColorOverlay(
            $wp_customize,
            'sub_nav_background_hover',
            array(
                'label' => esc_html('Sub-menu background color hover', 'evockans'),
                'section' => 'color_header',
                'priority' => 11
            )
        )
    );     

    // ADD SECTION COLOR FOOTER
    $wp_customize->add_section('color_footer',array(
        'title'=>'Footer',
        'priority'=>12,
        'panel'=>'color_panel',
    ));    

    // Footer background color    
    $wp_customize->add_setting(
        'footer_background_color',
        array(
            'default'           => themesflat_customize_default('footer_background_color'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( 
        new themesflat_ColorOverlay(
            $wp_customize,
            'footer_background_color',
            array(
                'label'         => esc_html__('Footer Backgound', 'evockans'),
                'section'       => 'color_footer',
                'settings'      => 'footer_background_color',
                'priority'      => 12
            )
        )
    );

    // Footer text color
    $wp_customize->add_setting(
        'footer_text_color',
        array(
            'default'           => themesflat_customize_default('footer_text_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'footer_text_color',
            array(
                'label'         => esc_html('Footer Text Color', 'evockans'),
                'section'       => 'color_footer',
                'settings'      => 'footer_text_color',
                'priority'      => 13
            )
        )
    ); 

    // bottom background color
    $wp_customize->add_setting(
        'bottom_background_color',
        array(
            'default'           => themesflat_customize_default('bottom_background_color'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new themesflat_ColorOverlay(
            $wp_customize,
            'bottom_background_color',
            array(
                'label'         => esc_html('Bottom Backgound', 'evockans'),
                'section'       => 'color_footer',
                'settings'      => 'bottom_background_color',
                'priority'      => 18
            )
        )
    );

    // Bottom text color
    $wp_customize->add_setting(
        'bottom_text_color',
        array(
            'default'           => themesflat_customize_default('bottom_text_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'bottom_text_color',
            array(
                'label'         => esc_html('Bottom Text Color', 'evockans'),
                'section'       => 'color_footer',
                'settings'      => 'bottom_text_color',
                'priority'      => 19
            )
        )
    ); 
   
    // Section Blog
    $wp_customize->add_section(
        'blog_options',
        array(
            'title' => esc_html('Blog Posts', 'evockans'),
            'priority' => 13,
        )
    );

    // Heading Blog
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'bloglist', array(
        'label' => esc_html('Blog List', 'evockans'),
        'section' => 'blog_options',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );    

    // Desc blog
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_bloglist', array(
        'label' => esc_html('All options in this section will be used to make style for blog page.','evockans'),
        'section' => 'blog_options',
        'settings' => 'themesflat_options[info]',
        'priority' => 2
        ) )
    );   

    $wp_customize->add_setting(
        'blog_layout',
        array(
            'default'           => themesflat_customize_default('blog_layout'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( 
        'blog_layout',
        array (
            'type'      => 'select',           
            'section'   => 'blog_options',
            'priority'  => 3,
            'label'         => esc_html('Sidebar Position', 'evockans'),
            'choices'   => array (
                'sidebar-right'     => esc_html( 'Sidebar Right','evockans' ),
                'sidebar-left'      =>  esc_html( 'Sidebar Left','evockans' ),
                'fullwidth'         =>   esc_html( 'Full Width','evockans' ),
                'fullwidth-small'   =>   esc_html( 'Full Width Small','evockans' ),
                'fullwidth-center'  =>   esc_html( 'Full Width Center','evockans' ),
                ) ,
        )
    );

    $wp_customize->add_setting(
        'blog_archive_layout',
        array(
            'default'           => themesflat_customize_default('blog_archive_layout'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( 
        'blog_archive_layout',
        array (
            'type'      => 'select',           
            'section'   => 'blog_options',
            'priority'  => 3,
            'label'         => esc_html('Blog Layout', 'evockans'),
            'choices'   => array (
                'blog-list' =>  esc_html( 'Blog List','evockans' ),                  
                'blog-grid'=> esc_html( 'Blog Grid','evockans' ),
                'blog-grid-simple'=> esc_html( 'Blog Grid Simple','evockans' ),
                )  
        )
    );

    // Gird columns Posts
    $wp_customize->add_setting(
        'blog_grid_columns',
        array(
            'default'           => themesflat_customize_default('blog_grid_columns'),
            'sanitize_callback' => 'themesflat_sanitize_grid_post_related',
        )
    );
    $wp_customize->add_control(
        'blog_grid_columns',
        array(
            'type'      => 'select',           
            'section'   => 'blog_options',
            'priority'  => 4,
            'label'     => esc_html('Post Grid Columns', 'evockans'),
            'choices'   => array(
                2     => esc_html( '2 Columns', 'evockans' ),
                3     => esc_html( '3 Columns', 'evockans' ),
                4     => esc_html( '4 Columns', 'evockans' ),                
            )
        )
    );

    $wp_customize->add_setting (
        'blog_sidebar_list',
        array(
            'default'           => themesflat_customize_default('blog_sidebar_list'),
            'sanitize_callback' => 'esc_html',
        )
    );
    $wp_customize->add_control( new themesflat_DropdownSidebars($wp_customize,
        'blog_sidebar_list',
        array(
            'type'      => 'dropdown',           
            'section'   => 'blog_options',
            'priority'  => 3,
            'label'         => esc_html('List Sidebar Position', 'evockans'),
            
        ))
    );

    // Excerpt
    $wp_customize->add_setting(
        'blog_archive_post_excepts_length',
        array(
            'default'   =>  themesflat_customize_default('blog_archive_post_excepts_length'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( new themesflat_Slide_Control( $wp_customize,
        'blog_archive_post_excepts_length',
            array(
                'type'      =>  'slide-control',
                'section'   =>  'blog_options',
                'label'     =>  'Post Excepts Length',
                'priority'  => 4,
                'input_attrs' => array(
                    'min' => 0,
                    'max' => 500,
                    'step' => 1,
                ),
            )

        )
    ); 

    // Show Header Content
    $wp_customize->add_setting ( 
        'show_header_content',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox' ,
            'default' => themesflat_customize_default('show_header_content'),     
        )
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'show_header_content',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show Header Content', 'evockans'),
            'section'   => 'blog_options',
            'priority'  => 5
        ))
    );

    // Show Content Posts
    $wp_customize->add_setting (
        'show_content',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('show_content'),     
        )
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'show_content',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show Content Posts', 'evockans'),
            'section'   => 'blog_options',
            'priority'  => 6,
        ))
    );

    // Show Read More
    $wp_customize->add_setting (
        'blog_archive_readmore',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('blog_archive_readmore'),     
        )
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'blog_archive_readmore',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show Read More', 'evockans'),
            'section'   => 'blog_options',
            'priority'  => 6,
        ))
    );    

    // Read More Text
    $wp_customize->add_setting (
        'blog_archive_readmore_text',
        array(
            'default' => themesflat_customize_default('blog_archive_readmore_text'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'blog_archive_readmore_text',
        array(
            'type'      => 'text',
            'label'     => esc_html('Read More Text', 'evockans'),
            'section'   => 'blog_options',
            'priority'  => 7
        )
    );

    // Pagination
    $wp_customize->add_setting(
        'blog_archive_pagination_style',
        array(
            'default'           => themesflat_customize_default('blog_archive_pagination_style'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( 
        'blog_archive_pagination_style',
        array(
            'type'      => 'select',           
            'section'   => 'blog_options',
            'priority'  => 8,
            'label'         => esc_html('Pagination Style', 'evockans'),
            'choices'   => array(
                'pager'     => esc_html('Pager','evockans'),
                'numeric'         =>  esc_html('Numeric','evockans'),
                'pager-numeric'         =>  esc_html('Pager & Numeric','evockans'),
                'loadmore' =>  esc_html__( 'Load More', 'evockans' )
            ),
        )
    );

    // Header Blog Single    
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'blogsingle', array(
        'label' => esc_html('Blog Single', 'evockans'),
        'section' => 'blog_options',
        'settings' => 'themesflat_options[info]',
        'priority' => 9
        ) )
    );    

    // Desc Blog Single
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_blogsingle', array(
        'label' => esc_html('Also, you can change the style for blog single to make your site unique.','evockans'),
        'section' => 'blog_options',
        'settings' => 'themesflat_options[info]',
        'priority' => 10
        ) )
    );   

    // Show Post Navigator
    $wp_customize->add_setting (
        'show_post_navigator',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('show_post_navigator'),     
        )
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'show_post_navigator',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show Post Navigator', 'evockans'),
            'section'   => 'blog_options',
            'priority'  => 12
        ))
    );

    // Enable Entry Footer Content
    $wp_customize->add_setting(
      'show_entry_footer_content',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('show_entry_footer_content'),     
        )   
    );

    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'show_entry_footer_content',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Show Entry Footer Content', 'evockans'),
            'section' => 'blog_options',
            'priority' => 14,
        ))
    );

    // Enable Social Share
    $wp_customize->add_setting(
      'show_social_share',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('show_social_share'),     
        )   
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'show_social_share',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Show Social Share', 'evockans'),
            'section' => 'blog_options',
            'priority' => 15,
        ))
    );

    // Show Related Posts
    $wp_customize->add_setting (
        'show_related_post',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => 0,     
        )
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'show_related_post',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show Related Posts', 'evockans'),
            'section'   => 'blog_options',
            'priority'  => 16
        ))
    );

    //Related Posts Style
    $wp_customize->add_setting(
        'related_post_style',
        array(
            'default'           => themesflat_customize_default('related_post_style'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( 
        'related_post_style',
        array(
            'type'      => 'select',           
            'section'   => 'blog_options',
            'priority'  => 16,
            'label'         => esc_html('Related Posts Style', 'evockans'),
            'choices'   => array(
                'blog-list' => esc_html( 'Blog List','evockans' ),
                'blog-grid'=>   esc_html( 'Blog Grid','evockans' ),
        ))
    );

    // Gird columns Related Posts
    $wp_customize->add_setting(
        'grid_columns_post_related',
        array(
            'default'           => 3,
            'sanitize_callback' => 'themesflat_sanitize_grid_post_related',
        )
    );
    $wp_customize->add_control(
        'grid_columns_post_related',
        array(
            'type'      => 'select',           
            'section'   => 'blog_options',
            'priority'  => 17,
            'label'     => esc_html('Columns Of Related Posts', 'evockans'),
            'choices'   => array(                
                2     => esc_html( '2 Columns', 'evockans' ),
                3     => esc_html( '3 Columns', 'evockans' ),
                4     => esc_html( '4 Columns', 'evockans' ),                
            )
        )
    );

    // Number Of Related Posts
    $wp_customize->add_setting (
        'number_related_post',
        array(
            'default' => esc_html('3', 'evockans'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'number_related_post',
        array(
            'type'      => 'text',
            'label'     => esc_html('Number Of Related Posts', 'evockans'),
            'section'   => 'blog_options',
            'priority'  => 18
        )
    );

    // Section portfolio
    $wp_customize->add_section(
        'portfolio_options',
        array(
            'title' => esc_html('Portfolio', 'evockans'),
            'priority' => 14,
        )
    );      
    // Header Portfolio Single    
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'portfoliosingle', array(
        'label' => esc_html('PORTFOLIO SINGLE', 'evockans'),
        'section' => 'portfolio_options',
        'settings' => 'themesflat_options[info]',
        'priority' => 10
        ) )
    );   

    // Show Post Navigator portfolio
    $wp_customize->add_setting (
        'portfolio_show_post_navigator',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => 1,     
        )
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'portfolio_show_post_navigator',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show Single Navigator', 'evockans'),
            'section'   => 'portfolio_options',
            'priority'  => 12
        ))
    );

    // Show Related Portfolios
    $wp_customize->add_setting (
        'show_related_portfolio',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('show_related_portfolio'),     
        )
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'show_related_portfolio',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show Related Portfolios', 'evockans'),
            'section'   => 'portfolio_options',
            'priority'  => 13
        ))
    );

    // Title widget reated
    $wp_customize->add_setting (
        'title_related_portfolio',
        array(
            'default' => esc_html('Related Portfolio', 'evockans'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'title_related_portfolio',
        array(
            'type'      => 'text',
            'label'     => esc_html('Portfolio panel title', 'evockans'),
            'section'   => 'portfolio_options',
            'priority'  => 14
        )
    );    

    // Gird columns portfolio related
    $wp_customize->add_setting(
        'grid_columns_portfolio_related',
        array(
            'default'           => themesflat_customize_default('grid_columns_portfolio_related'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        'grid_columns_portfolio_related',
        array(
            'type'      => 'select',           
            'section'   => 'portfolio_options',
            'priority'  => 16,
            'label'     => esc_html('Columns Related', 'evockans'),
            'choices'   => array(
                2     => esc_html( '2 Columns', 'evockans' ),
                3     => esc_html( '3 Columns', 'evockans' ),
                4     => esc_html( '4 Columns', 'evockans' )
            )
        )
    );
    
    // Number Of Related Portfolios
    $wp_customize->add_setting (
        'number_related_portfolio',
        array(
            'default' => themesflat_customize_default('number_related_portfolio'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'number_related_portfolio',
        array(
            'type'      => 'text',
            'label'     => esc_html('Number Of Related Portfolios', 'evockans'),
            'section'   => 'portfolio_options',
            'priority'  => 17
        )
    );
    
    // Section Typography
    $wp_customize->add_section(
        'flat_typography',
        array(
            'title' => esc_html('Typography', 'evockans'),
            'priority' => 6,            
        )
    );
    // Heading Typography
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-typography', array(
        'label' => esc_html('BODY FONT', 'evockans'),
        'section' => 'flat_typography',
        'settings' => 'themesflat_options[info]',
        'priority' => 2
        ) )
    );    

    // Desc Typography
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_logo', array(
        'label' => esc_html('You can modify the font family, size, color, ... for global content.', 'evockans'),
        'section' => 'flat_typography',
        'settings' => 'themesflat_options[info]',
        'priority' => 3
        ) )
    );
  
    //__Page title and breadcrumb__//
    $wp_customize->add_panel('page_title_panel',array(
        'title'         => esc_html__('Page Title & Breadcrumb','evockans'),
        'description'   => 'This is panel Description',
        'priority'      => 10,
    ));

    // ADD SECTION PAGE TITLE
    $wp_customize->add_section('page_title_style',array(
        'title'         => esc_html__('Page Title Style','evockans'),
        'priority'      => 10,
        'panel'         => 'page_title_panel',
    ));

   // Heading Page Title
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'page_title_style', array(
        'label' => esc_html__('Page Title Style', 'evockans'),
        'section' => 'page_title_style',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );  

    // Show page title
    $wp_customize->add_setting(
      'show_page_title',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('show_page_title'),     
        )   
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'show_page_title',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Show page title', 'evockans'),
            'section' => 'page_title_style',
            'priority' => 2,
        ))
    ); 

    $wp_customize->add_setting(
        'page_title_styles',
        array(
            'default'           => themesflat_customize_default('page_title_styles'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( 
        'page_title_styles',
        array (
            'type'      => 'select',           
            'section'   => 'page_title_style',
            'priority'  => 3,
            'label'         => esc_html('Page Title Style', 'evockans'),
            'choices'   => array (
                'inline' =>  esc_html( 'Inline Heading & Breadcrumbs','evockans' ),
                'default' =>  esc_html( 'Default','evockans' ),
                'parallax' =>  esc_html( 'Parallax','evockans' ),
                'video' =>  esc_html( 'Video','evockans' )
                ) ,
        )
    );

    $wp_customize->add_setting(
        'page_title_alignment',
        array(
            'default'           => themesflat_customize_default('page_title_alignment'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( 
        'page_title_alignment',
        array (
            'type'      => 'select',           
            'section'   => 'page_title_style',
            'priority'  => 4,
            'label'         => esc_html('Page Title Alignment', 'evockans'),
            'choices'   => array (
                'left' =>  esc_html( 'Left','evockans' ),
                'center' =>  esc_html( 'Center','evockans' ),
                'right' =>  esc_html( 'Right','evockans' )
                ) ,
        )
    ); 

     //Page Title Background
    $wp_customize->add_setting(
        'page_title_background_image',
        array(
            'default' => themesflat_customize_default('page_title_background_image'),
            'sanitize_callback' => 'esc_url_raw',
        )
    );    
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'page_title_background_image',
            array(
               'label'          => esc_html( 'Upload Your Page Title Background Image', 'evockans' ),
               'type'           => 'image',
               'section'        => 'page_title_style',
               'priority'       => 5,
            )
        )
    );

    $wp_customize->add_setting (
        'page_title_video_url',
        array(
            'default' => themesflat_customize_default('page_title_video_url') ,
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'page_title_video_url',
        array(
            'type'      => 'text',
            'label'     => esc_html('Page Title Background Video URL', 'evockans'),
            'section'   => 'page_title_style',
            'priority'  => 6
        )
    );

    // Page Title Color
    $wp_customize->add_setting(
        'page_title_text_color',
        array(
            'default'           => themesflat_customize_default('page_title_text_color'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new themesflat_ColorOverlay(
            $wp_customize,
            'page_title_text_color',
            array(
                'label'         => esc_html('Page Heading Text Color', 'evockans'),
                'section'       => 'page_title_style',
                'priority'      => 7
            )
        )
    );

    // Page Title Link Color
    $wp_customize->add_setting(
        'page_title_link_color',
        array(
            'default'           => themesflat_customize_default('page_title_link_color'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new themesflat_ColorOverlay(
            $wp_customize,
            'page_title_link_color',
            array(
                'label'         => esc_html('Breadcrumb Text Color', 'evockans'),
                'section'       => 'page_title_style',
                'priority'      => 8
            )
        )
    );

    // Page Title Overlay
    $wp_customize->add_setting(
        'page_title_background_color',
        array(
            'default'           => themesflat_customize_default('page_title_background_color'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( new themesflat_ColorOverlay(
            $wp_customize,
            'page_title_background_color',
            array(
                'label'         => esc_html__('Page Title Overlay Background Color', 'evockans'),
                'description'   => esc_html__(' Opacity =1 for Background Color', 'evockans'),
                'section'       => 'page_title_style',
                'priority'      => 9
            )
        )
    );      

    // Box control
    $wp_customize->add_setting(
        'page_title_controls',
        array(
            'default' => themesflat_customize_default('page_title_controls'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control( new themesflat_BoxControls($wp_customize,
        'page_title_controls',
        array(
            'label' => esc_html( 'Page Title Controls (px)', 'evockans' ),
            'section' => 'page_title_style',
            'type' => 'box-controls',
            'priority' => 10
        ))
    );  

    // Show page heading
    $wp_customize->add_setting(
      'show_page_title_heading',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('show_page_title_heading'),     
        )   
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'show_page_title_heading',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Show page title heading', 'evockans'),
            'section' => 'page_title_style',
            'priority' => 11,
        ))
    );

    // ADD SECTION BREADCRUMB
    $wp_customize->add_section('page_break_crumb_section',array(
        'title'         => esc_html__('Page Breadcrumb','evockans'),
        'priority'      => 10,
        'panel'         => 'page_title_panel',
    ));

   // Breadcrumb section
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'page_break_crumb_section', array(
        'label' => esc_html__('Page Breadcrumb', 'evockans'),
        'section' => 'page_break_crumb_section',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );  

    // Breadcrumb
    $wp_customize->add_setting(
      'breadcrumb_enabled',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default('breadcrumb_enabled'),     
        )   
    );
    $wp_customize->add_control( new themesflat_Checkbox( $wp_customize,
        'breadcrumb_enabled',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Enable Breadcrumb', 'evockans'),
            'section' => 'page_break_crumb_section',
            'priority' => 14,
        ))
    );    

    $wp_customize->add_setting (
        'bread_crumb_prefix',
        array(
            'default' => themesflat_customize_default('bread_crumb_prefix') ,
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'bread_crumb_prefix',
        array(
            'type'      => 'text',
            'label'     => esc_html('Breadcrumb Prefix', 'evockans'),
            'section'   => 'page_break_crumb_section',
            'priority'  => 15
        )
    );  

    $wp_customize->add_setting (
        'breadcrumb_separator',
        array(
            'default' => themesflat_customize_default('breadcrumb_separator'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'breadcrumb_separator',
        array(
            'type'      => 'text',
            'label'     => esc_html('Breadcrumb Separator', 'evockans'),
            'section'   => 'page_break_crumb_section',
            'priority'  => 16
        )
    );    

    // Body fonts
    $wp_customize->add_setting(
        'body_font_name',
        array(
            'default' => themesflat_customize_default('body_font_name'),
            'sanitize_callback' => 'esc_html',
        )
    );
    $wp_customize->add_control( new themesflat_Typography($wp_customize,
        'body_font_name',
        array(
            'label' => esc_html( 'Font name/style/sets', 'evockans' ),
            'section' => 'flat_typography',
            'type' => 'typography',
            'fields' => array('family','style','line_height','size'),
            'priority' => 4
        ))
    );

    // Headings fonts
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-heading-font', array(
        'label' => esc_html('Headings fonts', 'evockans'),
        'section' => 'flat_typography',
        'settings' => 'themesflat_options[info]',
        'priority' => 8
        ) )
    );    

    // Desc font
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_heading-font', array(
        'label' => esc_html('You can modify the font options for your headings. h1, h2, h3, h4, ...', 'evockans'),
        'section' => 'flat_typography',
        'settings' => 'themesflat_options[info]',
        'priority' => 9
        ) )
    );   

    $wp_customize->add_setting(
        'headings_font_name',
        array(
            'default' => themesflat_customize_default('headings_font_name'),
            'sanitize_callback' => 'esc_html',
        )
    );
    $wp_customize->add_control( new themesflat_Typography($wp_customize,
        'headings_font_name',
        array(
            'label' => esc_html( 'Font name/style/sets', 'evockans' ),
            'section' => 'flat_typography',
            'type' => 'typography',
            'fields' => array('family','style'),
            'priority' => 11
        ))
    );

    // H1 size
    $wp_customize->add_setting(
        'h1_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           => themesflat_customize_default('h1_size'),
        )       
    );
    $wp_customize->add_control( 'h1_size', array(
        'type'        => 'number',
        'priority'    => 13,
        'section'     => 'flat_typography',
        'label'       => esc_html('H1 font size (px)', 'evockans'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // H2 size
    $wp_customize->add_setting(
        'h2_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           =>  themesflat_customize_default('h2_size'),
        )       
    );
    $wp_customize->add_control( 'h2_size', array(
        'type'        => 'number',
        'priority'    => 14,
        'section'     => 'flat_typography',
        'label'       => esc_html('H2 font size (px)', 'evockans'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // H3 size
    $wp_customize->add_setting(
        'h3_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           => themesflat_customize_default('h3_size'),
        )       
    );
    $wp_customize->add_control( 'h3_size', array(
        'type'        => 'number',
        'priority'    => 15,
        'section'     => 'flat_typography',
        'label'       => esc_html('H3 font size (px)', 'evockans'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // H4 size
    $wp_customize->add_setting(
        'h4_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           =>  themesflat_customize_default('h4_size'),
        )       
    );
    $wp_customize->add_control( 'h4_size', array(
        'type'        => 'number',
        'priority'    => 16,
        'section'     => 'flat_typography',
        'label'       => esc_html('H4 font size (px)', 'evockans'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // H5 size
    $wp_customize->add_setting(
        'h5_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           =>  themesflat_customize_default('h5_size'),
        )       
    );
    $wp_customize->add_control( 'h5_size', array(
        'type'        => 'number',
        'priority'    => 17,
        'section'     => 'flat_typography',
        'label'       => esc_html('H5 font size (px)', 'evockans'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // H6 size
    $wp_customize->add_setting(
        'h6_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           =>  themesflat_customize_default('h6_size'),
        )       
    );
    $wp_customize->add_control( 'h6_size', array(
        'type'        => 'number',
        'priority'    => 18,
        'section'     => 'flat_typography',
        'label'       => esc_html('H6 font size (px)', 'evockans'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // Heading Menu fonts
    $wp_customize->add_setting('themesflat_options[info]', array(
            'type'              => 'info_control',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'esc_attr',            
        )
    );
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'menu_fonts', array(
        'label' => esc_html('Menu fonts', 'evockans'),
        'section' => 'flat_typography',
        'settings' => 'themesflat_options[info]',
        'priority' => 19
        ) )
    );

    $wp_customize->add_setting(
        'menu_font_name',
        array(
            'default' => themesflat_customize_default('menu_font_name'),
                'sanitize_callback' => 'esc_html',
        )
    );
    $wp_customize->add_control( new themesflat_Typography($wp_customize,
        'menu_font_name',
        array(
            'label' => esc_html( 'Font name/style/sets', 'evockans' ),
            'section' => 'flat_typography',
            'type' => 'typography',
            'fields' => array('family','style','size','line_height'),
            'priority' => 20
        ))
    );

    $wp_customize->add_setting(
        'layout_version',
        array(
            'default'           => themesflat_customize_default('layout_version'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( 
        'layout_version',
        array(
            'type'      => 'select',           
            'section'   => 'colors',
            'priority'  => 7,
            'label'         => esc_html('Layout version', 'evockans'),
            'choices'   => array(
                'wide'           =>  esc_html('Wide','evockans'),
                'boxed'         =>   esc_html('Boxed','evockans'),
        ))
    );

    // body background color
    $wp_customize->add_setting(
        'body_background_color',
        array(
            'default'           => themesflat_customize_default('body_background_color'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new themesflat_ColorOverlay(
            $wp_customize,
            'body_background_color',
            array(
                'label'         => esc_html('Body Background Color', 'evockans'),
                'description'   => esc_html__(' Opacity =1 for Background Color', 'evockans'),
                'section'       => 'colors',
                'priority'      => 8
            )
        )
    );

    // PAGE SIDEBAR
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'layout_body', array(
        'label' => esc_html('PAGE SIDEBAR', 'evockans'),
        'section' => 'colors',
        'settings' => 'themesflat_options[info]',
        'priority' => 10
        ) )
    );      

    $wp_customize->add_setting(
        'page_layout',
        array(
            'default'           => themesflat_customize_default('page_layout'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        'page_layout',
        array (
            'type'      => 'select',           
            'section'   => 'colors',
            'priority'  => 12,
            'label'         => esc_html('Sidebar Position', 'evockans'),
            'choices'   => array (
                'sidebar-right'     =>  esc_html( 'Sidebar Right','evockans' ),
                'sidebar-left'      =>   esc_html( 'Sidebar Left','evockans' ),
                'fullwidth'         =>   esc_html( 'Full Width','evockans' ),
                'fullwidth-small'   =>   esc_html( 'Full Width Small','evockans' ),
                'fullwidth-center'  =>   esc_html( 'Full Width Center','evockans' ),
        ))
    );

    $wp_customize->add_setting (
        'page_sidebar_list',
        array(
            'default'           => themesflat_customize_default('page_sidebar_list'),
            'sanitize_callback' => 'esc_html',
        )
    );
    $wp_customize->add_control( new themesflat_DropdownSidebars($wp_customize,
        'page_sidebar_list',
        array(
            'type'      => 'dropdown',           
            'section'   => 'colors',
            'priority'  => 13,
            'label'         => esc_html('List Sidebar Position', 'evockans'),            
        ))
    );

    // PAGE 404
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'layout_404', array(
        'label' => esc_html('PAGE 404', 'evockans'),
        'section' => 'colors',
        'settings' => 'themesflat_options[info]',
        'priority' => 14
        ) )
    );

    $wp_customize->add_setting(
        'page_404_style',
        array(
            'default'           => themesflat_customize_default('page_404_style'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        'page_404_style',
        array (
            'type'      => 'select',           
            'section'   => 'colors',
            'priority'  => 15,
            'label'         => esc_html('Page 404 Style', 'evockans'),
            'choices'   => array (
                'page-404-style1'  =>  esc_html( 'Page 404 Style 1','evockans' ),
                'page-404-style2'  =>   esc_html( 'Page 404 Style 2','evockans' ),
                'page-404-style3'  =>   esc_html( 'Page 404 Style 3','evockans' ),
        ))
    );

    //Background Video 404
    $wp_customize->add_setting(
        'page_404_bg_video_url',
        array(
            'default' => themesflat_customize_default('page_404_bg_video_url'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );    
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'page_404_bg_video_url',
            array(
                'label' => esc_html( 'Enter Url Video Background 404 Style1', 'evockans' ),
                'section' => 'colors',
                'type' => 'text',
                'description'    => esc_html( 'ex: http://corpthemes.com/wordpress/evockans/demo/Videos.mp4', 'evockans' ),
                'priority'       => 15,
            )
        )
    );

    // PAGE Comming Soon
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'layout_comming_soon', array(
        'label' => esc_html('PAGE COMMING SOON', 'evockans'),
        'section' => 'colors',
        'settings' => 'themesflat_options[info]',
        'priority' => 16
        ) )
    );

    // Comming Soon Time
    $wp_customize->add_setting(
        'comming_soon_time',
        array(
            'default' => themesflat_customize_default('comming_soon_time'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control(
        'comming_soon_time',
        array(
            'label' => esc_html( 'Comming Soon Set Time', 'evockans' ),
            'section' => 'colors',
            'type' => 'text',
            'description'    => esc_html( 'Year/Month/Day', 'evockans' ),
            'priority' => 17
        )
    );

    //Background image Comming Soon
    $wp_customize->add_setting(
        'bg_img_comming_soon',
        array(
            'default' => themesflat_customize_default('bg_img_comming_soon'),
            'sanitize_callback' => 'esc_url_raw',
        )
    );    
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'bg_img_comming_soon',
            array(
               'label'          => esc_html( 'Upload Your Comming Soon Background Image', 'evockans' ),
               'type'           => 'image',
               'section'        => 'colors',
               'priority'       => 18,
            )
        )
    );

    // Comming Soon background color
    $wp_customize->add_setting(
        'comming_soon_background_color',
        array(
            'default'           => themesflat_customize_default('comming_soon_background_color'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new themesflat_ColorOverlay(
            $wp_customize,
            'comming_soon_background_color',
            array(
                'label'         => esc_html('Comming Soon Backgound Color', 'evockans'),
                'description'   => esc_html__(' Opacity =1 for Background Color', 'evockans'),
                'section'       => 'colors',
                'settings'      => 'comming_soon_background_color',
                'priority'      => 19
            )
        )
    );

}
add_action( 'customize_register', 'themesflat_customize_register' );

// Text
function themesflat_sanitize_text( $input ) {
    return wp_kses_post( $input );
}

// Background size
function themesflat_sanitize_bg_size( $input ) {
    $valid = array(
        'cover'     => esc_html('Cover', 'evockans'),
        'contain'   => esc_html('Contain', 'evockans'),
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Blog Layout
function themesflat_sanitize_blog( $input ) {
    $valid = array(
        'sidebar-right'    => esc_html( 'Sidebar right', 'evockans' ),
        'sidebar-left'    => esc_html( 'Sidebar left', 'evockans' ),
        'fullwidth'  => esc_html( 'Full width (no sidebar)', 'evockans' )

    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// themesflat_sanitize_pagination
function themesflat_sanitize_pagination ( $input ) {
    $valid = array(
        'pager' => esc_html__('Pager', 'evockans'),
        'numeric' => esc_html__('Numeric', 'evockans'),
        'page_numeric' => esc_html__('Pager & Numeric', 'evockans')                
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// themesflat_sanitize_pagination
function themesflat_sanitize_layout_version ( $input ) {
    $valid = array(
        'boxed' => esc_html__('Boxed', 'evockans'),
        'wide' => esc_html__('Wide', 'evockans')          
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// themesflat_sanitize_related_post
function themesflat_sanitize_related_post ( $input ) {
    $valid = array(
        'simple_list' => esc_html__('Simple List', 'evockans'),
        'grid' => esc_html__('Grid', 'evockans')        
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Footer widget areas
function themesflat_sanitize_fw( $input ) {
    $valid = array(
        '0' => esc_html__('footer_default', 'evockans'),
        '1' => esc_html__('One', 'evockans'),
        '2' => esc_html__('Two', 'evockans'),
        '3' => esc_html__('Three', 'evockans'),
        '4' => esc_html__('Four', 'evockans')
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Header style sanitize
function themesflat_sanitize_headerstyle( $input ) {
    $valid = themesflat_predefined_header_styles();
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Checkboxes
function themesflat_sanitize_checkbox( $input ) {
    if ( $input == 1 ) {
        return 1;
    } else {
        return '';
    }
}

// Themesflat_sanitize_related_portfolio
function themesflat_sanitize_related_portfolio( $input ) {
    $valid = array(
        'grid'                 => esc_html( 'Grid', 'evockans' ),
        'grid_masonry'         => esc_html( 'Grid Masonry', 'evockans' ),
        'grid_nomargin'        => esc_html( 'Grid Masonry No Margin', 'evockans' ),
        'carosuel'             => esc_html( 'Carosuel', 'evockans' ),
        'carosuel_nomargin'    => esc_html( 'Carosuel No Margin', 'evockans' )       
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Themesflat_sanitize_portfolio_pagination
function themesflat_sanitize_portfolio_pagination( $input ) {
    $valid = array(
        'page_numeric'         => esc_html( 'Pager & Numeric', 'evockans' ),
        'load_more'         => esc_html( 'Load More', 'evockans' )     
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Themesflat_sanitize_portfolio_order
function themesflat_sanitize_portfolio_order( $input ) {
    $valid = array(
        'date'          => esc_html( 'Date', 'evockans' ),
        'id'            => esc_html( 'Id', 'evockans' ),
        'author'        => esc_html( 'Author', 'evockans' ),
        'title'         => esc_html( 'Title', 'evockans' ),
        'modified'      => esc_html( 'Modified', 'evockans' ),
        'comment_count' => esc_html( 'Comment Count', 'evockans' ),
        'menu_order'    => esc_html( 'Menu Order', 'evockans' )     
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Themesflat_sanitize_portfolio_order_direction
function themesflat_sanitize_portfolio_order_direction( $input ) {
    $valid = array(
        'DESC' => esc_html( 'Descending', 'evockans' ),
        'ASC'  => esc_html( 'Assending', 'evockans' )       
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Themesflat_sanitize_grid_portfolio
function themesflat_sanitize_grid_portfolio( $input ) {
    $valid = array(
        'portfolio-two-columns'     => esc_html( '2 Columns', 'evockans' ),
        'portfolio-three-columns'     => esc_html( '3 Columns', 'evockans' ),
        'portfolio-four-columns'     => esc_html( '4 Columns', 'evockans' ),
        'portfolio-five-columns'     => esc_html( '5 Columns', 'evockans' )
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// themesflat_sanitize_grid_portfolio_related
function themesflat_sanitize_grid_portfolio_related( $input ) {
    $valid = array(
        'portfolio-one-columns'     => esc_html( '1 Columns', 'evockans' ),
        'portfolio-two-columns'     => esc_html( '2 Columns', 'evockans' ),
        'portfolio-three-columns'     => esc_html( '3 Columns', 'evockans' ),
        'portfolio-four-columns'     => esc_html( '4 Columns', 'evockans' ),
        'portfolio-five-columns'     => esc_html( '5 Columns', 'evockans' )
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Themesflat_sanitize_grid_post_related
function themesflat_sanitize_grid_post_related( $input ) {
    $valid = array(        
        2     => esc_html( '2 Columns', 'evockans' ),
        3   => esc_html( '3 Columns', 'evockans' ),
        4    => esc_html( '4 Columns', 'evockans' ), 
        5    => esc_html( '5 Columns', 'evockans' ),       
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// themesflat_sanitize_layout_product
function themesflat_sanitize_layout_product( $input ) {
    $valid = array(        
        'fullwidth'         => esc_html( 'No Sidebar', 'evockans' ),
        'sidebar-right'           => esc_html( 'Sidebar Right', 'evockans' ),
        'sidebar-left'         => esc_html( 'Sidebar Left', 'evockans' )
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

