<?php
/**
 * @package evockans
 */
//Output all custom styles for this theme

function themesflat_custom_styles( $custom ) {
	$custom = '';
	$logo_position = themesflat_decode(themesflat_get_opt('logo_controls'));
	themesflat_render_box_position(".logo",$logo_position);

	$style = "padding-left: {$logo_position['padding-left']}px";

	themesflat_render_style('.header-style5 .wrap-header-content, .header-style4 .wrap-header-content',$style);
	$logo_width = themesflat_get_opt('logo_width');

	// Logo Height
	if ( $logo_width !='' ) {
		$custom .= ".logo img, .logo svg { width:" . $logo_width . "px; }"."\n";
	}
    $footer_controls = themesflat_decode(themesflat_choose_opt('footer_controls'));
    themesflat_render_box_position(".footer",$footer_controls);

	$page_title_controls = themesflat_decode(themesflat_choose_opt('page_title_controls'));
    themesflat_render_box_position(".page-title",$page_title_controls);

    //  Page Title Opacity
	$page_title_background_color = themesflat_choose_opt( 'page_title_background_color');
	if ( $page_title_background_color !='' ) {
		$custom .= ".page-title .overlay { background:" . esc_attr($page_title_background_color) . ";}"."\n";
	}

    $page_title_img = themesflat_choose_opt('page_title_background_image');
    if ( $page_title_img !='' ) {
	    $custom .= '.page-title {background: url('.$page_title_img.') center no-repeat;}'."\n";  
	}
	$custom .= ".page-title h1 {color:" . themesflat_choose_opt('page_title_text_color') . ";}"."\n";

	$custom .= ".breadcrumbs span, .breadcrumbs span a, .breadcrumbs a, .breadcrumbs span i, .breadcrumbs span.trail-browse i {color:" . themesflat_choose_opt('page_title_link_color') . ";}"."\n"; 

	$font = themesflat_get_json('body_font_name');

	$font_style = themesflat_font_style($font['style']);

	$body_fonts = $font['family'];

	$body_line_height = $font['line_height'];

	$body_font_weight = $font_style[0];

	$body_font_style = $font_style[1];

	$body_size = $font['size'];		

	$headings_fonts_ = themesflat_get_json('headings_font_name');

	$headings_fonts_family = $headings_fonts_['family'];	

	$headings_style = themesflat_font_style( $headings_fonts_['style'] );

	$headings_font_weight = $headings_style[0];

	$headings_font_style = $headings_style[1];

	$menu_fonts_ = themesflat_get_json('menu_font_name');

	$menu_fonts_family = $menu_fonts_['family'];

	$menu_fonts_size = $menu_fonts_['size'];

	$menu_line_height = $menu_fonts_['line_height'];

	$menu_style = themesflat_font_style( $menu_fonts_['style'] );

	$menu_font_weight = $menu_style[0];

	$menu_font_style = $menu_style[1];	

	// Body font family
	if ( $body_fonts !='' ) {
		$custom .= "body,button,input,select,textarea { font-family:'" . $body_fonts . "';}"."\n";
	}

	// Body font weight
	if ( $body_font_weight !='' ) {
		$custom .= "body,button,input,select,textarea { font-weight:" . $body_font_weight . ";}"."\n";
	}

	// Body font style
	if ( isset( $body_font_style ) ) {
        $custom .= "body,button,input,select,textarea { font-style:" . $body_font_style . "; }"."\n";        
	}

    // Body font size
    if ( $body_size !=''  ) {
        $custom .= "body,button,input,select,textarea { font-size:" . intval( $body_size ) . "px; }"."\n";    
    }

    // Body line height
    if ( $body_line_height != '' ) {
        $custom .= "body,button,input,select,textarea { line-height:" . intval( $body_line_height ) . "px ; }"."\n";    
    }

	// Headings font family
	if ( $headings_fonts_family !='' ) {
		$custom .= "h1,h2,h3,h4,h5,h6 { font-family:" . $headings_fonts_family . ";}"."\n";

	}

	//Headings font weight
	if ( $headings_font_weight !='' ) {
		$custom .= "h1,h2,h3,h4,h5,h6 { font-weight:" . $headings_font_weight . ";}"."\n";
	}

	// Headings font style
	if ( isset( $headings_font_style )) {
        $custom .= "h1,h2,h3,h4,h5,h6  { font-style:" . $headings_font_style . "; }"."\n";
	}

	// Menu font family
	if ( $menu_fonts_family != '') {
		$custom .= "#mainnav > ul > li > a, #mainnav ul.sub-menu > li > a { font-family:" . $menu_fonts_family . ";}"."\n";
	}

	// Menu font weight
	if ( $menu_font_weight != '' ) {
		$custom .= "#mainnav > ul > li > a, #mainnav ul.sub-menu > li > a { font-weight:" . $menu_font_weight . ";}"."\n";
	}

	// Menu font style
	if ( isset( $menu_font_style )) {
        $custom .= "#mainnav > ul > li > a, #mainnav ul.sub-menu > li > a  { font-style:" . $menu_font_style . "; }"."\n";   
	}

    // Menu font size
    if ( $menu_fonts_size != '' ) {
        $custom .= "#mainnav ul li a, #mainnav ul.sub-menu > li > a { font-size:" . intval($menu_fonts_size) . "px;}"."\n";
    }

    // Menu line height
    if ( $menu_line_height != '' ) {
        $custom .= "#mainnav > ul > li > a, #header .show-search a, #header .wrap-cart-count, .button-menu { line-height:" . intval($menu_line_height) . "px;}"."\n";
    }  

	// H1 font size
	if ( $h1_size = themesflat_get_opt( 'h1_size' ) ) {
		$custom .= "h1 { font-size:" . intval($h1_size) . "px; }"."\n";
	}

    // H2 font size
    if ( $h2_size = themesflat_get_opt( 'h2_size' ) ) {
        $custom .= "h2 { font-size:" . intval($h2_size) . "px; }"."\n";
    }

    // H3 font size
    if ( $h3_size = themesflat_get_opt( 'h3_size' ) ) {
        $custom .= "h3 { font-size:" . intval($h3_size) . "px; }"."\n";
    }

    // H4 font size
    if ( $h4_size = themesflat_get_opt( 'h4_size' ) ) {
        $custom .= "h4 { font-size:" . intval($h4_size) . "px; }"."\n";
    }

    // H5 font size
    if ( $h5_size = themesflat_get_opt( 'h5_size' ) ) {
        $custom .= "h5 { font-size:" . intval($h5_size) . "px; }"."\n";
    }

    // H6 font size
    if ( $h6_size = themesflat_get_opt( 'h6_size' ) ) {
        $custom .= "h6 { font-size:" . intval($h6_size) . "px; }"."\n";
    }   

    // Body color
	$body_text = themesflat_get_opt( 'body_text_color' );

	if ($body_text !='') {
		$custom .= "#Financial_Occult text,#F__x26__O tspan { fill:" . esc_attr( $body_text ) . ";}"."\n";
		$custom .= "body { color:" . esc_attr($body_text) . "}"."\n";

		$custom .= ".themesflat-portfolio .item .category-post a:hover,ul.iconlist .list-title .testimonial-content blockquote,.testimonial-content .author-info,.themesflat_counter.style2 .themesflat_counter-content-right,.themesflat_counter.style2 .themesflat_counter-content-left, .page-links a:hover, .page-links a:focus,.widget_search .search-form input[type=search],.entry-meta ul,.entry-meta ul.meta-right,.entry-footer strong, .themesflat_button_container .themesflat-button.no-background, .woocommerce div.product .woocommerce-tabs ul.tabs li a, .themesflat-action-box p, .portfolio-single .entry-content { color:" . esc_attr($body_text) . "}"."\n";

		//border bodycolor
		$custom .= ".widget .widget-title:after, .widget .widget-title:before,ul.iconlist li.circle:before { background-color:" . esc_attr($body_text) . "}"."\n";
	}

	// background bodycolor
	$custom .= ".page-links a:hover, .page-links a:focus, .page-links > span { border-color:" . esc_attr($body_text) . "}"."\n";

    if ( themesflat_get_opt ('top_background_color') !='' ) {
		$custom .= ".themesflat-top { background-color:" . esc_attr(themesflat_get_opt ('top_background_color')) ." ; } "."\n";
    }

    // background bodycolor
    if ( themesflat_choose_opt ('body_background_color') !='' ) {
		$custom .= "body, .page-wrap, .boxed .themesflat-boxed { background-color:" . esc_attr(themesflat_choose_opt ('body_background_color')) ." ; } "."\n";
    }	

    //Top text color
    $top_text_color = themesflat_get_opt( 'topbar_textcolor' );
    if ( themesflat_get_opt( 'topbar_textcolor' ) !='' ) {
	    $border_topbar_color = themesflat_hex2rgba($top_text_color,0.2);
    	$custom .= ".themesflat-top, .themesflat-top .content-left ul > li, .themesflat-top ul.themesflat-socials > li, .themesflat-top ul.themesflat-socials > li:last-child { border-color: ".esc_attr($border_topbar_color).";}";

		$custom .= ".themesflat-top, .info-top-right, .themesflat-top a  { color:" . esc_attr( themesflat_get_opt( 'topbar_textcolor' ) ) ." ;} "."\n";
    }	  

    // Menu Background
	$mainnav_backgroundcolor = themesflat_choose_opt( 'mainnav_backgroundcolor');
	if ( $mainnav_backgroundcolor !='' ) {		
		$custom .= ".header.widget-header .nav { background-color:" . esc_attr( $mainnav_backgroundcolor ) . ";}"."\n";
	} 	

	// Menu mainnav a color
	$mainnav_color = themesflat_choose_opt( 'mainnav_color');
	if ( $mainnav_color !='' ) {
		$custom .= "#mainnav > ul > li > a, header .show-search a , .show-search.active a > .fa-search:before, #header .wrap-cart-count a, .btn-menu:before, .btn-menu:after, .btn-menu span { color:" . esc_attr( $mainnav_color ) . ";}"."\n";
		$custom .= ".btn-menu:before, .btn-menu:after, .btn-menu span { background:" . esc_attr( $mainnav_color ) . ";}"."\n";
	}

	// mainnav_hover_color
	$mainnav_hover_color = themesflat_choose_opt( 'mainnav_hover_color');

	if ( $mainnav_hover_color !='' ) {
		$custom .= "#mainnav > ul > li > a:hover,#mainnav > ul > li.current-menu-item > a, #mainnav > ul > li.current-menu-ancestor > a , #header .show-search a:hover i, #header .show-search.active a > .fa-search:before, .header-absolute .header.header-sticky #mainnav > ul > li > a:hover, .header-absolute .header.header-sticky .show-search a:hover, .header-absolute .header.header-sticky #mainnav > ul > li.current-menu-ancestor > a { color:" . esc_attr( $mainnav_hover_color ) . ";}"."\n";
		$custom .= ".header-left .themesflat_header_wrap .themesflat-socials li a:hover { border-color:" . esc_attr( $mainnav_hover_color ) . ";}"."\n";
		$custom .= ".header-left .themesflat_header_wrap .themesflat-socials li a:hover { background:" . esc_attr( $mainnav_hover_color ) . ";}"."\n";
	}

	//Subnav a color
	$sub_nav_color = themesflat_get_opt( 'sub_nav_color');
	if ( $sub_nav_color !='' ) {
		$custom .= "#mainnav ul.sub-menu > li > a, #mainnav li.megamenu > ul.sub-menu > .menu-item-has-children > a { color:" . esc_attr( $sub_nav_color ) . ";}"."\n";
	}

	//Subnav background color
	$sub_nav_background = themesflat_get_opt( 'sub_nav_background');
	if ( $sub_nav_background !='' ) {
		$custom .= "#mainnav ul.sub-menu { background-color:" . esc_attr( $sub_nav_background ) . ";}"."\n";			
	}

	//sub_nav_color_hover
	$sub_nav_color_hover = themesflat_choose_opt( 'sub_nav_color_hover');
	if ( $sub_nav_color_hover !='' ) {
		$custom .= "#mainnav ul.sub-menu > li > a:hover, #mainnav ul.sub-menu > li.current-menu-item > a, #mainnav-mobi ul li.current-menu-item > a, #mainnav-mobi ul li.current-menu-ancestor > a, #mainnav ul.sub-menu > li.current-menu-ancestor > a, #mainnav-mobi ul li .current-menu-item > a, #mainnav-mobi ul li.current-menu-item .btn-submenu:before, #mainnav-mobi ul li .current-menu-item .btn-submenu:before { color:" . esc_attr( $sub_nav_color_hover ) . ";}"."\n";
	}

	//sub_nav_background_hover
	$sub_nav_background_hover = themesflat_get_opt( 'sub_nav_background_hover');
	if ( $sub_nav_background_hover !='' ) {
		$custom .= "#mainnav ul.sub-menu > li > a:hover, #mainnav ul.sub-menu > li.current-menu-item > a { background-color:" . esc_attr($sub_nav_background_hover) . ";}"."\n";
	}

	//border color sub nav
	$border_color_sub_nav = themesflat_get_opt( 'border_color_sub_nav');
	if ( $border_color_sub_nav !='' ) {
		$custom .= "#mainnav ul.sub-menu > li { border-color:" . esc_attr($border_color_sub_nav) . "!important;}"."\n";
	}

	$footer_background_color = themesflat_choose_opt( 'footer_background_color');
	if ( $footer_background_color !='' ) {
		$custom .= ".footer { background-color:" . esc_attr($footer_background_color) . ";}"."\n";
	}

	$footer_background_image = themesflat_choose_opt('footer_background_image');
    if ( $footer_background_image !='' ) {
	    $custom .= '.footer_background {background: url('.$footer_background_image.') center /cover no-repeat;}'."\n";  
	}

	// Footer simple text color
	$footer_text_color = themesflat_choose_opt( 'footer_text_color');
	if ( $footer_text_color !='' ) {
		$custom .= ".footer a, .footer, .footer-widgets .widget .widget-title { color:" . esc_attr($footer_text_color) . ";}"."\n";
	}

	$footer_text_color_opacity = themesflat_hex2rgba($footer_text_color,0.5);
	if ( $footer_text_color !='' ) {
		$custom .= ".footer .widget.widget_recent_entries ul li .post-date, .footer .widget.widget_latest_news ul li .post-date, .footer .widget.widget_text .textwidget p, .footer-widgets .widget.widget_text ul.contact-info li span { color:" . esc_attr($footer_text_color_opacity) . ";}"."\n";
	}

	// bottom_background_color
	$bottom_background_color = themesflat_choose_opt( 'bottom_background_color');
	if ( $bottom_background_color !='' ) {
		$custom .= ".bottom { background-color:" . esc_attr( $bottom_background_color ) . ";}"."\n";
	}

	// Bottom text color
	$bottom_text_color = themesflat_choose_opt( 'bottom_text_color');

	if ( $bottom_text_color !='' ) {
		$custom .= ".bottom .copyright p, .bottom .copyright a, .bottom #menu-bottom li a, .bottom  #menu-bottom-menu li a, .bottom ul li a { color:" . esc_attr( $bottom_text_color ) . ";}"."\n";
		$custom .= ".bottom  #menu-bottom-menu li a:before { background-color:" . esc_attr( $bottom_text_color ) . ";}"."\n";
	}
	$custom .='.white #Financial_Occult text,.white #F__x26__O tspan {
			fill: #fff; }';

	$custom .= 'test_filter_render';

	// Primary color
    $primary_color = themesflat_choose_opt( 'primary_color' );
    $links_color = get_theme_mod('links_color');

    if ( $primary_color !='' ) {
    	//color
    	$custom .= ".breadcrumbs span a:hover, .breadcrumbs a:hover, a:hover, a:focus, .themesflat-top ul.themesflat-socials li a:hover, .themesflat-top a:hover, article .entry-title a:hover, article .entry-meta ul li a:hover, .widget ul li a:hover, .sidebar .widget.widget_categories > ul > li a:hover, .bottom .copyright a:hover, .footer-widgets .widget.widget_nav_menu ul li a:hover, .footer-widgets .widget.widget_nav_menu ul li a:hover:before, .bottom #menu-bottom-menu li a:hover, .navigation.posts-navigation .nav-links li a .meta-nav, .single .main-single .administrator .admin-content h5 a, .comments-area ol.comment-list article .comment_content .comement_reply a, .search-form .search-submit:hover i, .sidebar .widget.widget_archive > ul > li a:hover, .bottom ul li a:hover, .blog-grid article .entry-meta ul li > i, .blog-grid article .themesflat-button-container > a:hover, .blog-grid-simple article .themesflat-button-container > a, .blog-grid-simple article .entry-title:before, .themesflat_sc_vc-headings .primary-color, .comments-area ol.comment-list article .comment_content .comment_meta .comment_time, .portfolios-box.style-1 .portfolios-text .title a:hover, .portfolios-box.style-1 .portfolios-text .category a:hover, .portfolios-box.style-3 .portfolios-text .title a:hover, .portfolios-box.style-3 .portfolios-text .category a:hover, .primary-list li i, .themesflat_sc_vc-teammembers.custom.image-right .team-position, .themesflat_sc_vc-teammembers.custom.image-right .team-box-social > a:hover, .footer a:hover, .themesflat_sc_vc-image-box .btn .themesflat_sc_vc-button.light { color:" . esc_attr($primary_color) . ";}"."\n";

    	// Background color
		$custom .= 'mark, ins, .themesflat-button, .sidebar .widget .widget-title:before, .navigation.paging-navigation .current, .navigation.paging-navigation a:hover, .btn-cons a, .widget.widget_tag_cloud .tagcloud a:hover, button, input[type="button"], input[type="reset"], input[type="submit"], .footer-widgets .widget .widget-title:before, .navigation.posts-navigation .nav-links li a:after, .cssload-loader .cssload-side, #preloader .sk-circle .sk-child:before, #preloader .load:before, #preloader .load:after, #preloader .double-bounce3, #preloader .double-bounce4, #preloader .saquare-loader-1, #preloader .line-loader > div, .themesflat_sc_vc-image-box.style-2 .sub-title, .go-top:hover, .portfolio-single h2:after, .portfolio-single h3:after, .portfolio-single h4:after, .portfolio-single h5:after, .portfolio-single h6:after, .portfolios-box.style-2 .portfolios-overlay, .portfolios-box.style-3 .portfolios-text .title:before, .contact-form1 input[type="submit"], .contact-form2 input[type="submit"], .contact-form3 input[type="submit"], .contact-form4 input[type="submit"], .contact-form5 input[type="submit"], .section-comming-soon .section-title-line, .hover-style4 .gallery-box .gallery-wrap .gallery-overlay, .owl-theme .owl-dots .owl-dot.active span, .themesflat_sc_vc-services-post.grid-style2 .wrap-category .category > a, .themesflat_sc_vc-action-box .button-wrap .themesflat_sc_vc-button.accent, .themesflat_sc_vc-image-box .btn .themesflat_sc_vc-button.accent { background:' . esc_attr($primary_color) . "; }"."\n";

		// color important
		$custom .= '.themesflat_sc_vc-teammembers .team-box-social > a:hover, .primary-color, .themesflat_sc_vc-teammembers.custom.image-right .team-position { color:' . esc_attr($primary_color) . " !important; }"."\n";

		// Background color !important
		$custom .= '.wpb-js-composer .vc_tta-color-grey.vc_tta-style-classic .vc_tta-tab.style1 > a, .hesperiden .tp-bullet:hover, .hesperiden .tp-bullet.selected, .themesflat_sc_vc-image-box.has-hover-content:hover .inner .text-wrap, .owl-theme .owl-dots .owl-dot.active span, .themesflat_sc_vc-button.light:hover { background:' . esc_attr($primary_color) . " !important; }"."\n";

		// border color 
		$custom .= 'textarea:focus, input[type="text"]:focus, input[type="password"]:focus, input[type="datetime"]:focus, input[type="datetime-local"]:focus, input[type="date"]:focus, input[type="month"]:focus, input[type="time"]:focus, input[type="week"]:focus, input[type="number"]:focus, input[type="email"]:focus, input[type="url"]:focus, input[type="search"]:focus, input[type="tel"]:focus, input[type="color"]:focus, .sidebar .widget_search .search-form input[type="search"]:focus, .loader-icon, .navigation.paging-navigation .current, .navigation.paging-navigation a:hover { border-color:' . esc_attr($primary_color) . " }"."\n";
		$custom .= ' .loader-icon { border-right-color: transparent; '." }"."\n";
		$custom .= ' blockquote { border-left-color:' . esc_attr($primary_color) . " }"."\n";
    }

    // Comming Soon Background Image    
    if ( themesflat_get_opt('bg_img_comming_soon') !='' ) {
	    $custom .= '.bg-image-comming-soon {background: url('.themesflat_get_opt('bg_img_comming_soon').') center /cover no-repeat;}'."\n";  
	}
	//Comming Soon Backgound Color
	if ( themesflat_get_opt ('comming_soon_background_color') !='' ) {
		$custom .= ".section-comming-soon .overlay { background:" . esc_attr(themesflat_get_opt ('comming_soon_background_color')) ." ; } "."\n";
    }

	$custom = apply_filters('themesflat/render/style',$custom);
	wp_add_inline_style( 'inline-css', $custom );

}

add_action( 'wp_enqueue_scripts', 'themesflat_custom_styles' );