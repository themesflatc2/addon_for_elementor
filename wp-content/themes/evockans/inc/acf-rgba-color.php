<?php
function include_field_types_rgba_color() {
	
	class themesflat_acf_field_extended_color_picker extends acf_field {		
		function __construct() {
			
			$this->name = 'extended-color-picker';
			$this->label = __('Color RGBA Picker','evockans');
			$this->category = 'jquery';
			$this->defaults = array(
				'hide_palette'	=> '',
				'color_palette'	=> '',
			);	
						
			parent::__construct();
			
		}	
		
		
		function input_admin_enqueue_scripts() {			
			
			global $wp_scripts, $wp_styles;			
			
			if( !isset($wp_scripts->registered['iris']) ) {
				wp_register_style('wp-color-picker', admin_url('css/color-picker.css'), array('wp-color-picker'), '', true);
				wp_register_script('iris', admin_url('js/iris.min.js'), array('jquery-ui-draggable', 'jquery-ui-slider', 'jquery-touch-punch'), '1.0.7', true);
				wp_register_script('wp-color-picker', admin_url('js/color-picker.min.js'), array('iris'), '', true);		
				
				wp_localize_script('wp-color-picker', 'wpColorPickerL10n', array(
					'clear'			=> __('Clear', 'evockans' ),
					'defaultString'	=> __('Default', 'evockans' ),
					'pick'			=> __('Select Color', 'evockans' ),
					'current'		=> __('Current Color', 'evockans' )
				));
			}
			
			wp_enqueue_script( 'wp-color-picker-alpha', THEMESFLAT_LINK."/js/admin/wp-color-picker-alpha.min.js", array( 'wp-color-picker' ), '2.0.0', true );
			wp_register_style( 'acf-rgba-color-picker-style', THEMESFLAT_LINK."/css/admin/acf-rgba-color-picker.css", false);		

			wp_register_script( 'acf-rgba-color-picker-script', THEMESFLAT_LINK."/js/admin/acf-rgba-color-picker.js", array('wp-color-picker-alpha'), true );

			wp_enqueue_style('wp-color-picker');
			wp_enqueue_style('acf-rgba-color-picker-style');
			wp_enqueue_script('wp-color-picker');
			wp_enqueue_script('acf-rgba-color-picker-script');
						
		}	
		
		function render_field( $field ) {
			$text = acf_get_sub_array( $field, array('id', 'class', 'name', 'value') );
			$hidden = acf_get_sub_array( $field, array('name', 'class', 'value') );

			$palettes = apply_filters( "acf/rgba_color_picker/palette", true );

			if ( $palettes == false ) {
				$palettes = 'no-palette';
			} else if ( !is_array($palettes) ) {
				if ( $field['hide_palette'] == 1 ) {
					$palettes = 'no-palette';
				} else {
					$palettes = $field['color_palette'];
				}
			} else {
				if ( $field['hide_palette'] == 1 ) {
					$palettes = 'no-palette';
				} else {
					$palettes = implode(";", $palettes);
				}
			}

			$text['class'] = 'valuetarget';
			$hidden['class'] = 'hiddentarget';			
			
			?>
			<div class="acf-color-picker" data-target="target" data-palette='<?php echo esc_attr($palettes) ?>' data-default="<?php echo esc_attr($field['default_value']) ?>">			
				<?php acf_hidden_input($hidden); ?>
				<input type="text" <?php echo acf_esc_attr($text); ?> data-alpha ="true" />
			</div>
			<?php
		}	
		
		function render_field_settings( $field ) {
			acf_render_field_setting( $field, array(
				'label'			=> __('Default Value','evockans'),
				'instructions'	=> '',
				'type'			=> 'text',
				'name'			=> 'default_value',
				'placeholder'	=> '#FFFFFF'
			));
			
			acf_render_field_setting( $field, array(
				'label'			=> __('Color Palette','evockans'),
				'instructions'	=> __('Enter color codes separated by semicolons. You can use HEX or RGBA color codes and can also mix them (e.g. #2ecc71; rgba(50,40,30,0.5).<br><br>This can (and is maybe) overwritten by the "acf/acfrb_color_picker/palette" filter.','evockans'),
				'type'			=> 'text',
				'name'			=> 'color_palette'
			));			
			
			acf_render_field_setting( $field, array(
				'label'			=> __('Hide Color Palette','evockans'),
				'instructions'	=> __('Don\'t show a color palette in the color picker','evockans'),
				'type'			=> 'true_false',
				'name'			=> 'hide_palette',
				'ui'			=> 1,
			));
		}
		
	}

	acf_register_field_type( new themesflat_acf_field_extended_color_picker() );
	
}

add_action('acf/include_field_types', 'include_field_types_rgba_color');	
?>
