<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package evockans
 */

get_header(); ?>
<div class="col-md-12">
	<div id="primary" class="fullwidth-404">
		<main id="main" class="site-main" role="main">
			<section class="error-404 not-found">
				<!-- Style 1 -->
				<?php if( themesflat_get_opt('page_404_style') == 'page-404-style1' ): ?>
					<div class="error-404-4">
						<video autoplay playsinline muted loop preload poster="<?php echo esc_url(THEMESFLAT_LINK . 'images/error-bg.jpg'); ?>">
							<source src="<?php echo esc_url( themesflat_get_opt('page_404_bg_video_url') ); ?>" />
						</video>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 285 90" preserveAspectRatio="xMidYMid slice">
							<defs>
								<mask id="mask" x="0" y="0" width="100%" height="100%" >
									<rect x="0" y="0" width="100%" height="100%" />
									<text x="92" y="50">404</text>
								</mask>
							</defs>
							<rect x="0" y="0" width="100%" height="100%" />
						</svg>
					</div>  
					<div class="error-404-4-text">
						<header class="page-header"><h2><?php esc_html_e( 'Error', 'evockans' ); ?></h2></header><!-- .page-header -->
						<h4><?php esc_html_e( 'Page not found!', 'evockans' ); ?></h4>
						<p><?php esc_html_e( 'We\'re sorry, but the page you were looking for doesn\'t exist.', 'evockans' ); ?></p>	
					</div>
				<?php endif; ?>

				<!-- Style 2 -->
				<?php if( themesflat_get_opt('page_404_style') == 'page-404-style2' ): ?>
					<div class="error-box-2">
						<div class="clip-text background-center" style="background-image: url(<?php echo esc_url(THEMESFLAT_LINK . 'images/error2.jpg') ?>);">
							<header> <h2><?php esc_html_e( '404', 'evockans' ); ?></h2></header><!-- .page-header -->
							<h3><?php esc_html_e( 'Error', 'evockans' ); ?></h3>
						</div> 
						<h4><?php
						$allowed = '<br>';
						echo sprintf( esc_html__( 'We\'re sorry, but the page you were looking %s for doesn\'t exist.', 'evockans' ), $allowed ); ?></h4>
						<div class="wrap-button-404">
							<a href="<?php echo esc_url( home_url('/') ); ?>" class="dark-button-bordered button-md"><?php esc_html_e( 'Back To Home','evockans' ) ?></a>
						</div>
					</div>
				<?php endif; ?>
				
				<!-- Style 3 -->
				<?php if( themesflat_get_opt('page_404_style') == 'page-404-style3' ): ?>
					<div class="error-box-404 vertical-center" style="background-image: url(<?php echo esc_url(THEMESFLAT_LINK . 'images/error3.jpg') ?>);">
						<div id="particles-js"></div>
						<div class="error-box-3">
							<header><h2><?php esc_html_e( '404', 'evockans' ); ?></h2></header><!-- .page-header -->
							<h3><?php esc_html_e( 'Page not found!', 'evockans' ); ?></h3>
							<p><?php esc_html_e( 'We\'re sorry, but the page you were looking for doesn\'t exist.', 'evockans' ); ?></p>
							<div class="wrap-button-404">
								<a href="<?php echo esc_url( home_url('/') ); ?>" class="white-button-bordered button-md"><?php esc_html_e( 'Back To Home Page','evockans' ) ?></a>
							</div>
						</div>
					</div>
				<?php endif; ?>

			</section><!-- .error-404 -->
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- /.col-md-12 -->
<?php get_footer(); ?>