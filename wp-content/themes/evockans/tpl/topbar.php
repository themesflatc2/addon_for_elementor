<?php 
// Ignore ouput topbar when it isn't enabled
$topbar = themesflat_choose_opt('topbar_show');
$topbar_show_array = array('1', '0');
if( !in_array($topbar, $topbar_show_array) ) {
    $topbar = themesflat_get_opt('topbar_show');
}
$top_content = themesflat_get_opt('top_content');
if ( $topbar != 1 ) return;
?>
<!-- Top -->
<div class="themesflat-top">    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container-inside">
                    <div class="content-left text-left">
                    <?php
                        echo wp_kses_post($top_content);
                    ?>
                    </div>

                    <div class="content-right text-right">
                    <?php  
                        if ( themesflat_get_opt('enable_socials_link_top') == 1 ):
                            themesflat_render_social();    
                        endif;    
                    ?>
                    </div>

                </div><!-- /.container-inside -->
            </div>
        </div>
    </div><!-- /.container -->        
</div><!-- /.top -->