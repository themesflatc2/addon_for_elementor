<?php
$logo_site = themesflat_choose_opt('site_logo');
$logo_retina_site = themesflat_choose_opt('site_retina_logo');
$site_logo_sticky = themesflat_choose_opt('site_logo_sticky');
$site_retina_logo_sticky = themesflat_choose_opt('site_retina_logo_sticky');
$logo_with = themesflat_get_opt('logo_width');
if ( $logo_site ) : ?>
    <div id="logo" class="logo" >                  
        <a href="<?php echo esc_url( home_url('/') ); ?>"  title="<?php bloginfo('name'); ?>">
            <?php if  (!empty($logo_site)) { ?>
                <img class="site-logo"  src="<?php echo esc_url($logo_site); ?>" alt="<?php bloginfo('name'); ?>"  data-retina="<?php echo esc_url( $logo_retina_site ); ?>" data-logo_site="<?php echo esc_url( $logo_site ); ?>" data-logo_sticky="<?php echo esc_url( $site_logo_sticky ); ?>" data-site_retina_logo_sticky="<?php echo esc_url( $site_retina_logo_sticky ); ?>" data-retina_base="<?php echo esc_url( $logo_retina_site ); ?>" data-width="<?php echo esc_attr( $logo_with ); ?>"/>
            <?php } ?>
        </a>
    </div>
<?php else : ?>
    <div id="logo" class="logo">
    	<h1 class="site-title"><a href="<?php echo esc_url( home_url('/') ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>			
		<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>	
    </div><!-- /.site-infomation -->          
<?php endif; ?>