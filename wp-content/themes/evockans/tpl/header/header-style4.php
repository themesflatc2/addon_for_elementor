<!-- Header -->
<?php 
$header_style = themesflat_choose_opt('style_header');
$header_style_array = array('header-style1', 'header-style2', 'header-style3', 'header-style4');
if( !in_array($header_style, $header_style_array) ) {
    $header_style = themesflat_get_opt('style_header');
}
?>
<header id="header" class="header navigation-push <?php echo esc_attr( $header_style ) ?> clearfix" >
    <div class="nav">
        <div class="header-wrap">
            <?php get_template_part( 'tpl/header/brand'); ?>
            <?php get_template_part( 'tpl/header/navigator'); ?> 
            <div class="btn-menu">
                <span></span>
            </div><!-- //mobile menu button -->                                        
        </div>
    </div>    
</header><!-- /.header --> 
<div class="flat-social-header">
    <?php themesflat_render_social(); ?>
</div> 