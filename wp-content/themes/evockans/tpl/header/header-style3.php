<!-- Header -->
<?php 
$header_style = themesflat_choose_opt('style_header');
$header_style_array = array('header-style1', 'header-style2', 'header-style3', 'header-style4');
if( !in_array($header_style, $header_style_array) ) {
    $header_style = themesflat_get_opt('style_header');
}

$header_search_box = themesflat_choose_opt('header_search_box');
$header_search_box_array = array('1', 'yes');
if( !in_array($header_search_box, $header_search_box_array) ) {
    $header_search_box = themesflat_get_opt('header_search_box');
}


?>
<header id="header" class="header widget-header <?php echo esc_attr( $header_style ) ?>" >
    <div class="nav">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-wrap">
                        <?php get_template_part( 'tpl/header/brand'); ?> 

                        <div class="btn-menu">
                            <span></span>
                        </div><!-- //mobile menu button -->

                        <?php if ( $header_search_box == 1 ) :?>                        
                            <div class="show-search">
                                <a href="#"><i class="ti-search"></i></a> 
                                <div class="submenu top-search widget_search">
                                    <?php get_search_form(); ?>
                                </div>        
                            </div> 
                        <?php endif;?>

                        <?php get_template_part( 'tpl/header/navigator'); ?>                                         
                    </div>                
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>
</header><!-- /.header -->  