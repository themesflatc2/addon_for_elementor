<?php if( themesflat_get_opt('show_related_portfolio') == 1 ): 

$column = themesflat_get_opt('grid_columns_portfolio_related');
$number_related_portfolio = themesflat_get_opt('number_related_portfolio');

?>
<div class="portfolios-related">
	<div class="col-md-12">
		<?php
		if( get_theme_mod( 'title_related_portfolio','Related Portfolio' ) != "" ) {
			echo '<h3 class="title_related_portfolio">'.get_theme_mod( 'title_related_portfolio','Related Portfolio' ).'</h3>';
		}

		$terms = get_the_terms( $post->ID, 'portfolios_category' );
		$term_ids = wp_list_pluck( $terms, 'term_id' );

		$query_args = array(
			'post_type' => 'portfolios',
			'tax_query' => array(
				array(
				'taxonomy' => 'portfolios_category',
				'field' => 'term_id',
				'terms' => $term_ids,
				'operator'=> 'IN'
				)),
			'ignore_sticky_posts' => 1,
			'post__not_in'=> array( $post->ID )
		);

		$query_args['posts_per_page'] = $number_related_portfolio;
		$query = new WP_Query( $query_args );
		?>
		<div class="portfolios-wrap-carousel">
			<div class="customizable-carousel" data-loop="false" data-items="<?php echo esc_attr($column); ?>" data-md-items="<?php echo esc_attr($column); ?>" data-sm-items="2" data-xs-items="1" data-space="30" data-autoplay="true" data-autospeed="4000" data-nav-dots="false" data-nav-arrows="true">							
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<div class="portfolios-box style-1">
					<div class="inner">
						<?php
                        if ( has_post_thumbnail() ) {
                            $img_size = 'themesflat_sc_vc-rectangle3';
                        }?>

                    <div class="portfolios-wrap">
                        <div class="portfolios-image"><?php echo get_the_post_thumbnail( get_the_ID(), $img_size ); ?></div>
                        <div class="portfolios-overlay">
                            <div class="portfolios-content">
                                <div class="portfolios-text">
                                    <h2 class="title"><a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php the_title_attribute(); ?>"><?php echo get_the_title(); ?></a></h2>
                                    <h4 class="category"><?php the_terms( get_the_ID(), 'portfolios_category', '', ' , ', '' ) ?></h4>
                                </div>
                            </div>
                        </div>
                    </div> <!-- portfolios-wrap -->
	                </div>
				</div><!-- /.portfolios-box -->
				<?php endwhile; wp_reset_postdata(); ?>						
			</div><!-- /.customizable-carousel -->				
		</div><!-- portfolios-wrap-carousel -->
		
	</div><!-- col-md-12 -->
</div><!-- portfolios-related -->
<?php endif; ?>