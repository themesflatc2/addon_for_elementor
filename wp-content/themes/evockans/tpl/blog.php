<?php
/*
Template Name: Blog
*/

get_header(); ?>

<?php 
$page_layout = themesflat_choose_opt('page_layout');
$page_layout_array = array('sidebar-right', 'sidebar-left', 'fullwidth', 'fullwidth-small', 'fullwidth-center');
if( !in_array($page_layout, $page_layout_array) ) {
    $page_layout = themesflat_get_opt('page_layout');
}
$class= array();
$class[] = $page_layout;
?>
<div class="col-md-12">
    <div class="wrap-content-area">
        <div id="primary" class="content-area <?php echo esc_attr(implode(" ",$class));?>">
        <?php
        global $themesflat_paging_style;
        $layouts = themesflat_choose_opt('blog_archive_layout');
        $exclude = themesflat_get_opt('blog_archive_exclude');
        $grid_columns = themesflat_choose_opt('blog_grid_columns');
        $themesflat_paging_style = themesflat_get_opt('blog_archive_pagination_style');
        $blog_posts_per_page = themesflat_choose_opt('blog_posts_per_page');
        $content_length = themesflat_choose_opt('blog_archive_post_excepts_length');
        $readmore_text = themesflat_choose_opt('blog_archive_readmore_text');
        $show_content = themesflat_get_opt('show_content');
        $hide_readmore = themesflat_get_opt('blog_archive_readmore');
        $orderby = themesflat_get_opt('blog_order_by');
        $order = themesflat_get_opt('blog_order_direction');

        if ( get_query_var('paged') ) {
            $paged = get_query_var('paged');
        } elseif ( get_query_var('page') ) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }
        
        $args = array(
            'post_status'         => 'publish',
            'post_type'           => 'post',
            'paged' => $paged,
            'ignore_sticky_posts' => true,
            'layout' => $layouts,
            'grid_columns' => $grid_columns,
            'content_length' => $content_length,
            'hide_readmore' => $hide_readmore,           
            'readmore_text' => $readmore_text,
            'order'     => $order,
            'orderby'   => $orderby,
            'exclude' => $exclude,
            'limit' => $blog_posts_per_page,
            'show_content' => $show_content,
        );       
            ?>
            <?php 
            // if (class_exists('themesflat_class_extend')) {
            //     echo themesflat_class_extend::themesflat_posts( $args ) ;
            // }
            ?>
            <div class="clearfix"></div>
            <?php
            global $themesflat_paging_for ;    
            $themesflat_paging_for = 'blog';   
            //get_template_part( 'tpl/pagination' );              
            ?>
        </div>   <!-- #primary -->   
        <?php get_sidebar();?>
    </div><!-- /.wrap-content-area -->
</div><!-- /.col-md-12 -->
<?php get_footer(); ?>