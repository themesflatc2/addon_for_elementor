<?php
/**
 * Template Name: CommingSoonS2
 */

get_header(); ?>
<div class="col-md-12">
    <div class="wrap-content-area">
        <div id="primary" class="content-area-comming-soon-page">
            <main id="main" class="site-main" role="main">
                <div class="entry-content">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; ?>
                </div>
                <!-- .entry-content -->
            </main>
            <!-- #main -->
        </div>
        <!-- #primary -->
    </div>
    <!-- /.wrap-content-area -->
</div>
<!-- /.col-md-12 -->
<?php get_footer(); ?>