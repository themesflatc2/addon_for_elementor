<?php
/**
 * Template Name: CommingSoonS1
 */

get_header(); ?>
<div class="col-md-12">
    <div id="primary" class="fullwidth-comming-soon">
        <main id="main" class="site-main" role="main">
            <section class="section-comming-soon not-found vertical-center bg-image-comming-soon">
                <div class="overlay"></div>
                <div class="col-lg-6 col-md-12 col-12">
                    <div class="box-comming-soon">
                        <header class="page-header">
                            <h1 class="title-comming-soon"><?php esc_html_e( 'We Are Getting Ready to Launch!', 'evockans' ); ?></h1>
                        </header>
                        <!-- .page-header -->
                        <div class="section-title-line"></div>
                        <div class="sub-title-comming-soon">
                            <?php esc_html_e( 'Our website is almost ready to launch, lorem ipsum dolor sit.', 'evockans' ); ?>
                        </div>
                        <!-- .title-comming-soon -->

                        <div class="page-content">
                            <div class="comming-soon-countdown">
                                <div id="countdown" class="countdown clearfix" data-set_time="<?php echo themesflat_choose_opt('comming_soon_time'); ?>">
                                    <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-6 col-3">
                                                <div class="square">
                                                    <div class="numb time-day">742</div>
                                                    <div class="text">
                                                        <?php esc_html_e( 'DAY', 'evockans' ); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-3">
                                                <div class="square">
                                                    <div class="numb time-hours">15</div>
                                                    <div class="text">
                                                        <?php esc_html_e( 'HOURS', 'evockans' ); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-3">
                                                <div class="square">
                                                    <div class="numb time-mins">03</div>
                                                    <div class="text">
                                                        <?php esc_html_e( 'MINS', 'evockans' ); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-3">
                                                <div class="square">
                                                    <div class="numb time-secs">30</div>
                                                    <div class="text">
                                                        <?php esc_html_e( 'SECS', 'evockans' ); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- CountDown -->
                            </div>
                            <!-- comming-soon-countdown -->
                        </div>
                        <!-- .page-content -->
                    </div>
                </div>
            </section>
            <!-- .error-comming-soon -->
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

</div>
<!-- /.col-md-12 -->
<?php get_footer(); ?>