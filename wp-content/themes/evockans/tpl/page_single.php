<?php

/*
Template Name: No title page
*/

get_header(); ?>
	<div class="col-md-12">
		<div class="wrap-content-area">
			<div class="single-page">
				<div id="primary" class="content-area">
					<main id="main" class="site-main" role="main">
						<?php while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; // end of the loop. ?>
					</main><!-- #main -->
				</div><!-- #primary -->
				<?php 
				get_sidebar();
				?>
			</div>
		</div><!-- /.wrap-content-area -->
	</div>
<?php get_footer(); ?>