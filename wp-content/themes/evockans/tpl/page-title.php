<?php 
    if ( is_page() && is_page_template( 'tpl/front-page.php' ) ) {
        echo '<div class="clearfix"></div>';
        return;
    }

    $title = esc_html__( 'Archives', 'evockans' );

    if ( is_home() ) {
        if (is_front_page() ) {
            $title = esc_html__( 'Home', 'evockans' );
        }
        else {
            $title = esc_html( wp_title('',FALSE) );
        }
    } elseif ( is_singular() ) {
        if (is_single()) {
            $title = get_the_title();
        }elseif(is_page_template('tpl/page_single.php')) {
            $title = get_the_title();
        }
         elseif(is_page_template('tpl/page_no_title.php')) {
            $title = '';
        }else {
            $title = get_the_title();
        }
    } elseif ( is_search() ) {
        $title = sprintf( esc_html__( 'Search results for &quot;%s&quot;', 'evockans' ), get_search_query() );
    } elseif ( is_404() ) {
        $title = esc_html__( 'Not Found', 'evockans' );
        return;
    } elseif ( is_author() ) {
        the_post();
        $title = sprintf( esc_html__( 'Author Archives: %s', 'evockans' ), get_the_author() );
        rewind_posts();
    } elseif ( is_day() ) {
        $title = sprintf( esc_html__( 'Daily Archives: %s', 'evockans' ), get_the_date() );
    } elseif ( is_month() ) {
        $title = sprintf( esc_html__( 'Monthly Archives: %s', 'evockans' ), get_the_date( 'F Y' ) );
    } elseif ( is_year() ) {
        $title = sprintf( esc_html__( 'Yearly Archives: %s', 'evockans' ), get_the_date( 'Y' ) );
    } elseif ( is_tax() || is_category() || is_tag() ) {
        $title = single_term_title( '', false );
    }
?>

<?php if (themesflat_get_opt('show_page_title') != 1 || $title =='' ) return;?>
<!-- Page title -->
<?php 
    $page_title_styles = themesflat_choose_opt('page_title_styles');
    $page_title_styles_array = array('inline', 'default', 'fullwidth', 'parallax', 'video');
    if( !in_array($page_title_styles, $page_title_styles_array) ) {
        $page_title_styles = themesflat_get_opt('page_title_styles');
    }

    $page_title_alignment = themesflat_choose_opt('page_title_alignment');
    $page_title_alignment_array = array('left', 'center', 'right');
    if( !in_array($page_title_alignment, $page_title_alignment_array) ) {
        $page_title_alignment = themesflat_get_opt('page_title_alignment');
    }
?>


<?php if( $page_title_styles == 'video' ): 
    $page_title_video_url = themesflat_choose_opt('page_title_video_url');
?>
<div class="page-title <?php echo esc_attr($page_title_styles); ?> <?php echo esc_attr($page_title_alignment); ?>">
    <div id="ptbgVideo" class="player" data-property="{videoURL:'<?php echo esc_url($page_title_video_url); ?>',containment:'.page-title', showControls:false, autoPlay:true, loop:true, mute:true, startAt:0, opacity:1, quality:'large'}"></div>
    <div class="overlay"></div>   
    <div class="container"> 
        <div class="row">
            <div class="col-md-12 page-title-container">
            <?php 
            if ( themesflat_get_opt('show_page_title_heading') == 1 ) {
                printf('<h1>%s</h1>',$title);
             }                 
            ?>
            <?php 
                if ( themesflat_get_opt( 'breadcrumb_enabled' ) == 1 ):
                    themesflat_breadcrumb_trail( array(
                        'separator'   => themesflat_get_opt('breadcrumb_separator'),
                        'show_browse' => true,
                        'labels'      => array(
                        'browse'  => themesflat_get_opt( 'bread_crumb_prefix', esc_html__( '', 'evockans' ) ),
                            'home'    => esc_html__( 'Home', 'evockans' )
                        )
                    ) );
                
                endif;                       
            ?>             
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                        
</div><!-- /.page-title -->
<?php else: ?>
<div class="page-title <?php echo esc_attr($page_title_styles); ?> <?php echo esc_attr($page_title_alignment); ?>">
    <div class="overlay"></div>   
    <div class="container"> 
        <div class="row">
            <div class="col-md-12 page-title-container">
            <?php 
            if ( themesflat_get_opt('show_page_title_heading') == 1 ) {
                printf('<h1>%s</h1>',$title);
             }                 
            ?>
            <?php 
                if ( themesflat_get_opt( 'breadcrumb_enabled' ) == 1 ):
                    themesflat_breadcrumb_trail( array(
                        'separator'   => themesflat_get_opt('breadcrumb_separator'),
                        'show_browse' => true,
                        'labels'      => array(
                        'browse'  => themesflat_get_opt( 'bread_crumb_prefix', esc_html__( '', 'evockans' ) ),
                            'home'    => esc_html__( 'Home', 'evockans' )
                        )
                    ) );
                
                endif;                       
            ?>             
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /.page-title --> 
<?php endif; ?>