<!-- Footer -->    
<div class="footer_background footer_s2">
    <?php 
    $show_footer = themesflat_choose_opt('show_footer');
    $show_footer_array = array('1', '0');
    if( !in_array($show_footer, $show_footer_array) ) {
        $show_footer = themesflat_get_opt('show_footer');
    }
    if ($show_footer == 1): 
    ?> 
    <footer class="footer <?php (themesflat_meta( 'footer_class' ) != "" ? esc_attr( themesflat_meta( 'footer_class' ) ):'') ;?>">
        <div class="footer-widgets">
            <div class="container">                
                <div class="row">
                    <?php                    
                    $footer_widget_areas = themesflat_choose_opt('footer_widget_areas');
                    $footer_widget_areas_array = array(1, 2, 3, 4);
                    if( !in_array($footer_widget_areas, $footer_widget_areas_array) ) {
                        $footer_widget_areas = themesflat_get_opt('footer_widget_areas');
                    }
                    $columns = themesflat_widget_layout_s2($footer_widget_areas);
                    $key = 0;
                    if (themesflat_get_opt('footer_widget_areas') == 5 ) {
                        echo '<div class="col-lg-12">';
                        for ( $widget_footer_columns = 0; $widget_footer_columns < 5;$widget_footer_columns++ ) {?>
                        <div class="flat-widget-footer widgets-areas">
                            <?php 
                                $key = $widget_footer_columns +1;
                                $widget = "footers2-".$key;
                                themesflat_dynamic_sidebar($widget);
                            ?>
                        </div>
                    <?php }
                    echo '</div>';
                    } else {
                        foreach ($columns as $key => $column) {?>
                        <div class="col-lg-<?php themesflat_esc_attr($column);?> col-md-6 widgets-areas">
                            <?php 
                                $key = $key +1;
                                $widget = themesflat_get_opt("footers2".$key);
                                themesflat_dynamic_sidebar($widget);
                            ?>
                        </div>
                    <?php }
                    }
                    ?>       
                </div><!-- /.row -->                  
            </div><!-- /.container --> 
        </div><!-- /.footer-widgets -->
    </footer>
    <?php endif ?>
    <!-- Bottom -->
    <div class="bottom ">
        <div class="container">           
            <div class="row">
                <div class="col-md-12">
                    <div class="container-inside">
                        <div class="content-left text-left ">                        
                            <div class="copyright">                     
                                <?php echo wp_kses_post(themesflat_get_opt( 'footer_copyright')); ?>
                            </div>
                        </div>

                        <?php if ( themesflat_get_opt('show_menu_bottom')== 1 ): ?>
                        <div class="content-right text-right">
                           <?php themesflat_dynamic_sidebar('bottom-menu-s2'); ?>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>                   
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div> 
</div> <!-- Footer Background Image -->