<?php
if ( ! get_theme_mod( 'show_related_post' ) )
    return;
$layout = get_theme_mod( 'related_post_style', 'blog-grid' );
$carousel = 1;
$show_readmore = themesflat_get_opt('blog_archive_readmore');
$show_content  = themesflat_get_opt('show_content');
$content_length = themesflat_get_opt('blog_archive_post_excepts_length');
$grid_columns  = get_theme_mod( 'grid_columns_post_related', 3 );
$readmore_text = themesflat_get_opt('blog_archive_readmore_text');
if ( get_query_var('paged') ) {
    $paged = get_query_var('paged');
} elseif ( get_query_var('page') ) {
    $paged = get_query_var('page');
} else {
    $paged = 1;
}
$args = array(                    
    'post_status'         => 'publish',
    'post_type'           => 'post',
    'paged' => $paged,
    'ignore_sticky_posts' => true,
    'posts_per_page'      => get_theme_mod( 'number_related_post', 3 ),
    ); 
$tags = (array) get_the_tags();
$categories = (array) get_the_category();


if ( empty( $tags ) && empty( $categories ) )
    return;

$args['tag']      = wp_list_pluck( $tags, 'slug' );
$args['category'] = wp_list_pluck( $categories, 'slug' );
if ( $layout != '' ) {
    $class[] = $layout;
}
if ( $grid_columns != '' ) {
    $class[] = 'columns-' . $grid_columns ;
}
if ( $carousel == 1 ) {
    $class[] = 'has-carousel';
}
global $themesflat_thumbnail;
$imgs = array(
    'blog-grid' => 'themesflat-blog-grid',
    'blog-list' => 'themesflat-blog',
    );
$themesflat_thumbnail = $imgs[$layout];
?>
<div class="related-post related-posts-box col-md-12">
    <div class="box-wrapper">
        <h3 class="box-title"><?php esc_html_e( 'Related Posts', 'evockans' ) ?></h3>
        <div class="box-content">
            <div class="<?php echo esc_attr( implode( ' ', $class ) ) ?>">
            <?php             
            $tags = wp_get_post_tags($post->ID);
            if ( $tags || $categories ) {
                $query = new WP_Query($args);
                if( $query->have_posts() ) {
                    while ($query->have_posts()) : $query->the_post(); ?>
                    <div class="item">
                        <article <?php echo esc_attr(post_class('entry'));?>>
                            <div class="entry-border">
                                <div class="featured-post">
                                    <a href="<?php the_permalink();?>">
                                        <?php  the_post_thumbnail( $themesflat_thumbnail ); ?>                                        
                                    </a>
                                </div>
                                
                                <div class="content-post">                                                                              
                                    <h3 class="entry-title"><a href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>"><?php the_title();?></a></h3>
                                    <div class="entry-meta meta-on clearfix">
                                        <?php themesflat_render_meta(); ?>
                                    </div><!-- /.entry-meta -->                                
                                    <div class="entry-content">

                                        <?php                                       
                                        $readmore = $show_readmore == 1  ? wp_kses_post( $readmore_text ) : '[...]';
                                        if ($show_content == 1) {
                                            themesflat_render_post($layout,$readmore,$content_length);
                                        }
                                        ?>

                                    </div>

                                </div>
                            </div>
                        </article><!-- /.entry -->
                    </div>
                    <?php
                    endwhile;
                }
                wp_reset_postdata();
            }
            ?>
            </div>
        </div>
    </div>
</div>


