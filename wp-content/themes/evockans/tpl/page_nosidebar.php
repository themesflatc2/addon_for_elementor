<?php

/*
Template Name: No Sidebar Page
*/

get_header(); ?>
<div class="col-md-12">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; // end of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<?php get_footer(); ?>