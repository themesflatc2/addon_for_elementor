<?php
/**
 * @package evockans
 */
?>

<div class="item">
<article id="post-<?php the_ID(); ?>" <?php post_class( 'blog-post' ); ?>>
	<div class="main-post entry-border">
		<?php get_template_part( 'tpl/feature-post'); ?>
		
		<div class="content-post">
			<!-- Blog List -->
			<?php if ( themesflat_get_opt('blog_archive_layout') == 'blog-list'):?>
				<div class="entry-box-title clearfix">
					<div class="wrap-entry-title">
						<?php
						if ( is_singular('post') ) :						
							the_title( '<h1 class="entry-title">', '</h1>' );
						else :
							the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
						endif;
						?>

						<?php themesflat_render_meta(); ?>				
					</div><!-- /.wrap-entry-title -->
				</div>
			<?php endif; ?>	

			<!-- Blog Grid -->	
			<?php if ( themesflat_get_opt('blog_archive_layout') == 'blog-grid'):?>
				<div class="entry-box-title clearfix">
					<div class="wrap-entry-title">
						<?php
						if ( is_singular('post') ) :						
							the_title( '<h1 class="entry-title">', '</h1>' );
						else :
							the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark" title="%s">', esc_url(get_permalink()), get_the_title() ), '</a></h3>' );
						endif;
						?>

						<?php themesflat_render_meta(); ?>				
					</div><!-- /.wrap-entry-title -->
				</div>
			<?php endif; ?>

			<!-- Blog Grid Simple -->			
			<?php if ( themesflat_get_opt('blog_archive_layout') == 'blog-grid-simple'):?>
			<div class="entry-box-title clearfix">
				<div class="wrap-entry-title">
					<?php
					if ( is_singular('post') ) :						
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark" title="%s">', esc_url(get_permalink()), get_the_title() ), '</a></h3>' );
					endif;
					?>				
				</div><!-- /.wrap-entry-title -->
			</div>
			<?php endif; ?>
			
			<?php if (themesflat_get_opt('show_content')): ?>
				<?php if ( (themesflat_get_opt( 'blog_archive_readmore' ) == 1 && is_home() ) || (themesflat_get_opt('blog_archive_readmore') == 1 && is_archive() ) ) : ?>
					<?php themesflat_render_post(themesflat_get_opt('blog_archive_layout'),themesflat_get_opt( 'blog_archive_readmore_text'));?>
				<?php else :  ?>
					<?php  themesflat_render_post(themesflat_get_opt('blog_archive_layout')) ?>
				<?php  endif; ?>

				<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'evockans' ),
					'after'  => '</div>',
					) );
					?>
			<?php endif; ?>
		</div><!-- /.entry-post -->
	
	</div><!-- /.main-post -->
</article><!-- #post-## -->
</div>