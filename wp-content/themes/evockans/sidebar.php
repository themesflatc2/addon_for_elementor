<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package evockans
 */
?>

<div id="secondary" class="widget-area" role="complementary">
	<div class="sidebar">
	<?php
		$sidebar = themesflat_get_opt( 'blog_sidebar_list' );
		if ( is_page() ) {			
			$sidebar= themesflat_get_opt( 'page_sidebar_list' );			
		}	  
        themesflat_dynamic_sidebar ( $sidebar ); 
	?>
	</div>
</div><!-- #secondary -->