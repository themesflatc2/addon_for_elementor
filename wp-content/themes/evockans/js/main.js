/**
* isMobile
* headerFixed
* headerAbsolute
* responsiveMenu
* headerLeft
* headerStickyLogo
* themesflatSearch
* detectViewport
* blogLoadMore
* commingsoon
* goTop
* retinaLogos
* customizable_carousel
* parallax
* iziModal
* bg_particles
* pagetitleVideo
* removePreloader
*/

;(function($) {

    "use strict";

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    }; 

    var headerFixed = function() {         
        if ( $('body').hasClass('header_sticky') ) {
            if ( ! $('body').hasClass('header-left') ) {
                var top_height = $('.themesflat-top').height(),
                hd_height = $('#header').height(),             
                injectSpace = $('<div />', { height: hd_height }).insertAfter($('#header'));   
                injectSpace.hide(); 
                $(window).on('load scroll resize', function() { 
                    var header = $("#header");
                    var offset = 0;
                    if (typeof(header.data('offset')) != 'undefined') {
                        offset = header.data('offset');
                    }
                    
                    if (typeof($('#wpadminbar')) != 'undefined' && matchMedia( 'only screen and (min-width: 601px)' ).matches) {
                        $('.header.header-sticky').css('top',$('#wpadminbar').height());
                    }
                    else {
                        $('.header.header-sticky').css('top',0);
                    }
                    if ( $(window).scrollTop() >= top_height + hd_height + 20 ) { 
                        header.addClass('header-sticky');
                        injectSpace.show();
                    } else {  
                        header.removeClass('header-sticky'); 
                        injectSpace.hide();
                    } 
                });
            }
        }             
    }; 

    var headerAbsolute = function() {
        if ( $('body').hasClass('header-absolute') ) {
            var top_height = $('.themesflat-top').height();
            if ($('body').hasClass('admin-bar')) {
                if ($('body').hasClass('boxed')) {
                    $('.themesflat_header_wrap').css('top',top_height);
                }else{
                    $('.themesflat_header_wrap').css('top',top_height + 32);
                }                
            }else {
                $('.themesflat_header_wrap').css('top',top_height);
            }
            
        }
    };  

    var responsiveMenu = function() {
        var menuType = 'desktop';

        $(window).on('load resize', function() {
            var currMenuType = 'desktop';

            if ( matchMedia( 'only screen and (max-width: 991px)' ).matches ) {                
                currMenuType = 'mobile';                              
            }

            if ( currMenuType !== menuType ) {
                menuType = currMenuType;

                if ( currMenuType === 'mobile' ) {
                    var $mobileMenu = $('#mainnav').attr('id', 'mainnav-mobi').hide();
                    var hasChildMenu = $('#mainnav-mobi').find('li:has(ul)');

                    $('#header .nav').after($mobileMenu);
                    hasChildMenu.children('ul').hide();
                    if (hasChildMenu.find(">span").length == 0) {
                        hasChildMenu.children('a').after('<span class="btn-submenu"></span>');
                    }
                    $('.btn-menu').removeClass('active');
                } else {
                    var $desktopMenu = $('#mainnav-mobi').attr('id', 'mainnav').removeAttr('style');
                    $desktopMenu.find('.sub-menu').removeAttr('style');
                    $('#header').find('.nav-wrap').append($desktopMenu);
              
                }
            }
        });

        $('.btn-menu').on('click', function() {  
            var header = $("#header");
            var offset = 0;
            if (typeof(header.data('offset')) != 'undefined') {
                offset = header.data('offset');
            }
        
            var $top = $(window).scrollTop() + header.height() + header.position().top + offset;
            $('#mainnav-mobi').slideToggle(300);
            $(this).toggleClass('active');
        });

        $(document).on('click', '#mainnav-mobi li .btn-submenu', function(e) {
            $(this).toggleClass('active').next('ul').slideToggle(300);
            e.stopImmediatePropagation();
        });
    };

    var headerLeft = function() {        
        if ( $('body').hasClass('header-left') ) {
            var $leftMenu = $('.themesflat_header_wrap.header-style4').attr('id', 'header-left');
            $('#preloader').after($leftMenu);

            var hasChildMenu = $('#mainnav').find('li:has(ul)');
            hasChildMenu.children('ul').hide();
            if (hasChildMenu.find(">span").length == 0) {
                hasChildMenu.children('a').after('<span class="btn-submenu"></span>');
            }

            $(document).on('click', '#header-left #mainnav li .btn-submenu', function(e) {
                $(this).toggleClass('active').next('ul').slideToggle(300);
                //e.stopImmediatePropagation();                    
            });
        }        
    };

    var headerStickyLogo = function() { 
        if ( $('body').hasClass('header_sticky') ) {
            var top_height = $('#header').height();
            $(window).on('load scroll', function() { 
                if ( $(window).scrollTop() >= top_height ) {
                    var logo_sticky = $('#logo').find('img').data('logo_sticky');
                    var site_retina_logo_sticky = $('#logo').find('img').data('site_retina_logo_sticky');
                    $('#logo').find('img').attr('src', logo_sticky);
                    $('#logo').find('img').attr('data-retina', site_retina_logo_sticky);                     
                } else {     
                    var logo_site = $('#logo').find('img').data('logo_site');
                    var retina_base = $('#logo').find('img').data('retina_base');   
                    $('#logo').find('img').attr('src', logo_site);   
                    $('#logo').find('img').attr('data-retina', retina_base);        
                }     
            });
        }
    };

    var themesflatSearch = function () {
       $(document).on('click', function(e) {   
            var clickID = e.target.id;   
            if ( ( clickID != 's' ) ) {
                $('.top-search').removeClass('show');   
                $('.show-search').removeClass('active');             
            } 
        });

        $('.show-search').on('click', function(event){
            event.stopPropagation();
        });

        $('.search-form').on('click', function(event){
            event.stopPropagation();
        });        

        $('.show-search').on('click', function (e) {           
            if( !$( this ).hasClass( "active" ) )
                $( this ).addClass( 'active' );
            else
                $( this ).removeClass( 'active' );
             e.preventDefault();

            if( !$('.top-search' ).hasClass( "show" ) )
                $( '.top-search' ).addClass( 'show' );
            else
                $( '.top-search' ).removeClass( 'show' );
        });
    };

    var detectViewport = function() {
        $('[data-inviewport="yes"]').waypoint(function() {
            $(this).trigger('on-appear');
        }, { offset: '90%', triggerOnce: true });

        $(window).on('load', function() {
            setTimeout(function() {
                $.waypoints('refresh');
            }, 100);
        });
    };   

    var blogLoadMore = function() { 
        var $container = $('.wrap-blog-article'),
        $container_faq = $('.wrap-faq');
        if ( $('body').hasClass('page-template') ) {
            var $container = $('.wrap-blog-article');
        }   

        $('.navigation.loadmore a').on('click', function(e) {
            e.preventDefault(); 
            var $item = '.item';
            if ($(this).parents('nav').hasClass("faq")) {
                $container = $container_faq;
                $item = '.item';
            }

            $('<span/>', {
                class: 'infscr-loading',
                text: 'Loading...',
            }).appendTo($container);

            $.ajax({
                type: "GET",
                url: $(this).attr('href'),
                dataType: "html",
                success: function( out ) {
                    var result = $(out).find($item);  
                    var nextlink = $(out).find('.navigation.loadmore a').attr('href');

                    result.css({ opacity: 0 });
                    if ($container.hasClass('blog-masonry')) {
                        $container.append(result).imagesLoaded(function () {
                            result.css({ opacity: 1 });
                            $container.masonry('appended', result);
                        });
                    }
                    else {
                        result.css({ opacity: 1 });
                        $container.append(result);
                    }

                    if ( nextlink != undefined ) {
                        $('.navigation.loadmore a').attr('href', nextlink);
                        $container.find('.infscr-loading').remove();
                    } else {
                        $container.find('.infscr-loading').addClass('no-ajax').text('All posts loaded.').delay(2000).queue(function() {$(this).remove();});
                        $('.navigation.loadmore a').remove();
                    }
                    customizable_carousel();
                    iziModal();
                }
            })
        })       
    } 

    var commingsoon = function() { 
        if ( $('body').hasClass('page-template-comming-soon-s1')) {
            $('.comming-soon-countdown').each(function() {
                var set_time    = $('#countdown').data('set_time'),
                    time_day    = $('.time-day'),
                    time_hours  = $('.time-hours'),
                    time_mins   = $('.time-mins'),
                    time_secs   = $('.time-secs');
                if ($().countdown) {
                    $("#countdown").countdown(set_time, function(event) {
                      time_day.html(event.strftime('%D'));
                      time_hours.html(event.strftime('%H'));
                      time_mins.html(event.strftime('%M'));
                      time_secs.html(event.strftime('%S'));
                    });
                } 
            }); 
        }
    };

    var goTop = function() {
        $(window).scroll(function() {
            if ( $(this).scrollTop() > 500 ) {
                $('.go-top').addClass('show');
            } else {
                $('.go-top').removeClass('show');
            }
        });

        $('.go-top').on('click', function(event) { 
            event.preventDefault();           
            $("html, body").animate({ scrollTop: 0 }, 1000);            
        });
    };    

    var retinaLogos = function() {
        var retina = window.devicePixelRatio > 1 ? true : false;
        var top_height = $('#header').height();
        var img_width =  $('.logo .site-logo').data('width');  
        $(window).on('load scroll', function() { 
            if ( $(window).scrollTop() >= top_height ) {
                var img_retina = $('.logo .site-logo').data('site_retina_logo_sticky');   
                if( retina ) {
                    $('#logo').find('img').attr({src:img_retina,width:img_width,height:'auto'});           
                }
            }else {
                var img_retina = $('.logo .site-logo').data('retina_base');
                if( retina ) {
                    $('#logo').find('img').attr({src:img_retina,width:img_width,height:'auto'});           
                }
            }
        }) 
    };

    var customizable_carousel = function() {
        var owl_carousel = $("div.customizable-carousel");
        if (owl_carousel.length > 0) {
            owl_carousel.each(function() {
                var $this = $(this),
                    $items = ($this.data('items')) ? $this.data('items') : 1,
                    $loop = ($this.attr('data-loop')) ? $this.data('loop') : true,
                    $navdots = ($this.data('nav-dots')) ? $this.data('nav-dots') : false,
                    $navarrows = ($this.data('nav-arrows')) ? $this.data('nav-arrows') : false,
                    $autoplay = ($this.attr('data-autoplay')) ? $this.data('autoplay') : false,
                    $autospeed = ($this.attr('data-autospeed')) ? $this.data('autospeed') : 3500,
                    $smartspeed = ($this.attr('data-smartspeed')) ? $this.data('smartspeed') : 950,
                    $autohgt = ($this.data('autoheight')) ? $this.data('autoheight') : false,
                    $space = ($this.attr('data-space')) ? $this.data('space') : 15;

                $(this).owlCarousel({
                    loop: $loop,
                    items: $items,
                    responsive: {
                        0: {
                            items: ($this.data('xs-items')) ? $this.data('xs-items') : 1
                        },
                        600: {
                            items: ($this.data('sm-items')) ? $this.data('sm-items') : 2,
                            nav: false
                        },
                        1000: {
                            items: ($this.data('md-items')) ? $this.data('md-items') : 3
                        },
                        1240:{
                            items: $items
                        }
                    },
                    dots: $navdots,
                    autoplayTimeout: $autospeed,
                    smartSpeed: $smartspeed,
                    autoHeight: $autohgt,
                    margin: $space,
                    nav: $navarrows,
                    autoplay: $autoplay,
                    autoplayHoverPause: true
                });
            });
        }
    };

    var parallax = function() {
        if ( $().parallax && isMobile.any() == null ) {
            $('.parallax').parallax("50%", -0.5);           
        }
    };

    var iziModal = function(){
        $(".izimodal").iziModal({
            width: 850,
            top: null,
            bottom: null,
            borderBottom: false,
            padding: 0,
            radius: 3,
            zindex: 999999,
            iframe: false,
            iframeHeight: 400,
            iframeURL: null,
            focusInput: false,
            group: '',
            loop: false,
            arrowKeys: true,
            navigateCaption: true,
            navigateArrows: true, // Boolean, 'closeToModal', 'closeScreenEdge'
            history: false,
            restoreDefaultContent: true,
            autoOpen: 0, // Boolean, Number
            bodyOverflow: false,
            fullscreen: false,
            openFullscreen: false,
            closeOnEscape: true,
            closeButton: true,
            appendTo: 'body', // or false
            appendToOverlay: 'body', // or false
            overlay: true,
            overlayClose: true,
            overlayColor: 'rgba(0, 0, 0, .7)',
            timeout: false,
            timeoutProgressbar: false,
            pauseOnHover: false,
            timeoutProgressbarColor: 'rgba(255,255,255,0)',
            transitionIn: 'comingIn',
            transitionOut: 'comingOut',
            transitionInOverlay: 'fadeIn',
            transitionOutOverlay: 'fadeOut',
            onFullscreen: function(){},
            onResize: function(){},
            onOpening: function(){},
            onOpened: function(){},
            onClosing: function(){},
            onClosed: function(){},
            afterRender: function(){}
        });

        $(document).on('click', '.trigger', function (event) {
            event.preventDefault();
            $('.izimodal').iziModal('setZindex', 99999999);
            $('.izimodal').iziModal('open', { zindex: 99999999 });
            $('.izimodal').iziModal('open');
        });
    }    

    var bg_particles = function() {
        if ($('body').hasClass('page-404-style3')) { 
            particlesJS("particles-js", {
                "particles": {
                    "number": {
                        "value": 80,
                        "density": {
                            "enable": true,
                            "value_area": 800
                        }
                    },
                    "color": {
                        "value": "#ffffff"
                    },
                    "shape": {
                        "type": "circle",
                        "stroke": {
                            "width": 0,
                            "color": "#000000"
                        },
                        "polygon": {
                            "nb_sides": 5
                        },
                        "image": {
                            "src": "img/github.svg",
                            "width": 100,
                            "height": 100
                        }
                    },
                    "opacity": {
                        "value": 0.5,
                        "random": false,
                        "anim": {
                            "enable": false,
                            "speed": 1,
                            "opacity_min": 0.1,
                            "sync": false
                        }
                    },
                    "size": {
                        "value": 3,
                        "random": true,
                        "anim": {
                            "enable": false,
                            "speed": 40,
                            "size_min": 0.1,
                            "sync": false
                        }
                    },
                    "line_linked": {
                        "enable": true,
                        "distance": 150,
                        "color": "#ffffff",
                        "opacity": 0.4,
                        "width": 1
                    },
                    "move": {
                        "enable": true,
                        "speed": 6,
                        "direction": "none",
                        "random": false,
                        "straight": false,
                        "out_mode": "out",
                        "bounce": false,
                        "attract": {
                            "enable": false,
                            "rotateX": 600,
                            "rotateY": 1200
                        }
                    }
                },
                "interactivity": {
                    "detect_on": "canvas",
                    "events": {
                        "onhover": {
                            "enable": true,
                            "mode": "repulse"
                        },
                        "onclick": {
                            "enable": true,
                            "mode": "push"
                        },
                        "resize": true
                    },
                    "modes": {
                        "grab": {
                            "distance": 400,
                            "line_linked": {
                                "opacity": 1
                            }
                        },
                        "bubble": {
                            "distance": 400,
                            "size": 40,
                            "duration": 2,
                            "opacity": 8,
                            "speed": 3
                        },
                        "repulse": {
                            "distance": 200,
                            "duration": 0.4
                        },
                        "push": {
                            "particles_nb": 4
                        },
                        "remove": {
                            "particles_nb": 2
                        }
                    }
                },
                "retina_detect": true
            });
        }
    };

    var pagetitleVideo = function(){
        if ( $('.page-title').hasClass('video') ) {
            jQuery(function () {          
               jQuery("#ptbgVideo").YTPlayer();     
            });
        }
    };

    var removePreloader = function() {        
        $(window).on("load", function () {
            $("#preloader").fadeOut('slow',function(){
                setTimeout(function() {
                    $(this).remove(); 
                });
          }); 
      });
    };


// Dom Ready
$(function() {
    headerFixed();
    headerAbsolute();
    responsiveMenu();
    headerLeft();
    headerStickyLogo();
    themesflatSearch();
    detectViewport(); 
    blogLoadMore();  
    commingsoon();  
    goTop();
    retinaLogos();  
    customizable_carousel();
    parallax();  
    iziModal();
    bg_particles();
    pagetitleVideo();
    removePreloader();
});
})(jQuery);