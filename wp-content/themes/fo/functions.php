<?php
/**
 * themesflat functions and definitions
 *
 * @package themesflat
 */

//remove_theme_mods();
define( 'THEMESFLAT_DIR', trailingslashit( get_template_directory() )) ;
define( 'THEMESFLAT_LINK', trailingslashit( get_template_directory_uri() ) );
define( 'themesflat_icon', THEMESFLAT_LINK.'images/controls/logo.jpg' );
if ( ! function_exists( 'themesflat_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */

function themesflat_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on burger, use a find and replace
	 * to change 'fo' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'fo', THEMESFLAT_DIR . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );


	// Content width
	global $content_width;
	if ( ! isset( $content_width ) ) {
		$content_width = 1170; /* pixels */
	}	

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );	
	add_image_size( 'themesflat-recent-news-thumb', 106, 73, true );		
	add_image_size( 'themesflat-portfolio-thumb', 370, 245, true );	
	add_image_size( 'themesflat-blog', 870, 350, true );	
	add_image_size( 'themesflat-blog-grid', 600, 406, true );	
	add_image_size( 'themesflat-blog-single', 870, 500, true );	
	add_image_size( 'themesflat-blog-listsmall', 370, 230, true );	
	add_image_size( 'themesflat-case-single', 570, 570, true );	
	add_image_size( 'themesflat-case-single2', 1170, 600, true );	
	add_image_size( 'themesflat-case', 570, 422, true );	
	add_image_size( 'themesflat-case2', 570, 416, true );	
	add_image_size( 'themesflat-case3', 570, 401, true );	
	add_image_size( 'themesflat-testimonial', 120, 120, true );	
	add_image_size( 'themesflat-faq', 570, 340, true );	

	//Get thumbnail url
	function themesflat_thumbnail_url($size){
	    global $post;
	    if( $size== '' ) {
	        $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
	        return esc_url($url);
	    } else {
	        $url = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), $size);
	        return esc_url($url[0]);
	    }
	}

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'fo' ),
		'footer' => esc_html__( 'Footer Menu', 'fo' ),
		'bottom' => esc_html__( 'Bottom Menu', 'fo' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'gallery', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	$args = array(
		'default-color' => 'ffffff',
		'default-image' => '',
	);

	add_theme_support( 'custom-background', $args );

	// Custom stylesheet to the TinyMCE visual editor
	function themesflat_add_editor_styles() {
	    add_editor_style( 'css/editor-style.css' );
	}
	add_action( 'admin_init', 'themesflat_add_editor_styles' );	

}
endif; // themesflat_setup

add_action( 'after_setup_theme', 'themesflat_setup' );

function themesflat_wpfilesystem() {
	include_once (ABSPATH . '/wp-admin/includes/file.php');
	WP_Filesystem();
}

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function themesflat_widgets_init() {

	//Sidebar footer
	register_sidebar( array(
        'name'          => esc_html__( 'Footer Style 2 Area 1', 'fo' ),
        'id'            => 'demo-footer-1',
        'description'   => esc_html__( 'Add widgets here to appear in your sidebar Footer Style Area 1.', 'fo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Footer Style 2 Area 2', 'fo' ),
        'id'            => 'demo-footer-2',
        'description'   => esc_html__( 'Add widgets here to appear in your sidebar Footer Style Area 2.', 'fo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Footer Style 2 Area 3', 'fo' ),
        'id'            => 'demo-footer-3',
        'description'   => esc_html__( 'Add widgets here to appear in your sidebar Footer Style Area 3.', 'fo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Footer Style 2 Area 4', 'fo' ),
        'id'            => 'demo-footer-4',
        'description'   => esc_html__( 'Add widgets here to appear in your sidebar Footer Style Area 3.', 'fo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );


	//Sidebar footer
	register_sidebar( array(
        'name'          => esc_html__( 'Footer Widget 1', 'fo' ),
        'id'            => 'footer-1',
        'description'   => esc_html__( 'Add widgets here to appear in your sidebar Footer1.', 'fo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Blog Sidebar', 'fo' ),
        'id'            => 'blog-sidebar',
        'description'   => esc_html__( 'Add widgets here to appear in your sidebar Footer1.', 'fo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

	register_sidebar( array(
        'name'          => esc_html__( 'Page Sidebar', 'fo' ),
        'id'            => 'page-sidebar',
        'description'   => esc_html__( 'Add widgets here to appear in your sidebar Footer1.', 'fo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Nav Widget Area Header Style 5', 'fo' ),
        'id'            => 'widget-nav-header-style5',
        'description'   => esc_html__( 'Nav header style 5.', 'fo' ),
        'before_widget' => '<div id="%1$s" class="widget nav-widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Languages Sidebar', 'fo' ),
        'id'            => 'languages-sidebar',
        'description'   => esc_html__( 'Add widgets here to appear in Languages Sidebar.', 'fo' ),
        'before_widget' => '<div id="%1$s" class="widget themesflat-widget-languages %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );


    register_sidebar( array(
        'name'          => esc_html__( 'Footer Widget 2', 'fo' ),
        'id'            => 'footer-2',
        'description'   => esc_html__( 'Add widgets here to appear in your sidebar Footer2.', 'fo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Footer Widget 3', 'fo' ),
        'id'            => 'footer-3',
        'description'   => esc_html__( 'Add widgets here to appear in your sidebar Footer3.', 'fo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Footer Widget 4', 'fo' ),
        'id'            => 'footer-4',
        'description'   => esc_html__( 'Add widgets here to appear in your sidebar Footer4.', 'fo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );    

    register_sidebar( array(
        'name'          => esc_html__( 'Footer custom menu', 'fo' ),
        'id'            => 'fb-custom-menu',
        'description'   => esc_html__( 'Add widgets here to appear in your sidebar Footer custom menu.', 'fo' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );    
	
	//Register the front page widgets	
	register_widget( 'Themesflat_Flicker' );
	register_widget( 'Themesflat_Recent_Post' );
	register_widget( 'Themesflat_Categories' );
		
}
add_action( 'widgets_init', 'themesflat_widgets_init' );

/**
 * Load the front page widgets.
 */
require THEMESFLAT_DIR . "widgets/themesflat_flickr.php";
require THEMESFLAT_DIR . "widgets/themesflat_recent_post.php";	
require THEMESFLAT_DIR . "widgets/themesflat_categories.php";	
require THEMESFLAT_DIR . "widgets/themesflat_testimonial.php";	

require THEMESFLAT_DIR . "inc/js_composer.php";
require THEMESFLAT_DIR . "inc/options/options.php";
require THEMESFLAT_DIR . "inc/options/options-definition.php";
require_once(THEMESFLAT_DIR .'inc/options/controls/social_icons.php');
require_once(THEMESFLAT_DIR .'inc/options/controls/number.php');
require_once(THEMESFLAT_DIR .'inc/options/controls/dropdown-sidebars.php');
require_once(THEMESFLAT_DIR .'inc/options/controls/box-control.php');
require_once(THEMESFLAT_DIR .'inc/options/controls/typography.php');
require_once(THEMESFLAT_DIR .'inc/options/controls/radio-images.php');
require_once(THEMESFLAT_DIR .'inc/options/controls/color_overlay.php');
require THEMESFLAT_DIR . "inc/admin/sample-data.php";

function themesflat_get_style($style) {
	return str_replace('italic', 'i', $style);
}

function themesflat_fonts_url() {
    $fonts_url = ''; 

    $body_font_name =  themesflat_get_json('body_font_name');
    $headings_font_name = themesflat_get_json('headings_font_name');
    $menu_font_name = themesflat_get_json('menu_font_name');
    $font_families = array(); 

    if ( '' != $body_font_name ) {
        $font_families[] = $body_font_name['family'].':300,400,500,600,700,900,'.themesflat_get_style($body_font_name['style']);
    } else {
    	$font_families[] = 'Lato:400,400i,700,700i,900';
    }    

    if ( '' != $headings_font_name ) {
        $font_families[] = $headings_font_name['family'].':300,400,500,600,700,900,'.themesflat_get_style($headings_font_name['style']);
    }

     else {
    	$font_families[] = 'Poppins:400,500,600,700';
    }    

    if ( '' != $menu_font_name ) {
        $font_families[] = $menu_font_name['family'].':'.themesflat_get_style($menu_font_name['style']);
    } else {
    	$font_families[] = 'Poppins:400,500,600,700';
    }    
    
    $query_args = array(
        'family' => urlencode( implode( '|', $font_families ) ),        
    );

    $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );

    return esc_url_raw( $fonts_url );
}

function themesflat_scripts_styles() {
    wp_enqueue_style( 'themesflat-theme-slug-fonts', themesflat_fonts_url(), array(), null );
}

add_action( 'wp_enqueue_scripts', 'themesflat_scripts_styles' );

/**
 * Enqueue scripts and styles.
 */

function themesflat_scripts() {

	// Theme stylesheet.
	wp_enqueue_style( 'themesflat_main', THEMESFLAT_LINK . 'css/main.css' );
	wp_enqueue_style( 'themesflat-style', get_stylesheet_uri() );
	wp_enqueue_style( 'font-fontawesome', THEMESFLAT_LINK . 'css/font-awesome.css' );
	wp_enqueue_style( 'themesflat-ionicons', THEMESFLAT_LINK . 'css/ionicons.min.css' );	
    wp_enqueue_style('vc_simpleline-css', THEMESFLAT_LINK. 'css/simple-line-icons.css');
    wp_enqueue_style('vc_ion_icon','http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css');

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'ie9', THEMESFLAT_LINK . 'css/ie.css');
	wp_style_add_data( 'ie9', 'conditional', 'lte IE 9' );

    wp_enqueue_style( 'themesflat_logo', THEMESFLAT_LINK . 'css/logo.css' );  
	wp_enqueue_style( 'themesflat_animate', THEMESFLAT_LINK . 'css/animate.css' );	
	wp_enqueue_style( 'themesflat_responsive', THEMESFLAT_LINK . 'css/responsive.css' );
	wp_enqueue_style( 'themesflat-inline-css', THEMESFLAT_LINK . 'css/inline-css.css' );
	
	wp_enqueue_script( 'flexslider');
	wp_enqueue_script( 'jquery-svginjector', THEMESFLAT_LINK . 'js/svg-injector.min.js', array(),'2.5.0', true );
	
	// Load the html5 shiv..	
	wp_enqueue_script( 'jquery-html5', THEMESFLAT_LINK . 'js/html5shiv.js', array('jquery'), '1.3.0' ,true);	
	wp_enqueue_script( 'jquery-respond', THEMESFLAT_LINK . 'js/respond.min.js', array(), '1.3.0',true);
	wp_enqueue_script( 'jquery-easing', THEMESFLAT_LINK . 'js/jquery.easing.js', array(),'1.3' ,true);
	wp_enqueue_script( 'jquery-waypoints', THEMESFLAT_LINK . 'js/jquery-waypoints.js', array(),'1.3' ,true);	

	wp_enqueue_script( 'jquery-match', THEMESFLAT_LINK . 'js/matchMedia.js', array(),'1.2',true);
	wp_register_script( 'jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array('jquery'),'1.12',true);

	wp_enqueue_script( 'jquery-fitvids', THEMESFLAT_LINK . 'js/jquery.fitvids.js', array(),'1.1',true);
    wp_enqueue_script( 'themesflat-carousel', THEMESFLAT_LINK . 'js/owl.carousel.js', array(),'1.1',true);
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply', array(),'2.0.4',true );
	}

	// Load the main js
	wp_enqueue_script( 'themesflat-main', THEMESFLAT_LINK . 'js/main.js', array(),'2.0.4',true);
}

add_action( 'wp_enqueue_scripts', 'themesflat_scripts' );

/**
 * Enqueue Bootstrap
 */
function themesflat_enqueue_bootstrap() {
	wp_enqueue_style( 'bootstrap', THEMESFLAT_LINK . 'css/bootstrap.css', array(), true );
}
add_action( 'wp_enqueue_scripts', 'themesflat_enqueue_bootstrap', 9 );

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses themesflat_header_style()
 */

// Customizer additions.
require THEMESFLAT_DIR . 'inc/customizer.php';

// Revo Slider
require THEMESFLAT_DIR . 'inc/revo-slider.php';

// metabox-options
require THEMESFLAT_DIR . 'inc/metabox-options.php';

// Helpers
require THEMESFLAT_DIR . 'inc/helpers.php';
// Struct
require THEMESFLAT_DIR . 'inc/structure.php';

// Breadcrumbs additions.
require THEMESFLAT_DIR . 'inc/breadcrumb.php';

// Pagination additions.
require THEMESFLAT_DIR . 'inc/pagination.php';

// Custom template tags for this theme.
require THEMESFLAT_DIR . 'inc/template-tags.php';

// Style.
require THEMESFLAT_DIR . 'inc/styles.php';

// Required plugins
require_once THEMESFLAT_DIR . 'inc/plugins/class-tgm-plugin-activation.php';

// Plugin Activation
require_once THEMESFLAT_DIR . 'inc/plugins/plugins.php';