<?php
/**
 * @package themesflat
 */
$portfolio_single_style = themesflat_choose_opt('portfolio_single_style');
$imgs = array(
	'full_content' => 'themesflat-case-single2',
	'right_content'=> 'themesflat-case-single'
	);
$featured_img = $imgs[$portfolio_single_style];
?>

<section class="portfolio-detail <?php echo esc_attr($portfolio_single_style);?> ">	
	<div class="container">
		<?php the_title( '<h1 class="single-portfolio-title">', '</h1>' ); ?>
		<div class="row">
			<div class="col-md-6">             	
            	<?php 
            	wp_enqueue_script( 'flexslider' ); 
            	wp_enqueue_style( 'flexslider' );           	
            	?>  
            	<div class="themesflat-portfolio-single-slider">
                    <div id="themesflat-portfolio-flexslider">
                    <?php  ?>

                        <ul class="slides">
                        <?php 

		            		$images = themesflat_decode(themesflat_meta( 'gallery_portfolio')); 
		            		echo '<li>';
		            		the_post_thumbnail($featured_img);      		;
		            		echo '</li>';
					        if ( !empty( $images ) && is_array( $images ) ) {
					           foreach ( $images as $image ) {
					              echo '<li>';             
					              echo wp_get_attachment_image($image,$featured_img);
					              echo '</li>';                                 
					           }
					        } 
		        		?>                         
                        </ul>
                    </div>
                    <div id="themesflat-portfolio-carousel">
                        <ul class="slides">
                        <?php 
	                        echo '<li>';
		            		the_post_thumbnail($featured_img);      		;
		            		echo '</li>';
		            		$images = themesflat_decode(themesflat_meta( 'gallery_portfolio'));
					        if ( !empty( $images ) && is_array( $images ) ) {
					           foreach ( $images as $image ) {
					              echo '<li>';             
					              echo wp_get_attachment_image($image,$featured_img);
					              echo '</li>';                                 
					           }
					        } 
		        		?>                    
                        </ul>
                    </div>
                </div><!-- /.themesflat-portfolio-single-slider --> 
            </div>   
			<div class="col-md-6">
				<div class="content-portfolio-detail">
            		<?php the_content(); ?>
            	</div>
				
            </div>
                    
		</div>
	</div>
</section>