

<?php 
// Ignore ouput topbar when it isn't enabled
$top_status = themesflat_choose_opt('topbar_enabled');
$top_content = themesflat_choose_opt('top_content');
$top_content_right = themesflat_choose_opt('top_content_right');
if ( $top_status != 1 ) return;
?>
<!-- Top -->
<div class="themesflat-top <?php echo esc_attr( themesflat_choose_opt('header_style') ) ?>">    
    <div class="container">
        <div class="container-inside">
        	<div class="content-left">
            <?php
            	echo wp_kses_post($top_content);
            ?>
            </div><!-- /.col-md-7 -->

            <div class="content-right">
            <?php     
                 if ( themesflat_choose_opt('enable_social_link') == 1 ):
                    themesflat_render_social();    
                endif;              
                if ( themesflat_choose_opt('enable_content_right_top') == 1 ):
                    echo wp_kses_post( $top_content_right );
                endif;

                if (themesflat_choose_opt('enable_language_switch') == 1) {
                   themesflat_dynamic_sidebar('languages-sidebar');
                }
            ?>
            </div><!-- /.col-md-5 -->

        </div><!-- /.container -->
    </div><!-- /.container -->        
</div><!-- /.top -->