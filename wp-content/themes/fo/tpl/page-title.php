<?php 
    if ( is_page() && is_page_template( 'tpl/front-page.php' ) ) {
        echo '<div class="clearfix"></div>';
        return;
    }

    $title = esc_html__( 'Archives', 'fo' );

    if ( is_home() ) {
        if (is_front_page() ) {
            $title = esc_html__( 'Home', 'fo' );
        }
        else {
            $title =  get_the_title();
        }
    } elseif ( is_singular() ) {
        if (is_single()) {
          
                $cat = get_the_category();
                if ( !empty($cat)) {
                    $title = $cat[0]->name;
                }
        }
        elseif(is_page_template('tpl/page_single.php')) {
            $title = '';
        }

         else {
            $title = get_the_title();
        }
    } elseif ( is_search() ) {
        $title = sprintf( esc_html__( 'Search results for &quot;%s&quot;', 'fo' ), get_search_query() );
    } elseif ( is_404() ) {
        $title = esc_html__( 'Not Found', 'fo' );
    } elseif ( is_author() ) {
        the_post();
        $title = sprintf( esc_html__( 'Author Archives: %s', 'fo' ), get_the_author() );
        rewind_posts();
    } elseif ( is_day() ) {
        $title = sprintf( esc_html__( 'Daily Archives: %s', 'fo' ), get_the_date() );
    } elseif ( is_month() ) {
        $title = sprintf( esc_html__( 'Monthly Archives: %s', 'fo' ), get_the_date( 'F Y' ) );
    } elseif ( is_year() ) {
        $title = sprintf( esc_html__( 'Yearly Archives: %s', 'fo' ), get_the_date( 'Y' ) );
    } elseif ( is_tax() || is_category() || is_tag() ) {
        $title = single_term_title( '', false );
    }
?>

   <?php 
   $box_control = themesflat_decode(themesflat_choose_opt('page_title_controls')) ;
   $page_title_img = themesflat_choose_opt('page_title_background_image');
   $page_title_overlay_color = themesflat_choose_opt('page_title_overlay_color');
   $page_title_text_color = themesflat_choose_opt('page_title_text_color');
   $str = "background: url($page_title_img) no-repeat center / cover; ";
   themesflat_render_box_position('.page-title',$box_control,$str);
   $str = " background-color: $page_title_overlay_color ";
   themesflat_render_style('.page-title .overlay',$str);
   $str = " color: $page_title_text_color ";
   themesflat_render_style('.page-title h1',$str);
   ?>
<?php if (themesflat_choose_opt('show_page_title') != 1 || $title =='' ) return;?>
<!-- Page title -->
<div class="page-title">
    <div class="overlay"></div>   
    <div class="container"> 
        <div class="row">
            <div class="col-md-12 page-title-container">
            <?php 
                if ( themesflat_choose_opt( 'breadcrumb_enabled' ) == 1 ):

                    themesflat_breadcrumb_trail( array(
                        'separator'   => themesflat_choose_opt('breadcrumb_separator'),
                        'show_browse' => true,
                        'labels'      => array(
                        'browse'  => get_theme_mod( 'breadcrumb_prefix', esc_html__( '', 'fo' ) ),
                            'home'    => esc_html__( 'Home', 'fo' )
                        )
                    ) );
                
                endif;                       
            ?> 
            <?php
            if ( themesflat_choose_opt( 'show_page_title_heading' ) == 1 ):
            printf('<h1>%s</h1>',$title);
            endif;
            ?>
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /.page-title --> 