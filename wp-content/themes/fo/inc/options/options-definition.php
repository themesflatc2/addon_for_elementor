<?php
/**
 * Return the default options of the theme
 * 
 * @return  void
 */

function themesflat_customize_default_options2($key) {
	$default = array(
		'logo_controls' => array('padding-top' => 24,'padding-left' => 15),
		'footer_controls' => array('padding-top' => 64,'padding-bottom' => 74),
		'bread_crumb_prefix' =>'',
		'bottom_style' => 'bottom-center',
		'footer_background_image' => '',
		'breadcrumb_separator' =>  esc_html('>', 'fo'),
		'footer_widget_areas'				=> 4,
		'enable_footer' => 1,
		'show_post_navigator' => 0,
		'breadcrumb_prefix' => '',
		'logo_height' => 50,
		'menu_location_primary' => 'primary',
		'site_logo'	=> THEMESFLAT_LINK . 'images/logo.svg',
		'site_retina_logo' => THEMESFLAT_LINK . 'images/logo.svg',
		'social_links'	=> array ("facebook" => 'facebook.com/themesflat', "twitter"=>"#", "instagram"=>"#", "rss"=>"#"),
		'enable_footer_social' => 0,
		'socials_link_footer' => 0,
		'page_title_overlay_color' => '#fff',
		'page_title_text_color' => '#222',
		'show_page_title_heading'=> 1,
		'page_title_link_color' => '#999',
		'page_title_overlay_opacity' => 0,
		'page_title_controls' => array('padding-top' => 21),
		'page_title_background_image' => '',
		'footer1' => 'footer-1',
		'footer2' => 'footer-2',
		'footer3' => 'footer-3',
		'footer4' => 'footer-4',
		'enable_social_link'  => 1,
		'logo_margin_left' 	  => 29,
		'show_page_title'	  => 1,
		'key_api_google'     =>'AIzaSyCOYt9j4gB6udRh420WRKttoGoN38pzI7w',
		'portfolio_show_post_navigator' => 0,
		'enable_content_right_top'  => 1,
		'enable_language_switch'  => 0,
		'top_background_color'	=> '#3a526a',
		'topbar_textcolor'	=> '#ffffff',
		'mainnav_backgroundcolor'=>'#fff',
		'mainnav_color'		=> '#424242',
		'mainnav_hover_color'=>'#18ba60',
		'sub_nav_color'		=>'#fff',
		'sub_nav_background'=>'#1d2738',
		'border_clor_sub_nav'=>'#2d374a',
		'sub_nav_background_hover'=>'#18ba60',
		'primary_color'=>'#18ba60',
		'body_text_color'=>'#666',
		'body_font_name'	=> array(
			'family' => 'Poppins',
			'style'  => 'regular',
			'size'   => '14',
			'line_height'=>'25'
			), 
		'header_style'	=> 'header-style1',
		'header_content'	=> '<ul>
									<li>
										<i class=" icon_pin_alt"></i>
										<strong style="margin-bottom:5px">Address</strong>
										<p style="font-size:12px">2901 Marmora Road, Central New,
												United Kingdom.</p>
									</li>
									<li >
										<i class="icon_clock_alt"></i>
										<strong style="margin-bottom:5px">Open House</strong><p style="font-size:12px">
										Mon - Sat: 8.00 am - 5.30 pm<br/>
										Sunday: CLOSED </p>
									</li>
								</ul>
								<a class="appoinment" href="#">Get An Appointment</a>',
		'headings_font_name'	=> array(
			'family' => 'Poppins',
			'style'  => '600'			
			),
		'h1_size' => 32,
		'h2_size' => 25,
		'h3_size' => 22,
		'h4_size' => 18,
		'h5_size' => 15,
		'h6_size' => 14,
		'breadcrumb_enabled' => 1,
		'show_post_paginator' => 0,
		'blog_grid_columns' => 3,
		'blog_archive_exclude' => '',
		'testimonial_rating' => 0,
		'blog_layout' => 'sidebar-left',
		'page_layout' => 'sidebar-left',
		'blog_archive_layout' => 'blog-list',
		'blog_archive_post_excepts_length' => 51,
		'related_post_style'	=> 'blog-grid',
		'blog_sidebar_list'		  => 'blog-sidebar',
		'blog_archive_show_post_meta'	=> 1,		
		'blog_archive_readmore' => 1,
		'blog_archive_post_excepts_length' => 55,
		'blog_archive_readmore_text' => 'Read More <i class="fa fa-chevron-right" aria-hidden="true"></i>',
		'blog_archive_pagination_style' => 'pager-numeric',
		'blog_posts_per_page'	=> 9,
		'blog_order_by'	=> 'date',
		'blog_order_direction' => 'DESC',
		'page_sidebar_list'	=> 'blog-sidebar',
		'menu_font_name'	=> array(
			'family' => 'Poppins',
			'style'  => '600',
			'size'   => '14',
			'line_height'=>'100',
			),
		'show_readmore'	  => 1,
		'show_filter_portfolio' => 1,
		'portfolio_style'		=>'grid',
		'grid_columns_portfolio' => 'one-three',
		'portfolio_exclude' =>'',
		'portfolio_archive_pagination_style' => 'pager-numeric',
		'portfolio_grid_columns' => 'one-four',	
		'portfolios_sidebar'		=> 'fullwidth',
		'portfolio_post_perpage'	=> 9,
		'portfolio_order_by'	=> 'date',
		'portfolio_order_direction' => 'DESC',
		'portfolio_category_order' => '',
		'portfolio_single_style'	=> 'right_content',
		'related_portfolio_style' => 'grid',
		'grid_columns_portfolio_related' => 'one-three',
		'number_related_portfolio' => 3,
		'show_related_portfolio' => 0,		
		'enable_custom_topbar'  => 0,
		'enable_page_callout'	=> 0,
		'topbar_enabled' => 1,
		'header_sticky' => 1,
		'preloader'		=> 1,
		'header_searchbox' => 0,		
		'footer_background_color'	=> '#222',
		'footer_text_color'			=> '#888',
		'bottom_background_color'	=> '#1b1b1b',
		'bottom_text_color'			=> '#888',
		'go_top'					=> 1,
		'layout_version'			=> 'wide',		
		'footer_copyright'			=> '<p>Coyright <i class="fa fa-copyright" aria-hidden="true"></i>
 2017 <a href="#">ThemesFlat</a>. All rights reserved.</p>',
		'top_content' => '
		<ul>
			<li class="border-right">
				<i class="fa fa-phone"></i>Call us: 290 986 1386     
			</li>
			<li >
				<i class="fa fa-envelope"></i> <a href="mailto:someone@example.com?Subject=Hello%20again" target="_top">Email: support24-7@gmail.com</a> 
			</li>

		</ul>	
		',
		'top_content_right' => '<div class="info-top-right border-left">
		<span><i class="fa fa-question-circle"></i>Have any questions</span>
		<a class="appoinment" href="#">Get An Appointment</a>
	</div>',
	);
	return $default[$key];
}

/**
 * Return an array that used to declare options
 * for the page
 * 
 * @return  array
 */
function themesflat_portfolio_options_fields() {
	$options['cover_heading'] = array(
		'type' => 'heading',
		'section' => 'general',
		'title' => esc_html__( 'Portfolio', 'fo' ),
		'description' => esc_html__( 'This is an special option, it allow to set Portfolio informations.', 'fo' )
		);

	$options['gallery_portfolio'] = array(
		'type'    => 'image-control',
		'section' => 'general',
		'title' => esc_html__( 'Images', 'fo' ),
		'default' => ''
		);

	themesflat_prepare_options($options);
	return $options;
}

function themesflat_testimonial_options_fields() {
	$options['cover_heading'] = array(
		'type' => 'heading',
		'section' => 'testimonial_details',
		'title' => esc_html__( 'Testimonial Details', 'fo' ),
		);

	$options['testimonial_subtitle'] = array(
		'type'    => 'text',
		'section' => 'testimonial_details',
		'title' => esc_html__( 'Subtitle', 'fo' ),
		'default' => ''
		);

	$options['testimonial_company'] = array(
		'type'    => 'text',
		'section' => 'testimonial_details',
		'title' => esc_html__( 'Company', 'fo' ),
		'default' => ''
		);

	$options['testimonial_rating'] = array(
		'type'    => 'select',
		'section' => 'testimonial_details',
		'title'   => esc_html__( 'Ratings', 'fo' ),
		'default' => themesflat_get_opt('testimonial_rating'),
		'choices'   => array(
				'5' => esc_html__('5 Stars','fo'),
				'4' => esc_html__('4 Stars','fo'),
				'3' => esc_html__('3 Stars','fo'),
				'2' => esc_html__('2 Stars','fo'),
				'1' => esc_html__('1 Stars','fo'),
				'0' => esc_html__('No Rating','fo')
			)
	);

	$options['testimonial_link'] = array(
		'type'    => 'text',
		'section' => 'testimonial_details',
		'title' => esc_html__( 'Link', 'fo' ),
		'default' => ''
	);

	themesflat_prepare_options($options);
	return $options;
}

function themesflat_post_options_fields() {
	$options['blog_heading'] = array(
		'type' => 'heading',
		'section' => 'blog',
		'title' => esc_html__( 'Dear friends,', 'fo' ),
		'description' => esc_html__( 'Option just view if post format is gallery or video! <br/>Thanks!', 'fo' )
		);
	$options['gallery_images_heading'] = array(
		'type' => 'heading',
		'section' => 'blog',
		'title' => esc_html__( 'Post Format: Gallery .', 'fo' ),
		'description' => esc_html__( '', 'fo' )
		);

	$options['gallery_images'] = array(
		'type'    => 'image-control',
		'section' => 'blog',
		'title' => esc_html__( 'Images', 'fo' ),
		'default' => ''
		);

	$options['video_url_heading'] = array(
		'type' => 'heading',
		'section' => 'blog',
		'title' => esc_html__( 'Post Format: Video ( Embeded video from youtube, vimeo ...).', 'fo' ),
		'description' => esc_html__( '', 'fo' )
		);

	$options['video_url'] = array(
		'type'    => 'textarea',
		'section' => 'blog',
		'title' => esc_html__( 'iframe video link', 'fo' ),
		'default' => ''
		);
	themesflat_prepare_options($options);
	return $options;
}

function themesflat_blog_options_fields() {
	$options['position_field_heading'] = array(
		'type' => 'heading',
		'section' => 'events',
		'title' => esc_html__( 'Events', 'fo' ),
		'description' => esc_html__( 'This is an special option, it allow to set Causes informations.', 'fo' )
		);

	$options['position_field'] = array(
		'type'    => 'text',
		'section' => 'events',
		'title' => esc_html__( 'Position', 'fo' ),
		'default' => ''
		);

	$options['address'] = array(
		'type'    => 'textarea',
		'section' => 'events',
		'title' => esc_html__( 'Address', 'fo' ),
		'default' => ''
		);

	$options['event_time'] = array(
		'type'    => 'datetime',
		'section' => 'events',
		'title' => esc_html__( 'Event date time', 'fo' ),
		'default' => ''
		);

	$options['event_link'] = array(
		'type'    => 'text',
		'section' => 'events',
		'title' => esc_html__( 'Link to join', 'fo' ),
		'default' => ''
		);
	themesflat_prepare_options($options);
	return $options;
}
function themesflat_page_options_fields() {
	global $wp_registered_sidebars;

	$options  = array();
	$sidebars = array();

	// Retrieve all registered sidebars
	foreach( $wp_registered_sidebars as $params )
		$sidebars[$params['id']] = $params['name'];

	/**
	 * General
	 */	
	$options['layout_heading'] = array(
		'type' => 'heading',
		'section' => 'general',
		'title' => esc_html__( 'Layout', 'fo' ),
		'description' => esc_html__( 'Choose between a full or a boxed layout to set how this page layout will look like.', 'fo' )
		);

	$options['enable_custom_layout'] = array(
		'type'    => 'power',
		'title'   => esc_html__( 'Enable Custom Layout', 'fo' ),
		'section' => 'general',
		'children'=> array('layout_version','page_layout','sidebar_default','page_sidebar_list'),
		'default' => false
		);

	$options['layout_version'] = array(
		'type'    => 'select',
		'title'   => esc_html__( 'Display Style', 'fo' ),
		'section' => 'general',
		'default' => 'wide',
		'choices' => array(
			'wide'  =>  esc_html__( 'Wide', 'fo' ),
			'boxed'  =>  esc_html__( 'Boxed', 'fo' )
			)
		);

	$options['page_layout'] = array(
		'type'    => 'select',
		'title'   => esc_html__( 'Sidebar Position', 'fo' ),
		'section' => 'general',
		'default' => 'sidebar-right',
		'choices' => array(
			'fullwidth' => esc_html__( 'No Sidebar', 'fo' ),
			'sidebar-left' => esc_html__( 'Sidebar Left', 'fo' ),
			'sidebar-right' =>  esc_html__( 'Sidebar Right', 'fo' )
			)
		);

	$options['page_sidebar_list'] = array(
		'type'    => 'dropdown-sidebar',
		'title'   => esc_html__( 'Custom Sidebar', 'fo' ),
		'section' => 'general',
		'default' => 'sidebar-page'
		);

	$options['page_title_heading'] = array(
		'type' => 'heading',
		'section' => 'general',
		'title' => esc_html__( 'Page Title', 'fo' ),
		);

	$options['enable_custom_page-title'] = array(
		'type'    => 'power',
		'title'   => esc_html__( 'Enable Custom Page Title', 'fo' ),
		'section' => 'general',
		'children' => array('show_page_title','page_title_overlay_color','page_title_overlay_opacity','page_title_controls','page_title_background_image'),
		'default' => false
		);

	$options['show_page_title'] = array(
		'type'    => 'power',
		'title'   => esc_html__( 'Show Page Tile', 'fo' ),
		'section' => 'general',
		'default' => themesflat_customize_default_options2( 'topbar_enabled' )
		);

	$options['page_title_overlay_color'] = array(
		'type'    => 'color-picker',
		'title'   => esc_html__( 'Page Title Overlay Color', 'fo' ),
		'section' => 'general',
		'default' => themesflat_customize_default_options2( 'page_title_overlay_color' )
		);

	$options['page_title_controls'] = array(
		'type' => 'box-controls',
		'title'   => esc_html__( 'Page Title Positions', 'fo' ),
		'section' => 'general',
		'default' => themesflat_customize_default_options2('page_title_controls')
		);
	$options['page_title_background_image'] = array(
		'type' => 'single-image-control',
		'title'   => esc_html__( 'Page Title Background Image', 'fo' ),
		'section' => 'general',
		'default' => themesflat_customize_default_options2('page_title_background_image')
		);

	$options['page_class_heading'] = array(
		'type' => 'heading',
		'section' => 'general',
		'title' => esc_html__( 'Custom Page Class', 'fo' ),
		);
	
	$options['custom_page_class'] = array(
		'type'    => 'text',
		'section' => 'general',
		);

	/**
	 * Header
	 */
	$options['topbar_heading'] = array(
		'type' => 'heading',
		'section' => 'header',
		'title' => esc_html__( 'Top Bar', 'fo' ),
		'description' => esc_html__( 'Turn on/off the top bar and change it styles.', 'fo' )
		);



	$options['enable_custom_topbar'] = array(
		'type'    => 'power',
		'title'   => esc_html__( 'Enable Custom Topbar', 'fo' ),
		'section' => 'header',
		'children' => array('topbar_enabled','enable_content_right_top','top_background_color','topbar_textcolor','top_content','top_content_right','enable_social_link','enable_language_switch'),
		'default' => false
		);



	$options['topbar_enabled'] = array(
		'type'    => 'power',
		'title'   => esc_html__( 'Display Topbar On This Page', 'fo' ),
		'section' => 'header',
		'default' => themesflat_customize_default_options2( 'topbar_enabled' )
		);

	$options['enable_social_link'] = array(
		'type'    => 'power',
		'title'   => esc_html__( 'Enable Social Links', 'fo' ),
		'section' => 'header',
		'default' => themesflat_customize_default_options2( 'enable_social_link' )
		);

	$options['enable_content_right_top'] = array(
		'type'    => 'power',
		'title'   => esc_html__( 'Enable Content Right Top', 'fo' ),
		'section' => 'header',
		'default' => themesflat_customize_default_options2( 'enable_content_right_top' )
		);

	$options['enable_language_switch'] = array(
		'type'    => 'power',
		'title'   => esc_html__( 'Enable Language Switch', 'fo' ),
		'section' => 'header',
		'default' => themesflat_customize_default_options2( 'enable_language_switch' )
		);

	$options['top_background_color'] = array(
		'type'    => 'color-picker',
		'title'   => esc_html__( 'Topbar Background', 'fo' ),
		'section' => 'header',
		'default' => themesflat_get_opt( 'top_background_color' )
		);

	$options['topbar_textcolor'] = array(
		'type'    => 'color-picker',
		'title'   => esc_html__( 'Topbar Text Color', 'fo' ),
		'section' => 'header',
		'default' => themesflat_get_opt( 'topbar_textcolor' )
		);

	$options['top_content'] = array(
		'type'    => 'textarea',
		'title'   => esc_html__( 'Content Left', 'fo' ),
		'section' => 'header',
		'default' => themesflat_get_opt( 'top_content' )
		);

	$options['top_content_right'] = array(
		'type'    => 'textarea',
		'title'   => esc_html__( 'Content Right', 'fo' ),
		'section' => 'header',
		'default' => themesflat_get_opt( 'top_content_right' )
		);

	$options['header_style_heading'] = array(
		'type'        => 'heading',
		'section'     => 'header',
		'title'       => esc_html__( 'Custom Header', 'fo' ),
		'description' => esc_html__( 'Change the header style, toggle sticky header feature and turn on/off extra menu icons.', 'fo' )
		);

	$options['enable_custom_header_style'] = array(
		'type'    => 'power',
		'title'   => 'Enable Custom Styles',
		'title'   => esc_html__( 'Enable Custom Header', 'fo' ),
		'section' => 'header',
		'children' => array('header_sticky','header_searchbox','header_show_offcanvas','header_image','header_content','header_style','logo_controls'),
		'default' => false
		);

	$options['logo_controls'] = array(
		'type' => 'box-controls',
		'title'   => esc_html__( 'Logo Positions', 'fo' ),
		'section' => 'header',
		'default' => themesflat_customize_default_options2('logo_controls')
		);

	$options['header_style'] = array(
		'type'    => 'radio-images',
		'title'   => esc_html__( 'Header Style', 'fo' ),
		'section' => 'header',
		'default' => themesflat_customize_default_options2('header_style'),
		'choices' => array (
                'header-style1' => array (
                    'tooltip'   => esc_html( 'Header Style 1','fo' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/header1.jpg'
                ) ,
                'header-style2'=>  array (
                    'tooltip'   => esc_html( 'Header Style 2','fo' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/header2.jpg'
                ) ,
               'header-style3'=>  array(
                    'tooltip'   => esc_html( 'Header Style 3','fo' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/header3.jpg'
                ) ,
               'header-style4'=>  array(
                    'tooltip'   => esc_html( 'Header Style 4','fo' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/header4.jpg'
                ) ,
                'header-style5'=>  array(
                    'tooltip'   => esc_html( 'Header Style 5','fo' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/header2.jpg'
                ) ,
            )
		);


	$options['header_content'] = array(
		'type'    => 'textarea',
		'title'   => esc_html__( 'Header Content', 'fo' ),
		'section' => 'header',
		'default' => themesflat_get_opt( 'header_content' )
		);

	$options['header_sticky'] = array(
		'type'    => 'power',
		'section' => 'header',
		'title'   => esc_html__( 'Enable Sticky Header', 'fo' )
		);

	$options['header_searchbox'] = array(
		'type'    => 'power',
		'section' => 'header',
		'title'   => esc_html__( 'Show Search Menu', 'fo' ),
		'default' => ( 'header_searchbox' )
		);

	$options['navigator_heading'] = array(
		'type'        => 'heading',
		'section'     => 'header',
		'title'       => esc_html__( 'Navigator', 'fo' ),
		'description' => esc_html__( 'Just select your menu that you wish assign it to the location on the theme.', 'fo' )
		);

	$options['enable_custom_navigator'] = array(
		'type'    => 'power',
		'section' => 'header',
		'title'   => esc_html__( 'Enable Custom Navigator', 'fo' ),
		'children' => array('onepage_nav_script','mainnav_color','mainnav_backgroundcolor','menu_location_primary'),
		'default' => false
		);

	$options['mainnav_backgroundcolor'] = array(
		'type'    => 'color-picker',
		'title'   => esc_html__( 'Mainnav Background', 'fo' ),
		'section' => 'header',
		'default' => themesflat_get_opt( 'mainnav_backgroundcolor' )
		);

	$options['mainnav_color'] = array(
		'type'    => 'color-picker',
		'title'   => esc_html__( 'Mainnav Color', 'fo' ),
		'section' => 'header',
		'default' => themesflat_get_opt( 'mainnav_color' )
		);

	$options['onepage_nav_script'] = array(
		'type'    => 'power',
		'section' => 'header',
		'title'   => esc_html__( 'Load One Page Navigator Script ( Comming Soon New Versions )', 'fo' ),
		'default' => ( 'onepage_nav_script' )
		);

	// Navigator
	$menus     = wp_get_nav_menus();

	if ( $menus ) {
		$choices = array( 0 => esc_html__( '-- Select --', 'fo' ) );
		foreach ( $menus as $menu ) {
			$choices[ $menu->term_id ] = wp_html_excerpt( $menu->name, 40, '&hellip;' );
		}

		$options["menu_location_primary"] = array(
				'title'   => esc_html__('Choose menu for page','fo'),
				'section' => 'header',
				'type' 	  => 'select',
				'choices' => $choices,
				'default' => themesflat_customize_default_options2('menu_location_primary')
			);
	}

	/**
	 * Footer
	 */	
	$options['footer_widgets_heading'] = array(
		'type'        => 'heading',
		'section'     => 'footer',
		'title'       => esc_html__( 'Footer Widgets', 'fo' ),
		'description' => esc_html__( 'This section allow to change the layout and styles of footer widgets to match as you need.', 'fo' )
		);

	$options['enable_custom_footer_widgets'] = array(
		'type'    => 'power',
		'title'   => esc_html__( 'Enable Custom Footer Widgets', 'fo' ),
		'section' => 'footer',
		'children'=> array('footer_background_color','footer_text_color','footer_widget_areas','footer1','footer2','footer3','footer4','footer_controls','footer_background_image'),
		'default' => false
		);
	
	$options['footer_widget_areas'] = array(
		'type'    => 'select',
		'title'   => esc_html__( 'Footer Widget Layout', 'fo' ),
		'section' => 'footer',
		'choices'   => array(                
                0    => esc_html( 'None', 'fo' ),
                1     => esc_html( '1 Columns', 'fo' ),
                2     => esc_html( '2 Columns', 'fo' ),
                3     => esc_html( '3 Columns', 'fo' ),
                4     => esc_html( '4 Columns', 'fo' ),                
                5     => esc_html( '4 Columns Equals', 'fo' ),                
            ),
		'default' => themesflat_customize_default_options2('footer_widget_areas')
		);

	$options['footer1'] = array(
		'type'    => 'dropdown-sidebar',
		'title'   => esc_html__( 'Footer Widget Area 1', 'fo' ),
		'section' => 'footer',
		'default' => themesflat_customize_default_options2('footer1')
		);

	$options['footer2'] = array(
		'type'    => 'dropdown-sidebar',
		'title'   => esc_html__( 'Footer Widget Area 2', 'fo' ),
		'section' => 'footer',
		'default' => themesflat_customize_default_options2('footer2')
		);

	$options['footer3'] = array(
		'type'    => 'dropdown-sidebar',
		'title'   => esc_html__( 'Footer Widget Area 3', 'fo' ),
		'section' => 'footer',
		'default' => themesflat_customize_default_options2('footer3')
		);

	$options['footer4'] = array(
		'type'    => 'dropdown-sidebar',
		'title'   => esc_html__( 'Footer Widget Area 4', 'fo' ),
		'section' => 'footer',
		'default' => themesflat_customize_default_options2('footer4')
		);

	$options['footer_controls'] = array(
		'type' => 'box-controls',
		'title'   => esc_html__( 'Footer Controls', 'fo' ),
		'section' => 'footer',
		'default' => themesflat_customize_default_options2('footer_controls')
		);

	$options['footer_background_image'] = array(
		'type' => 'single-image-control',
		'title'   => esc_html__( 'Footer Background Image', 'fo' ),
		'section' => 'footer',
		'default' => themesflat_customize_default_options2('footer_background_image')
		);

	$options['footer_background_color'] = array(
		'type'    => 'color-picker',
		'title'   => esc_html__( 'Footer Color Background/Overlay', 'fo' ),
		'section' => 'footer',
		'default' => themesflat_get_opt( 'footer_background_color' )
		);

	$options['footer_text_color'] = array(
		'type'    => 'color-picker',
		'title'   => esc_html__( 'Footer Top Text Color', 'fo' ),
		'section' => 'footer',
		'default' => themesflat_get_opt( 'footer_text_color' )
		);
	
	$options['footer_heading'] = array(
		'type'        => 'heading',
		'class'       => 'no-border',
		'section'     => 'footer',
		'title'       => esc_html__( 'Custom Footer', 'fo' ),
		'description' => esc_html__( 'You can change the copyright text, show/hide the social icons on the footer.', 'fo' )
		);

	$options['enable_custom_footer'] = array(
		'type'    => 'power',
		'title'   => esc_html__( 'Enable Custom Footer Content', 'fo' ),
		'section' => 'footer',
		'children'=>array('socials_link_footer','footer_copyright','bottom_text_color','bottom_background_color','enable_footer_social','bottom_style'),
		'default' => false
		);

	$options['footer_copyright'] = array(
		'type'    => 'textarea',
		'title'   => esc_html__( 'Copyright', 'fo' ),
		'section' => 'footer',
		'default' => themesflat_customize_default_options2( 'footer_copyright' )
		);

	$options['enable_footer_social'] = array(
		'type'    => 'power',
		'section' => 'footer',
		'title'   => esc_html__( 'Show Footer Social', 'fo' ),
		'default' => themesflat_get_opt('enable_footer_social')
		);

	$options['bottom_style'] = array(
		'type'    => 'select',
		'title'   => esc_html__( 'Footer Bottom Style', 'fo' ),
		'section' => 'footer',
		'default' => 'bottom-center',
		'choices' => array(
			'bottom-center' => esc_html__( 'Center', 'fo' ),
			'bottom-inline' => esc_html__( 'Bottom Inline', 'fo' ),
			)
		);

	$options['bottom_background_color'] = array(
		'type'    => 'color-picker',
		'title'   => esc_html__( 'Bottom Background Color', 'fo' ),
		'section' => 'footer',
		'default' => themesflat_get_opt( 'bottom_background_color' )
		);

	$options['bottom_text_color'] = array(
		'type'    => 'color-picker',
		'title'   => esc_html__( 'Bottom Text Color', 'fo' ),
		'section' => 'footer',
		'default' => themesflat_get_opt( 'bottom_text_color' )
		);

	/**
	 * Portfolio
	 */
	$options['portfolio_list_heading'] = array(
		'type'        => 'heading',
		'class'       => 'no-border',
		'section'     => 'portfolio',
		'title'       => esc_html__( 'Portfolio', 'fo' ),
		'description' => esc_html__( 'Change options in this section to custom style for portfolio listing page.', 'fo' )
		);

	$options['enable_custom_portfolio'] = array(
		'type'    => 'power',
		'title'   => esc_html__( 'Enable Custom Portfolio layout', 'fo' ),
		'section' => 'portfolio',
		'children'=> array('portfolio_grid_columns','show_filter_portfolio','portfolio_archive_pagination_style','portfolio_post_perpage','portfolio_order_by','portfolio_order_direction','portfolio_pagination_style','portfolio_style','portfolio_exclude','portfolio_category_order'),		
		'default' => false
		);


	$options['portfolio_grid_columns'] = array(
		'type'    => 'select',
		'section' => 'portfolio',
		'title'   => esc_html__( 'Grid Columns', 'fo' ),
		'default' => themesflat_get_opt('portfolio_grid_columns'),
		'choices'   => array(
			'one-half'     => esc_html( '2 Columns', 'fo' ),
			'one-three'     => esc_html( '3 Columns', 'fo' ),
			'one-four'     => esc_html( '4 Columns', 'fo' ),
			'one-five'     => esc_html( '5 Columns', 'fo' )
			)
		);

	$options['show_filter_portfolio'] = array(
		'type'    => 'power',
		'section' => 'portfolio',
		'title'   => esc_html__( 'Show Filter', 'fo' ),
		'default' => themesflat_get_opt('show_filter_portfolio')
		);	
	$options['portfolio_category_order'] = array(
		'type'     => 'text',
		'section'  => 'portfolio',
		'title'    => esc_html__( 'Portfolio categories order.EX:travel,aviation,business. Leave empty for auto load', 'fo' ),
		'default'  => themesflat_get_opt( 'portfolio_category_order' )
		);

	$options['portfolio_style'] = array(
		'type'    => 'select',
		'title'   => esc_html__( 'Portfolio Style', 'fo' ),
		'section' => 'portfolio',
		'default' => 'grid',
		'choices'   => array(
			'grid'           => esc_html( 'Grid', 'fo' ),
			'grid2'         =>   esc_html( 'Grid No Margin', 'fo' ) ,
			)
		);

	$options['portfolio_exclude'] = array(
		'type'     => 'text',
		'section'  => 'portfolio',
		'title'    => esc_html__( 'Not Show these portfolios by IDs EX:1,2,3', 'fo' ),
		'default'  => themesflat_get_opt( 'portfolio_exclude' )
		);


	$options['portfolio_post_perpage'] = array(
		'type'     => 'spinner',
		'section'  => 'portfolio',
		'title'    => esc_html__( 'Posts Per Page', 'fo' ),
		'default'  => themesflat_get_opt( 'portfolio_post_perpage' )
		);

	$options['portfolio_archive_pagination_style'] = array(
		'type'    => 'select',
		'title'   => esc_html__( 'Pagination Style', 'fo' ),
		'section' => 'portfolio',
		'default' => 'pager-numeric',
		'choices' => array(			
			'pager-numeric' =>  esc_html__( 'Numeric', 'fo' ),
			'loadmore' => esc_html__( 'Load More', 'fo' )
			)
		);
	

	$options['portfolio_order_by'] = array(
		'type'     => 'select',
		'section'  => 'portfolio',
		'title'    => esc_html__( 'Order By', 'fo' ),
		'default'  => 'date',
		'choices'  => array(
			'date'          => esc_html__( 'Date', 'fo' ),
			'ID'            => esc_html__( 'ID', 'fo' ),
			'author'        => esc_html__( 'Author', 'fo' ),
			'title'         => esc_html__( 'Title', 'fo' ),
			'modified'      => esc_html__( 'Modified', 'fo' ),
			'rand'          => esc_html__( 'Random', 'fo' ),
			'comment_count' => esc_html__( 'Comment count', 'fo' ),
			'menu_order'    => esc_html__( 'Menu order', 'fo' ),
			)
		);

	$options['portfolio_order_direction'] = array(
		'type'     => 'select',
		'section'  => 'portfolio',
		'title'    => esc_html__( 'Order Direction', 'fo' ),
		'default'  => 'DESC',
		'choices'  => array(
			'ASC'  => esc_html__( 'Ascending', 'fo' ),
			'DESC' => esc_html__( 'Descending', 'fo' )
			)
		);

	/**
	 * Blog Options
	 */
	$options['blog_list_heading'] = array(
		'type'        => 'heading',
		'class'       => 'no-border',
		'section'     => 'blog',
		'title'       => esc_html__( 'Blog', 'fo' ),
		'description' => esc_html__( 'All options in this section will be used to make style for blog page.', 'fo' )
		);

	$options['enable_custom_blog'] = array(
		'type'    => 'power',
		'title'   => esc_html__( 'Enable Custom Blog layout', 'fo' ),
		'section' => 'blog',
		'children'=> array('blog_grid_columns','blog_archive_post_excepts','blog_archive_post_excepts_length','blog_archive_show_post_meta','blog_archive_readmore','blog_archive_readmore_text','blog_posts_per_page','blog_order_by','blog_order_direction','blog_archive_pagination_style','blog_show_content', 'blog_archive_layout','blog_archive_exclude'),		
		'default' => false
		);

	$options['blog_grid_columns'] = array(
		'type'    => 'select',
		'section' => 'blog',
		'title'   => esc_html__( 'Grid Columns', 'fo' ),
		'default' => themesflat_customize_default_options2('blog_grid_columns'),
		'choices' => array(
			2 => esc_html__( '2 Columns', 'fo' ),
			3 => esc_html__( '3 Columns', 'fo' ),
			4 => esc_html__( '4 Columns', 'fo' )
			)
		);	

	$options['blog_archive_layout'] = array(
		'type'    => 'select',
		'title'   => esc_html__( 'Blog Layout', 'fo' ),
		'section' => 'blog',
		'default' => themesflat_customize_default_options2('blog_archive_layout'),
		'choices' => array(
			'blog-list' =>  esc_html( 'Blog List','fo' ),
            'blog-list-small'=>  esc_html( 'Blog List Small','fo' ),
            'blog-grid'=>   esc_html( 'Blog Grid','fo' ),
			)
		);

	$options['blog_archive_post_excepts_length'] = array(
		'type'    => 'text',
		'title'   => esc_html__( 'Post Excepts Length', 'fo' ),
		'section' => 'blog',
		'default' => themesflat_customize_default_options2('blog_archive_post_excepts_length')
		);	

	$options['blog_archive_readmore'] = array(
		'type'    => 'power',
		'title'   => esc_html__( 'Show Read More', 'fo' ),
		'section' => 'blog',
		'default' => true,
		'children' => array ('blog_archive_readmore_text')
		);

	$options['blog_archive_readmore_text'] = array(
		'type'    => 'text',
		'title'   => esc_html__( 'Read More Text', 'fo' ),
		'section' => 'blog',
		'default' =>themesflat_customize_default_options2('blog_archive_readmore_text')
		);

	$options['blog_archive_exclude'] = array(
		'type'    => 'text',
		'title'   => esc_html__( 'Post IDs will be inorged. Ex: 1,2,3', 'fo' ),
		'section' => 'blog',
		'default' =>themesflat_customize_default_options2('blog_archive_exclude')
		);

	$options['blog_posts_per_page'] = array(
		'type'     => 'spinner',
		'section'  => 'blog',
		'title'    => esc_html__( 'Posts Per Page', 'fo' ),
		'default'  => get_option( 'posts_per_page' )
		);

	$options['blog_archive_pagination_style'] = array(
		'type'    => 'select',
		'title'   => esc_html__( 'Pagination Style', 'fo' ),
		'section' => 'blog',
		'default' => themesflat_customize_default_options2('blog_archive_pagination_style'),
		'choices' => array(
			'pager' =>  esc_html__( 'Pager', 'fo' ),
			'numeric' =>  esc_html__( 'Numeric', 'fo' ),
			'pager-numeric' =>  esc_html__( 'Pager & Numeric', 'fo' ),
			'loadmore' =>  esc_html__( 'Load More', 'fo' )
			)
		);
	
	themesflat_prepare_options($options);
	
	return $options;
}
function themesflat_get_children($ar){
	if (isset($ar['children'])){
	 return $ar['children'];
	}
}
function themesflat_prepare_options($options) {
	$flat_data = get_option('flatopts');
	$flatopts = array();
	if(!is_array($flat_data)) $flat_data = array();
	$children = array_map('themesflat_get_children', $options);
	$children = array_filter($children);
	foreach ($children as $key => $value) {
		if (is_array($value)) {
			foreach ($value as $_key => $_value) {
				$flatopts[$_value] = $key;
			}
		}
		else {
			$flatopts[$value] = $key;
		}
	}
	$flat_data = array_merge($flat_data,$flatopts);
	update_option('flatopts',$flat_data);
}