<?php
/**
 * Radio Images control
 */
if (class_exists('WP_Customize_Control')) {

	class ColorOverlay extends WP_Customize_Control {
		public $type = 'color-picker';
		public $choices = array();
		public function render() {
			$id    = 'themesflat-options-control-' . $this->id;
			$class = 'themesflat-options-control themesflat-options-control-' . $this->type;

			if ( $this->value() )
				$this->class = 'active';

			if ( ! empty( $this->class ) )
				$class .= " {$this->class}";

			if ( empty( $this->label ) )
				$class .= ' no-label';

			?><li id="<?php themesflat_esc_attr( $id ); ?>" class="<?php themesflat_esc_attr( $class ) ?>">
				<span class="themesflat-options-control-title themesflat-title-option"> <?php echo themesflat_esc_attr ($this->label); ?></span>
				<?php $this->render_content(); ?>
			</li><?php
		}
		public function render_content() { 
			$name = "_options-box-control-$this->id"; ?>
			<label>
			<span class="customize-control-title"><?php themesflat_esc_html($this->label);?></span>
			<span class="description customize-control-description"><?php themesflat_esc_html($this->description);?></span>
                <div class="background-color">

                    <div class="themesflat-options-control-color-picker">

                        <div class="themesflat-options-control-inputs">

                            <input type="text" class='flat-color-picker wp-color-picker' <?php $this->input_attrs(); ?> id="<?php themesflat_esc_attr( $name ) ?>-color" data-alpha="true" name="<?php themesflat_esc_attr($name);?>" value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); ?>" />

                        </div>

                    </div>

                </div>
            </label>
				
			<?php
		}
	}

}