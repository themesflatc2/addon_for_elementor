<?php
/**
 * FO Theme Customizer
 *
 * @package FO
 */

function themesflat_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
    $wp_customize->get_section( 'header_image' )->title = esc_html('Backgound PageTitle', 'fo');
    $wp_customize->get_section( 'header_image' )->priority = '22';   
    $wp_customize->get_section( 'title_tagline' )->priority = '1';
    $wp_customize->get_section( 'title_tagline' )->title = esc_html('General', 'fo');
    $wp_customize->get_section( 'colors' )->title = esc_html('Layout Style', 'fo');  
  
    //Heading
    class themesflat_Info extends WP_Customize_Control {
        public $type = 'heading';
        public $label = '';
        public function render_content() {
        ?>
            <h3 class="themesflat-title-control"><?php echo esc_html( $this->label ); ?></h3>
        <?php
        }
    }    

    //Title
    class themesflat_Title_Info extends WP_Customize_Control {
        public $type = 'title';
        public $label = '';
        public function render_content() {
        ?>
            <h4><?php echo esc_html( $this->label ); ?></h4>
        <?php
        }
    }    

    //Desc
    class themesflat_Theme_Info extends WP_Customize_Control {
        public $type = 'info';
        public $label = '';
        public function render_content() {
        ?>
            <h3><?php echo esc_html( $this->label ); ?></h3>
        <?php
        }
    }    

    //Desc
    class themesflat_Desc_Info extends WP_Customize_Control {
        public $type = 'desc';
        public $label = '';
        public function render_content() {
        ?>
            <p class="themesflat-desc-control"><?php echo esc_html( $this->label ); ?></p>
        <?php
        }
    }       


    //___General___//
    $wp_customize->add_setting('themesflat_options[info]', array(
            'type'              => 'info_control',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'esc_attr',            
        )
    );

    // Heading site infomation
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-stie-infomation', array(
        'label' => esc_html('SITE INFORMATION', 'fo'),
        'section' => 'title_tagline',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );    

    // Desc site infomaton
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_siteinfomation', array(
        'label' => esc_html('This section have basic information of your site, just change it to match with you need.', 'fo'),
        'section' => 'title_tagline',
        'settings' => 'themesflat_options[info]',
        'priority' => 2
        ) )
    );  

    // Api key google map
    $wp_customize->add_setting (
        'key_api_google',
        array (
            'default'           => themesflat_customize_default_options2('key_api_google'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );

    $wp_customize->add_control (
        'key_api_google',
        array (
            'label'    => esc_html( 'Enter your key api google','fo' ),
            'section'  => 'title_tagline',
            'type'     => 'text',
            'priority' => 3

        )
    );

    // Api key google font
    $wp_customize->add_setting (
        'api_key_googlefont',
        array (
            'default'           => 'AIzaSyCOYt9j4gB6udRh420WRKttoGoN38pzI7w',
            'sanitize_callback' =>'themesflat_sanitize_text'
        )
    );

    $wp_customize->add_control(
        'api_key_googlefont',
        array(
            'label'   => esc_html('Enter your key api google font','fo'),
            'section' =>'title_tagline',
            'priority'=> 4,
            'type'    => 'text'
        )
     );




  //  =============================
    //  // Socials              //
    //  =============================
    $wp_customize->add_section(
        'themesflat_socials',
        array(
            'title'         => esc_html('Socials', 'fo'),
            'priority'      => 2,
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    ); 

      // social links
    $wp_customize->add_setting(
      'social_links',
      array(
        'sanitize_callback' => 'esc_attr',
        'default' => themesflat_customize_default_options2('social_links'),     
      )   
    );

    $wp_customize->add_control( new SocialIcons($wp_customize,
        'social_links',
        array(
            'type' => 'social-icons',
            'label' => esc_html('Social', 'fo'),
            'section' => 'themesflat_socials',
            'priority' => 1,
        ))
    );      

    //___Header___//
    $wp_customize->add_section(
        'themesflat_header',
        array(
            'title'         => esc_html('Header', 'fo'),
            'priority'      => 2,
        )
    );

    // Heading custom logo
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-logo', array(
        'label' => esc_html('Custom Logo', 'fo'),
        'section' => 'themesflat_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 2
        ) )
    );    

    // Desc custon logo
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_logo', array(
        'label' => esc_html('In this section You can upload your own custom logo, change the way your logo can be displayed', 'fo'),
        'section' => 'themesflat_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 3
        ) )
    );    

    //Logo
    $wp_customize->add_setting(
        'site_logo',
        array(
            'default' => themesflat_customize_default_options2('site_logo'),
            'sanitize_callback' => 'esc_url_raw',
        )
    );    
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'site_logo',
            array(
               'label'          => esc_html( 'Upload your logo ', 'fo' ),
               'description'    => esc_html( 'The best size is 168x50px ( If you don\'t display logo please remove it your website display 
                Site Title default in General )', 'fo' ),
               'type'           => 'image',
               'section'        => 'themesflat_header',
               'priority'       => 5,
            )
        )
    );

    // Logo Retina
    $wp_customize->add_setting(
        'site_retina_logo',
        array(
            'default'           => themesflat_customize_default_options2('site_retina_logo'),
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'site_retina_logo',
            array(
               'label'          => esc_html( 'Upload your logo retina', 'fo' ),
               'description'    => esc_html( 'The best size is 372x90px', 'fo' ),
               'type'           => 'image',
               'section'        => 'themesflat_header',
               'priority'       => 6,
            )
        )
    );

    // Logo Size
    $wp_customize->add_control( new themesflat_Title_Info( $wp_customize, 'logo-size', array(
        'label' => esc_html('Logo Size', 'fo'),
        'section' => 'themesflat_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 7
        ) )
    );  

    // Height
    $wp_customize->add_setting(
        'logo_height',
        array(
            'default' => themesflat_customize_default_options2('logo_height'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control(
        'logo_height',
        array(
            'label' => esc_html( 'Height (px)', 'fo' ),
            'section' => 'themesflat_header',
            'type' => 'text',
            'priority' => 9
        )
    );  

    // Box control
    $wp_customize->add_setting(
        'logo_controls',
        array(
            'default' => themesflat_customize_default_options2('logo_controls'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control( new BoxControls($wp_customize,
        'logo_controls',
        array(
            'label' => esc_html( 'Box Controls (px)', 'fo' ),
            'section' => 'themesflat_header',
            'type' => 'box-controls',
            'priority' => 10
        ))
    );  

    // Heading Header Style     
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'header_style', array(
        'label' => esc_html('Header Style', 'fo'),
        'section' => 'themesflat_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 12
        ) )
    );     

    // Desc
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_header', array(
        'label' => esc_html('Change the header style, toggle sticky header feature and turn on/off extra menu icons', 'fo'),
        'section' => 'themesflat_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 12
        ) )
    );     

     $wp_customize->add_setting(
        'header_style',
        array(
            'default'           => themesflat_customize_default_options2('header_style'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( new RadioImages($wp_customize,
        'header_style',
        array (
            'type'      => 'radio-images',           
            'section'   => 'themesflat_header',
            'priority'  => 13,
            'label'         => esc_html('Header Style', 'fo'),
            'choices'   => array (
                'header-style1' => array (
                    'tooltip'   => esc_html( 'Header Style 1','fo' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/header1.jpg'
                ) ,
                'header-style2'=>  array (
                    'tooltip'   => esc_html( 'Header Style 2','fo' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/header2.jpg'
                ) ,
               'header-style3'=>  array(
                    'tooltip'   => esc_html( 'Header Style 3','fo' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/header3.jpg'
                ) ,
               'header-style4'=>  array(
                    'tooltip'   => esc_html( 'Header Style 4','fo' ),
                    'src'       => THEMESFLAT_LINK . 'images/controls/header4.jpg'
                ) ,
            ),
        ))
    );

      // Read More Text
    $wp_customize->add_setting (
        'header_content',
        array(
            'default' => themesflat_customize_default_options2('header_content'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );

    $wp_customize->add_control(
        'header_content',
        array(
            'type'      => 'textarea',
            'label'     => esc_html('Header Content', 'fo'),
            'section'   => 'themesflat_header',
            'priority'  => 14
        )
    );

      // Header Sticky
    $wp_customize->add_setting(
      'header_searchbox',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default_options2('header_searchbox'),     
        )   
    );

    $wp_customize->add_control(
        'header_searchbox',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Show search', 'fo'),
            'section' => 'themesflat_header',
            'priority' => 15,
        )
    );  


    // Header Sticky
    $wp_customize->add_setting(
      'header_sticky',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default_options2('header_sticky'),     
        )   
    );

    $wp_customize->add_control(
        'header_sticky',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Enable sticky header', 'fo'),
            'section' => 'themesflat_header',
            'priority' => 15,
        )
    );  

    // On off preloader
    $wp_customize->add_setting(
      'preloader',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default_options2('preloader'),     
        )   
    );

    $wp_customize->add_control(
        'preloader',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Enable preloader', 'fo'),
            'section' => 'themesflat_header',
            'priority' => 16,
        )
    );  

    // Heading Top Bar 
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'top-bar', array(
        'label' => esc_html('Top Bar', 'fo'),
        'section' => 'themesflat_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 17
        ) )
    );    

    // Desc Top Bar 
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc', array(
        'label' => esc_html('Turn on/off the top bar and change it styles', 'fo'),
        'section' => 'themesflat_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 18
        ) )
    );  

    // Top bar
    $wp_customize->add_setting(
      'topbar_enabled',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default_options2('topbar_enabled'),     
        )   
    );

    $wp_customize->add_control(
        'topbar_enabled',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Enable Topbar', 'fo'),
            'section' => 'themesflat_header',
            'priority' => 19,
        )
    );      

        // Enable Socials Top
    $wp_customize->add_setting(
      'enable_social_link',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default_options2('enable_social_link'),     
        )   
    );

    $wp_customize->add_control(
        'enable_social_link',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Enable Socials ', 'fo'),
            'section' => 'themesflat_header',
            'priority' => 120,
        )
    );      
  

  
    // Enable Content Right Top
    $wp_customize->add_setting(
      'enable_content_right_top',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default_options2('enable_content_right_top'),     
        )   
    );

    $wp_customize->add_control(
        'enable_content_right_top',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Enable Content Right On Top', 'fo'),
            'section' => 'themesflat_header',
            'priority' => 21,
        )
    );  

      // Enable Language Switch
    $wp_customize->add_setting(
      'enable_language_switch',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default_options2('enable_language_switch'),     
        )   
    );

    $wp_customize->add_control(
        'enable_language_switch',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Enable Language Switch', 'fo'),
            'section' => 'themesflat_header',
            'priority' => 22,
        )
    );  

    // Top bar background color
    $wp_customize->add_setting(
        'top_background_color',
        array(
            'default'           => themesflat_get_opt('top_background_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'top_background_color',
            array(
                'label'         => esc_html('Topbar Backgound', 'fo'),
                'section'       => 'themesflat_header',
                'settings'      => 'top_background_color',
                'priority'      => 23
            )
        )
    );

    // Top bar text color
    $wp_customize->add_setting(
        'topbar_textcolor',
        array(
            'default'           => themesflat_get_opt('topbar_textcolor'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'topbar_textcolor',
            array(
                'label'         => esc_html('Topbar Text Color', 'fo'),
                'section'       => 'themesflat_header',
                'settings'      => 'topbar_textcolor',
                'priority'      => 24
            )
        )
    );

    // Top Content
    $wp_customize->add_setting(
        'top_content',
        array(
            'default' => themesflat_customize_default_options2('top_content'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'top_content',
        array(
            'label' => esc_html( 'Content Left', 'fo' ),
            'section' => 'themesflat_header',
            'type' => 'textarea',
            'priority' => 25
        )
    );  

    // Top Content right
    $wp_customize->add_setting(
        'top_content_right',
        array(
            'default' => themesflat_customize_default_options2('top_content_right'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'top_content_right',
        array(
            'label' => esc_html( 'Content Right', 'fo' ),
            'section' => 'themesflat_header',
            'type' => 'textarea',
            'priority' => 26
        )
    );  

   //___Footer___//
    $wp_customize->add_section(
        'flat_footer',
        array(
            'title'         => esc_html('Footer', 'fo'),
            'priority'      => 4,
        )
    );        

    $wp_customize->remove_control('display_header_text');
    $wp_customize->remove_control('header_textcolor');
  

    // Footer widget
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-widget-footer', array(
        'label' => esc_html('footer widgets', 'fo'),
        'section' => 'flat_footer',
        'settings' => 'themesflat_options[info]',
        'priority' => 9
        ) )
    );    

    // Desc
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_widget_footer', array(
        'label' => esc_html('This section allow to change the layout and styles of footer widgets to match as you need', 'fo'),
        'section' => 'flat_footer',
        'settings' => 'themesflat_options[info]',
        'priority' => 10
        ) )
    );  

     // Enable Footer
    $wp_customize->add_setting ( 
        'enable_footer',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox' ,
            'default' => themesflat_customize_default_options2('enable_footer'),     
        )
    );

    $wp_customize->add_control(
        'enable_footer',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Enable Footer', 'fo'),
            'section'   => 'flat_footer',
            'priority'  => 11
        )
    );

    // Gird columns Related Posts
    $wp_customize->add_setting(
        'footer_widget_areas',
        array(
            'default'           => themesflat_customize_default_options2('footer_widget_areas'),
            'sanitize_callback' => 'themesflat_sanitize_grid_post_related',
        )
    );

    $wp_customize->add_control(
        'footer_widget_areas',
        array(
            'type'      => 'select',           
            'section'   => 'flat_footer',
            'priority'  => 14,
            'label'     => esc_html('Columns  Footer', 'fo'),
            'choices'   => array(                
                1     => esc_html( '1 Columns', 'fo' ),
                2     => esc_html( '2 Columns', 'fo' ),
                3     => esc_html( '3 Columns', 'fo' ),
                4     => esc_html( '4 Columns', 'fo' ),                
            )
        )
    );      

    // Footer background color
    $wp_customize->add_setting(
        'footer_background_color',
        array(
            'default' => themesflat_customize_default_options2('footer_background_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'footer_background_color',
            array(
                'label'         => esc_html('Footer Backgound', 'fo'),
                'section'       => 'flat_footer',
                'settings'      => 'footer_background_color',
                'priority'      => 12
            )
        )
    );

    // Footer text color
    $wp_customize->add_setting(
        'footer_text_color',
        array(
            'default'           => themesflat_customize_default_options2('footer_text_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'footer_text_color',
            array(
                'label'         => esc_html('Footer Text Color', 'fo'),
                'section'       => 'flat_footer',
                'settings'      => 'footer_text_color',
                'priority'      => 13
            )
        )
    ); 

    // Footer title
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-footer-content', array(
        'label' => esc_html('CUSTOM FOOTER', 'fo'),
        'section' => 'flat_footer',
        'settings' => 'themesflat_options[info]',
        'priority' => 14
        ) )
    );    

    // Desc
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_footer', array(
        'label' => esc_html('You can change the copyright text, show/hide the social icons on the footer.', 'fo'),
        'section' => 'flat_footer',
        'settings' => 'themesflat_options[info]',
        'priority' => 15
        ) )
    );   

   
    // Footer Content
    $wp_customize->add_setting(
        'footer_copyright',
        array(
            'default' => themesflat_get_opt('footer_copyright'),
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control(
        'footer_copyright',
        array(
            'label' => esc_html( 'Copyright', 'fo' ),
            'section' => 'flat_footer',
            'type' => 'textarea',
            'priority' => 17
        )
    );  

    // bottom background color
    $wp_customize->add_setting(
        'bottom_background_color',
        array(
            'default'           => themesflat_customize_default_options2('bottom_background_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'bottom_background_color',
            array(
                'label'         => esc_html('Bottom Backgound', 'fo'),
                'section'       => 'flat_footer',
                'settings'      => 'bottom_background_color',
                'priority'      => 18
            )
        )
    );

    // Bottom text color
    $wp_customize->add_setting(
        'bottom_text_color',
        array(
            'default'           => themesflat_customize_default_options2('bottom_text_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'bottom_text_color',
            array(
                'label'         => esc_html('Bottom Text Color', 'fo'),
                'section'       => 'flat_footer',
                'settings'      => 'bottom_text_color',
                'priority'      => 19
            )
        )
    ); 

    //  =============================
    //  // Color              //
    //  ============================= 
    $wp_customize->add_panel('color_panel',array(
        'title'         => 'Color',
        'description'   => 'This is panel Description',
        'priority'      => 10,
    ));

    // ADD SECTION GENERAL
    $wp_customize->add_section('color_general',array(
        'title'         => 'General',
        'priority'      => 10,
        'panel'         => 'color_panel',
    ));

    // Heading Color Scheme
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'color_scheme', array(
        'label' => esc_html('SCHEME COLOR', 'fo'),
        'section' => 'color_general',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );    

    // Desc color scheme
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_color_schemer', array(
        'label' => esc_html('Select the color that will be used for theme color.','fo'),
        'section' => 'color_general',
        'settings' => 'themesflat_options[info]',
        'priority' => 2
        ) )
    );   

    $wp_customize->add_setting(
        'primary_color',
        array(
            'default'           => themesflat_customize_default_options2('primary_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'primary_color',
            array(
                'label'         => esc_html('Scheme color', 'fo'),
                'section'       => 'color_general',
                'settings'      => 'primary_color',
                'priority'      => 3
            )
        )
    );    

    // Body Color
    $wp_customize->add_setting(
        'body_text_color',
        array(
            'default'           => themesflat_customize_default_options2('body_text_color'),
            'sanitize_callback' => 'sanitize_hex_color',
            'transport'         => 'postMessage'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'body_text_color',
            array(
                'label' => esc_html('Body Color', 'fo'),
                'section' => 'color_general',
                'priority' => 4
            )
        )
    ); 

     // ADD SECTION HEADER COLOR
    $wp_customize->add_section('color_header',array(
        'title'=>'Header',
        'priority'=>11,
        'panel'=>'color_panel',
    ));

    // Title section portfolio
    $wp_customize->add_setting('themesflat_options[info]', array(
        'type'              => 'info_control',
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'esc_attr',            
        )
    );
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'topbar_color', array(
        'label' => esc_html('Top Color', 'fo'),
        'section' => 'color_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );      

    // ADD SECTION HEADER COLOR
    $wp_customize->add_section('color_header',array(
        'title'=>'Header',
        'priority'=>11,
        'panel'=>'color_panel',
    ));

    // Title section portfolio
    $wp_customize->add_setting('themesflat_options[info]', array(
        'type'              => 'info_control',
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'esc_attr',            
        )
    );
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'topbar_color', array(
        'label' => esc_html('Top Color', 'fo'),
        'section' => 'color_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );  

    // Top bar background color
    $wp_customize->add_setting(
        'top_background_color',
        array(
            'default'           => themesflat_get_opt('top_background_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'top_background_color',
            array(
                'label'         => esc_html('Topbar Backgound', 'fo'),
                'section'       => 'color_header',
                'settings'      => 'top_background_color',
                'priority'      => 2
            )
        )
    );

    // Top bar text color
    $wp_customize->add_setting(
        'topbar_textcolor',
        array(
            'default'           => themesflat_get_opt('topbar_textcolor'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'topbar_textcolor',
            array(
                'label'         => esc_html('Topbar Text Color', 'fo'),
                'section'       => 'color_header',
                'settings'      => 'topbar_textcolor',
                'priority'      => 3
            )
        )
    );

    // MENU COLOR
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'menu_color', array(
        'label' => esc_html('MENU COLOR', 'fo'),
        'section' => 'color_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 3
        ) )
    );    

    // Desc
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_menu_color', array(
        'label' => esc_html('Select color for background menu, background submenu color menu a, color menu a:hover, background menu a:hover...','fo'),
        'section' => 'color_header',
        'settings' => 'themesflat_options[info]',
        'priority' => 4
        ) )
    );   

    // Menu Background
    $wp_customize->add_setting(
        'mainnav_backgroundcolor',
        array(
            'default'   => themesflat_customize_default_options2('mainnav_backgroundcolor'),
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'mainnav_backgroundcolor',
            array(
                'label' => esc_html('Mainnav Background', 'fo'),
                'section' => 'color_header',
                'priority' => 5
            )
        )
    );   

    // Menu a color
    $wp_customize->add_setting(
        'mainnav_color',
        array(
            'default'           => themesflat_customize_default_options2('mainnav_color'),
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'mainnav_color',
            array(
                'label' => esc_html('Mainnav a color', 'fo'),
                'section' => 'color_header',
                'priority' => 6
            )
        )
    );

    // Menu a:hover color
    $wp_customize->add_setting(
        'mainnav_hover_color',
        array(
            'default'           => themesflat_customize_default_options2('mainnav_hover_color'),
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'mainnav_hover_color',
            array(
                'label' => esc_html('Mainnav a:hover color', 'fo'),
                'section' => 'color_header',
                'priority' => 7
            )
        )
    );

    // Sub menu a color
    $wp_customize->add_setting(
        'sub_nav_color',
        array(
            'default'           => themesflat_customize_default_options2('sub_nav_color'),
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'sub_nav_color',
            array(
                'label' => esc_html('Sub nav a color', 'fo'),
                'section' => 'color_header',
                'priority' => 8
            )
        )
    );

    // Sub nav background
    $wp_customize->add_setting(
        'sub_nav_background',
        array(
            'default'           => themesflat_customize_default_options2('sub_nav_background'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'sub_nav_background',
            array(
                'label' => esc_html('Sub nav background color', 'fo'),
                'section' => 'color_header',
                'priority' => 9
            )
        )
    );

    // Border color li sub nav
    $wp_customize->add_setting(
        'border_clor_sub_nav',
        array(
            'default'           => themesflat_customize_default_options2('border_clor_sub_nav'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'border_clor_sub_nav',
            array(
                'label' => esc_html('Border color sub nav', 'fo'),
                'section' => 'color_header',
                'priority' => 10
            )
        )
    );

    // Sub nav background hover
    $wp_customize->add_setting(
        'sub_nav_background_hover',
        array(
            'default'   => themesflat_customize_default_options2('sub_nav_background_hover'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'sub_nav_background_hover',
            array(
                'label' => esc_html('Sub-menu background color hover', 'fo'),
                'section' => 'color_header',
                'priority' => 11
            )
        )
    );     

    // ADD SECTION COLOR FOOTER
    $wp_customize->add_section('color_footer',array(
        'title'=>'Footer',
        'priority'=>12,
        'panel'=>'color_panel',
    ));    

     // Footer background color
    $wp_customize->add_setting(
        'footer_background_color',
        array(
            'default' => themesflat_customize_default_options2('footer_background_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'footer_background_color',
            array(
                'label'         => esc_html('Footer Backgound', 'fo'),
                'section'       => 'color_footer',
                'settings'      => 'footer_background_color',
                'priority'      => 12
            )
        )
    );

    // Footer text color
    $wp_customize->add_setting(
        'footer_text_color',
        array(
            'default'           => themesflat_customize_default_options2('footer_text_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'footer_text_color',
            array(
                'label'         => esc_html('Footer Text Color', 'fo'),
                'section'       => 'color_footer',
                'settings'      => 'footer_text_color',
                'priority'      => 13
            )
        )
    ); 

    // bottom background color
    $wp_customize->add_setting(
        'bottom_background_color',
        array(
            'default'           => themesflat_customize_default_options2('bottom_background_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'bottom_background_color',
            array(
                'label'         => esc_html('Bottom Backgound', 'fo'),
                'section'       => 'color_footer',
                'settings'      => 'bottom_background_color',
                'priority'      => 18
            )
        )
    );

    // Bottom text color
    $wp_customize->add_setting(
        'bottom_text_color',
        array(
            'default'           => themesflat_customize_default_options2('bottom_text_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'bottom_text_color',
            array(
                'label'         => esc_html('Bottom Text Color', 'fo'),
                'section'       => 'color_footer',
                'settings'      => 'bottom_text_color',
                'priority'      => 19
            )
        )
    );   

   //___Footer___//
    $wp_customize->add_section(
        'themesflat_footer',
        array(
            'title'         => esc_html('Footer', 'fo'),
            'priority'      => 4,
        )
    );        

    $wp_customize->remove_control('display_header_text');
    $wp_customize->remove_control('header_textcolor'); 

   
    // Section Blog
    $wp_customize->add_section(
        'blog_options',
        array(
            'title' => esc_html('Post', 'fo'),
            'priority' => 13,
        )
    );

    // Heading Blog
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'bloglist', array(
        'label' => esc_html('Blog List', 'fo'),
        'section' => 'blog_options',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );    

    // Desc blog
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_bloglist', array(
        'label' => esc_html('All options in this section will be used to make style for blog page.','fo'),
        'section' => 'blog_options',
        'settings' => 'themesflat_options[info]',
        'priority' => 2
        ) )
    );   

    $wp_customize->add_setting(
        'blog_layout',
        array(
            'default'           => themesflat_customize_default_options2('blog_layout'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( 
        'blog_layout',
        array (
            'type'      => 'select',           
            'section'   => 'blog_options',
            'priority'  => 3,
            'label'         => esc_html('List Sidebar Position', 'fo'),
            'choices'   => array (
                'sidebar-right' => esc_html( 'Sidebar Right','fo' ),
                'sidebar-left'=>  esc_html( 'Sidebar Left','fo' ),
                'fullwidth' =>   esc_html( 'Full Width','fo' )
                ) ,
        )
    );

    $wp_customize->add_setting(
        'blog_archive_layout',
        array(
            'default'           => themesflat_customize_default_options2('blog_archive_layout'),
            'sanitize_callback' => 'esc_attr',
        )
    );

     $wp_customize->add_control( 
        'blog_archive_layout',
        array (
            'type'      => 'select',           
            'section'   => 'blog_options',
            'priority'  => 3,
            'label'         => esc_html('Blog Layout', 'fo'),
            'choices'   => array (
                'blog-list' =>  esc_html( 'Blog List','fo' ),
                'blog-list-small'=>  esc_html( 'Blog List Small','fo' ),
                'blog-grid'=> esc_html( 'Blog Grid','fo' ),
                )  
        )
    );

        // Gird columns Related Posts
    $wp_customize->add_setting(
        'blog_grid_columns',
        array(
            'default'           => themesflat_customize_default_options2('blog_grid_columns'),
            'sanitize_callback' => 'themesflat_sanitize_grid_post_related',
        )
    );

    $wp_customize->add_control(
        'blog_grid_columns',
        array(
            'type'      => 'select',           
            'section'   => 'blog_options',
            'priority'  => 4,
            'label'     => esc_html('Post Grid Columns', 'fo'),
            'choices'   => array(                
                2     => esc_html( '2 Columns', 'fo' ),
                3     => esc_html( '3 Columns', 'fo' ),
                4     => esc_html( '4 Columns', 'fo' ),                
            )
        )
    );

    $wp_customize->add_setting (
        'blog_sidebar_list',
        array(
            'default'           => themesflat_customize_default_options2('blog_sidebar_list'),
            'sanitize_callback' => 'esc_html',
        )
    );

    $wp_customize->add_control( new DropdownSidebars($wp_customize,
        'blog_sidebar_list',
        array(
            'type'      => 'dropdown',           
            'section'   => 'blog_options',
            'priority'  => 3,
            'label'         => esc_html('List Sidebar Position', 'fo'),
            
        ))
    );

    // Excerpt
    $wp_customize->add_setting(
        'blog_archive_post_excepts_length',
        array(
            'sanitize_callback' => 'absint',
            'default'           => themesflat_customize_default_options2('blog_archive_post_excepts_length'),
        )       
    );
    $wp_customize->add_control( 'blog_archive_post_excepts_length', array(
        'type'        => 'number',
        'priority'    => 4,
        'section'     => 'blog_options',
        'label'       => esc_html('Post Excepts Length', 'fo'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 200,
            'step'  => 5
        ),
    ) );

    // Show Post Meta
    $wp_customize->add_setting ( 
        'blog_archive_show_post_meta',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox' ,
            'default' => themesflat_customize_default_options2('blog_archive_show_post_meta'),     
        )
    );

    $wp_customize->add_control(
        'blog_archive_show_post_meta',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show Post Meta', 'fo'),
            'section'   => 'blog_options',
            'priority'  => 5
        )
    );

    // Show Read More
    $wp_customize->add_setting (
        'blog_archive_readmore',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default_options2('blog_archive_readmore'),     
        )
    );

    $wp_customize->add_control( 
        'blog_archive_readmore',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show Read More', 'fo'),
            'section'   => 'blog_options',
            'priority'  => 6,
        )
    );

    // Read More Text
    $wp_customize->add_setting (
        'blog_archive_readmore_text',
        array(
            'default' => themesflat_customize_default_options2('blog_archive_readmore_text'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );

    $wp_customize->add_control(
        'blog_archive_readmore_text',
        array(
            'type'      => 'text',
            'label'     => esc_html('Read More Text', 'fo'),
            'section'   => 'blog_options',
            'priority'  => 7
        )
    );

    // Pagination
    $wp_customize->add_setting(
        'blog_archive_pagination_style',
        array(
            'default'           => themesflat_customize_default_options2('blog_archive_pagination_style'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( 
        'blog_archive_pagination_style',
        array(
            'type'      => 'select',           
            'section'   => 'blog_options',
            'priority'  => 8,
            'label'         => esc_html('Pagination Style', 'fo'),
            'choices'   => array(
                'pager'     => esc_html('Pager','fo'),
                'numeric'         =>  esc_html('Numeric','fo'),
                'pager-numeric'         =>  esc_html('Pager & Numeric','fo'),
                'loadmore' =>  esc_html__( 'Load More', 'fo' )
            ),
        )
    );

    // Header Blog Single    
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'blogsingle', array(
        'label' => esc_html('Blog Single', 'fo'),
        'section' => 'blog_options',
        'settings' => 'themesflat_options[info]',
        'priority' => 9
        ) )
    );    

    // Desc Blog Single
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_blogsingle', array(
        'label' => esc_html('Also, you can change the style for blog single to make your site unique.','fo'),
        'section' => 'blog_options',
        'settings' => 'themesflat_options[info]',
        'priority' => 10
        ) )
    );   

    // Show Post Navigator
    $wp_customize->add_setting (
        'show_post_navigator',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default_options2('show_post_navigator'),     
        )
    );

    $wp_customize->add_control( 
        'show_post_navigator',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show Post Navigator', 'fo'),
            'section'   => 'blog_options',
            'priority'  => 12
        )
    );

    //$wp_customize->remove_section('header_image');

  
    // Show Related Posts
    $wp_customize->add_setting (
        'show_related_post',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => 0,     
        )
    );

    $wp_customize->add_control( 
        'show_related_post',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show Related Posts', 'fo'),
            'section'   => 'blog_options',
            'priority'  => 15
        )
    );

    //Related Posts Style
    $wp_customize->add_setting(
        'related_post_style',
        array(
            'default'           => themesflat_customize_default_options2('related_post_style'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( 
        'related_post_style',
        array(
            'type'      => 'select',           
            'section'   => 'blog_options',
            'priority'  => 16,
            'label'         => esc_html('Related Posts Style', 'fo'),
            'choices'   => array(
                'blog-list' => esc_html( 'Blog List','fo' ),
                'blog-list-small'=> esc_html( 'Blog List Small','fo' ),
                'blog-grid'=>   esc_html( 'Blog Grid','fo' ),
        ))
    );

    // Gird columns Related Posts
    $wp_customize->add_setting(
        'grid_columns_post_related',
        array(
            'default'           => 3,
            'sanitize_callback' => 'themesflat_sanitize_grid_post_related',
        )
    );

    $wp_customize->add_control(
        'grid_columns_post_related',
        array(
            'type'      => 'select',           
            'section'   => 'blog_options',
            'priority'  => 17,
            'label'     => esc_html('Columns Of Related Posts', 'fo'),
            'choices'   => array(                
                2     => esc_html( '2 Columns', 'fo' ),
                3     => esc_html( '3 Columns', 'fo' ),
                4     => esc_html( '4 Columns', 'fo' ),                
            )
        )
    );

    // Number Of Related Posts
    $wp_customize->add_setting (
        'number_related_post',
        array(
            'default' => esc_html('3', 'fo'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );

    $wp_customize->add_control(
        'number_related_post',
        array(
            'type'      => 'text',
            'label'     => esc_html('Number Of Related Posts', 'fo'),
            'section'   => 'blog_options',
            'priority'  => 18
        )
    );

    // Section portfolio
    $wp_customize->add_section(
        'portfolio_options',
        array(
            'title' => esc_html('Portfolio', 'fo'),
            'priority' => 14,
        )
    );

    // Heading portfolio
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'portfoliolist', array(
        'label' => esc_html('Portfolio List', 'fo'),
        'section' => 'portfolio_options',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );    

    // Desc portfolio
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_portfoliolist', array(
        'label' => esc_html('Change options in this section to custom style for Portfolio listing page.','fo'),
        'section' => 'portfolio_options',
        'settings' => 'themesflat_options[info]',
        'priority' => 2
        ) )
    );   

    // Show filter portfolio
    $wp_customize->add_setting (
        'show_filter_portfolio',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default_options2('show_filter_portfolio'),     
        )
    );

    $wp_customize->add_control( 
        'show_filter_portfolio',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show filter ', 'fo'),
            'section'   => 'portfolio_options',
            'priority'  => 3
        )
    );   

    // Portfolios Style
    $wp_customize->add_setting(
        'portfolio_style',
        array(
            'default'           => 'grid',
            'sanitize_callback' => 'esc_attr',
        )
    );

    $wp_customize->add_control( 
        'portfolio_style',
        array(
            'type'      => 'select',           
            'section'   => 'portfolio_options',
            'priority'  => 4,            
            'label'         => esc_html('Portfolio Style', 'fo'),
            'choices'   => array(
                'grid'           => esc_html( 'Grid', 'fo' ),
                'grid2'         =>   esc_html( 'Grid 2', 'fo' ) ,
        ))
    );

    // Gird columns portfolio
    $wp_customize->add_setting(
        'portfolio_grid_columns',
        array(
            'default'           => themesflat_get_opt('portfolio_grid_columns'),
            'sanitize_callback' => 'esc_attr',
        )
    );

    $wp_customize->add_control(
        'portfolio_grid_columns',
        array(
            'type'      => 'select',           
            'section'   => 'portfolio_options',
            'priority'  => 5,
            'label'     => esc_html('Grid Columns', 'fo'),
            'choices'   => array(
                'one-half'     => esc_html( '2 Columns', 'fo' ),
                'one-three'     => esc_html( '3 Columns', 'fo' ),
                'one-four'     => esc_html( '4 Columns', 'fo' ),
                'one-five'     => esc_html( '5 Columns', 'fo' )
            )
        )
    );

    // Pagination portfolio
    $wp_customize->add_setting(
        'portfolio_archive_pagination_style',
        array(
            'default'           => themesflat_get_opt('portfolio_archive_pagination_style'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( 
        'portfolio_archive_pagination_style',
        array(
            'type'      => 'select',           
            'section'   => 'portfolio_options',
            'priority'  => 6,
            'label'         => esc_html('Pagination Style', 'fo'),
            'choices'   => array(
                'pager-numeric'           =>  esc_html( 'Pager & Numeric', 'fo' ),
                'loadmore'         =>   esc_html( 'Load More', 'fo' ) ,
        ))
    );

    // post per page portfolio
    $wp_customize->add_setting (
        'portfolio_post_perpage',
        array(
            'default' => esc_html('9', 'fo'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );

    $wp_customize->add_control(
        'portfolio_post_perpage',
        array(
            'type'      => 'text',
            'label'     => esc_html('Posts Per Page', 'fo'),
            'section'   => 'portfolio_options',
            'priority'  => 7
        )
    );

    // Order By portfolio
    $wp_customize->add_setting(
        'portfolio_order_by',
        array(
            'default' => 'date',
            'sanitize_callback' => 'themesflat_sanitize_portfolio_order',
        )
    );

    $wp_customize->add_control(
        'portfolio_order_by',
        array(
            'type'      => 'select',
            'label'     => esc_html('Order By', 'fo'),
            'section'   => 'portfolio_options',
            'priority'  => 8,
            'choices' => array(
                'date'          => esc_html( 'Date', 'fo' ),
                'id'            => esc_html( 'Id', 'fo' ),
                'author'        => esc_html( 'Author', 'fo' ),
                'title'         => esc_html( 'Title', 'fo' ),
                'modified'      => esc_html( 'Modified', 'fo' ),
                'comment_count' => esc_html( 'Comment Count', 'fo' ),
                'menu_order'    => esc_html( 'Menu Order', 'fo' )
            )        
        )
    ); 

    // Order Direction portfolio
    $wp_customize->add_setting(
        'portfolio_order_direction',
        array(
            'default' => 'ASC',
            'sanitize_callback' => 'themesflat_sanitize_portfolio_order_direction',
        )
    );

    $wp_customize->add_control(
        'portfolio_order_direction',
        array(
            'type'      => 'select',
            'label'     => esc_html('Order Direction', 'fo'),
            'section'   => 'portfolio_options',
            'priority'  => 9,
            'choices' => array(
                'DESC' => esc_html( 'Descending', 'fo' ),
                'ASC'  => esc_html( 'Assending', 'fo' )
            )        
        )
    ); 
    
    // Header Portfolio Single    
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'portfoliosingle', array(
        'label' => esc_html('SINGLE PORTFOLIO', 'fo'),
        'section' => 'portfolio_options',
        'settings' => 'themesflat_options[info]',
        'priority' => 10
        ) )
    );    

    // Desc Portfolio Single
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_portfoliosingle', array(
        'label' => esc_html('Also, you can change the style for blog single to make your site unique.','fo'),
        'section' => 'portfolio_options',
        'settings' => 'themesflat_options[info]',
        'priority' => 11
        ) )
    );   

    // Portfolios Single Style
    $wp_customize->add_setting(
        'portfolio_single_style',
        array(
            'default'           => themesflat_customize_default_options2('portfolio_single_style'),
            'sanitize_callback' => 'esc_attr',
        )
    );

    $wp_customize->add_control( 
        'portfolio_single_style',
        array(
            'type'      => 'select',           
            'section'   => 'portfolio_options',
            'priority'  => 12,
            'label'         => esc_html('Portfolio Single style', 'fo'),
            'choices'   => array(
                'full_content'    =>      esc_html( 'Full Content', 'fo' ),
                'right_content'         =>   esc_html( 'Right Content', 'fo' ) ,
        ))
    );   

    // Show Post Navigator portfolio
    $wp_customize->add_setting (
        'portfolio_show_post_navigator',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => 1,     
        )
    );

    $wp_customize->add_control( 
        'portfolio_show_post_navigator',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show Single Navigator', 'fo'),
            'section'   => 'portfolio_options',
            'priority'  => 12
        )
    );

    // Show Related Portfolios
    $wp_customize->add_setting (
        'show_related_portfolio',
        array (
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => 0,     
        )
    );

    $wp_customize->add_control( 
        'show_related_portfolio',
        array(
            'type'      => 'checkbox',
            'label'     => esc_html('Show Related Portfolios', 'fo'),
            'section'   => 'portfolio_options',
            'priority'  => 13
        )
    );

    // Title widget reated
    $wp_customize->add_setting (
        'title_related_portfolio',
        array(
            'default' => esc_html('Related Portfolio', 'fo'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );

    $wp_customize->add_control(
        'title_related_portfolio',
        array(
            'type'      => 'text',
            'label'     => esc_html('Portfolio panel title', 'fo'),
            'section'   => 'portfolio_options',
            'priority'  => 14
        )
    );    

    // Gird columns portfolio related
    $wp_customize->add_setting(
        'grid_columns_portfolio_related',
        array(
            'default'           => 'one-half',
            'sanitize_callback' => 'esc_attr',
        )
    );

    $wp_customize->add_control(
        'grid_columns_portfolio_related',
        array(
            'type'      => 'select',           
            'section'   => 'portfolio_options',
            'priority'  => 16,
            'label'     => esc_html('Columns Count', 'fo'),
            'choices'   => array(
                'one-half'     => esc_html( '2 Columns', 'fo' ),
                'one-three'     => esc_html( '3 Columns', 'fo' ),
                'one-four'     => esc_html( '4 Columns', 'fo' ),
                'one-five'     => esc_html( '5 Columns', 'fo' )
            )
        )
    );
    
    // Number Of Related Portfolios
    $wp_customize->add_setting (
        'number_related_portfolio',
        array(
            'default' => 4,
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );

    $wp_customize->add_control(
        'number_related_portfolio',
        array(
            'type'      => 'text',
            'label'     => esc_html('Number Of Related Portfolios', 'fo'),
            'section'   => 'portfolio_options',
            'priority'  => 17
        )
    );
    
    // Section Typography
    $wp_customize->add_section(
        'flat_typography',
        array(
            'title' => esc_html('Typography', 'fo'),
            'priority' => 6,            
        )
    );

    // Heading Typography
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-typography', array(
        'label' => esc_html('BODY FONT', 'fo'),
        'section' => 'flat_typography',
        'settings' => 'themesflat_options[info]',
        'priority' => 2
        ) )
    );    

    // Desc Typography
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_logo', array(
        'label' => esc_html('You can modify the font family, size, color, ... for global content.', 'fo'),
        'section' => 'flat_typography',
        'settings' => 'themesflat_options[info]',
        'priority' => 3
        ) )
    );

     //  =============================
    //  // Page title and breadcrumb              //
    //  ============================= 
    $wp_customize->add_panel('page_title_panel',array(
        'title'         => esc_html__('Page Title & Breadcrumb','fo'),
        'description'   => 'This is panel Description',
        'priority'      => 10,
    ));

    // ADD SECTION PAGE TITLE
    $wp_customize->add_section('page_title_style',array(
        'title'         => esc_html__('Page Title Style','fo'),
        'priority'      => 10,
        'panel'         => 'page_title_panel',
    ));

   // Heading Color Scheme
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'page_title_style', array(
        'label' => esc_html__('Page Title Style', 'fo'),
        'section' => 'page_title_style',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );   

    // Header page title
    $wp_customize->add_setting(
      'show_page_title',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default_options2('show_page_title'),     
        )   
    );

    $wp_customize->add_control(
        'show_page_title',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Show page title', 'fo'),
            'section' => 'page_title_style',
            'priority' => 15,
        )
    );  

    // Page page title heading
    $wp_customize->add_setting(
      'show_page_title_heading',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default_options2('show_page_title_heading'),     
        )   
    );

    $wp_customize->add_control(
        'show_page_title_heading',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Show page title heading', 'fo'),
            'section' => 'page_title_style',
            'priority' => 16,
        )
    );        

       // Box control
    $wp_customize->add_setting(
        'page_title_controls',
        array(
            'default' => '',
            'sanitize_callback' => 'themesflat_sanitize_text',
        )
    );
    $wp_customize->add_control( new BoxControls($wp_customize,
        'page_title_controls',
        array(
            'label' => esc_html( 'Page Title Controls', 'fo' ),
            'section' => 'page_title_style',
            'type' => 'box-controls',
            'priority' => 10
        ))
    );  

     //Page Title Background
    $wp_customize->add_setting(
        'page_title_background_image',
        array(
            'default' => themesflat_customize_default_options2('page_title_background_image'),
            'sanitize_callback' => 'esc_url_raw',
        )
    );    
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'page_title_background_image',
            array(
               'label'          => esc_html( 'Upload your Page Title Image ', 'fo' ),
               'type'           => 'image',
               'section'        => 'page_title_style',
               'priority'       => 5,
            )
        )
    );

    // Page Title Color
    $wp_customize->add_setting(
        'page_title_text_color',
        array(
            'default'           => themesflat_get_opt('page_title_text_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'page_title_text_color',
            array(
                'label'         => esc_html('Page Title Text Color', 'fo'),
                'section'       => 'page_title_style',
                'priority'      => 6
            )
        )
    );

    // Page Title Link Color
    $wp_customize->add_setting(
        'page_title_link_color',
        array(
            'default'           => themesflat_get_opt('page_title_link_color'),
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'page_title_link_color',
            array(
                'label'         => esc_html('page_title_link_color', 'fo'),
                'section'       => 'page_title_style',
                'priority'      => 7
            )
        )
    );

    // Overlay
    $wp_customize->add_setting(
        'page_title_overlay_color',
        array(
            'default'           => themesflat_get_opt('page_title_overlay_color'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( new ColorOverlay(
            $wp_customize,
            'page_title_overlay_color',
            array(
                'label'         => esc_html__('Page Title Overlay Color', 'fo'),
                'description'   => esc_html__(' Opacity =1 for Background Color'),
                'section'       => 'page_title_style',
                'priority'      => 8
            )
        )
    );

    // ADD SECTION BREADCRUMB
    $wp_customize->add_section('page_break_crumb_section',array(
        'title'         => esc_html__('Page Breadcrumb','fo'),
        'priority'      => 10,
        'panel'         => 'page_title_panel',
    ));

   // Breadcrumb section
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'page_break_crumb_section', array(
        'label' => esc_html__('Page Breadcrumb', 'fo'),
        'section' => 'page_break_crumb_section',
        'settings' => 'themesflat_options[info]',
        'priority' => 1
        ) )
    );  

     // Breadcrumb
    $wp_customize->add_setting(
      'breadcrumb_enabled',
        array(
            'sanitize_callback' => 'themesflat_sanitize_checkbox',
            'default' => themesflat_customize_default_options2('breadcrumb_enabled'),     
        )   
    );

    $wp_customize->add_control( 
        'breadcrumb_enabled',
        array(
            'type' => 'checkbox',
            'label' => esc_html('Enable Breadcrumb', 'fo'),
            'section' => 'page_break_crumb_section',
            'priority' => 14,
        )
    );    

    $wp_customize->add_setting (
        'breadcrumb_prefix',
        array(
            'default' => themesflat_customize_default_options2('breadcrumb_prefix') ,
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );

    $wp_customize->add_control(
        'breadcrumb_prefix',
        array(
            'type'      => 'text',
            'label'     => esc_html('Breadcrumb Prefix', 'fo'),
            'section'   => 'page_break_crumb_section',
            'priority'  => 15
        )
    );  

    $wp_customize->add_setting (
        'breadcrumb_separator',
        array(
            'default' => themesflat_customize_default_options2('breadcrumb_separator'),
            'sanitize_callback' => 'themesflat_sanitize_text'
        )
    );

    $wp_customize->add_control(
        'breadcrumb_separator',
        array(
            'type'      => 'text',
            'label'     => esc_html('Breadcrumb Separator', 'fo'),
            'section'   => 'page_break_crumb_section',
            'priority'  => 16
        )
    );    



      // Body fonts
    $wp_customize->add_setting(
        'body_font_name',
        array(
            'default' => themesflat_customize_default_options2('body_font_name'),
            'sanitize_callback' => 'esc_html',
        )
    );
    $wp_customize->add_control( new Typography($wp_customize,
        'body_font_name',
        array(
            'label' => esc_html( 'Font name/style/sets', 'fo' ),
            'section' => 'flat_typography',
            'type' => 'typography',
            'fields' => array('family','style','line_height','size'),
            'priority' => 4
        ))
    );

    // Headings fonts
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'custom-heading-font', array(
        'label' => esc_html('Headings fonts', 'fo'),
        'section' => 'flat_typography',
        'settings' => 'themesflat_options[info]',
        'priority' => 8
        ) )
    );    

    // Desc font
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_customizer_heading-font', array(
        'label' => esc_html('You can modify the font options for your headings. h1, h2, h3, h4, ...', 'fo'),
        'section' => 'flat_typography',
        'settings' => 'themesflat_options[info]',
        'priority' => 9
        ) )
    );   

    $wp_customize->add_setting(
        'headings_font_name',
        array(
            'default' => themesflat_customize_default_options2('headings_font_name'),
            'sanitize_callback' => 'esc_html',
        )
    );
    $wp_customize->add_control( new Typography($wp_customize,
        'headings_font_name',
        array(
            'label' => esc_html( 'Font name/style/sets', 'fo' ),
            'section' => 'flat_typography',
            'type' => 'typography',
            'fields' => array('family','style'),
            'priority' => 11
        ))
    );

    // H1 size
    $wp_customize->add_setting(
        'h1_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           => themesflat_customize_default_options2('h1_size'),
        )       
    );
    $wp_customize->add_control( 'h1_size', array(
        'type'        => 'number',
        'priority'    => 13,
        'section'     => 'flat_typography',
        'label'       => esc_html('H1 font size (px)', 'fo'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // H2 size
    $wp_customize->add_setting(
        'h2_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           =>  themesflat_customize_default_options2('h2_size'),
        )       
    );
    $wp_customize->add_control( 'h2_size', array(
        'type'        => 'number',
        'priority'    => 14,
        'section'     => 'flat_typography',
        'label'       => esc_html('H2 font size (px)', 'fo'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // H3 size
    $wp_customize->add_setting(
        'h3_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           => themesflat_customize_default_options2('h3_size'),
        )       
    );
    $wp_customize->add_control( 'h3_size', array(
        'type'        => 'number',
        'priority'    => 15,
        'section'     => 'flat_typography',
        'label'       => esc_html('H3 font size (px)', 'fo'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // H4 size
    $wp_customize->add_setting(
        'h4_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           =>  themesflat_customize_default_options2('h4_size'),
        )       
    );
    $wp_customize->add_control( 'h4_size', array(
        'type'        => 'number',
        'priority'    => 16,
        'section'     => 'flat_typography',
        'label'       => esc_html('H4 font size (px)', 'fo'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // H5 size
    $wp_customize->add_setting(
        'h5_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           =>  themesflat_customize_default_options2('h5_size'),
        )       
    );
    $wp_customize->add_control( 'h5_size', array(
        'type'        => 'number',
        'priority'    => 17,
        'section'     => 'flat_typography',
        'label'       => esc_html('H5 font size (px)', 'fo'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // H6 size
    $wp_customize->add_setting(
        'h6_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           =>  themesflat_customize_default_options2('h6_size'),
        )       
    );
    $wp_customize->add_control( 'h6_size', array(
        'type'        => 'number',
        'priority'    => 18,
        'section'     => 'flat_typography',
        'label'       => esc_html('H6 font size (px)', 'fo'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 60,
            'step'  => 1,
            'style' => 'margin-bottom: 15px; padding: 10px;',
        ),
    ) );

    // Heading Menu fonts
    $wp_customize->add_setting('themesflat_options[info]', array(
            'type'              => 'info_control',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'esc_attr',            
        )
    );
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'menu_fonts', array(
        'label' => esc_html('Menu fonts', 'fo'),
        'section' => 'flat_typography',
        'settings' => 'themesflat_options[info]',
        'priority' => 19
        ) )
    );

    $wp_customize->add_setting(
        'menu_font_name',
        array(
            'default' => themesflat_customize_default_options2('menu_font_name'),
                'sanitize_callback' => 'esc_html',
        )
    );
    $wp_customize->add_control( new Typography($wp_customize,
        'menu_font_name',
        array(
            'label' => esc_html( 'Font name/style/sets', 'fo' ),
            'section' => 'flat_typography',
            'type' => 'typography',
            'fields' => array('family','style','size','line_height'),
            'priority' => 20
        ))
    );

    $wp_customize->add_setting(
        'layout_version',
        array(
            'default'           => themesflat_customize_default_options2('layout_version'),
            'sanitize_callback' => 'esc_attr',
        )
    );
    $wp_customize->add_control( 
        'layout_version',
        array(
            'type'      => 'select',           
            'section'   => 'colors',
            'priority'  => 7,
            'label'         => esc_html('Layout version', 'fo'),
            'choices'   => array(
                'wide'           =>  esc_html('Wide','fo'),
                'boxed'         =>   esc_html('Boxed','fo'),
        ))
    );

    // Sidebars
    $wp_customize->add_control( new themesflat_Info( $wp_customize, 'layout_body', array(
        'label' => esc_html('SIDEBAR', 'fo'),
        'section' => 'colors',
        'settings' => 'themesflat_options[info]',
        'priority' => 10
        ) )
    );    

    // Desc
    $wp_customize->add_control( new themesflat_Desc_Info( $wp_customize, 'desc_color_scheme', array(
        'label' => esc_html('Select the position of sidebar that you wish to display.','fo'),
        'section' => 'colors',
        'settings' => 'themesflat_options[info]',
        'priority' => 11
        ) )
    );   

     $wp_customize->add_setting(
        'page_layout',
        array(
            'default'           => themesflat_customize_default_options2('page_layout'),
            'sanitize_callback' => 'esc_attr',
        )
    );

    $wp_customize->add_control(
        'page_layout',
        array (
            'type'      => 'select',           
            'section'   => 'colors',
            'priority'  => 12,
            'label'         => esc_html('List Sidebar Position', 'fo'),
            'choices'   => array (
                'sidebar-right' =>  esc_html( 'Sidebar Right','fo' ),
                'sidebar-left'=>   esc_html( 'Sidebar Left','fo' ),
                'fullwidth'=>   esc_html( 'Full Width','fo' ),
        ))
    );

    $wp_customize->add_setting (
        'page_sidebar_list',
        array(
            'default'           => themesflat_customize_default_options2('page_sidebar_list'),
            'sanitize_callback' => 'esc_html',
        )
    );

    $wp_customize->add_control( new DropdownSidebars($wp_customize,
        'page_sidebar_list',
        array(
            'type'      => 'dropdown',           
            'section'   => 'colors',
            'priority'  => 13,
            'label'         => esc_html('List Sidebar Position', 'fo'),            
        ))
    );

   
}
add_action( 'customize_register', 'themesflat_customize_register' );

/**
 * Sanitize
 */

// Text
function themesflat_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

// Background size
function themesflat_sanitize_bg_size( $input ) {
    $valid = array(
        'cover'     => esc_html('Cover', 'fo'),
        'contain'   => esc_html('Contain', 'fo'),
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Blog Layout
function themesflat_sanitize_blog( $input ) {
    $valid = array(
        'sidebar-right'    => esc_html( 'Sidebar right', 'fo' ),
        'sidebar-left'    => esc_html( 'Sidebar left', 'fo' ),
        'fullwidth'  => esc_html( 'Full width (no sidebar)', 'fo' )

    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// themesflat_sanitize_pagination
function themesflat_sanitize_pagination ( $input ) {
    $valid = array(
        'pager' => esc_html__('Pager', 'fo'),
        'numeric' => esc_html__('Numeric', 'fo'),
        'page_numeric' => esc_html__('Pager & Numeric', 'fo')                
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// themesflat_sanitize_pagination
function themesflat_sanitize_layout_version ( $input ) {
    $valid = array(
        'boxed' => esc_html__('Boxed', 'fo'),
        'wide' => esc_html__('Wide', 'fo')          
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// themesflat_sanitize_related_post
function themesflat_sanitize_related_post ( $input ) {
    $valid = array(
        'simple_list' => esc_html__('Simple List', 'fo'),
        'grid' => esc_html__('Grid', 'fo')        
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Footer widget areas
function themesflat_sanitize_fw( $input ) {
    $valid = array(
        '0' => esc_html__('footer_default', 'fo'),
        '1' => esc_html__('One', 'fo'),
        '2' => esc_html__('Two', 'fo'),
        '3' => esc_html__('Three', 'fo'),
        '4' => esc_html__('Four', 'fo')
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Header style sanitize
function themesflat_sanitize_headerstyle( $input ) {
    $valid = themesflat_predefined_header_styles();
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Checkboxes
function themesflat_sanitize_checkbox( $input ) {
    if ( $input == 1 ) {
        return 1;
    } else {
        return '';
    }
}

// Themesflat_sanitize_related_portfolio
function themesflat_sanitize_related_portfolio( $input ) {
    $valid = array(
        'grid'                 => esc_html( 'Grid', 'fo' ),
        'grid_masonry'         => esc_html( 'Grid Masonry', 'fo' ),
        'grid_nomargin'        => esc_html( 'Grid Masonry No Margin', 'fo' ),
        'carosuel'             => esc_html( 'Carosuel', 'fo' ),
        'carosuel_nomargin'    => esc_html( 'Carosuel No Margin', 'fo' )       
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Themesflat_sanitize_portfolio_pagination
function themesflat_sanitize_portfolio_pagination( $input ) {
    $valid = array(
        'page_numeric'         => esc_html( 'Pager & Numeric', 'fo' ),
        'load_more'         => esc_html( 'Load More', 'fo' )     
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Themesflat_sanitize_portfolio_order
function themesflat_sanitize_portfolio_order( $input ) {
    $valid = array(
        'date'          => esc_html( 'Date', 'fo' ),
        'id'            => esc_html( 'Id', 'fo' ),
        'author'        => esc_html( 'Author', 'fo' ),
        'title'         => esc_html( 'Title', 'fo' ),
        'modified'      => esc_html( 'Modified', 'fo' ),
        'comment_count' => esc_html( 'Comment Count', 'fo' ),
        'menu_order'    => esc_html( 'Menu Order', 'fo' )     
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Themesflat_sanitize_portfolio_order_direction
function themesflat_sanitize_portfolio_order_direction( $input ) {
    $valid = array(
        'DESC' => esc_html( 'Descending', 'fo' ),
        'ASC'  => esc_html( 'Assending', 'fo' )       
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Themesflat_sanitize_grid_portfolio
function themesflat_sanitize_grid_portfolio( $input ) {
    $valid = array(
        'portfolio-two-columns'     => esc_html( '2 Columns', 'fo' ),
        'portfolio-three-columns'     => esc_html( '3 Columns', 'fo' ),
        'portfolio-four-columns'     => esc_html( '4 Columns', 'fo' ),
        'portfolio-five-columns'     => esc_html( '5 Columns', 'fo' )
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// themesflat_sanitize_grid_portfolio_related
function themesflat_sanitize_grid_portfolio_related( $input ) {
    $valid = array(
        'portfolio-one-columns'     => esc_html( '1 Columns', 'fo' ),
        'portfolio-two-columns'     => esc_html( '2 Columns', 'fo' ),
        'portfolio-three-columns'     => esc_html( '3 Columns', 'fo' ),
        'portfolio-four-columns'     => esc_html( '4 Columns', 'fo' ),
        'portfolio-five-columns'     => esc_html( '5 Columns', 'fo' )
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// Themesflat_sanitize_grid_post_related
function themesflat_sanitize_grid_post_related( $input ) {
    $valid = array(        
        2     => esc_html( '2 Columns', 'fo' ),
        3   => esc_html( '3 Columns', 'fo' ),
        4    => esc_html( '4 Columns', 'fo' ),        
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

// themesflat_sanitize_layout_product
function themesflat_sanitize_layout_product( $input ) {
    $valid = array(        
        'fullwidth'         => esc_html( 'No Sidebar', 'fo' ),
        'sidebar-right'           => esc_html( 'Sidebar Right', 'fo' ),
        'sidebar-left'         => esc_html( 'Sidebar Left', 'fo' )
    );
    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

function themesflat_load_customizer_style() {   
    wp_register_style( 'themesflat_customizer_css', THEMESFLAT_LINK .'css/admin/customizer.css', false, '1.0.0' );
    wp_register_style( 'font_awesome', THEMESFLAT_LINK .'css/font-awesome.css', false, '1.0.0' );   
    wp_enqueue_style( 'themesflat_customizer_css' );
    wp_enqueue_style( 'font_awesome' );  
    wp_enqueue_script('jquery-ui');
    wp_enqueue_script( 'themesflat-color-alpha', THEMESFLAT_LINK . 'js/wp-color-picker-alpha.js', array('wp-color-picker'),'1.1',true);
    wp_enqueue_script( 
          'themesflat_choosen',            //Give the script an ID
          THEMESFLAT_LINK .'js/admin/3rd/chosen.jquery.min.js',//Point to file
          array( 'jquery'),    //Define dependencies
          '',                       //Define a version (optional) 
          true                      //Put script in footer?
    );
    wp_enqueue_script( 
          'themesflat_customizer_js',            //Give the script an ID
          THEMESFLAT_LINK .'js/admin/customizer.js',//Point to file
          array( 'jquery','customize-preview' ),    //Define dependencies
          '',                       //Define a version (optional) 
          true                      //Put script in footer?
    );

    wp_enqueue_style( 'themesflat_choosen', THEMESFLAT_LINK . 'css/chosen.css', array(), true ); 
}

add_action( 'admin_enqueue_scripts', 'themesflat_load_customizer_style' );