<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package themesflat
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="themesflat-boxed">
	<?php  if ( themesflat_get_opt('preloader') == 1 ) : ?>
	<!-- Preloader -->
	<div class="preloader">
		<div class="clear-loading loading-effect-2">
			<span></span>
		</div>
	</div>
	<?php endif; ?>

	<?php 
	global $themesflat_mainID;
	$themesflat_mainID =  get_the_ID();
	$logo_position = themesflat_decode(themesflat_choose_opt('logo_controls'));
	themesflat_render_box_position(".logo",$logo_position);
	$style = "padding-left: {$logo_position['padding-left']}px";
	themesflat_render_style('.header-style5 .wrap-header-content, .header-style4 .wrap-header-content',$style);
	?>

	<?php 
		get_template_part( 'tpl/topbar');
		get_template_part( 'tpl/site-header');
	?>
	<!-- Page Title -->
	<?php get_template_part( 'tpl/page-title'); ?>
	
	<div id="content" class="page-wrap <?php echo esc_attr( themesflat_blog_layout() ); ?>">
		<div class="container content-wrapper">
			<div class="row">