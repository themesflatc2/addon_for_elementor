<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package themesflat
 */
?>

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- #content -->

    <!-- Footer -->
    <div class="footer_background">
        <?php 
        global $themesflat_mainID;
        if ( themesflat_get_opt( 'enable_footer' ) == 1 ): ?>
        <footer class="footer <?php (themesflat_meta( 'footer_class' ) != "" ? esc_attr( themesflat_meta( 'footer_class' ) ):'') ;?>">      
            <div class="container">
                <div class="row"> 
                 <div class="footer-widgets">
                    <?php
                    
                    $footer_controls = themesflat_decode(themesflat_choose_opt('footer_controls',$themesflat_mainID));
                    themesflat_render_box_position(".footer",$footer_controls);

                    $columns = themesflat_widget_layout(themesflat_choose_opt('footer_widget_areas',$themesflat_mainID));
                    foreach ($columns as $key => $column) {?>
                        <div class="col-md-<?php themesflat_esc_attr($column);?> col-sm-6">
                            <?php 
                                $key = $key +1;
                                $widget = themesflat_choose_opt("footer".$key,$themesflat_mainID);
                                themesflat_dynamic_sidebar($widget);
                            ?>
                        </div>
                    <?php }?>
                   
                    </div><!-- /.footer-widgets -->           
                </div><!-- /.row -->    
            </div><!-- /.container -->   
        </footer>
        <?php endif; ?>

        <!-- Bottom -->
        <div class="bottom ">
            <div class="container">           
                <div class="row">
                    <div class="col-md-12">
                        <?php if ( themesflat_choose_opt('enable_footer_social',$themesflat_mainID) == 1 ):
                                themesflat_render_social();    
                            endif;    
                        ?>    
                        <div class="copyright">                        
                            <?php echo wp_kses_post(themesflat_choose_opt( 'footer_copyright',$themesflat_mainID)); ?>
                        </div>

                        <?php if ( themesflat_choose_opt( 'go_top') == 1 ) : ?>
                            <!-- Go Top -->
                            <a class="go-top show">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        <?php endif; ?>   

                            
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>    
    </div> <!-- Footer Background Image -->
</div><!-- /#boxed -->
<?php wp_footer(); ?>
</body>
</html>